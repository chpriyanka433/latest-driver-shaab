import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  PermissionsAndroid,
  AsyncStorage,
  StatusBar,
  Modal,                                                                  
  Text,
  TouchableOpacity,                                                                  
  ToastAndroid,
  NativeModules, 
  NativeEventEmitter,
  Platform
} from "react-native";

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,CardView,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase,createStackNavigator,image,Geolocation,NetInfo} from "././screen/common";

import MobileAuthentication from "./screen/MobileAuthentication";
import OTPAuthentication from "./screen/OTPAuthentication";
import DriverRegistration from "./screen/DriverRegistration";
import TermsService from  "./screen/TermsService";
import DriverNotVerifiedScreen from "./screen/DriverNotVerifiedScreen";
import Checkin from "./screen/CheckIn";
import test from "./screen/Test"
import StartTrip from "./screen/StartTrip";
import GpsScreen from "./screen/GpsScreen";
import InternetScreen from "./screen/InternetCheck";
import AppTour from "./screen/AppTour";
import QuestionReview from "./screen/Question/QuestionReview"
import ResultPass from './screen/Question/ResultPass';
import ResultFail from './screen/Question/ResultFail';
import QuestionScreen from './screen/Question/QuestionScreen';
import Questiontype from './screen/Question/Questiontype';
import AddFuel from "./screen/AddFuel";
import MyRideList from './screen/MyRideList';
import ActiveBooking from './screen/ActiveBooking';
import BackgroundTimer from 'react-native-background-timer';
import OneSignal from 'react-native-onesignal';
import VIForegroundService from '@voximplant/react-native-foreground-service';
import AddmoreCarTripEnd from './screen/AddmoreCarTripEnd';
import AddmoreCarStartRide from './screen/AddmoreCarStartRide';






/**
 * Google Maps Api Key Constant
 */
const googleMaps_apiKey = "AIzaSyCcSBfkyEnp5QMLiMY__OKOxjzeIAiTVNk";

export class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      internetConnectionStatus: null,
      openServiceNotAvailableModal: false
    };
  OneSignal.init("2eed6a53-d25c-49a7-a37c-9fac1bc4ec04");
  OneSignal.inFocusDisplaying(2); 
   OneSignal.addEventListener('received', this.onReceived);
   OneSignal.addEventListener('opened', this.onOpened);
   OneSignal.addEventListener('ids', this.onIds);
    this.getallpermissions();
    //console.disableYellowBox = true;
  }


  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  async getallpermissions(){
    try {
      if (Platform.OS === "android") {
        const userResponse = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        ]);
        return userResponse;
      } else {
        console.log("checked permission")
      } 
    } catch (err) {
      console.log(err);
    }
    return null;
  }

  async checkpermission(){
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        {
          title: 'DriverShaab App need SMS Permission',
          message: 'DriverShaab App needs access to send SMS',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         this.checkLocationPermission();
      } else {
        console.log('SMS Permission Denied');
      }
    } else {
      console.log('Next Permission');
    }
  }

  async checkLocationPermission(){
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'DriverShaab App need Location Permission',
          message: 'DriverShaab App needs to access location',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Permission granted');
      } else {
        console.log('Location Permission Denied');
      }
    } else {
      console.log("checked permission");
    }
  }

  async updateLocation(){
    longArray=[]; 
    timeStampArray=[];
    let rideStatus = await AsyncStorage.getItem("initiateFlag");
    if(rideStatus=="flaged"){
      BackgroundTimer.runBackgroundTimer(() => { 
       Geolocation.getCurrentPosition(
           (position) => {
              const currentLongitude = JSON.stringify(position.coords.longitude);
              const currentLatitude = JSON.stringify(position.coords.latitude);
              const timeStamp = JSON.stringify(position.timestamp);
              longArray.push(currentLongitude.concat(",").concat(currentLatitude))
              timeStampArray.push(timeStamp)
              //console.log("longArray",longArray);
              //console.log("currentLongitude",currentLongitude);
              //console.log("currentLatitude",currentLatitude)
              //console.log("timestamp",timeStamp)
             // console.log("concat ==", currentLongitude.concat(",").concat(currentLatitude))
              var resultLatlng = {};
                for (var i = 0; i < longArray.length; ++i){
                  resultLatlng["position" + (i+1)] = longArray[i];                                                                                                                          
              }
              var resultTimestamp = {};
                for (var i = 0; i < timeStampArray.length; ++i){
                  resultTimestamp["position" + (i+1)] = timeStampArray[i];                                                
              }
              //console.log("resultLatlng==",resultLatlng)
              const finalLatLong = Object.keys(resultLatlng).map(function(k){return resultLatlng[k]}).join(";");
              const finaltimestamp = Object.keys(resultTimestamp).map(function(k){return parseInt(resultTimestamp[k]/1000)}).join(";");
              AsyncStorage.setItem("finalLatLong",JSON.stringify(finalLatLong))
              AsyncStorage.setItem("finaltimestamp",JSON.stringify(finaltimestamp))
              // console.log("finaltimestamp==",finaltimestamp)
              // console.log("finalLatLong==",finalLatLong)
              this.sendLocation(finalLatLong,finaltimestamp)
           },
           (error) => console.log("error.message",error.message),
           //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
      }, 20000);
    } else {
      BackgroundTimer.stopBackgroundTimer();
    }
  }

  async sendLocation(finalLatLong,finaltimestamp){
    const driverId = await AsyncStorage.getItem('driverId');
    const ride_history_id = await AsyncStorage.getItem("ride_history_id");
    try {
      var formData = new FormData();
      formData.append("action", "update_live_location");
      formData.append("driver_id",driverId);
      formData.append("driver_booking_id", "");
      formData.append("b2b_ride_history_id",ride_history_id);
      formData.append("latlong_coordinate",finalLatLong);
      formData.append("timestamp", finaltimestamp);
      //console.log("formdata==",formData)
      const response = await fetch(BaseUrl.url5, {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data"
        }
      });
      const responseJson = await response.json();
      console.log('driverdataride===',responseJson)  
      if(responseJson.success===true){
       //console.log('responseJson.success',responseJson)  
       } else {
         //console.log('responseJson.false',responseJson)  
      }  
    } catch (error) {
      console.log("error rsp==",error);
    }
  }

  componentWillMount() {
    this._internetConnectionCheck();
  }

  _internetConnectionCheck() {
    NetInfo.fetch().then(connection => {
      if(connection.isConnected){
        this.setState({ internetConnectionStatus: true });
      } else {
        this.setState({ internetConnectionStatus: false });
        this.props.navigation.navigate("InternetScreen")
      }
    });
  }

  async componentDidMount() {
    this._locationCheckAndRedirect();
    // const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
    // if (notificationOpen) {
    // }
  }

  _locationCheckAndRedirect() {
    let myInterval = setInterval(() => {
      try {
        AsyncStorage.getItem("isLoggedIn").then(result => {
        if (this.state.internetConnectionStatus) {
            if (result === "true") {
              console.log('result===',true)
              this._checkPermissionAndRedirect();
            } else {
              console.log('result===',false)
              this.props.navigation.replace("mobile_auth");
            }
          }
        });
      } catch (error) {
        console.log(error);
      } finally {
        clearInterval(myInterval);
      }
    }, 2000);
    //StatusBar.setHidden(true);
    // this.loadingDots.play();
  }

  // _retryButtonClick() {
  //   this._internetConnectionCheck();
    
  // }

  async _checkPermissionAndRedirect() {
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (!granted) {
        this.props.navigation.navigate("GpsScreen");
      } else {
        this.checkGpsStatus();
       }
    }
  }

  checkGpsStatus(){
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
    .then(data => {
      if(data){
        this._fetchCurrentLocationToRedirect().then(()=>{
          return this.updateLocation();
        })
      } else {
        this.props.navigation.navigate("GpsScreen");
      }
    }).catch(err => {
      this.props.navigation.navigate("GpsScreen");
    });
  }

  checkLocation(){
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
  .then(data => {
    if(data){
      this._fetchCurrentLocationToRedirect().then(()=>{
        return this.updateLocation();
      })
    } else {
      ToastAndroid.show('Please Turn ON your Location.',ToastAndroid.LONG)
    }
  }).catch(err => {
    this.props.navigation.navigate("GpsScreen");
    
    // alert(err)
    //ToastAndroid.show(err.message,ToastAndroid.LONG)
  });
  }

  async _fetchCurrentLocationToRedirect() {
    const channelConfig = {
      id: "channelId",
      name: 'DriverShaab B2B',
      description: 'Channel description',
      enableVibration: false
  };
  VIForegroundService.createNotificationChannel(channelConfig);
    const notificationConfig = {
      channelId: 'channelId',
      id: 3456,
      title: 'DriverShaab B2B',
      text: 'App is running in the background',
      icon: image.driver
  };
  try {
      await VIForegroundService.startService(notificationConfig);
  } catch (e) {
      console.error("errrr==",e);
  }
    await Geolocation.getCurrentPosition(
      position => {
        this._geoCodeToAddress(
          position.coords.latitude,
          position.coords.longitude
        );
      },
      error => {
        // alert(error)
        //ToastAndroid.show(error.message, ToastAndroid.SHORT);
      },
      // { enableHighAccuracy: false,
      //   timeout: 5000,
      //   maximumAge: 10000 }
    );
  }


  _geoCodeToAddress(latitude, longitude) {
    this.props.navigation.replace("Checkin", {
      latitude: latitude,
      longitude: longitude
    });
  }

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={styles.containerView}>
        {/* <StatusBar backgroundColor="#19376c" barStyle="light-content" /> */}
          <Image
            resizeMode="cover"
            style={{ width: "100%", height:"100%" }}
            source={image.splash}
          />
        {/* <View style={{ marginTop: 10 }}>
          <Animation
            ref={loadingDots => {
              this.loadingDots = loadingDots;
            }}
            style={{ width: 250, height: 120 }}
            loop={true}
            source="loading_dots.json"
          />
        </View> */}

        {/* <View style={{ position: "absolute", bottom: 0 }} /> */}

        {/* <Modal
          animationType="fade"
          transparent={true}
          style={{ backgroundColor: "#000" }}
          visible={this.state.openServiceNotAvailableModal}
          onRequestClose={() => console.log("dkf")}
        >
          <View style={styles.parentView}>
            <View style={styles.childViewForModal}>
              <Text style={{ fontSize: 20, color: "#000" }}>Alert !</Text>
              <View style={styles.emptyView} />

              <Text style={styles.serviceNotAvailableText}>
                We donot provide our services in your Location right now.
              </Text>

              <Text style={styles.thanksText}>Thankyou for Visiting.</Text>

              <View>
                <TouchableOpacity
                  onPress={() => BackHandler.exitApp()}
                  style={styles.buttonContainer}
                >
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ justifyContent: "center" }}>
                      <Image
                        source={require("./src/ic_icon_close.png")}
                        style={styles.closeModalIcon}
                      />
                    </View>

                    <View style={{ justifyContent: "center" }}>
                      <Text>Close</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal> */}

        {/* {!this.state.internetConnectionStatus && (
          <View
            style={{
              bottom: 0,
              height: 50,
              alignItems: "center",
              position: "absolute",
              flexDirection: "row",
              width: "100%",
              backgroundColor: "#841584"
            }}
          >
            <Text style={{ marginLeft: 8, color: "#fff", fontSize: 16 }}>
              No internet connection !
            </Text>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                marginLeft: "auto",
                marginRight: 8
              }}
              onPress={() => this._retryButtonClick()}
            >
              <Text
                style={{
                  borderColor: "#fff",
                  borderRadius: 2,
                  borderWidth: 2,
                  padding: 10,
                  fontSize: 16,
                  color: "#fff"
                }}
              >
                Retry
              </Text>
            </TouchableOpacity>
          </View>
        )} */}
      </View>
    );
  }
}

const RootStack = createStackNavigator(
  {
    Splash: SplashScreen,
    mobile_auth: MobileAuthentication,
    otp_auth: OTPAuthentication,
    driver_reg: DriverRegistration,
    drivernotverified_screen: DriverNotVerifiedScreen,
    TermsService:TermsService,
    Checkin: Checkin,
    test:test,
    StartTrip:StartTrip,
    GpsScreen:GpsScreen,
    InternetScreen:InternetScreen,
    QuestionReview:QuestionReview,
    ResultPass:ResultPass,
    QuestionScreen:QuestionScreen,
    Questiontype:Questiontype,
    ResultFail:ResultFail,
    AppTour:AppTour,
    AddFuel:AddFuel,
    MyRideList:MyRideList,
    ActiveBooking:ActiveBooking,
    AddmoreCarTripEnd:AddmoreCarTripEnd,
    AddmoreCarStartRide:AddmoreCarStartRide
  },
  {
    headerMode: "none",
    initialRouteName: "Splash"
    //initialRouteName: "RatingScreen"
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}

const styles = StyleSheet.create({
  containerView: {
      flex: 1,
      paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
  },
  textStyle: {
    color: "#003366",
    fontSize: 24,
    marginTop: 5
  },
  quote: {
    fontSize: 16,
    fontFamily: "casual",
    bottom: 80,
    position: "absolute"
  },
  quote2: {
    fontSize: 16,
    fontFamily: "casual",
    bottom: 40,
    position: "absolute",
    alignItems: "center"
  }
});
