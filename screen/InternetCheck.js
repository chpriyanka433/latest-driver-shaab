import React, {Component} from 'react';
//Import React
import { SafeAreaView, View, Text, ToastAndroid,PermissionsAndroid,Image,Dimensions, AsyncStorage } from 'react-native';

import {appColor,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,image,Geolocation,Fonts,NetInfo} from "././common";

const googleMaps_apiKey = "AIzaSyCcSBfkyEnp5QMLiMY__OKOxjzeIAiTVNk";
export default class App extends Component {
    _internetConnectionCheck() {
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){  
            this.setState({ internetConnectionStatus: true });
            this._checkPermissionAndRedirect()
        } else {
          this.setState({ internetConnectionStatus: false });
          this.props.navigation.navigate("InternetScreen")
        }
        });
      }

      _checkPermissionAndRedirect() {
        if (
          PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
          )
        ) {
          this.checkLocation();
        } else {
          try {
            const granted = PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
              {
                title: "Cool Location App required Location permission",
                message:
                  "We required Location permission in order to get device location " +
                  "Please grant us."
              }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              this.checkLocation();
            } else {
              Alert.alert("You don't have access for the location");
            }
          } catch (err) {
          //  alert(err);
          }
        }
      }
    
      checkLocation(){
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
      .then(data => {
        if(data){
          this._fetchCurrentLocationToRedirect()
        } else {
          ToastAndroid.show('Please Turn ON your Location.',ToastAndroid.LONG)
        }
      }).catch(err => {
        this.props.navigation.navigate("GpsScreen");
        
        // alert(err)
        //ToastAndroid.show(err.message,ToastAndroid.LONG)
      });
      }

      async _fetchCurrentLocationToRedirect() {
        await Geolocation.getCurrentPosition(
          position => {
            this._geoCodeToAddress(
              position.coords.latitude,
              position.coords.longitude
            );
          },
          error => {
            // alert(error)
            //ToastAndroid.show(error.message, ToastAndroid.SHORT);
          },
          // { enableHighAccuracy: false,
          //   timeout: 5000,
          //   maximumAge: 10000 }
        );
      }
    
      /**
       * Function to convert Geo-code(Latitude, Longitude) to Human readable address
       */
      async _geoCodeToAddress(latitude, longitude) {
        let checkLoginStatus = await AsyncStorage.getItem("status");
        try {
          AsyncStorage.getItem("isLoggedIn").then(result => {
              if (result === "true") {
                  fetch(
                  "https://maps.googleapis.com/maps/api/geocode/json?address=" +
                    latitude +
                    ", " +
                    longitude +
                    "&key=" +
                    googleMaps_apiKey 
                )
                  .then(response => response.json())
                  .then(responseJson => { 
                    console.log("ssss",responseJson)
                    this.props.navigation.replace("Checkin", {
                      latitude: latitude,
                      longitude: longitude
                    });
                  });
              } else {
                this.props.navigation.replace("mobile_auth");
              }
          });
        } catch (error) {
          console.log(error);
        }
      }

      
  render() {
    return (
      <SafeAreaView>
        <View style={{ padding: 20 }}>
          
        <Image
            source={image.internet}
            resizeMode={"contain"}
            style={{ width:"100%", height:200,marginTop:50}}
          />
         <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:30,marginBottom:10,marginRight:10,marginTop:30}}>Ooops !</Text>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.lightBlack,textAlign
          :"center",fontSize:16,marginBottom:5,marginRight:10}}>There is no Internet Connection, Please check your internet setting.</Text>
          <View style={{justifyContent:'center',marginBottom:20,alignItems:'center', marginTop:Dimensions.get("window").height/5}}>
            <AppButton
            text='Try Again'
            bgColor={appColor.Blue}
            width={'100%'}
            textColor={appColor.white} 
            onClickFunc={()=>this._internetConnectionCheck()}     
              
            />
        </View>
        </View>
      </SafeAreaView>
    );
  }
}