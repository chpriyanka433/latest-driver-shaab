import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right, Button, ActionSheet, Item } from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  PermissionsAndroid,
  AsyncStorage,
  ToastAndroid,
  BackHandler,
  RefreshControl,
  Dimensions,
  Platform,
  Modal,
  Vibration
} from 'react-native';
// import SearchHeader from "../common/SearchHeader";

import {appColor,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,moment,image, Geolocation,Fonts,Header,RestApiKey,ClientId,ClientSecret,grantType,RNAndroidLocationEnabler,Sound,OneSignal,MapsSdkkey,NetInfo} from "././common";

import { ScrollView } from 'react-native-gesture-handler';
import SideMenu from "./SideMenu/SideMenu";
import TripHistory from "./TripHistory";
import CheckOut from './CheckOut';
import ViewProfileScreen from "./ViewProfileScreen";
import StartTrip from "./StartTrip";
import EndTrip from "./EndTrip";
import Initiate from "./Initiate";
import AttendanceHistory from "./AttendanceHistory";
import AddFuel from "./AddFuel";
import CreateToken from "./CreateToken";
import BackgroundTimer from 'react-native-background-timer';
import MapmyIndiaGL from 'mapmyindia-map-react-native-beta';
import Mapmyindia from 'mapmyindia-restapi-react-native-beta';
import MyRideList from './MyRideList';
import ActiveBooking from './ActiveBooking';
import bbox from "@turf/bbox";
import OTPAuthenticationCheckOut from './OTPAuthenticationCheckOut';
import MobileAuthenticationCheckOut from './MobileAuthenticationCheckOut'
import AddmoreCarTripEnd from './AddmoreCarTripEnd'


var BUTTONS = ["Camera", "Cancel"];
var CANCEL_INDEX = 1;
const DURATION = 9000;

const layerStyles = {
  route: {
    lineColor: 'blue',
    lineCap: MapmyIndiaGL.LineJoin.Round,
    lineWidth: 3,
    lineOpacity: 0.84,
  },
};

class Checkin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      path: "",
      images: "",
      imagePath: "",
      latitude: "",
      longitude: "",
      checkedin_id: "",
      userName: "",
      geofencingData: [],
      loading: true,
      dataSource: [],
      mTotalKilometers: "",
      checkStatus: "",
      isLoading: false,
      checkInflag: false,
      test:"",
      poc_id:"",
      request_id:"",
      source:"",
      destination:"",
      company_name:"",
      company_id:"",
      poc_name:"",
      trip_type:"",
      schedule_time:"",
      booking_status:"",
      sourcelonglat:"77.231409,28.6162",
      destinationlonglat:"77.186982,28.554676",
      route: {
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: [],
        },
      },
      rideModalPopup: false,
      rideModalPopup2: false,
      sourceLng:"",
      sourceLat:"",
      destinationLng:"",
      destinationLat:"",
      tour_type:"",
      vehicle_type:"",
      request_type:"",
      vehicle_number:"",
      poc_number:"",
      numbers_of_drivers:"",
      notification_type:"",
      sourcecity:"",
      isVisible: false
    }
    
    this.checkInflag=false
    OneSignal.init("2eed6a53-d25c-49a7-a37c-9fac1bc4ec04");
    OneSignal.inFocusDisplaying(2); 
    OneSignal.addEventListener('received', this.onReceived.bind(this));
    OneSignal.addEventListener('opened', this.onOpened.bind(this));
    OneSignal.addEventListener('ids', this.onIds.bind(this));
    MapmyIndiaGL.setMapSDKKey(MapsSdkkey);
    MapmyIndiaGL.setRestAPIKey(RestApiKey);
    MapmyIndiaGL.setAtlasClientId(ClientId);
    MapmyIndiaGL.setAtlasClientSecret(ClientSecret);
    MapmyIndiaGL.setAtlasGrantType(grantType);
    Mapmyindia.setRestApiKey(RestApiKey);
    Mapmyindia.setClientId(ClientId);
    Mapmyindia.setClientSecret(ClientSecret);
  }
  intervalID;
//  async componentWillMount() {   _acceptBooking
//     await this._fetchCurrentLocationToRedirect();

//   }

setModalVisible = (visible) => {
  this.setState({ modalVisible: visible });
}

onReceived(notification) {
  console.log("Notification received: ", notification);
  console.log('additionalData nnnnnn: ', notification.payload.additionalData);
  if(notification.payload.additionalData.notification_type==="booking_request"||notification.payload.additionalData.notification_type==="assigned_driver_info"){
    Vibration.vibrate(DURATION);
   this.mySound = new Sound('bang2.mp3', Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
  console.log('duration in seconds: ' + this.mySound.getDuration() + 'number of channels: ' + this.mySound.getNumberOfChannels());
  this.mySound.play((success) => {
    if (success) {
      console.log('successfully finished playing');
    } else {
      console.log('playback failed due to audio decoding errors');
    }
  });
});
}
}

onOpened(openResult) {
  // alert('open notification');
  console.log('notification opennnnnnnnnnnn')
  console.log('Message: ', openResult.notification.payload.body);
  console.log('Data: ', openResult.notification.payload.additionalData);
  console.log('isActive: ', openResult.notification.isAppInFocus);
  console.log('openResult: ', openResult);
  console.log('notification type')
  console.log('request type',openResult.notification.payload.additionalData.request_type)
  let check = openResult.notification.payload.additionalData;
  console.log("check dataaaa==",check)
  if(check.notification_type=="assigned_driver_info"){
    this.props.navigation.navigate("MyRideList");
    
  }
  else if(check.notification_type=="ride_notify"){
    this.setState({rideModalPopup: false})
    this.setState({rideModalPopup2: true})
    this.displayModal(true);
    console.log('ride_notify----------------------------------')
    this.props.navigation.navigate("MyRideList")
    //Alert.alert(check.message)
    this.setState({
      rideNotifyMessage:check.message
    })
    console.log(rideNotifyMessage);
    // Alert.alert(
    //   'Reminder Message',
     
    //   [
    //     {text: (check.message), },
      
    //   ],
    //   { cancelable: false }
    // );
  }
  
  else {
    this.setState({rideModalPopup: true})
    this.setState({rideModalPopup2: false})
  this.setState({
    poc_id: check.poc_id,
    request_id: check.request_id,
    source: check.source,
    destination: check.destination,
    company_name: check.company_name,
    schedule_time: check.schedule_time,
    company_id: check.company_id,
    poc_name: check.poc_name,
    trip_type: check.trip_type,
    booking_status:check.status,
    sourceLng:check.sourcelng,
    sourceLat:check.sourcelat,
    destinationLng:check.destinationlng,
    destinationLat:check.destinationlat,
    tour_type:check.tour_type,
    vehicle_type:check.vehicle_type,
    request_type:check.request_type,
    vehicle_number:check.vehicle_number,
    poc_number:check.poc_number,
    numbers_of_drivers:check.numbers_of_drivers,
    notification_type:check.notification_type,
    sourcecity:check.sourcecity
  });
  }
  // if(check.notification_type=="booking_request"){
  //  alert('booking request')
  // } else {
  //   console.log("ride_notify")
  // }
  
  if(check.trip_type=="one_way"){
    this.callAPI('driving');
  } else {
    console.log("round trip")
  }
  
}
 
callAPI(setProfile) {
  console.log("this.state.sourcelonglat",this.state.sourcelonglat)
  console.log("this.state.destinationlonglat",this.state.destinationlonglat)
  Mapmyindia.route_adv(
    {
      source: (this.state.sourceLng+","+this.state.sourceLat),
      destination: (this.state.destinationLng+","+this.state.destinationLat),
      profile: setProfile,
      geometries: 'geojson',
    },
    response => {
      console.log("response.routes[0].geometry.coordinates",response.routes[0].geometry.coordinates);
      this.setState({
        distance: this.getFormattedDistance(response.routes[0].distance),
        duration: this.getFormattedDuration(response.routes[0].duration),
        route: {
          type: 'Feature',
          geometry: {
            type: 'LineString',
            coordinates: response.routes[0].geometry.coordinates,
          },
        },
        //response.routes[0].geometry.coordinates
      });
    },
  );
}

getFormattedDistance(distance) {
  if (distance / 1000 < 1) {
    return distance + 'mtr.';
  }
  let dis = distance / 1000;
  dis = dis.toFixed(2);
  return dis + 'Km.';
}

getFormattedDuration(duration) {
  let min = parseInt((duration % 3600) / 60);
  let hours = parseInt((duration % 86400) / 3600);
  let days = parseInt(duration / 86400);
  if (days > 0) {
    return (
      days +
      ' ' +
      (days > 1 ? 'Days' : 'Day') +
      ' ' +
      hours +
      ' ' +
      'hr' +
      (min > 0 ? ' ' + min + ' ' + 'min.' : '')
    );
  } else {
    return hours > 0
      ? hours + ' ' + 'hr' + (min > 0 ? ' ' + min + ' ' + 'min' : '')
      : min + ' ' + 'min.';
  }
}


onIds(device) {
  console.log('Device info: ', device);
}



  componentDidMount() {
    const {navigation} =this.props;
    this.focusListener = navigation.addListener('willFocus', () => {});
    this._fetchUserDetails().then(()=>{
      this._fetchCurrentLocationToRedirect();
    })
    // this._acceptBooking = this.props.navigation.addListener('focus', () => {
    //   Alert.alert('Refreshed');
    // });
    // this.intervalID = setInterval(this._acceptBooking.bind(this), 5000);
    //this.geofencing();
    this.RideSessionDetails();
    this.CheckInSessionDetails().then(()=>{
      return this.updateDriverLatLong();
    })
  }

  async updateDriverLatLong(){
    try{
      BackgroundTimer.runBackgroundTimer(() => { 
        Geolocation.getCurrentPosition(
            (position) => {
               const currentLongitude = JSON.stringify(position.coords.longitude);
               const currentLatitude = JSON.stringify(position.coords.latitude);
               //const timeStamp = JSON.stringify(position.timestamp);
               console.log(`location update: ${currentLongitude},${currentLatitude}`)
               this.sendLatLong(currentLongitude,currentLatitude)
            },
            (error) => console.log("error.message",error.message),
            //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
         );
       }, 60000);
    } catch(error){
      console.log(error)
    } 
  }
 
  async sendLatLong(currentLongitude,currentLatitude){
    try {
      const driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "update_driver_lat_long");
      formData.append("driver_id", driverId);
      formData.append("latitude", currentLatitude);
      formData.append("longitude", currentLongitude);
      const response = await fetch(
        BaseUrl.url5,
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      if (responseJson.success === true) {
        console.log("new live lat long",responseJson)
      } else {
        console.log("checknew live lat long",responseJson)
      }
    } catch (error) {error}
  }

  async updateLocation(){
    longArray=[]; 
    timeStampArray=[];
    let rideStatus = await AsyncStorage.getItem("initiateFlag");
    if(rideStatus=="flaged"){
      BackgroundTimer.runBackgroundTimer(() => { 
       Geolocation.getCurrentPosition(
           (position) => {
              const currentLongitude = JSON.stringify(position.coords.longitude);
              const currentLatitude = JSON.stringify(position.coords.latitude);
              const timeStamp = JSON.stringify(position.timestamp);
              longArray.push(currentLongitude.concat(",").concat(currentLatitude))
              timeStampArray.push(timeStamp)
              //console.log("longArray",longArray);
              //console.log("currentLongitude",currentLongitude);
              //console.log("currentLatitude",currentLatitude)
              //console.log("timestamp",timeStamp)
             // console.log("concat ==", currentLongitude.concat(",").concat(currentLatitude))
              var resultLatlng = {};
                for (var i = 0; i < longArray.length; ++i){
                  resultLatlng["position" + (i+1)] = longArray[i];                                                                                                                          
              }
              var resultTimestamp = {};
                for (var i = 0; i < timeStampArray.length; ++i){
                  resultTimestamp["position" + (i+1)] = timeStampArray[i];                                                
              }
              //console.log("resultLatlng==",resultLatlng)
              const finalLatLong = Object.keys(resultLatlng).map(function(k){return resultLatlng[k]}).join(";");
              const finaltimestamp = Object.keys(resultTimestamp).map(function(k){return parseInt(resultTimestamp[k]/1000)}).join(";");
              AsyncStorage.setItem("finalLatLong",JSON.stringify(finalLatLong))
              AsyncStorage.setItem("finaltimestamp",JSON.stringify(finaltimestamp))
              //console.log("finaltimestamp==",finaltimestamp)
              //console.log("finalLatLong==",finalLatLong)
              this.sendLocation(finalLatLong,finaltimestamp)
           },
           (error) => console.log("error.message",error.message),
           //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
      }, 20000);
    } else {
      BackgroundTimer.stopBackgroundTimer();
    }
  }

  async sendLocation(finalLatLong,finaltimestamp){
    const driverId = await AsyncStorage.getItem('driverId');
    const ride_history_id = await AsyncStorage.getItem("ride_history_id");
    try {
      var formData = new FormData();
      formData.append("action", "update_live_location");
      formData.append("driver_id",driverId);
      formData.append("driver_booking_id", "");
      formData.append("b2b_ride_history_id",ride_history_id);
      formData.append("latlong_coordinate",finalLatLong);
      formData.append("timestamp", finaltimestamp);
      //console.log("formdata==",formData)
      const response = await fetch(BaseUrl.url5, {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      //console.log('driverdataride===',responseJson)  
      if(responseJson.success===true){
       console.log('responseJson.success',responseJson)  
       } else {
         console.log('responseJson.false',responseJson)  
      }  
    } catch (error) {
      console.log("error rsp==",error);
    }
  }

  async updateGeoAddress(lat,long){
    const tokenType = await AsyncStorage.getItem("tokenType");
    const token = await AsyncStorage.getItem("token")
    console.log("url==", "http://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long)

    try {
      const response = await fetch(
        "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      if(responseJson.error==="invalid_token"){
          this.updateToken().then(()=>{
            return this.updateGeoAddress(lat,long)
          })
      } else {
        console.log("responseJson===",responseJson.results[0].formatted_address)
        this.setState({test:responseJson.results[0].formatted_address})
      }
    } catch(error){
        console.log(error)
    }
  }

  async updateToken(){
    try {
      var formData = new FormData();
      formData.append("grant_type", grantType);
      formData.append("client_id", ClientId);
      formData.append("client_secret",ClientSecret);
      const response = await fetch(
        "https://outpost.mapmyindia.com/api/security/oauth/token",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      await AsyncStorage.setItem("token",JSON.stringify(responseJson.access_token))
      await AsyncStorage.setItem("tokenType",JSON.stringify(responseJson.token_type))
    } catch(error){
        console.log(error)
    }
  }

  async updateCallback() {
    //this.componentDidMount();
    //this.geofencing();
    let checkflag = await AsyncStorage.getItem("checkflag");
    if (checkflag === 'true') {
      this.props.navigation.navigate("CheckOut");
    } else {
      console.log("not checked")
      this.setState({ imagePath: null })
    }
  }

  componentWillUnmount() {
    // clearInterval(this.intervalID);
    // this._acceptBooking
    this.focusListener.remove();
  }

  async checkDistance() {
    console.log(this.state.geofencingData, 'chgeckdistance');
    let size = this.state.geofencingData.length;
    if (size > 0) {
      AsyncStorage.setItem("geoLogin","")
      this.checkInflag = false;
      for (var i = 0; i < size; i++) {
        let lat = this.state.geofencingData[i].latitude;
        let long = this.state.geofencingData[i].longitude;
         await Geolocation.getCurrentPosition(
          function (position) {
            let value = geolib.getDistance(position.coords, { latitude: lat, longitude: long })
            //alert(JSON.stringify(value))
            console.log('value===in meter', value)
            if (value < 15000) {
              console.log("login")
              this.checkInflag = true;
              AsyncStorage.setItem("geoLogin","login")
            } 
          }
        );
      }
        let geoLoginFlag = await AsyncStorage.getItem("geoLogin");
     
      if(JSON.stringify(geoLoginFlag).replace(/"/g,'') == "login"){
        this.checkInflag = true;
        this.setState({isLoading:false})
      } 
      else{
        AsyncStorage.setItem("geoLogin","")
        this.setState({isLoading:false})
      }
      
      console.log("geoLogin === ",this.checkInflag)
    }
     return this.checkInflag; 
  }

  async geofencing() {
    this.setState({isLoading:true})
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "get_company_geofencing");

      const response = await fetch(BaseUrl.url2 + "service.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      console.log(responseJson.data);
      if (responseJson.success === true) {

        this.setState({ geofencingData: responseJson.data });
        return this.checkDistance();

      }

    } catch (error) {
      console.log(error);
    }
  }

  async RideSessionDetails() {
    this.setState({isLoading:true})
     try {
       const driverId = await AsyncStorage.getItem('driverId');
       var formData = new FormData();
       formData.append("action", "get_running_ride");
       formData.append("driver_id", driverId);
       const response = await fetch(BaseUrl.url2+"service.php", {
         body: formData,
         method: "POST",
         headers: {
           "Accept": "multipart/form-data",
           "Content-Type": "multipart/form-data",
           'Cache-Control': 'no-cache, no-store, must-revalidate',
           'Pragma': 'no-cache',
           'Expires': 0
         }
       });
       const responseJson = await response.json();
       console.log('driverdataride===',responseJson)
       if(responseJson.data.ride_history_id){
         this.setState({ isLoading: false });
         AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id)
         if(responseJson.data.status == 'initiated'){
            AsyncStorage.setItem("initiateFlag",'flaged')
         }
         else if(responseJson.data.status == 'ride_started'){
            AsyncStorage.setItem("initiateFlag",'flaged')
            AsyncStorage.setItem("startTipFlag",'flaged')
         }
       }
       else{
         this.setState({ isLoading: false });
         AsyncStorage.setItem("ride_history_id","")
         AsyncStorage.setItem("startTipFlag","")
         AsyncStorage.setItem("initiateFlag","")
         this.props.navigation.navigate("StartTrip")
       }
            
     } catch (error) {
       this.setState({ isLoading: false });
       console.log(error);
     }
   }


  async CheckInSessionDetails() {
    this.setState({ isLoading: true });
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "check_checkedin_or_not");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url2 + "service.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      console.log('checkinid formdata===', formData)
      const responseJson = await response.json();
      console.log('driverdatacheck===', responseJson)
      if (responseJson.data.checkedin_id) {
        this.setState({ isLoading: false });
        AsyncStorage.setItem("checkedin_id", responseJson.data.checkedin_id)
        AsyncStorage.setItem("check_in_time",responseJson.data.check_in_time)
        AsyncStorage.setItem("checkedInDateTime",responseJson.data.checkedInDateTime)
        this.props.navigation.navigate("CheckOut")
      }
      else {
        this.setState({ isLoading: false });
        AsyncStorage.removeItem("checkedin_id")
        AsyncStorage.removeItem("check_in_time")
        AsyncStorage.removeItem("checkedInDateTime")
        this.props.navigation.navigate("CheckIn")
        
      }

    } catch (error) {
      console.log(error);
    }
  }




  async _fetchUserDetails() {
    let currentStatus = await AsyncStorage.getItem("checkedin_id")
    console.log("check", currentStatus)
    if (currentStatus != null) {
      this.props.navigation.navigate("CheckOut")
    }
    else {
      this.props.navigation.navigate("CheckIn")
    }
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "get_driver_info");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url1 + "index.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      this.setState({
        userName: responseJson.name
      })
    } catch (error) {
      console.log(error);
    }
  }


  async _fetchCurrentLocationToRedirect() {
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (!granted) {
        this.props.navigation.navigate("GpsScreen");
      } else {
        this.checkGpsStatus();
      }
    }
  }

  checkGpsStatus(){
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
    .then(data => {
      if(data){
        this.checkNavigation();
      } else {
        this.props.navigation.navigate("GpsScreen");
      }
    }).catch(err => {
      this.props.navigation.navigate("GpsScreen");
    });
  }

  async checkNavigation(){
    await Geolocation.getCurrentPosition(
      (position) => {
        console.log("position", position.coords.latitude)
        this.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude })
        this.updateGeoAddress(position.coords.latitude,position.coords.longitude)
      });
  }

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  };

  async CheckIn() {
    //await this.geofencing();
    // if(this.checkInflag === false){
    //   ToastAndroid.show("It seems that you are away from the office.", ToastAndroid.LONG);
    //   } else {
      if(this.state.imagePath == undefined || this.state.imagePath == null || this.state.imagePath == ""){
        ToastAndroid.show("Please upload a picture.", ToastAndroid.LONG);
      } else {
        try {
          this.setState({ isLoading: true });
          let time = moment(new Date()).format("YYYY-MM-DD HH:mm")
          var driverId = await AsyncStorage.getItem("driverId");
          var formData = new FormData();
          formData.append("action", "b2b_check_in_check_out");
          formData.append("type", "checkin");
          formData.append("driver_id", driverId);
          formData.append("check_in_time", time)
          formData.append("check_in_latitude", this.state.latitude);
          formData.append("check_in_longitude", this.state.longitude);
          formData.append("location", this.state.test);
          formData.append("check_in_photo", {
            uri: this.state.imagePath,
            type: "image/jpeg",
            name: this.state.imagePath.substring(
              this.state.imagePath.lastIndexOf("/") + 1,
              this.state.imagePath.length
            )
          })
          console.log('formdata', formData)
          console.log('url', BaseUrl.url2 + "service.php")
  
          const response = await fetch(
            BaseUrl.url2 + "service.php",
            {
              body: formData,
              method: "POST",
              headers: {
                Accept: "multipart/form-data",
                "Content-Type": "multipart/form-data",
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': 0
              }
            }
          );
  
          const responseJson = await response.json();
          console.log('data===', responseJson)
          if (responseJson.success === true) {
            this.setState({ isLoading: false })
            AsyncStorage.setItem("checkedin_id", responseJson.data.checkedin_id);
            await AsyncStorage.setItem("checkflag", 'true');
            var checkedin_id = await AsyncStorage.getItem("checkedin_id");
            console.log("cherckin", checkedin_id)
            AsyncStorage.setItem("check_in_time",responseJson.data.check_in_time)
            AsyncStorage.setItem("checkedInDateTime",responseJson.data.checkedInDateTime)
            console.log("time===",responseJson.data.checkedInDateTime)
            ToastAndroid.show("Check-in successfully.", ToastAndroid.LONG);
            let bookingType = await this.props.navigation.getParam("checktype")
            if(bookingType=="notification"){
              this.props.navigation.navigate("MyRideList");
            } else {
              this.props.navigation.navigate("Initiate");
            }
          } else {
            this.setState({ isLoading: false })
            ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
          }
        } catch (error) {
          this.setState({ isLoading: false })
          NetInfo.fetch().then(connection => {
            if(!connection.isConnected){
              Toast.show("Please Check Your Internet Connection !",Toast.LONG);
            } else {
              alert(error)
            }
          });
        }
      }
    // }
  }

  async uploadFiles() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          compressImageQuality: 0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ imagePath: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }


  _onRefresh(){
    this.setState({refreshing:true});
    this.geofencing().then(() =>{
      this.setState({refreshing:false})
    });
  }

  async _acceptBooking() {
    console.log('Booking Accept')
    this.setState({ rideModalPopup: false });
    this.setState({rideModalPopup2: false});
    NetInfo.fetch().then(connection => {
      if(!connection.isConnected){
        Toast.show("Please Check Your Internet Connection !",Toast.LONG);
      }
    });
    if(typeof this.mySound!="undefined"){
      this.mySound = await this.mySound.pause();
    } else {
      console.log("else part sound")
    }
    try {
      this.setState({ isSpinnerLoading: true });
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "b2b_assigned_driver_create_history");
      formData.append(
        "poc_id",
        this.state.poc_id
      );
      formData.append("driver_id", driverId);
      formData.append("request_id", this.state.request_id);
      formData.append("source", this.state.source);
      formData.append("destination", this.state.destination);
      formData.append("company_name", this.state.company_name);
      formData.append("schedule_time",this.state.schedule_time);
      formData.append("company_id", this.state.company_id);
      formData.append("poc_name", this.state.poc_name);
      formData.append("trip_type", this.state.trip_type);
      formData.append("booking_status", this.state.booking_status);
      formData.append("tour_type", this.state.tour_type);
      formData.append("vehicle_type", this.state.vehicle_type);
      formData.append("vehicle_number", this.state.vehicle_number);
      formData.append("sourcelat", this.state.sourceLat);
      formData.append("sourcelng", this.state.sourceLng);
      formData.append("destinationlng", this.state.destinationLng);
      formData.append("destinationlat", this.state.destinationLat);
      formData.append("notification_type", this.state.notification_type);
      formData.append("request_type", this.state.request_type);
      formData.append("poc_number", this.state.poc_name);
      formData.append("numbers_of_drivers", this.state.numbers_of_drivers);
      formData.append("sourcecity", this.state.sourcecity);

      console.log("final pos==",formData)

      const response = await fetch(
        BaseUrl.url4,
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();

      if (responseJson.success === true) {
        console.log("rsp b2b_Assigned_driver_create_his")
        console.log("rsp1==",responseJson)
        this.setState({ isLoading: false,rideModalPopup: false});
        this.setState({isLoading: false, rideModalPopup2: false});
        if(responseJson.message==="This Ride hasbeen Expired!"){
          this.setState({ rideModalPopup: false });
          this.setState({rideModalPopup2: false});
          ToastAndroid.show("Ride has been expired", ToastAndroid.LONG);
        } else {
          this.setState({ rideModalPopup: false });
          this.setState({rideModalPopup2: false});
          ToastAndroid.show("Ride Accepted", ToastAndroid.LONG);
          await AsyncStorage.setItem("request_type",this.state.request_type)
          await AsyncStorage.setItem("bookingType","notification")
          this.props.navigation.navigate("MyRideList",{
            bookingType:"accepted",
            poc_id: this.state.poc_id,
            request_id: this.state.request_id,
            source: this.state.source,
            destination: this.state.destination,
            company_name: this.state.company_name,
            schedule_time: this.state.schedule_time,
            company_id: this.state.company_id,
            poc_name: this.state.poc_name,
            trip_type: this.state.trip_type,
            booking_status:this.state.booking_status,
            sourceLng:this.state.sourceLng,
            sourceLat:this.state.sourceLat,
            destinationLng:this.state.destinationLng,
            destinationLat:this.state.destinationLat,
            tour_type:this.state.tour_type,
            vehicle_type:this.state.vehicle_type,
            request_type:this.state.request_type,
            vehicle_number:this.state.vehicle_number,
            poc_number:this.state.poc_number,
            numbers_of_drivers:this.state.numbers_of_drivers,
            notification_type:this.state.notification_type
          })
                   console.log("notification type",this.state.notification_type)
                   this.props.navigation.navigate("MyRideList")
        }
        
      } 
      
      else {
        this.setState({ isLoading: false});
      }
      this.props.navigation.navigate("MyRideList")
    } catch (error) {
      console.log(error)
      ToastAndroid.show("Ride has been expired", ToastAndroid.LONG);
      this.setState({ isLoading: false,rideModalPopup: false});
      this.setState({isLoading: false, rideModalPopup2: false});
      
    }
  }
  _declineRideFunc() {
    this.setState({ rideModalPopup: false });
    this.setState({rideModalPopup2: false});
    //this.props.navigation.navigate("MyRideList")
    ToastAndroid.show("Ride Declined", ToastAndroid.LONG);
    if(typeof this.mySound!="undefined"){
      this.mySound = this.mySound.pause();
      Vibration.cancel();
    } else {
      console.log("else part")
    }
  }
 

  // hide show modal
  displayModal(show){
    this.setState({isVisible: show})
  }
  render() {
    const { modalVisible } = this.state;
    var modalBackgroundStyle = { backgroundColor: "rgba(0, 0, 0, 0.5)" };
    return (

      <Container style={{Height:Dimensions.get('window').height,Width:Dimensions.get('window').width}}>
        <Content>
          <Header
          text={"Start Your Day"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
          <ScrollView
      //     refreshControl={
      // <RefreshControl    refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)} />}
        >
            <ImageBackground style={{ width: "100%", height: 180, marginBottom: 20, backgroundColor: appColor.Blue, borderBottomRightRadius: 30, borderBottomLeftRadius: 30 }}>

              {this.state.userName === false ? <Image style={styles.circleView}
                source={image.profile}
              /> : null}
              {this.state.userName ? <View style={styles.circleView}><Text style={styles.circleViewText}>{this.state.userName.split().map((word) => word.charAt(0)).join(' ')}</Text></View> : null}


              <Text style={{
                fontFamily:Fonts.Medium, color: appColor.white, marginTop: 10, textAlign
                  : 'center', fontSize: 18
              }}>{this.state.userName}</Text>
            </ImageBackground>
            <View style={{ padding: 20 }}>
              <Spinner
                visible={this.state.isLoading}
                style={{ color: '#000' }}
                color={appColor.Blue}
                overlayColor="rgba(0, 0, 0, 0.55)"
              />
              <NavigationEvents
                onDidFocus={() => this.updateCallback()}
              />
              <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                <Text style={{ fontSize: 20, fontFamily: 'Roboto Light', fontFamily:Fonts.Regular}}>Upload Your Picture</Text>
              </View>
              
                <View style={{ width: 100, height: 100, backgroundColor: appColor.darkGrey, marginHorizontal: "34%" }}>
                <TouchableOpacity
                     style={{width: 100, height: 100, backgroundColor: appColor.darkGrey}}
                       onPress={() => this.uploadFiles()}
                     >
                       {/* {this.state.imagePath == false ? <Image onPress={() => this.uploadFiles()}
                    source={CameraIcon}
                    style={{ width: 32, height: 28, marginTop: 35, marginHorizontal: 34 }}
                  /> : null} */}
                  {
                    this.state.imagePath ? <Image source={{ uri: this.state.imagePath }}
                      style={{ width: 100, height: 100 }} /> : null
                  }
                       </TouchableOpacity>
               
                </View>
              
              <View style={{ justifyContent: 'center', marginBottom: 20, alignItems: 'center', marginTop: 80 }}>
                <AppButton
                  text='Check In'
                  // disabled={this.checkInflag ? false : true}
                  // bgColor={this.checkInflag ? appColor.Blue : appColor.darkGrey}
                  bgColor={appColor.Blue}
                  width={'100%'}
                  textColor={appColor.white}
                  onClickFunc={() => this.CheckIn()}
                />
              </View>
              <TouchableOpacity onPress={this.navigateToScreen("AttendanceHistory")}>
                <Text style={{ fontSize: 15, fontFamily: 'Roboto Light', fontFamily:Fonts.Medium, textAlign: "center", color: appColor.Blue }}>View Your Attendance History</Text>
              </TouchableOpacity>
            </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.rideModalPopup}
          onRequestClose={() => this.setState({ rideModalPopup: false })}
        >
          <View style={[styles.modalContainer, modalBackgroundStyle]}>
            <View style={styles.modalHeaderView}>
            <Text style={styles.modalHeaderTextView}>Booking Details !</Text>
              <View style={styles.dummyTextSpacer} />
              <View style={styles.userNameBaseView}>
                <View>
                  <Image
                    style={styles.userNameIcon}
                    source={image.avtar}
                  />
                </View>
                <View>
                  <Text style={styles.userNameTextView}>
                    {this.state.company_name}
                  </Text>
                </View>
              </View>

              <View style={styles.sourceAddressView}>
                <View>
                  <Image
                    style={styles.sourceAddressIcon}
                    source={image.source}
                  />
                </View>

                <View>
                  <Text
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={{ marginRight: 30,fontFamily:Fonts.Regular}}
                  >
                    {this.state.source}
                  </Text>
                </View>
              </View>

              {this.state.trip_type === "one_way" && (
                <View style={styles.destAddressView}>
                  <View>
                    <Image
                      style={styles.destAddressIcon}
                      source={image.destination}
                    />
                  </View>

                  <View
                    numberOfLines={1}                                                 
                    ellipsizeMode="tail"
                    style={{ marginRight: 30 }}
                  >
                    <Text style={{fontFamily:Fonts.Regular}}>{this.state.destination}</Text>
                  </View>
                </View>
              )}
              <View style={styles.tripTypeView}>
                <View>
                  <Image
                    style={styles.tripTypeIcon}
                    source={image.tripType}
                  />
                </View>

                <View
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{ marginRight: 10 }}
                >
                  <Text style={{fontFamily:Fonts.Regular}}>Trip Type :</Text>
                </View>

                <View
                  numberOfLines={1}
                  llipsizeMode="tail"
                  style={{ marginRight: 30 }}
                >
                  <Text
                    style={[
                      styles.text,
                      { textAlign: "left", fontFamily:Fonts.Medium }
                    ]}
                  >
                    {this.state.trip_type=="one_way"?"One Way":this.state.trip_type=="round_trip"?"Round Trip":this.state.trip_type}
                  </Text>
                </View>
              </View>

              <View style={styles.bookingTypeView}>
                <View>
                  <Image
                    style={styles.bookingTypeIcon}
                    source={image.calenderModal}
                  />
                </View>

                <View
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{ marginRight: 10 }}
                >
                  <Text style={{fontFamily:Fonts.Regular}}>Booking Type :</Text>
                </View>

                <View
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{ marginRight: 30 }}
                >
                  <Text
                    style={[
                      styles.text,
                      { textAlign: "left", color: "green", fontFamily:Fonts.Medium }
                    ]}
                  >
                    {this.state.booking_status}
                  </Text>
                </View>
              </View>

              <View style={[styles.bookingTypeView,{marginTop:0}]}>
                <View>
                  <Image
                    style={styles.bookingTypeIcon}
                    source={image.editIcon}
                  />
                </View>

                <View
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{ marginRight: 10 }}
                >
                  <Text style={{fontFamily:Fonts.Regular}}>Schedule Time:</Text>
                </View>

                <View
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{ marginRight: 30 }}
                >
                  <Text
                    style={[
                      styles.text,
                      { textAlign: "left", color: appColor.darkGrey, fontFamily:Fonts.Regular }
                    ]}
                  >
                    {this.state.schedule_time}
                  </Text>
                </View>
              </View>

              {/* <MapmyIndiaGL.MapView  style={{height:120}}>
              <MapmyIndiaGL.Camera
                  ref={c  => (this.camera = c)}
                  zoomLevel={8}
                  minZoomLevel={4}
                  maxZoomLevel={22}
                  centerCoordinate={[this.state.sourceLng,this.state.sourceLat]}
              />
                </MapmyIndiaGL.MapView> */}

                 {
                  this.state.trip_type!="one_way"&&(
                    <MapmyIndiaGL.MapView  style={{height:120}}>
                <MapmyIndiaGL.Camera
                    ref={c  => (this.camera = c)}
                    zoomLevel={12}
                    minZoomLevel={4}
                    maxZoomLevel={22}
                  centerCoordinate={[77.231409,28.6162]}  
                  //centerCoordinate={[this.state.sourceLng,this.state.sourceLat]}
                />
                </MapmyIndiaGL.MapView>
                  )
                }

                {
                  this.state.trip_type=="one_way"&&(
                    <MapmyIndiaGL.MapView  style={{height:120}}>
                <MapmyIndiaGL.Camera
                    ref={c  => (this.camera = c)}
                    //zoomLevel={12}
                    bounds={{
                      ne: [bbox(this.state.route)[0],bbox(this.state.route)[1]],
                      sw: [bbox(this.state.route)[2],bbox(this.state.route)[3]],
                      paddingBottom:50,
                      paddingLeft:50,
                      paddingRight:50,
                      paddingTop:50
                    }}        
                    //centerCoordinate={[this.state.sourceLng,this.state.sourceLat]}
                />
                <MapmyIndiaGL.ShapeSource id="routeSource" shape={this.state.route}>
                <MapmyIndiaGL.LineLayer id="routeFill" style={layerStyles.route} />
                </MapmyIndiaGL.ShapeSource>
                <MapmyIndiaGL.PointAnnotation id="1" coordinate={[this.state.sourceLng,this.state.sourceLat]}/>
                <MapmyIndiaGL.PointAnnotation id="2" coordinate={[this.state.destinationLng,this.state.destinationLat]}/>
                </MapmyIndiaGL.MapView>
                  )
                }
              {/* <View>
                <MapView
                  ref={ref => (this.mapView = ref)}
                  provider={PROVIDER_GOOGLE}
                  style={{ height: 120 }}
                  showsUserLocation={true}
                  region={this.state.region}
                >
                  <MapView.Marker
                    coordinate={{
                      latitude: this.state.srcFirebaseLat,
                      longitude: this.state.srcFirebaseLong,
                      latitudeDelta: 0.003,
                      longitudeDelta: 0.003
                    }}
                  />
                  <MapView.Marker
                    coordinate={{
                      latitude: this.state.destFirebaseLat,
                      longitude: this.state.destFirebaseLong,
                      latitudeDelta: 0.003,
                      longitudeDelta: 0.003
                    }}
                  />
                  {!(this.state.tripTypeFirebaseNotif === "round_trip") && (
                    <MapViewDirections
                      origin={{
                        latitude: this.state.srcFirebaseLat,
                        longitude: this.state.srcFirebaseLong
                      }}
                      destination={{
                        latitude: this.state.destFirebaseLat,
                        longitude: this.state.destFirebaseLong
                      }}
                      apikey={googleMaps_apiKey}
                      strokeColor={"#0000e4"}
                      strokeWidth={3}
                      resetOnChange={true}
                      onReady={result => {
                        this.setState({
                          distance: result.distance,
                          duration: result.duration
                        });
                        this.mapView.fitToCoordinates(result.coordinates, {
                          edgePadding: {
                            right: screenWidth / 50,
                            bottom: screenHeight / 50,
                            left: screenWidth / 50,
                            top: screenHeight / 50
                          },
                          animated: true
                        });
                      }}
                    />
                  )}
                </MapView>
              </View> */}

              <View style={styles.modalButtonView}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => this._declineRideFunc()}
                >
                  <Text style={styles.declineButtonView}>Decline</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.button}
                  onPress={() => this._acceptBooking()}
                >
                  <Text style={styles.acceptButtonView}>Accept</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isVisible}
          onRequestClose={() => this.setState({ rideModalPopup: false })}
        >
          <View style={[styles.modalContainer, modalBackgroundStyle]}>
            <View style={styles.modalHeaderView}>
            {/* <Text style={styles.modalHeaderTextView}>Booking Details !</Text> */}
              <View style={styles.dummyTextSpacer} />
              <View style={styles.userNameBaseView}>
                
                <View>
                  <Text style={styles.userNameTextView}>
                    {this.state.rideNotifyMessage}
                  </Text>
                </View>
              </View>

                         </View>
          </View>
        </Modal>
        {/* <Modal
            animationType = {"slide"}
            transparent={false}
            visible={this.state.isVisible}
            onRequestClose={() => {
              Alert.alert('Modal has now been closed.');
            }}>

           
              <Text style = { styles.text }>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                  Maecenas eget tempus augue, a convallis velit.</Text>

              <Text 
                style={styles.closeText}
                onPress={() => {
                  this.displayModal(!this.state.isVisible);}}>Close Modal</Text>
          </Modal> */}
          {/* <TouchableOpacity
              
              onPress={() => {
                this.displayModal(true);
              }}>
              <Text>Show Modal</Text>
          </TouchableOpacity>   */}
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  RegisterButton: {
    backgroundColor: appColor.yellow,
    marginTop: 20
  },
  circleView: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: appColor.lightGrey,
    marginHorizontal: "42%",
    marginTop: 30
    // marginRight:20

  },
  circleViewText: {
    fontSize: 25,
    fontFamily:Fonts.Medium,
    textAlign: "center",
    color:appColor.Blue,
    marginTop:12
  },
  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  headerTextView: {
    fontSize: 18,
    fontFamily:Fonts.Medium,
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    marginLeft: Dimensions.get("window").width/5
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 10,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  tourTypeIcon: {
    width: 22,
    height: 22,
    padding: 10,
    marginRight: 20
  },
  tourTypeView: {
    flexDirection: "row",
    marginBottom: 0,
    marginTop: 0,
    justifyContent: "flex-start",
    alignContent: "flex-start",
    alignItems: "flex-start"
  },
  destAddressIcon: {
    width: 22,
    height: 22,
    padding: 10,
    marginRight: 20
  },
  destAddressView: {
    flexDirection: "row",
    marginBottom: 0,
    marginTop: 5,
    justifyContent: "flex-start",
    alignContent: "flex-start",
    alignItems: "flex-start"
  },
  sourceAddressIcon: {
    width: 22,
    height: 22,
    padding: 10,
    marginRight: 20
  },
  sourceAddressView: {
    flexDirection: "row",
    marginBottom: 5,
    marginTop: 5,
    justifyContent: "flex-start",
    alignContent: "flex-start",
    alignItems: "flex-start"
  },
  userNameIcon: {
    width: 22,
    height: 22,
    padding: 10,
    marginRight: 20
  },
  userNameTextView: {
    fontSize: Fonts.h20,
    color: "#757575",
    fontFamily:Fonts.Medium,
    textAlign: "center"
  },
  tripTypeIcon: {
    width: 22,
    height: 22,
    padding: 10,
    marginRight: 20
  },
  tripTypeView: {
    flexDirection: "row",
    marginBottom: 0,
    marginTop: 5,
    justifyContent: "flex-start",
    alignContent: "flex-start",
    alignItems: "flex-start"
  },
  bookingTypeIcon: {
    width: 22,
    height: 22,
    padding: 10,
    marginRight: 20
  },
  bookingTypeView: {
    flexDirection: "row",
    marginBottom: 10,
    marginTop: 5,
    justifyContent: "flex-start",
    alignContent: "flex-start",
    alignItems: "flex-start"
  },
  reportingTimeView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 20,
    marginRight: 20
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 20,
    backgroundColor: "#ecf0f1"
  },
  modalHeaderView: {
    padding: 10,
    margin: 50,
    justifyContent: "center",
    backgroundColor: "white",
    elevation: 20,
    padding: 10,
    borderRadius: 4
  },
  modalHeaderTextView: {
    fontSize: Fonts.h20,
    color: "#000",
    fontFamily:Fonts.Medium,
    textAlign: "center"
  },
  dummyTextSpacer: {
    height: StyleSheet.hairlineWidth,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appColor.Blue,
    marginTop: 5
  },
  userNameBaseView: {
    flexDirection: "row",
    marginBottom: 10,
    marginTop: 15,
    justifyContent: "flex-start",
    alignContent: "flex-start",
    alignItems: "flex-start"
  },
  modalButtonView: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginTop: 10
  },
  declineButtonView: {
    alignSelf: "center",
    fontSize: Fonts.h18,
    fontFamily:Fonts.Medium,
    color: "white"
  },
  reportingTimeView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 20,
    marginRight: 20
  },
  acceptButtonView: {
    alignSelf: "center",
    fontSize: Fonts.h18,
    fontFamily:Fonts.Medium,
    color: "white"
  },
  button: {
    backgroundColor: appColor.Blue,
    width: "45%",
    borderRadius: 1,
    borderColor: "#000",
    height: 40,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    borderRadius: 4
  },
});
const MyDrawerNavigator = createDrawerNavigator(
  {
    Checkin: {
      screen: Checkin
    },
    TripHistory: {
      screen: TripHistory
    },
    CheckOut:
    {
      screen: CheckOut
    },
    ViewProfileScreen:
    {
      screen: ViewProfileScreen
    },
    StartTrip: {
      screen: StartTrip
    },
    EndTrip: {
      screen: EndTrip
    },
    Initiate:{
      screen:Initiate
    },
    AttendanceHistory: {
      screen: AttendanceHistory
    },
    AddFuel:{
      screen: AddFuel
    },
    CreateToken:{
      screen:CreateToken
    },
    ActiveBooking:{
      screen:ActiveBooking
    },
    MyRideList:{
      screen:MyRideList
    },
    OTPAuthenticationCheckOut:{
      screen:OTPAuthenticationCheckOut
    },
    MobileAuthenticationCheckOut:{
      screen:MobileAuthenticationCheckOut
    },
    AddmoreCarTripEnd:{
      screen:AddmoreCarTripEnd
    }
  },
  {
    contentComponent: SideMenu,
    drawerWidth: 300,
    //drawerWidth: Dimensions.get("window").width-40
  }
);

const MyApp = createAppContainer(MyDrawerNavigator);
export default MyApp;
