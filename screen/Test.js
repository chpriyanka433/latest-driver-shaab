
import React, { Component } from "react";

//import react in our code. 
import {View, Text,  StyleSheet, Image ,PermissionsAndroid,Platform,DeviceEventEmitter,
  NativeAppEventEmitter,
  AsyncStorage,Dimensions,ScrollView} from 'react-native';
import {Geolocation,RestApiKey,ClientId,ClientSecret,grantType,MapsSdkkey, appColor,AppButton,BaseUrl} from "../screen/common";
import MapmyIndiaGL from 'mapmyindia-map-react-native-beta';
import BackgroundTimer from 'react-native-background-timer';
import Button from "./common/Button";
import ImageZoom from 'react-native-image-pan-zoom';

//import all the components we are going to use.
//import Geolocation from '@react-native-community/geolocation';


var pts1; 

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLongitude: 88.3639,//Initial Longitude
      currentLatitude: 22.5726,//Initial Latitude
      arratValue:"",
      pts1:"",
      finalDistance:"",
      Geometry:"",
      Testgeometry: "ie`iByrp}MN{HKeMXmOKuGa@gIiAqFeCyHmBuDsBmB_CuAuGeBeP{D",
      imgUrl:""
   };
    MapmyIndiaGL.setMapSDKKey(MapsSdkkey);
    MapmyIndiaGL.setRestAPIKey(RestApiKey);
    MapmyIndiaGL.setAtlasClientId(ClientId);
    MapmyIndiaGL.setAtlasClientSecret(ClientSecret);
    MapmyIndiaGL.setAtlasGrantType(grantType);
  }
  
 componentDidMount = (async) => {
   let groupLatlng = "[[77.21892,28.607336],[77.218963,28.607148],[77.218942,28.60696],[77.218824,28.606677],[77.21876,28.606602],[77.218578,28.606385],[77.218374,28.606263],[77.218235,28.606216],[77.218063,28.606188],[77.217956,28.606197],[77.217945,28.605199],[77.217902,28.60438],[77.217859,28.603674],[77.217827,28.603033],[77.217773,28.602223],[77.217698,28.600933],[77.217805,28.600924],[77.217902,28.600886],[77.217988,28.600829],[77.218074,28.600707],[77.218106,28.600622],[77.218106,28.6005],[77.218085,28.600434],[77.218021,28.60033],[77.217882,28.600226],[77.217753,28.600188],[77.217571,28.600197],[77.217453,28.600244],[77.217389,28.600291],[77.217292,28.600404],[77.216798,28.600197],[77.215897,28.599801],[77.214384,28.599123],[77.212785,28.598407],[77.211326,28.597729],[77.211347,28.597682],[77.211336,28.597588],[77.211239,28.597484],[77.211196,28.597465],[77.211067,28.597465],[77.210992,28.597493],[77.210938,28.59755],[77.209575,28.596947],[77.20935,28.596853],[77.207998,28.59625],[77.20759,28.596071],[77.207526,28.596005],[77.207462,28.595996],[77.207033,28.596109],[77.20684,28.596156],[77.205295,28.596495],[77.204555,28.596561],[77.203546,28.596533],[77.20243,28.59642],[77.201218,28.59609],[77.200596,28.596109],[77.19992,28.596137],[77.199019,28.596175],[77.198826,28.596053],[77.198826,28.595996],[77.198772,28.595855],[77.198675,28.59577],[77.1986,28.595742],[77.198557,28.595723],[77.198428,28.595723],[77.198299,28.595761],[77.198235,28.595808],[77.197055,28.595516],[77.196529,28.59545],[77.196132,28.595431],[77.19566,28.595459],[77.194533,28.595695],[77.193986,28.595865],[77.193911,28.595799],[77.193772,28.595761],[77.193633,28.595789],[77.193515,28.595883],[77.193472,28.596005],[77.193483,28.596062],[77.193032,28.596241],[77.190908,28.597108],[77.190854,28.597042],[77.190768,28.596995],[77.190661,28.596967],[77.190532,28.596976],[77.190457,28.597004],[77.19035,28.597089],[77.190307,28.597155],[77.190286,28.597287],[77.190318,28.597391],[77.189095,28.5979],[77.188655,28.59807],[77.187775,28.598419],[77.187743,28.598362],[77.187646,28.598296],[77.187549,28.598268],[77.18741,28.598287],[77.187313,28.598334],[77.187238,28.598438],[77.187227,28.598532],[77.187259,28.598626],[77.183686,28.600058],[77.181991,28.600727],[77.181809,28.60084],[77.181498,28.601057],[77.181058,28.601528],[77.180897,28.601726],[77.180715,28.601811],[77.180618,28.60183],[77.180403,28.601811],[77.17933,28.601265],[77.175446,28.599212],[77.17521,28.59909],[77.174877,28.598911],[77.173965,28.598431],[77.173718,28.598309],[77.173471,28.598168],[77.172666,28.597754]]";
  console.log("url==","https://apis.mapmyindia.com/advancedmaps/v1/" + RestApiKey+"/still_image_polyline?height=500&width=800&polyline="+groupLatlng+"&color=#ff00f7&offset=10,44&padding_x=50&padding_y=100")
  let picurl = "https://apis.mapmyindia.com/advancedmaps/v1/" + RestApiKey+"/still_image_polyline?height=500&width=800&polyline="+groupLatlng+"&color=#ff00f7&offset=10,44&padding_x=50&padding_y=100"; 
  this.setState({imgUrl:picurl})
  //Checking for the permission just after component loaded
  // let latlng =   AsyncStorage.getItem("finalLatLong")
  // let finaltimestamp =   AsyncStorage.getItem("finaltimestamp")
  // console.log("updated latlng",latlng)
  // console.log("updated timestamp",finaltimestamp)
  // console.log("updated latlng1",JSON.parse(latlng))
  // console.log("updated timestamp1",JSON.parse(finaltimestamp))
  that=this;
  if(Platform.OS === 'ios'){
    this.callLocation(that);
  }else{
    async function requestLocationPermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
            'title': 'Location Access Required',
            'message': 'This App needs to Access your location'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //To Check, If Permission is granted
          that.callLocation(that);
        } else {
          alert("Permission Denied");
        }
      } catch (err) {
        console.warn(err)
      }
    }
    requestLocationPermission();
  }    
 }
 async callLocation(that){
  //alert("callLocation Called");
    Geolocation.getCurrentPosition(
      //Will give you the current location
       (position) => {
          const currentLongitude = JSON.stringify(position.coords.longitude);
          //getting the Longitude from the location json
          const currentLatitude = JSON.stringify(position.coords.latitude);
          //getting the Latitude from the location json
          that.setState({ currentLongitude:currentLongitude });
          //Setting state Longitude to re re-render the Longitude Text
          that.setState({ currentLatitude:currentLatitude });
          //Setting state Latitude to re re-render the Longitude Text
       },
       (error) => alert(error.message),
       //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
    that.watchID = Geolocation.watchPosition((position) => {
      //Will give you the location on location change
        console.log("position change==",position);
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json
       that.setState({ currentLongitude:currentLongitude });
       //Setting state Longitude to re re-render the Longitude Text
       that.setState({ currentLatitude:currentLatitude });
       //Setting state Latitude to re re-render the Longitude Text
    });
 }

 chunkStoredtimestamp=(timestampArray,chunksize)=>{
  var results = [];
    while (timestampArray.length) {
        results.push(timestampArray.splice(0, chunksize));
    }
    return results;
 }

 chunkStoredlatlong(latlongArray, chunksize){
  var results = [];
  while (latlongArray.length) {
      results.push(latlongArray.splice(0, chunksize));
  }
  return results;
}

myfunction(test){
  console.log("test fnnn==",test)
}
 async getSnaptoRoad(){
          var encodedString="}l{}Cc|_`O??GbAc@bFqCe@{@Kh@_G\\cE`BBbBDSbE?A?????@?????A??????????????????????????????";
        var dis = this.get_distance(encodedString);
        console.log("distance===",await dis)

  // let geo =  this.state.Testgeometry;  
  // console.log("geometry===",geo)
  // console.log("geometry===11",JSON.stringify(geo))

  // Get Geometry
  // var latlng = await AsyncStorage.getItem("finalLatLong")
  // var timestamp = await AsyncStorage.getItem("finaltimestamp")
  // // console.log("array timestamp===",JSON.parse(timestamp.split(",")))
  // // console.log("array latlng===",JSON.parse(latlng.split(",")))

  // var chunktimeStamp = this.chunkStoredtimestamp(JSON.parse(timestamp.split(",")), 2);
  // console.log("chunk timestamp",chunktimeStamp);
  // var chunklatlong = this.chunkStoredlatlong(JSON.parse(latlng.split(",")), 2);
  // console.log("chunk chunklatlong",chunklatlong);



  // console.log("url==", "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/snapToRoad?timestamps="+JSON.parse(timestamp)+"&pts="+JSON.parse(latlng))

  // try {
  //   const response = await fetch(
  //     "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/snapToRoad?timestamps="+JSON.parse(timestamp)+"&pts="+JSON.parse(latlng),
  //     {
  //       method: "GET",
  //       headers: {
  //         Accept: "application/json",
  //         "Content-Type": "application/json",
  //       }
  //     }
  //   );
  //   const responseJson = await response.json();
  //   if(responseJson.responseCode==200){
  //     console.log("success", responseJson)
  //     this.setState({Geometry:responseJson.results.matchings[0].geometry})
  //     // console.log("geometry===",geometry)
  //     // console.log("geometry===11",JSON.stringify(geometry))
  //     if(geometry!=""||geometry!=null||geometry!=undefined){
  //       var encodedString=JSON.stringify(responseJson.results.matchings[0].geometry);
  //       var dis = this.get_distance(encodedString);
  //       alert(JSON.stringify(dis))
  //     } else {
  //       console.log("no geometry found")
  //     }
  //   } else {
  //     console.log("success ! check", responseJson)
  //     alert(JSON.stringify(responseJson.message))
  //   }
  // } catch(error){
  //     console.log(error)
  // }
  
 }

 async get_distance(encodedString){
          if(!encodedString) return false;
          var count=0;
          var pts1 = this.decode_path(encodedString);
          let ptsValue = await pts1;
          this.setState({pts1:ptsValue})
          // console.log("pts1 value---",ptsValue)
          // console.log("pts1 value---",ptsValue.length)
          //let ptsValue = pts1.then((value)=>{this.setState({pts1:value})})
          if(pts1){
            for(i=0; i<this.state.pts1.length; i++){ 
                if(this.state.pts1[i+1]){
                  var dis=this.distance(this.state.pts1[i][0], this.state.pts1[i][1], this.state.pts1[i+1][0], this.state.pts1[i+1][1], "K");
                  if(dis){
                    let distance = await dis;
                    count=count+parseInt(distance);
                    // console.log("final distance is==",count)
                    // this.setState({finalDistance:count})
                  }
                }

                // console.log("distance meter",dis)
                // console.log("count meter",count)
                // console.log("type of dis",typeof dis)
                // console.log("type of count",typeof count)
                //console.log("total distance==",parseInt(dis)+parseInt(count)) 

            }
        }
          return count;
  }
        


async distance(lat1, lon1, lat2, lon2, unit) {
  if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
  }
  else {
      var radlat1 = Math.PI * lat1/180;
      var radlat2 = Math.PI * lat2/180;
      var theta = lon1-lon2;
      var radtheta = Math.PI * theta/180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
          dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180/Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit=="K") { dist = dist * 1.609344 }
      if (unit=="N") { dist = dist * 0.8684 }
      dist=Math.round(dist*1000)/1000;
      dist=dist*1000;
      console.log("distance==",dist)
      return dist;
  }
}

async decode_path(encoded) {
  if (encoded != 'undefined') {
      var pts = [];
      var index = 0, len = encoded.length;
      var lat = 0, lng = 0;
      while (index < len) {
          var b, shift = 0, result = 0;
          do {
              b = encoded.charAt(index++).charCodeAt(0) - 63;
              result |= (b & 0x1f) << shift;
              shift += 5;
          } while (b >= 0x20);
          var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
          lat += dlat;
          shift = 0;
          result = 0;
          do {
              b = encoded.charAt(index++).charCodeAt(0) - 63;
              result |= (b & 0x1f) << shift;
              shift += 5;
          } while (b >= 0x20);
          var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
          lng += dlng;
          pts.push([lat / 1E6, lng / 1E6]);
      }
      return pts;
  } else {
      return '';
  }
}

async sendLocation(){
  try {
    var formData = new FormData();
    formData.append("action", "update_live_location");
    formData.append("driver_id", "c6d8fd04-f11e-5017-a4a9-5d96ca6183ea");
    formData.append("driver_booking_id", "114a3b53-b19e-d999-e06d-5e84864d2334");
    formData.append("latlong_coordinate", );
    formData.append("timestamp", );
    console.log("formdata==",formData)
    const response = await fetch(BaseUrl.url5, {
      body: formData,
      method: "POST",
      headers: {
        "Accept": "multipart/form-data",
        "Content-Type": "multipart/form-data"
      }
    });
    const responseJson = await response.json();
    console.log('driverdataride===',responseJson)  
    if(responseJson.success === true){
     console.log('responseJson.success',responseJson)  
     } else {
       console.log('responseJson.false',responseJson)  
    }  
  } catch (error) {
    console.log("error rsp==",error);
  }
}

async checkotherAPI(){
  let groupLatlng = "[[77.21892,28.607336],[77.218963,28.607148],[77.218942,28.60696],[77.218824,28.606677],[77.21876,28.606602],[77.218578,28.606385],[77.218374,28.606263],[77.218235,28.606216],[77.218063,28.606188],[77.217956,28.606197],[77.217945,28.605199],[77.217902,28.60438],[77.217859,28.603674],[77.217827,28.603033],[77.217773,28.602223],[77.217698,28.600933],[77.217805,28.600924],[77.217902,28.600886],[77.217988,28.600829],[77.218074,28.600707],[77.218106,28.600622],[77.218106,28.6005],[77.218085,28.600434],[77.218021,28.60033],[77.217882,28.600226],[77.217753,28.600188],[77.217571,28.600197],[77.217453,28.600244],[77.217389,28.600291],[77.217292,28.600404],[77.216798,28.600197],[77.215897,28.599801],[77.214384,28.599123],[77.212785,28.598407],[77.211326,28.597729],[77.211347,28.597682],[77.211336,28.597588],[77.211239,28.597484],[77.211196,28.597465],[77.211067,28.597465],[77.210992,28.597493],[77.210938,28.59755],[77.209575,28.596947],[77.20935,28.596853],[77.207998,28.59625],[77.20759,28.596071],[77.207526,28.596005],[77.207462,28.595996],[77.207033,28.596109],[77.20684,28.596156],[77.205295,28.596495],[77.204555,28.596561],[77.203546,28.596533],[77.20243,28.59642],[77.201218,28.59609],[77.200596,28.596109],[77.19992,28.596137],[77.199019,28.596175],[77.198826,28.596053],[77.198826,28.595996],[77.198772,28.595855],[77.198675,28.59577],[77.1986,28.595742],[77.198557,28.595723],[77.198428,28.595723],[77.198299,28.595761],[77.198235,28.595808],[77.197055,28.595516],[77.196529,28.59545],[77.196132,28.595431],[77.19566,28.595459],[77.194533,28.595695],[77.193986,28.595865],[77.193911,28.595799],[77.193772,28.595761],[77.193633,28.595789],[77.193515,28.595883],[77.193472,28.596005],[77.193483,28.596062],[77.193032,28.596241],[77.190908,28.597108],[77.190854,28.597042],[77.190768,28.596995],[77.190661,28.596967],[77.190532,28.596976],[77.190457,28.597004],[77.19035,28.597089],[77.190307,28.597155],[77.190286,28.597287],[77.190318,28.597391],[77.189095,28.5979],[77.188655,28.59807],[77.187775,28.598419],[77.187743,28.598362],[77.187646,28.598296],[77.187549,28.598268],[77.18741,28.598287],[77.187313,28.598334],[77.187238,28.598438],[77.187227,28.598532],[77.187259,28.598626],[77.183686,28.600058],[77.181991,28.600727],[77.181809,28.60084],[77.181498,28.601057],[77.181058,28.601528],[77.180897,28.601726],[77.180715,28.601811],[77.180618,28.60183],[77.180403,28.601811],[77.17933,28.601265],[77.175446,28.599212],[77.17521,28.59909],[77.174877,28.598911],[77.173965,28.598431],[77.173718,28.598309],[77.173471,28.598168],[77.172666,28.597754]]";
  try {
    const response = await fetch("https://apis.mapmyindia.com/advancedmaps/v1/" + RestApiKey+"/still_image_polyline?height=500&width=800&polyline="+groupLatlng+"&color=#ff00f7&offset=10,44&padding_x=50&padding_y=100", {
      method: "GET",
      // headers: {
      //   "Accept": "multipart/form-data",
      //   "Content-Type": "multipart/form-data"
      // }
    });

    console.log("url==","https://apis.mapmyindia.com/advancedmaps/v1/" + RestApiKey+"/still_image_polyline?height=500&width=800&polyline="+groupLatlng+"&color=#ff00f7&offset=10,44&padding_x=50&padding_y=100")
    let picurl = "https://apis.mapmyindia.com/advancedmaps/v1/" + RestApiKey+"/still_image_polyline?height=500&width=800&polyline="+groupLatlng+"&color=#ff00f7&offset=10,44&padding_x=50&padding_y=100";
    this.setState({imgUrl:picurl})
    const responseJson = await response.json();
    console.log(JSON.stringify(responseJson))
    if (responseJson.success === true) {
      console.log("responseJson.success==",responseJson)
    } else {
      console.log("responseJson.false==",responseJson)
    }

  } catch (error) {
    console.log("error===",error)
  }
}


 componentWillUnmount = () => {
    Geolocation.clearWatch(this.watchID);
 }
 render() {
    return (
       <View style = {styles.container}>
          <Text style = {[styles.boldText,{marginTop:20}]}>
             Your Location
          </Text>
          <Text style={{textAlign:"center",marginTop:10}}>
            Longitude: {this.state.currentLongitude}
          </Text>
          <Text style={{textAlign:"center",marginTop:10, marginBottom:10}}>
            Latitude: {this.state.currentLatitude}
          </Text>
          <View style={{flexDirection:"row", justifyContent:"center"}}>
          <AppButton
            text='Check Distance'
            bgColor={appColor.Blue}
            width={'70%'}
            textColor={appColor.white} 
            onClickFunc={()=>this.getSnaptoRoad()}  
            //onClickFunc={()=>this.sendLocation()}
            //onClickFunc={()=>this.checkotherAPI()}       
            />
            </View>
          {/* <MapmyIndiaGL.MapView  style={{flex:1, marginTop:20}}  >
          <MapmyIndiaGL.Camera
              ref={c  => (this.camera = c)}
              zoomLevel={12}
              minZoomLevel={4}
              maxZoomLevel={22}
              coordinate={13}
              centerCoordinate={[this.state.currentLongitude,this.state.currentLatitude]}
          />
          </MapmyIndiaGL.MapView> */}
          {
            this.state.imgUrl?
            <View style={{padding:10}}>
              <ScrollView minimumZoomScale={1} maximumZoomScale={10} >
               <Image source={{uri:this.state.imgUrl}} resizeMode="contain" style={{height:500,width:350}}/></ScrollView>
            </View>:<Text>No Image</Text>

          }
          {/* {
            this.state.imgUrl?<View style={{marginLeft:50}}>
              <ImageZoom cropWidth={Dimensions.get('window').width}
                       cropHeight={Dimensions.get('window').height}
                       imageWidth={500}
                       imageHeight={350}>
               <Image source={{uri:this.state.imgUrl}} resizeMode="contain" style={{height:500,width:350}}/>
            </ImageZoom></View>:<Text>No Image</Text>

          } */}
       </View>
    )
 }
}
const styles = StyleSheet.create ({
 container: {
    flex: 1,
 },
 boldText: {
    fontSize: 30,
    color: 'red',
    textAlign:"center"
 }
})