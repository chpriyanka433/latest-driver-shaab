
import React, { Component } from "react";

//import react in our code. 
import {View, Text,  StyleSheet, Image ,PermissionsAndroid,Platform,DeviceEventEmitter,
  NativeAppEventEmitter,
  AsyncStorage,} from 'react-native';
import {Geolocation,RestApiKey,ClientId,ClientSecret,grantType,MapsSdkkey, appColor,AppButton,BaseUrl} from "../screen/common";
import MapmyIndiaGL from 'mapmyindia-map-react-native-beta';
import BackgroundTimer from 'react-native-background-timer';
import Button from "./common/Button";

//import all the components we are going to use.
//import Geolocation from '@react-native-community/geolocation';


var pts1; 

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLongitude: 88.3639,//Initial Longitude
      currentLatitude: 22.5726,//Initial Latitude
      arratValue:"",
      pts1:"",
      finalDistance:"",
      Geometry:"",
      Testgeometry: "ie`iByrp}MN{HKeMXmOKuGa@gIiAqFeCyHmBuDsBmB_CuAuGeBeP{D"
   };
    MapmyIndiaGL.setMapSDKKey(MapsSdkkey);
    MapmyIndiaGL.setRestAPIKey(RestApiKey);
    MapmyIndiaGL.setAtlasClientId(ClientId);
    MapmyIndiaGL.setAtlasClientSecret(ClientSecret);
    MapmyIndiaGL.setAtlasGrantType(grantType);
  }
  
 componentDidMount = (async) => {
  //Checking for the permission just after component loaded
  // let latlng =   AsyncStorage.getItem("finalLatLong")
  // let finaltimestamp =   AsyncStorage.getItem("finaltimestamp")
  // console.log("updated latlng",latlng)
  // console.log("updated timestamp",finaltimestamp)
  // console.log("updated latlng1",JSON.parse(latlng))
  // console.log("updated timestamp1",JSON.parse(finaltimestamp))
  that=this;
  if(Platform.OS === 'ios'){
    this.callLocation(that);
  }else{
    async function requestLocationPermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
            'title': 'Location Access Required',
            'message': 'This App needs to Access your location'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //To Check, If Permission is granted
          that.callLocation(that);
        } else {
          alert("Permission Denied");
        }
      } catch (err) {
        console.warn(err)
      }
    }
    requestLocationPermission();
  }    
 }
 async callLocation(that){
  //alert("callLocation Called");
    Geolocation.getCurrentPosition(
      //Will give you the current location
       (position) => {
          const currentLongitude = JSON.stringify(position.coords.longitude);
          //getting the Longitude from the location json
          const currentLatitude = JSON.stringify(position.coords.latitude);
          //getting the Latitude from the location json
          that.setState({ currentLongitude:currentLongitude });
          //Setting state Longitude to re re-render the Longitude Text
          that.setState({ currentLatitude:currentLatitude });
          //Setting state Latitude to re re-render the Longitude Text
       },
       (error) => alert(error.message),
       //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
    that.watchID = Geolocation.watchPosition((position) => {
      //Will give you the location on location change
        console.log("position change==",position);
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json
       that.setState({ currentLongitude:currentLongitude });
       //Setting state Longitude to re re-render the Longitude Text
       that.setState({ currentLatitude:currentLatitude });
       //Setting state Latitude to re re-render the Longitude Text
    });
 }

 chunkStoredtimestamp=(timestampArray,chunksize)=>{
  var results = [];
    while (timestampArray.length) {
        results.push(timestampArray.splice(0, chunksize));
    }
    return results;
 }

 chunkStoredlatlong(latlongArray, chunksize){
  var results = [];
  while (latlongArray.length) {
      results.push(latlongArray.splice(0, chunksize));
  }
  return results;
}

myfunction(test){
  console.log("test fnnn==",test)
}
 async getSnaptoRoad(){
  // let geo =  this.state.Testgeometry;  
  // console.log("geometry===",geo)
  // console.log("geometry===11",JSON.stringify(geo))

  // Get Geometry
  var latlng = await AsyncStorage.getItem("finalLatLong")
  var timestamp = await AsyncStorage.getItem("finaltimestamp")
  // console.log("array timestamp===",JSON.parse(timestamp.split(",")))
  // console.log("array latlng===",JSON.parse(latlng.split(",")))

  var chunktimeStamp = this.chunkStoredtimestamp(JSON.parse(timestamp.split(",")), 2);
  console.log("chunk timestamp",chunktimeStamp);
  var chunklatlong = this.chunkStoredlatlong(JSON.parse(latlng.split(",")), 2);
  console.log("chunk chunklatlong",chunklatlong);



  // console.log("url==", "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/snapToRoad?timestamps="+JSON.parse(timestamp)+"&pts="+JSON.parse(latlng))

  // try {
  //   const response = await fetch(
  //     "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/snapToRoad?timestamps="+JSON.parse(timestamp)+"&pts="+JSON.parse(latlng),
  //     {
  //       method: "GET",
  //       headers: {
  //         Accept: "application/json",
  //         "Content-Type": "application/json",
  //       }
  //     }
  //   );
  //   const responseJson = await response.json();
  //   if(responseJson.responseCode==200){
  //     console.log("success", responseJson)
  //     this.setState({Geometry:responseJson.results.matchings[0].geometry})
  //     // console.log("geometry===",geometry)
  //     // console.log("geometry===11",JSON.stringify(geometry))
  //     if(geometry!=""||geometry!=null||geometry!=undefined){
  //       var encodedString=JSON.stringify(responseJson.results.matchings[0].geometry);
  //       var dis = this.get_distance(encodedString);
  //       alert(JSON.stringify(dis))
  //     } else {
  //       console.log("no geometry found")
  //     }
  //   } else {
  //     console.log("success ! check", responseJson)
  //     alert(JSON.stringify(responseJson.message))
  //   }
  // } catch(error){
  //     console.log(error)
  // }
  
 }

 async get_distance(encodedString){
          if(!encodedString) return false;
          var count=0;
          var pts1 = this.decode_path(encodedString);
          let ptsValue = await pts1;
          this.setState({pts1:ptsValue})
          // console.log("pts1 value---",ptsValue)
          // console.log("pts1 value---",ptsValue.length)
          //let ptsValue = pts1.then((value)=>{this.setState({pts1:value})})
          if(pts1){
            for(i=0; i<this.state.pts1.length; i++){ 
                if(this.state.pts1[i+1]){
                  var dis=this.distance(this.state.pts1[i][0], this.state.pts1[i][1], this.state.pts1[i+1][0], this.state.pts1[i+1][1], "K");
                  if(dis){
                    let distance = await dis;
                    count=count+parseInt(distance);
                    // console.log("final distance is==",count)
                    // this.setState({finalDistance:count})
                  }
                }

                // console.log("distance meter",dis)
                // console.log("count meter",count)
                // console.log("type of dis",typeof dis)
                // console.log("type of count",typeof count)
                //console.log("total distance==",parseInt(dis)+parseInt(count)) 

            }
        }
          return count;
  }
        


async distance(lat1, lon1, lat2, lon2, unit) {
  if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
  }
  else {
      var radlat1 = Math.PI * lat1/180;
      var radlat2 = Math.PI * lat2/180;
      var theta = lon1-lon2;
      var radtheta = Math.PI * theta/180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
          dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180/Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit=="K") { dist = dist * 1.609344 }
      if (unit=="N") { dist = dist * 0.8684 }
      dist=Math.round(dist*1000)/1000;
      dist=dist*1000;
      console.log("distance==",dist)
      return dist;
  }
}

async decode_path(encoded) {
  if (encoded != 'undefined') {
      var pts = [];
      var index = 0, len = encoded.length;
      var lat = 0, lng = 0;
      while (index < len) {
          var b, shift = 0, result = 0;
          do {
              b = encoded.charAt(index++).charCodeAt(0) - 63;
              result |= (b & 0x1f) << shift;
              shift += 5;
          } while (b >= 0x20);
          var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
          lat += dlat;
          shift = 0;
          result = 0;
          do {
              b = encoded.charAt(index++).charCodeAt(0) - 63;
              result |= (b & 0x1f) << shift;
              shift += 5;
          } while (b >= 0x20);
          var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
          lng += dlng;
          pts.push([lat / 1E6, lng / 1E6]);
      }
      return pts;
  } else {
      return '';
  }
}

async sendLocation(){
  try {
    var formData = new FormData();
    formData.append("action", "update_live_location");
    formData.append("driver_id", "c6d8fd04-f11e-5017-a4a9-5d96ca6183ea");
    formData.append("driver_booking_id", "114a3b53-b19e-d999-e06d-5e84864d2334");
    formData.append("latlong_coordinate", );
    formData.append("timestamp", );
    console.log("formdata==",formData)
    const response = await fetch(BaseUrl.url5, {
      body: formData,
      method: "POST",
      headers: {
        "Accept": "multipart/form-data",
        "Content-Type": "multipart/form-data"
      }
    });
    const responseJson = await response.json();
    console.log('driverdataride===',responseJson)  
    if(responseJson.success === true){
     console.log('responseJson.success',responseJson)  
     } else {
       console.log('responseJson.false',responseJson)  
    }  
  } catch (error) {
    console.log("error rsp==",error);
  }
}

async checkotherAPI(){
  try {
    var formData = new FormData();
    formData.append("action", "get_company_geofencing");
    const response = await fetch(BaseUrl.url2 + "service.php", {
      body: formData,
      method: "POST",
      headers: {
        "Accept": "multipart/form-data",
        "Content-Type": "multipart/form-data"
      }
    });
    const responseJson = await response.json();
    if (responseJson.success === true) {
      console.log("responseJson.success==",responseJson)
    } else {
      console.log("responseJson.false==",responseJson)
    }

  } catch (error) {
    console.log("error===",error)
  }
}


 componentWillUnmount = () => {
    Geolocation.clearWatch(this.watchID);
 }
 render() {
    return (
       <View style = {styles.container}>
          <Text style = {[styles.boldText,{marginTop:20}]}>
             Your Location
          </Text>
          <Text style={{textAlign:"center",marginTop:10}}>
            Longitude: {this.state.currentLongitude}
          </Text>
          <Text style={{textAlign:"center",marginTop:10, marginBottom:10}}>
            Latitude: {this.state.currentLatitude}
          </Text>
          <View style={{flexDirection:"row", justifyContent:"center"}}>
          <AppButton
            text='Check Distance'
            bgColor={appColor.Blue}
            width={'70%'}
            textColor={appColor.white} 
            //onClickFunc={()=>this.getSnaptoRoad()}  
            onClickFunc={()=>this.sendLocation()}
            //onClickFunc={()=>this.checkotherAPI()}       
            />
            </View>
          <MapmyIndiaGL.MapView  style={{flex:1, marginTop:20}}  >
          <MapmyIndiaGL.Camera
              ref={c  => (this.camera = c)}
              zoomLevel={12}
              minZoomLevel={4}
              maxZoomLevel={22}
              coordinate={13}
              centerCoordinate={[this.state.currentLongitude,this.state.currentLatitude]}
          />
          </MapmyIndiaGL.MapView>
       </View>
    )
 }
}
const styles = StyleSheet.create ({
 container: {
    flex: 1,
 },
 boldText: {
    fontSize: 30,
    color: 'red',
    textAlign:"center"
 }
})