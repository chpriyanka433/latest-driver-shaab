import React, { Component } from 'react';
import { Container, Picker, Tabs, ScrollableTab, Body, Left, Right, Button, Form, Item, Input } from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  NetInfo,
  PermissionsAndroid,
  AsyncStorage
} from 'react-native';

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,moment, image,Fonts} from "././common";

import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { ThemeProvider } from 'react-native-paper';
const Months = [
  "Jan",  
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
]

const AttenYear = [
  "2019",
  "2020",
  "2021",
  "2022",
  "2023",
  "2024",
  "2025",
  "2026",
  "2027",
  "2028",
  "2029",
  "2030"
]

export default class AttendanceHistory extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      latitude: "",
      longitude: "",
      checkedin_id: "",
      userName: "",
      chosenDate: new Date(),
      date: "",
      AttendanceYear: "",
      selectYear: "",
      AttendanceMonth: "",
      selectMonth: "",
      isLoading: false,
      AttenKeys: [],
      AttenValues: [],
      refreshing:false,
      currentYear:"",
      currentMonth:"",
      startdate:"",
      Enddate:"",
      EachHours:"",
      currentdate:'',
      Currentweekdate:"",
      dataResponse:"",
      totalhours:""
    }

  }

  async componentDidMount() {
    let newdate = moment(new Date()).startOf('week').format("DD/MM/YYYY)");
    this.setState({currentdate: newdate})
    let  newweekdate= moment(new Date())
    this.setState({Currentweekdate: newweekdate})

    // let Year =moment(new Date()).format("YYYY")
    // let Month =moment(new Date()).format("MM")
    // let newmonth=moment(Month, 'MM').format('MMM');
    // this.setState({currentMonth:newmonth,currentYear:Year})
    this.AttendanceHistory();
  }


  async updateCallback(){
   this.AttendanceHistory();
   this.getstartDate();
   this.getEnddate();
  // this.getMonth();
  // this.getYear();
}  


  async AttendanceHistory() {
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      if (connectionInfo.type === 'none' || connectionInfo.type === 'NONE') {
        ToastAndroid.show("Please Check Your Internet Connection !", ToastAndroid.LONG);
      }
    })
    this.setState({ isLoading: true });
    try {
      // let Year = moment(new Date()).format("YYYY")
      // let Month = moment(new Date()).format("MM")
     let currentdate = moment(new Date()).startOf('week').format("YYYY-MM-DD")
     let Currentweekdate = moment(new Date()).format("YYYY-MM-DD")
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "calculate_checkin_checkout_hours");
      formData.append("startdate",currentdate ?currentdate:Year);
      formData.append("driver_id", driverId);
      formData.append("enddate",Currentweekdate ? Currentweekdate:Month);


      console.log('formdata', formData)
      console.log('url', BaseUrl.url2 + "service.php")

      const response = await fetch(
        BaseUrl.url2 + "service.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data"
          }
        }
      );

      const responseJson = await response.json();
      console.log('data===', responseJson)

      if (responseJson.success === true) {
        //alert(JSON.stringify(responseJson.eachdayhours))
        this.setState({ isLoading: false, data:responseJson,dataResponse:responseJson.success,totalhours:responseJson.totalhours})
        // console.log(JSON.stringify(responseJson.eachdayhours))
        // this.setState({ AttenKeys: Object.keys(obj) })
        // this.setState({ AttenValues: Object.values(obj) })
        // console.log("attenkeys=", this.state.AttenKeys)
        // console.log("attenValues=", this.state.AttenValues)
     } else {
        this.setState({ data: [] })
        this.setState({ isLoading: false,refreshing:false });
        console.log("Check chote");
      }
    } catch (error) {
      this.setState({isLoading:false})
      console.log(error);
    }
  }

     async getstartDate(val){
       let formatdate =await moment(val,'DD/MM/YYYY').format('YYYY-MM-DD');
      //  alert(formatdate)
       this.setState({startdate:formatdate,start_date:val})
       this.AttendanceHistory();
     }

     async getEnddate(val){
      let formatend =await moment(val,'DD/MM/YYYY').format('YYYY-MM-DD');
      // alert(formatend)
      this.setState({Enddate:formatend,End_date:val})
      this.AttendanceHistory();
      
     }
    

  async getMonth(index) {
    let Month = moment(new Date()).format("MM")
    console.log("month==",Month)
    this.setState({ selectMonth: index, AttendanceMonth: Months[index] })
    this.AttendanceHistory();
  }

  async getYear(item) {
    let Year = moment(new Date()).format("YYYY")
    console.log("Year==",Year)
    console.log("Year index====",AttenYear.indexOf('2021'))
    console.log('index', item)
    this.setState({ selectYear: item, AttendanceYear: AttenYear[item]})
    this.AttendanceHistory();
  }

  check(item) {
    console.log('item===', item)
  }
 
   handleRefresh =() => {
     this.setState({
       page:1,
       refreshing:true,
       seed:this.state.seed +1
     },
     () =>{
       this.makeRemoteRequest();
     }
     )
   }
     
  // renderSeparator =() => {
  // return(
  //   <View style={{ height: 2, width: "100%", backgroundColor: appColor.lightGrey,marginBottom:15}}></View>
  // )
  // }
  

  render() {
    return (

      <View style={{ flex: 1 }}>
        <Header
          text={"Attendance History"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
          <Spinner
            visible={this.state.isLoading}
            style={{ color: '#000' }}
            color={appColor.spinnerColor}
            overlayColor="rgba(0, 0, 0, 0.55)"
          />
         
          <View style={{backgroundColor:appColor.lightGrey,height:120,marginBottom:20}}>
            
          <View style={{ flexDirection: "row",justifyContent:"space-between",marginHorizontal:20}}>
            
            <Text style={styles.SelectDatetext}>Select Date</Text>
            {/* <Text style={{ fontFamily:Fonts.Medium, fontSize: 15, color: appColor.Blue, marginTop: 15,marginRight:"37%" }}>Year</Text> */}
            </View>
            <View style={styles.DateTextView}>
                  <Text style={[styles.DateTextstyle]}>Start Date</Text>
                  <Text style={[styles.DateTextstyle,{marginRight:"32%"}]}>End Date</Text>
                  </View>
            <View style={styles.DatePickerView}>
            <DatePicker
                style={{ width: "45%" }}
                date={this.state.start_date ? this.state.start_date:this.state.currentdate} //initial date from state
                mode='date' //The enum of date, datetime and time
                placeholder="Start Date"
                format="DD/MM/YYYY"
                minDate="01/01/2000"
                maxDate={new Date()}
                confirmBtnText="Confirm"
                onDateChange={val => this.getstartDate(val)}
                value={this.state.start_date }
                cancelBtnText="Cancel"
                showIcon={false}
                customStyles={{
                  dateText: {
                    color: appColor.black,
                    marginLeft: 10
                  },
                  placeholderText: {
                    marginLeft: 10,
                    color: appColor.black
                  },
                  dateInput: {
                    //marginLeft: 10,
                    alignItems: 'flex-start',
                    borderColor: appColor.transparent,
                    backgroundColor: appColor.white,
                    borderRadius: 10,
                    height:40,
                    fontFamily:Fonts.Regular
                  }
                }}
              />
              <View style={{ height: 5, width: 5, backgroundColor: "transparent" }} />
              <DatePicker
                style={{ width: "52%" }}
                date={this.state.End_date ? this.state.End_date:this.state.Currentweekdate} //initial date from state
                mode="date" //The enum of date, datetime and time
                placeholder="End Date"
                format="DD/MM/YYYY"
                minDate="01/01/2000"
                maxDate={new Date()}
                confirmBtnText="Confirm"
                onDateChange={(val) => this.getEnddate(val)}
                value={this.state.End_date }
                cancelBtnText="Cancel"
                showIcon={false}
                customStyles={{
                  dateText: {
                    color: appColor.black,
                    marginLeft: 10
                  },
                  placeholderText: {
                    color: appColor.black,
                    marginLeft: 10
                  },
                  dateInput: {
                    marginLeft: 10,
                    alignItems: 'flex-start',
                    borderColor: appColor.transparent,
                    backgroundColor: appColor.white,
                    borderRadius: 10,
                    height:40,
                    fontFamily:Fonts.Regular
                  }
                }}
              />
         </View>
            {/* <View style={{marginBottom:10,flexDirection:'row'}}>
              <Form style={{width:'40%',marginHorizontal:20,height:20}}>
              <Item regular success style={{borderRadius:5,backgroundColor:appColor.red, borderColor:appColor.lightGrey}}>
            <Picker
              mode="note"
              style={{backgroundColor:appColor.white,height:40 }}
              selectedValue={this.state.selectMonth?this.state.selectMonth:this.state.currentMonth}
              onValueChange={(selectMonth) => this.getMonth(selectMonth)}>
              {
                Months.map((item, index) => {
                  return (<Picker.Item label={item} value={item} key={index} />)
                })
              }
              </Picker>
              </Item>
              </Form>
              <Form style={{width:'40%',marginLeft:10}}>
              <Item regular success style={{borderRadius:5,backgroundColor:appColor.lightGrey, borderColor:appColor.lightGrey}}>
              <Picker
              style={{backgroundColor:appColor.white,height:40 }}
              mode="note"
              selectedValue={this.state.selectYear?this.state.selectYear:this.state.currentYear}
              onValueChange={(selectYear) => this.getYear(selectYear)}>
              {
                AttenYear.map((item, index) => {
                  return (<Picker.Item label={item} value={item} key={index} />)
                })
              }
            </Picker>
            </Item>
            </Form>
            </View> */}
            
        </View>
        
      <View style={{ marginHorizontal: 20 }}>
      <View style={{ flexDirection: "row", justifyContent:"space-between", marginBottom: 10 }}>
              <Text style={styles.Date_logtext}>Date</Text>
              <Text style={[styles.Date_logtext,]}>Log Hrs</Text>
            </View>
      </View>
      <View style={{ marginHorizontal: 20 }}>
        <View style={{justifyContent:'space-between',flexDirection:"row"}}>
            <View style={{width:"10%",height:2,backgroundColor:appColor.Blue}}></View>
            <View style={{width:"16%",height:2,backgroundColor:appColor.Blue}}></View>
            </View>
      </View>
        
          {/* <View style={{ marginHorizontal: 20 }}>
            <View style={{ flexDirection: "row", justifyContent:"space-between", marginBottom: 10 }}>
              <Text style={{ color: appColor.Blue, fontFamily: "Roboto Medium", fontSize: 15, fontWeight: "700", }}>Check In</Text>
              <Text style={{ color: appColor.Blue, fontFamily: "Roboto Medium", fontSize: 15, fontWeight: "700",textAlign:"center",marginLeft:15}}>Check Out</Text>
              <Text style={{ color: appColor.Blue, fontFamily: "Roboto Medium", fontSize: 15, fontWeight: "700",textAlign:"center",marginLeft:5}}>Total Hrs</Text>
            </View>
            </View> */}
            
            <ScrollView>
            {this.state.dataResponse==true?<FlatList 
     data={this.state.data}
     keyExtractor={(item, index) => index}
     extraData={this.state.data}
     ItemSeparatorComponent={this.renderSeparator}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
            renderItem={({ item, index }) =><View>
            <View style={{ flexDirection: "row", justifyContent: "space-between",padding:20}}>
          <Text style={{fontSize:15,color:appColor.black,fontWeight:'300'}}>{item.eachdayhours[0].date}</Text>
          <Text style={{fontSize:15,color:appColor.Blue,fontWeight:'300'}}>{item.eachdayhours[0].hrs}</Text>
  </View>
  <View style={{width:"100%",height:1,backgroundColor:appColor.lightGrey}}></View>

  </View>}
  />:<View style={{justifyContent:"center",marginTop:"30%",width:"100%",height:200}}><Image style={{height:100, width:100,marginHorizontal:"38%"}}
  resizeMode={"contain"} source={image.noHistory}/>
  <Text style={{fontSize:18, fontFamily:Fonts.Medium, color:appColor.Blue, marginTop:10,textAlign:"center"}}>No Attendance History Found</Text>
    </View>}
  <View style={{height:50,backgroundColor:appColor.transparent,width:"100%"}}></View>
  </ScrollView>
  <View style={{width:"100%",height:50,backgroundColor:appColor.Blue,position:"absolute",bottom:0}}>
    <View style={{marginHorizontal:20,flexDirection:"row",justifyContent:"space-between"}}>
    <Text style={{fontSize:15,color:appColor.white,marginTop:12,fontFamily:Fonts.Medium}}>Total Log Hours</Text>
    <Text style={{fontSize:15,color:appColor.white,marginTop:12}}>{this.state.totalhours}</Text>
    </View>
    </View>
{/* {this.state.data.length>0?
    <FlatList 
     data={this.state.data}
     keyExtractor={(item, index) => index}
     extraData={this.state.data}
     ItemSeparatorComponent={this.renderSeparator}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
            renderItem={({ item, index }) =>
            
                        <View style={{ flexDirection: "row", justifyContent: "space-between", backgroundColor: index % 2 === 0 ? '#DFE5EB' : '#fff',height:40}}>
                          <Text style={{ color: appColor.black, fontFamily: "Roboto Medium", fontSize: 11,textAlign:"center",top:12,marginLeft:10}}>{item.check_in_time}</Text>
                          <Text style={{ color: appColor.black, fontFamily: "Roboto Medium", fontSize: 11,textAlign:"left",top:12, }}>{item.check_out_time}</Text>
                          <Text style={{ color: appColor.black, fontFamily: "Roboto Medium", fontSize: 11,textAlign:"left",top:12,marginRight:30 }}>{item.totaltime}</Text>
                        </View>
                      
            }
            />:<View style={{justifyContent:"center",marginTop:"30%",width:"100%",height:200}}><Image style={{height:100, width:100,marginHorizontal:"38%"}}
            resizeMode={"contain"} source={image.noHistory}/>
            <Text style={{fontSize:18, fontFamily:"Raleway-Bold", color:appColor.Blue, marginTop:10,textAlign:"center"}}>No Attendance History Found</Text>
            </View>} */}
            
            {/* {this.state.AttenKeys.length>0?
            <FlatList data={this.state.AttenKeys}
            extraData={this.state.AttenKeys}
            ItemSeparatorComponent={this.renderSeparator}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
              renderItem={({ item, index }) =>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: 5 }}>
                  <View >
                    <Text style={{ color: appColor.black, fontFamily: "Roboto Medium", fontSize: 15, }}>{item}</Text>
                  </View>
                  <FlatList data={this.state.AttenValues[index]}
                  extraData={this.state.AttenValues}
                    renderItem={({ item }) =>
                      <View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: 5 }}>
                          <Text style={{ color: appColor.black, fontFamily: "Roboto Medium", fontSize: 14, marginLeft: 70,marginBottom:10 }}>{item.check_in_time}</Text>
                          <Text style={{ color: appColor.black, fontFamily: "Roboto Medium", fontSize: 14, marginLeft: 40 }}>{item.check_out_time}</Text>
                        </View>
                      </View>

                    }
                  />
                </View>
                }
            />:<View style={{justifyContent:"center",alignItems:"center", alignSelf:"center",marginTop:"50%"}}><Image style={{height:100, width:100}} source={NoClaim}/>
            <Text style={{fontSize:18, fontFamily:"Raleway-Bold", color:appColor.Blue, marginTop:10}}>No Attendance History Found</Text>
            </View>
            } */}
  
         
        
      </View>
    );
  }
}



const styles = StyleSheet.create({

  RegisterButton: {
    backgroundColor: appColor.yellow,
    marginTop: 20
  },
  flatliststyle: {
    width: "100%",
    height: 200,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: appColor.lightGrey,
    padding: 10
  },
  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  headerTextView: {
    fontSize: 18,
    fontFamily:Fonts.Medium,
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    marginLeft: 80
  },
  SelectDatetext:{
    color: appColor.Blue, 
     fontFamily:Fonts.Medium,
     fontSize: 15, 
     marginTop:15,
     marginBottom:10
  },
  DatePickerView:{
    flexDirection: 'row', 
    justifyContent: 'flex-start', 
    alignItems: 'flex-start', 
    marginBottom: 20,
    marginHorizontal:20
  },
  DateTextstyle:{
    marginBottom:5, 
    fontFamily:Fonts.Medium
 },
 DateTextView:{
  flexDirection:"row",
  justifyContent:"space-between",
  marginHorizontal:20
  },
Date_logtext:{
  color: appColor.Blue,
   fontFamily:Fonts.Medium,
    fontSize: 15,
}
});