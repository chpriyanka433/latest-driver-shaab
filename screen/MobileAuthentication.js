import React, { Component } from "react";
import { Container, Content, ListItem, Body, Left, Right,Form,Item,Input} from 'native-base';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  KeyboardAvoidingView,
  ToastAndroid,
  TouchableOpacity,
  AsyncStorage,
  Dimensions,
  ImageBackground,
  Linking
} from "react-native";

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase,TextInput,image,OneSignal,Fonts,NetInfo} from "././common";

import { ScrollView } from "react-native-gesture-handler";

export default class MobileAuthentication extends Component {
  constructor(props) {
    super(props);

    this.state = {
      text: "",
      buttonStatus: true,
      clipboardContent: null,
      isLoading: false,
      fcmToken:""
    };
    //OneSignal.init("a4aec9d6-9203-4134-a789-e7c01fe1caf0");
    OneSignal.init("2eed6a53-d25c-49a7-a37c-9fac1bc4ec04");
    OneSignal.inFocusDisplaying(2);
    OneSignal.addEventListener('received', this.onReceived.bind(this));
    OneSignal.addEventListener('opened', this.onOpened.bind(this));
    OneSignal.addEventListener('ids', this.onIds.bind(this));

  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
    this.setState({fcmToken:device.userId})
  }


  static navigationOptions = {
    header: null
  };

  onMobNumTxtInputChanged(text) {
    this.setState({
      text: text.replace(/[^0-9]/g, "")
    });
  }

  async onLogin() {
    this.setState({isLoading: true});
    try {
      await OneSignal.addEventListener('ids', this.onIds.bind(this));
      // let fcmToken = await firebase.messaging().getToken();
      // console.log(fcmToken);
      var formData = new FormData();
      formData.append("action", "login");
      formData.append("phone_number", this.state.text);
      formData.append("device_id", this.state.fcmToken);
      const response = await fetch(
        BaseUrl.url1+"index.php?action=login&phone_number=" +
          this.state.text +
          "&device_id=" +
          this.state.fcmToken
      );
      const responseJson = await response.json();
      console.log("formdata id ===",formData)
      if (responseJson.data) {
        let interval = setInterval(
          () => this.intervalFunction(interval, responseJson),
          3000
        );
      }
    } catch (error) {
      NetInfo.fetch().then(connection => {
        this.setState({isLoading:false})
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          alert(error);
        }
      });
    }
  }

  intervalFunction(interval, responseJson) {
    this.setState({ isLoading: false });
    AsyncStorage.setItem("id",responseJson.data.driver_id);
    AsyncStorage.setItem("mob_num",this.state.text);
    this.props.navigation.replace("otp_auth", {
      mob_num: this.state.text,
      record_status: responseJson.data.type,
      id: responseJson.data.driver_id
    });
    ToastAndroid.show(
      "OTP has been sent to your number",
      //"OTP has been sent to your number" + responseJson.data.otp,
      ToastAndroid.LONG
    );
    clearInterval(interval);
  }

  onFocus() {
    this.setState({
        backgroundColor: 'red',

    })
  }

  appTour(){
    const URL = "https://youtu.be/kiyoDa9bHGQ"
    Linking.openURL(URL).catch((err) => console.error('An error occurred', err));
    //this.props.navigation.navigate("AppTour");
  }

  render() {
    return (
      <View style={styles.container}>
          <Header
           text={"Get Started"}
          />
         <ScrollView>
      <View style={{marginBottom:30}}>
        <Image style={styles.circleView}
                 source={image.driver}
          />
        </View>
      
        <ImageBackground source={image.loginBg} style={styles.imageContainer}>
        <View style={{padding:20,marginTop:30}}>      
                   <Text style={{fontFamily:Fonts.Medium,color:appColor.white,marginTop:10,textAlign
          :'center',fontSize:25,marginBottom:20}}>Please Enter Your Mobile Number So that we can verify</Text>
          <View style={{marginTop:20,marginBottom:30}}>
          <Spinner
              visible={this.state.isLoading}
              style={{color:'#000'}}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            />
          <TextInput
                placeholder="Phone number"
                style={styles.inputView}
                render={props => (
                  <TextInputMask
                   placeholder="Your Phone Number"
                   value={this.state.text}
                    // onFocus={ () => this.onFocus()}
                    style={{marginLeft:10}}
                    keyboardType="number-pad"
                    fontFamily={Fonts.Regular}
                    onChangeText={(formatted, extracted) => {
                      // console.log(formatted); // +1 (123) 456-78-90
                      console.log(extracted); // 1234567890

                      this.onMobNumTxtInputChanged(extracted);
                    }}
                    refInput={ref => {
                      this.input = ref;
                    }}
                    mask={"[0000000000]"}
                  />
                )}
              />
              <Text style={{fontFamily:Fonts.Regular,color:appColor.white,textAlign
  :'center',fontSize:15}}>We will send you one time sms message</Text>
              </View>
              <View style={styles.buttonContainer}>
              <TouchableOpacity
                disabled={this.state.text.length === 10 ? false : true}
                onPress={this.onLogin.bind(this)}
                style={[
                  styles.button,
                  {
                    backgroundColor:
                      this.state.text.length === 10 ? appColor.Blue : "#dedede"
                  }
                ]}
              >
                <Text style={styles.buttonText}>GET OTP</Text>
              </TouchableOpacity>
            </View>
            <View style={{justifyContent:"center", flexDirection:"row"}}>
            <TouchableOpacity
              onPress={()=>this.appTour()}
              style={[
                styles.submitButtonStyle,
                {
                  borderColor:appColor.lightGrey
                }
              ]}
            >
              <Text style={styles.submitTextStyle}>
                App Guide
              </Text>
            </TouchableOpacity>
            </View>
        </View>
       
        </ImageBackground>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  description: {
    fontSize: 16,
    color: "#fff",
    textAlign: "center",
    fontWeight: "bold"
  },
  header: {
    paddingTop: 20,
    padding: 15,
    backgroundColor: appColor.Blue
  },
  spinnerStyle: {
    color: "#000000"
  },
  boldText: {
    textAlign: "center",
    fontSize: 17,
    color: "#696969",
    marginTop: 50,
    marginLeft: 18,
    marginRight: 18
  },
  buttonText: {
    color: "#fff",
    fontFamily:Fonts.Medium,
    fontSize: 16,
  },
  button: {
    marginTop: 10,
    width:"100%",
    flexDirection:"row",
    height: 50,
    borderRadius: 50/2,
    justifyContent: "center",
    alignItems: "center"

  },
  sectionStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appColor.white,
    height: 60,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 40 
  },
  loading: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: "black",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonContainer: {
    marginTop: 10,
    width:"100%",
    flexDirection:"row",
    height: 50,
    borderRadius:100,
    justifyContent: "center",
    alignItems: "center"

  },
  textInput: {
    flex: 1,
    fontSize: 22,
    marginRight: 40,
    paddingLeft: 10,
    paddingBottom: 15
  },
  circleView:{
    width:100,
    height:100,
    borderRadius:50,
    // backgroundColor:appColor.DarkGrey,
    marginHorizontal:"35%",
    marginTop:30
    // marginRight:20
 },
 imageContainer: {
  width: Dimensions.get("window").width,
  height: Dimensions.get("window").height,
  
},
inputStyle: {
  color: appColor.white, 
  marginLeft: 5,
  fontFamily:'Roboto Medium',
  backgroundColor:appColor.white,
  width:"100%",
  height:50
},
inputView: {
  borderTopRightRadius:10,
  borderTopLeftRadius:10,
  borderRadius:10,
  backgroundColor:appColor.white,
   borderColor:appColor.white,
   marginBottom:10
},
submitButtonStyle: {
  justifyContent:"center",
  width: "60%",
  marginTop: 60,
  height:50,
  borderRadius: 30,
  borderWidth: 1
},
submitTextStyle: {
  textAlign: "center",
  fontSize: 16,
  fontFamily:Fonts.Medium,
  color:appColor.white
},
});
