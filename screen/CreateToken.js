import React, { Component } from 'react';
import { Form, Item, Input, Picker} from 'native-base';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  PermissionsAndroid,
  AsyncStorage, ToastAndroid,
  Platform
} from 'react-native';

import {appColor,Header,InputText,AppButton,Spinner,ImagePicker,DrawerActions,NavigationEvents,DatePicker,moment, image,Fonts,BaseUrl,Geolocation,RestApiKey,ClientId,ClientSecret,grantType} from "././common";

import { ScrollView } from 'react-navigation';

export default class CreateToken extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedToken:"",
      getTokenList:[],
      test:""
    }
  }

  async componentDidMount() {
   this.getTokenOptions();
   if (Platform.OS === 'android') {
    const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
    if (!granted) {
      return;
    } else {
      await Geolocation.getCurrentPosition(
        (position) => {
          console.log("position",position.coords.latitude)
          this.updateGeoAddress(position.coords.latitude,position.coords.longitude)
        });
    }
  } else {
    console.log("ios platform")
  }
  }

  async updateCallback(){

  }

  async getTokenOptions(){
    try {
        var formData = new FormData();
        formData.append("action", "b2b_ride_token_options");
        const response = await fetch(BaseUrl.url4, {
            body: formData,
            method: "POST",
            headers: {
            "Accept": "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
            }
        });
        const responseJson = await response.json();
        if(responseJson.success===true){
            this.setState({ isLoading: false, getTokenList:responseJson.data.ride_issues_list}); 
        } else {
            //ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
            this.setState({ isLoading: false});
        }        
        } catch (error) {
         this.setState({ isLoading: false });
        console.log(error);
        }
  }

  async updateGeoAddress(lat,long){
    const tokenType = await AsyncStorage.getItem("tokenType");
    const token = await AsyncStorage.getItem("token")
    console.log("url==", "http://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long)

    try {
      const response = await fetch(
        "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      if(responseJson.error==="invalid_token"){
          this.updateToken().then(()=>{
            return this.updateGeoAddress(lat,long)
          })
      } else {
        console.log("responseJson===",responseJson.results[0].formatted_address)
        this.setState({test:responseJson.results[0].formatted_address})
      }
    } catch(error){
        console.log(error)
    }
  }

  async updateToken(){
    try {
      var formData = new FormData();
      formData.append("grant_type", grantType);
      formData.append("client_id", ClientId);
      formData.append("client_secret",ClientSecret);
      const response = await fetch(
        "https://outpost.mapmyindia.com/api/security/oauth/token",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      await AsyncStorage.setItem("token",JSON.stringify(responseJson.access_token))
      await AsyncStorage.setItem("tokenType",JSON.stringify(responseJson.token_type))
    } catch(error){
        console.log(error)
    }
  }


  async onSubmit(){
    if(this.state.selectedToken==null||this.state.selectedToken=="Select"||this.state.selectedToken==undefined||this.state.selectedToken==""){
      ToastAndroid.show("Please select token type.",ToastAndroid.LONG)
    } else {
       this.setState({isLoading:true})
        try {
        const driverId = await AsyncStorage.getItem('driverId');
        const ride_history_id = await AsyncStorage.getItem("ride_history_id");
        var formData = new FormData();
        formData.append("action", "b2b_ride_token_save");
        formData.append("driver_id", driverId);
        formData.append("ride_issues",this.state.selectedToken);
        formData.append("booking_id", ride_history_id);
        formData.append("location", this.state.test);
        console.log("create token formdata==",formData)
        const response = await fetch(BaseUrl.url4, {
            body: formData,
            method: "POST",
            headers: {
            "Accept": "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
            }
        });
        const responseJson = await response.json();
        if(responseJson.success===true){
            this.setState({ isLoading: false});
            ToastAndroid.show("Issue Ticket is created successfully.",ToastAndroid.LONG);
            this.CheckTrip();
        } else {
            this.setState({ isLoading: false});
            ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
        }        
        } catch (error) {
          this.setState({ isLoading: false });
          console.log(error);
        }
    }
  }

  async CheckTrip(){
    let currentStatus = await AsyncStorage.getItem("ride_history_id")
    let startTipFlag = await AsyncStorage.getItem("startTipFlag")
    let initiateFlag = await AsyncStorage.getItem("initiateFlag")
    let fuelCheck = await AsyncStorage.getItem("isFuel")
    console.log("check",currentStatus)
    if(currentStatus==null && initiateFlag == null){
      this.props.navigation.navigate("Initiate")
    }
    else if(currentStatus!=null && initiateFlag == 'flaged' && startTipFlag == null){
      this.props.navigation.navigate("StartTrip")
    }
    else if(currentStatus!=null && startTipFlag == 'flaged' && initiateFlag == 'flaged'){
        if(fuelCheck=="done"){
          this.props.navigation.navigate("EndTrip")
        } else {
          this.props.navigation.navigate("AddFuel")
        }
    }
  }

  async getTokenType(item,index){
    this.setState({selectedToken:item})
  }



  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header
          text={"Create Issue Ticket"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
          <Spinner
            visible={this.state.isLoading}
            style={{ color: '#000' }}
            color={appColor.spinnerColor}
            overlayColor="rgba(0, 0, 0, 0.55)"
          /> 
        <View style={styles.ContainerView}>
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:15}}>Select Ticket Type<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
          <Form>
                <Item regular success style={{ borderRadius: 5,backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey,height:50 }}>
          <Picker
              mode="note"
              placeholder="Ticket Type"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              selectedValue={this.state.selectedToken}
              onValueChange={(item, index) => this.getTokenType(item, index)}>
                <Picker.Item label="Select" value="Select" />
              {
               this.state.getTokenList.map((item, index) => {
                  return (<Picker.Item label={item.label} value={item.value} key={index} />)
                })
              }
              </Picker>
              </Item>
               </Form>
        </View>
        </View> 
        <View style={styles.buttonContainer}>
            <TouchableOpacity
                onPress={()=>this.onSubmit()}
                style={[styles.verifyButton]}>
                <Text style={styles.buttonText}>Submit</Text>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}



const styles = StyleSheet.create({
    buttonContainer: {
        width: "100%",
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        height: 40,
        justifyContent:"center"
      },
      verifyButton: {
        position: "absolute",
        height: 50,
        bottom: 10,
        left: 10,
        right: 10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 10,
        backgroundColor:appColor.Blue
      },
      ContainerView:{
          flex:1,
          padding:20
      },
      buttonText: {
        color: "#fff",
        fontSize: Fonts.h18,
        fontFamily:Fonts.Medium
      },
});