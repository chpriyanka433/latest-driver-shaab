import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right,Form,Item,Input,ActionSheet} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  NetInfo,
  AsyncStorage,
  PermissionsAndroid,
  ToastAndroid,
  Dimensions
} from 'react-native';

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,moment,image,Geolocation,Fonts} from "././common";

import { ScrollView, } from 'react-native-gesture-handler';
const googleMaps_apiKey = "AIzaSyComH3uVSYHWeEtzyEByzgi76SOseHvSG0";
var BUTTONS = ["Camera", "Cancel"];
var CANCEL_INDEX = 1;
export default class StartTrip extends Component {
  constructor(props){
    super(props);
    // this.image_1 = this.image_1.bind(this);
    // this.image_2 = this.image_2.bind(this);
    // this.image_3 = this.image_3.bind(this);
    // this.image_4 = this.image_4.bind(this);
    // this.image_5 = this.image_5.bind(this);
   
    this.state ={
     isLoading:false,
      fileSource:"",
      imageName:'',
      path:"",
      imageType:"",
      imageArray:[],
      ride_history_id:"",
      image1:null,
      image2:null,
      image3:null,
      image4:null,
      image5:null,
      name:"",
      MobileNo:"",
      lat:"",
      long:"",
     
    }
  }

 async componentDidMount(){
    this.RideSessionDetails();
    this.CheckTrip();
    // let lat = await this.props.navigation.getParam("latitude")
    // let long = await this.props.navigation.getParam("longitude")
    // await Geolocation.getCurrentPosition(
    //   (position) => {
    //     console.log("position",position.coords.latitude)
    //     this._geoCodeToAddress(position.coords.latitude,position.coords.longitude);
    //   });
    
   
  }

  async updateCallback(){
       this.setState({name:null})
       this.setState({MobileNo:null})
}

  async RideSessionDetails() {
    this.setState({isLoading:true})
     try {
       const driverId = await AsyncStorage.getItem('driverId');
       var formData = new FormData();
       formData.append("action", "get_running_ride");
       formData.append("driver_id", driverId);
       const response = await fetch(BaseUrl.url2+"service.php", {
         body: formData,
         method: "POST",
         headers: {
           "Accept": "multipart/form-data",
           "Content-Type": "multipart/form-data",
           'Cache-Control': 'no-cache, no-store, must-revalidate',
           'Pragma': 'no-cache',
           'Expires': 0
         }
       });
       const responseJson = await response.json();
       //console.log('driverdataride===',responseJson)
       if(responseJson.data.ride_history_id){
         this.setState({ isLoading: false });
         AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id)
         if(responseJson.data.status == 'initiated'){
            AsyncStorage.setItem("initiateFlag",'flaged')
            this.props.navigation.navigate("StartTrip")
         }
         else if(responseJson.data.status == 'ride_started'){
            AsyncStorage.setItem("initiateFlag",'flaged')
            AsyncStorage.setItem("startTipFlag",'flaged')
            this.props.navigation.navigate("EndTrip")
         }
       }
       else{
         this.setState({ isLoading: false });
         AsyncStorage.setItem("ride_history_id","")
         AsyncStorage.setItem("startTipFlag","")
         AsyncStorage.setItem("initiateFlag","")
         this.props.navigation.navigate("StartTrip")
       }
            
     } catch (error) {
       this.setState({ isLoading: false });
       console.log(error);
     }
   }

  async CheckTrip(){
    let currentStatus = await AsyncStorage.getItem("ride_history_id")
    let startTipFlag = await AsyncStorage.getItem("startTipFlag")
    let initiateFlag = await AsyncStorage.getItem("initiateFlag")
    console.log("check",currentStatus)
    if(currentStatus==null && initiateFlag == null){
      this.props.navigation.navigate("Initiate")
    }
    else if(currentStatus!=null && initiateFlag == 'flaged' && startTipFlag == null){
      this.props.navigation.navigate("StartTrip")
    }
    else if(currentStatus!=null && startTipFlag == 'flaged' && initiateFlag == 'flaged'){
      this.props.navigation.navigate("EndTrip")
    }
  }



  async InitiateFlag() {
    
   try {
    let currentStatus = await AsyncStorage.getItem("checkedin_id")
    console.log("check",currentStatus)
    if(currentStatus==null || currentStatus==undefined){
      ToastAndroid.show("You haven't check-in, Please check-in first.",ToastAndroid.LONG);
    } else if (this.state.name==""||this.state.name==undefined){
      ToastAndroid.show("Please enter customer name.",ToastAndroid.LONG);
    } else if(this.state.MobileNo==""||this.state.MobileNo==undefined){
      ToastAndroid.show("Please enter customer mobile number",ToastAndroid.LONG);
    } else{  
           this.setState({isLoading:true});
            let time= moment(new Date()).format("YYYY-MM-DD HH:mm")
            console.log("Date",time)
            var driverId = await AsyncStorage.getItem("driverId");
            var formData = new FormData();
            formData.append("action", "b2b_rideStart_rideEnd");
            formData.append("type", "initiate_trip");
            formData.append("driver_id", driverId);
            formData.append("trip_initiate_time",time)
            formData.append("name",this.state.name);
            formData.append("customer_number",this.state.MobileNo);
           
            console.log('formdata',formData)
            console.log('url',BaseUrl.url2+"service.php")
        
            const response = await fetch(
              BaseUrl.url2+"service.php",
              {
                body: formData,
                method: "POST",
                headers: {
                  Accept: "multipart/form-data",
                  "Content-Type": "multipart/form-data",
                  'Cache-Control': 'no-cache, no-store, must-revalidate',
                  'Pragma': 'no-cache',
                  'Expires': 0
                }
              }
            );
        
            const responseJson = await response.json();
            console.log('data===',responseJson)
            if (responseJson.success === true) {
              this.setState({isLoading:false});
             AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id)
             console.log("check",responseJson.data.ride_history_id)
             AsyncStorage.setItem("Tripflag", 'true');
             AsyncStorage.setItem("initiateFlag", 'flaged');
             ToastAndroid.show("Your trip has been initiated successfully.",ToastAndroid.LONG);
             this.props.navigation.navigate("StartTrip")
             
            } else {
              this.setState({isLoading:false});
              ToastAndroid.show("Please fill all mandatory fields.",ToastAndroid.LONG);
          }
     }
    
   } catch (error) {
     this.setState({isLoading:false});
     NetInfo.getConnectionInfo().then((connectionInfo) => {
      if(connectionInfo.type==='none' || connectionInfo.type ==='NONE'){
        ToastAndroid.show("Please Check Your Internet Connection !",ToastAndroid.LONG);
      } else {
        console.log(error);
      }
    });
    }
  }
  

  onMobNumTxtInputChanged(text) {
    this.setState({
      MobileNo: text.replace(/[^0-9]/g, "")
    });
  }
  onNameChange(text){
    this.setState({
 name:text.replace(/[^a-zA-Z ]/g,"")
})
}
  
 
  render() {
    return (
     
      <View style={{flex:1}}>
        <Header
          text={"Start Trip"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.dispatch(DrawerActions.openDrawer())}
          />
        <ScrollView>
        <ImageBackground  style={{width:"100%",height:100,marginBottom:20,backgroundColor:appColor.Blue,borderBottomRightRadius:30,borderBottomLeftRadius:30}}>
           <Text style={{fontFamily:Fonts.Medium,color:appColor.white,marginTop:50,textAlign
          :'center',fontSize:15}}>Initiate Trip</Text>
        </ImageBackground>
        {/* <Image style={styles.circleView}
                 source={ProfileIcon}
          /> */}
        <View style={{padding:20}}>  
        <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
        <Spinner
              visible={this.state.isLoading}
              style={{color:'#000'}}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            /> 
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Customer Name<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
        <Form>
                <Item regular success style={{ borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey }}>
                  <Input 
                  maxLength={50}
                  keyboardType='email-address' 
                  placeholder="Name" 
                  style={{ color: appColor.white, fontSize: 16, color: appColor.black }} 
                  onChangeText={text=>this.onNameChange(text)}
                  value={this.state.name}
                  />
                  
                </Item>
               </Form>
        </View>
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Customer Phone Number<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
        <Form>
                <Item regular success style={{ borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey }}>
                  <Input 
                  maxLength={10}
                  keyboardType='phone-pad' 
                  placeholder="Phone Number" 
                  style={{ color: appColor.white, fontSize: 16, color: appColor.black }} 
                  onChangeText={text=>this.onMobNumTxtInputChanged(text)}
                  value={this.state.MobileNo}
                  />
                  
                </Item>
               </Form>
        </View>
       
       
 
      {/* <View style={{justifyContent:'center',marginBottom:20,alignItems:'center', marginTop:60}}>
            <AppButton
            text='Initiate Trip'
            bgColor={appColor.Blue}
            width={'100%'}
            textColor={appColor.white} 
            onClickFunc={()=>this.InitiateFlag()}       
            />
        </View> */}
        </View>
       </ScrollView>
       <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={()=>this.InitiateFlag()}
                style={[styles.verifyButton]}>
                <Text style={styles.buttonText}>Initiate Trip</Text>
              </TouchableOpacity>
            </View>
       {/* <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={()=>this.InitiateFlag()}
                style={[
                  styles.verifyButton
                ]}
              >
                <Text style={styles.buttonText}>Initiate Trip</Text>
              </TouchableOpacity>
            </View> */}
       </View>
    );
  }
}

const styles = StyleSheet.create({
   
headerView: {
  justifyContent: "center",
  alignContent: "center",
  alignItems: "center",
},
headerTextView: {
  fontSize: 18,
  fontFamily:Fonts.Medium,
  textAlign: "center",
  alignItems: "center",
  color: "#fff",
  alignContent: "center",
  alignSelf: "center",
  marginLeft:Dimensions.get("window").width/4
},
buttonContainer: {
  width: "100%",
  position: "absolute",
  bottom: 0,
  left: 0,
  right: 0,
  height: 40
},
verifyButton: {
  position: "absolute",
  height: 50,
  bottom: 10,
  left: 10,
  right: 10,
  alignItems: "center",
  justifyContent: "center",
  borderRadius: 10,
  backgroundColor:appColor.Blue
},
buttonText: {
  color: "#fff",
  fontSize: Fonts.h16,
  fontFamily:Fonts.Medium
},

});

