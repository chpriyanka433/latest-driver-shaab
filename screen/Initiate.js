import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right,ActionSheet,Form,Item,Input,Picker} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  PermissionsAndroid,
  ToastAndroid,
  Dimensions,
  Platform
} from 'react-native';

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,moment,image,Geolocation,Fonts,NetInfo} from "././common";
import { ScrollView, } from 'react-native-gesture-handler';
import BackgroundTimer from 'react-native-background-timer';

const googleMaps_apiKey = "AIzaSyComH3uVSYHWeEtzyEByzgi76SOseHvSG0";
var BUTTONS = ["Camera", "Cancel"];
var CANCEL_INDEX = 1;
const Months = [
  "Jan",  
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
]

export default class StartTrip extends Component {
  constructor(props){
    super(props);
    // this.image_1 = this.image_1.bind(this);
    // this.image_2 = this.image_2.bind(this);
    // this.image_3 = this.image_3.bind(this);
    // this.image_4 = this.image_4.bind(this);
    // this.image_5 = this.image_5.bind(this);
   
    this.state ={
      isLoading:false,
      fileSource:"",
      imageName:'',
      path:"",
      imageType:"",
      imageArray:[],
      ride_history_id:"",
      image1:null,
      image2:null,
      image3:null,
      image4:null,
      image5:null,
      name:"",
      MobileNo:"",
      lat:"",
      long:"",
      selectedCompany:"",
      selectedVehical:"",
      selectedTriptype:"",
      company_name:[],
      Trip_type:[],
      vehicleType:[],
      companyflag:false,
      Companyname:"",
      company_id:"",
      otherCompany:"",
      manual_ride:""
    }
  }

 async componentDidMount(){
    this.InitiateTripFunction()
    // .then(()=>{
    //   return this.RideSessionDetails();
    // })
    //this.CheckTrip()
    this._fetchUserDetails()

    // .then(()=>{
    //   return this.updateLocation();
    // })
    
    // let lat = await this.props.navigation.getParam("latitude")
    // let long = await this.props.navigation.getParam("longitude")
    // await Geolocation.getCurrentPosition(
    //   (position) => {
    //     console.log("position",position.coords.latitude)
    //     this._geoCodeToAddress(position.coords.latitude,position.coords.longitude);
    //   });
    
   
  }

  async _fetchUserDetails() {
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "get_driver_info");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url2+"service.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      if (responseJson.success === true)
        this.setState({
          manual_ride: responseJson.data.manual_ride,
        })
    } catch (error) {
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          console.log(error);
        }
      });  
    }
  }

  async updateLocation(){
    longArray=[]; 
    timeStampArray=[];
    let rideStatus = await AsyncStorage.getItem("initiateFlag");
    if(rideStatus=="flaged"){
      BackgroundTimer.runBackgroundTimer(() => { 
       Geolocation.getCurrentPosition(
           (position) => {
              const currentLongitude = JSON.stringify(position.coords.longitude);
              const currentLatitude = JSON.stringify(position.coords.latitude);
              const timeStamp = JSON.stringify(position.timestamp);
              longArray.push(currentLongitude.concat(",").concat(currentLatitude))
              timeStampArray.push(timeStamp)
              //console.log("longArray",longArray);
              //console.log("currentLongitude",currentLongitude);
              //console.log("currentLatitude",currentLatitude)
              //console.log("timestamp",timeStamp)
             // console.log("concat ==", currentLongitude.concat(",").concat(currentLatitude))
              var resultLatlng = {};
                for (var i = 0; i < longArray.length; ++i){
                  resultLatlng["position" + (i+1)] = longArray[i];                                                                                                                          
              }
              var resultTimestamp = {};
                for (var i = 0; i < timeStampArray.length; ++i){
                  resultTimestamp["position" + (i+1)] = timeStampArray[i];                                                
              }
              //console.log("resultLatlng==",resultLatlng)
              const finalLatLong = Object.keys(resultLatlng).map(function(k){return resultLatlng[k]}).join(";");
              const finaltimestamp = Object.keys(resultTimestamp).map(function(k){return parseInt(resultTimestamp[k]/1000)}).join(";");
              AsyncStorage.setItem("finalLatLong",JSON.stringify(finalLatLong))
              AsyncStorage.setItem("finaltimestamp",JSON.stringify(finaltimestamp))
              //console.log("finaltimestamp==",finaltimestamp)
              //console.log("finalLatLong==",finalLatLong)
              this.sendLocation(finalLatLong,finaltimestamp)
           },
           (error) => console.log("error.message",error.message),
           //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
      }, 20000);
    } else {
      BackgroundTimer.stopBackgroundTimer();
    }
  }

  async sendLocation(finalLatLong,finaltimestamp){
    const driverId = await AsyncStorage.getItem('driverId');
    const ride_history_id = await AsyncStorage.getItem("ride_history_id");
    try {
      var formData = new FormData();
      formData.append("action", "update_live_location");
      formData.append("driver_id",driverId);
      formData.append("driver_booking_id", "");
      formData.append("b2b_ride_history_id",ride_history_id);
      formData.append("latlong_coordinate",finalLatLong);
      formData.append("timestamp", finaltimestamp);
      //console.log("formdata request for location==",formData)
      const response = await fetch(BaseUrl.url5, {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      console.log('driverdataride===',responseJson)  
      if(responseJson.success===true){
       console.log('responseJson.success',responseJson)  
       } else {
         console.log('responseJson.false',responseJson)  
      }  
    } catch (error) {
      console.log("error rsp==",error);
    }
  }

  async updateCallback(){
      await AsyncStorage.removeItem("request_type");
       this.setState({selectedCompany:"",selectedTriptype:"",selectedVehical:"",companyflag:false,otherCompany:"",company_name:[],Trip_type:[],vehicleType:[]})
       this._fetchUserDetails().then(()=>{
         return this.InitiateTripFunction();
       })
}

  async RideSessionDetails() {
    this.setState({isLoading:true})
     try {
       const driverId = await AsyncStorage.getItem('driverId');
       var formData = new FormData();
       formData.append("action", "get_running_ride");
       formData.append("driver_id", driverId);
       const response = await fetch(BaseUrl.url2+"service.php", {
         body: formData,
         method: "POST",
         headers: {
           "Accept": "multipart/form-data",
           "Content-Type": "multipart/form-data",
           'Cache-Control': 'no-cache, no-store, must-revalidate',
           'Pragma': 'no-cache',
           'Expires': 0
         }
       });
       const responseJson = await response.json();
       console.log('driverdataride===',responseJson)
       if(responseJson.data.ride_history_id){
         this.setState({ isLoading: false });
         AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id)
         if(responseJson.data.status == 'initiated'){
            AsyncStorage.setItem("initiateFlag",'flaged')
            this.props.navigation.navigate("StartTrip")
         } else if(responseJson.data.status == 'ride_started'){
            AsyncStorage.setItem("initiateFlag",'flaged')
            AsyncStorage.setItem("startTipFlag",'flaged')
            let fuelCheck = await AsyncStorage.getItem("isFuel")
            if(fuelCheck=="done"){
              this.props.navigation.navigate("EndTrip")
            } else {
              this.props.navigation.navigate("AddFuel")
            }
         }
       }
       else {
         this.setState({ isLoading: false });
         AsyncStorage.setItem("ride_history_id","")
         AsyncStorage.setItem("startTipFlag","")
         AsyncStorage.setItem("initiateFlag","")
         this.props.navigation.navigate("StartTrip")
       }
            
     } catch (error) {
       this.setState({ isLoading: false });
       console.log(error);
     }
   }

  async CheckTrip(){
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (!granted) {
        this.props.navigation.navigate("GpsScreen");
      } else {
        let currentStatus = await AsyncStorage.getItem("ride_history_id")
        let startTipFlag = await AsyncStorage.getItem("startTipFlag")
        let initiateFlag = await AsyncStorage.getItem("initiateFlag")
        let fuelCheck = await AsyncStorage.getItem("isFuel")
        console.log("check",currentStatus)
        if(currentStatus==null && initiateFlag == null){
          this.props.navigation.navigate("Initiate")
        }
        else if(currentStatus!=null && initiateFlag == 'flaged' && startTipFlag == null){
          this.props.navigation.navigate("StartTrip")
        }
        else if(currentStatus!=null && startTipFlag == 'flaged' && initiateFlag == 'flaged'){
            if(fuelCheck=="done"){
              this.props.navigation.navigate("EndTrip")
            } else {
              this.props.navigation.navigate("AddFuel")
            }
        }
      }
    }
  }


  async InitiateTripFunction() {
    this.setState({isLoading:true})
     try {
      //  const driverId = await AsyncStorage.getItem('driverId');
       var formData = new FormData();
       formData.append("action", "get_details_for_initiate_ride");
      //  formData.append("driver_id", driverId);
       const response = await fetch(BaseUrl.url4, {
         body: formData,
         method: "POST",
         headers: {
           "Accept": "multipart/form-data",
           "Content-Type": "multipart/form-data",
           'Cache-Control': 'no-cache, no-store, must-revalidate',
           'Pragma': 'no-cache',
           'Expires': 0
         }
       });
       const responseJson = await response.json();
      console.log('driverdataride===',responseJson)
      //alert(JSON.stringify(responseJson.data.company_list))
      
       if(responseJson.success===true){
         this.setState({ isLoading: false,company_name:responseJson.data.company_list,Trip_type:responseJson.data.trip_type_list,vehicleType:responseJson.data.vehicle_type}); 
          }
       else{
         this.setState({ isLoading: false });
       }
            
     } catch (error) {
       this.setState({ isLoading: false });
       console.log(error);
     }
   }
  

  async InitiateTripSubmit() {
   try {
    let currentStatus = await AsyncStorage.getItem("checkedin_id")
    console.log("check",currentStatus)
    if(currentStatus==null || currentStatus==undefined){
      ToastAndroid.show("You haven't check-in, Please check-in first.",ToastAndroid.LONG);
    } else if(this.state.selectedCompany==""||this.state.selectedCompany==undefined||this.state.selectedCompany=="Select"){
      ToastAndroid.show("Please select company name",ToastAndroid.LONG)
    } else if((this.state.otherCompany==""||this.state.otherCompany==undefined)&&(this.state.company_id==="Other")){
      ToastAndroid.show("Please enter company name",ToastAndroid.LONG)
    }  else if(this.state.selectedTriptype==""||this.state.selectedTriptype==undefined||this.state.selectedTriptype=="Select"){
      ToastAndroid.show("Please select trip type",ToastAndroid.LONG)
    } else if(this.state.selectedVehical==""||this.state.selectedVehical==undefined||this.state.selectedVehical=="Select"){
      ToastAndroid.show("Please select vehicle type",ToastAndroid.LONG)
    } else {  
            this.setState({isLoading:true});
            let time= moment(new Date()).format("YYYY-MM-DD HH:mm")
            console.log("Date",time)
            var driverId = await AsyncStorage.getItem("driverId");
            var formData = new FormData();
            formData.append("action", "b2b_rideStart_rideEnd");
            formData.append("type", "initiate_trip");
            formData.append("driver_id", driverId);
            formData.append("company_id", this.state.company_id);
            formData.append("trip_type", this.state.selectedTriptype);
            formData.append("vehicle_type", this.state.selectedVehical);
            formData.append("company_name", this.state.otherCompany);
            formData.append("trip_initiate_time",time)
            console.log('url',BaseUrl.url2+"service.php")
        
            const response = await fetch(
              BaseUrl.url2+"service.php",
              {
                body: formData,
                method: "POST",
                headers: {
                  Accept: "multipart/form-data",
                  "Content-Type": "multipart/form-data",
                  'Cache-Control': 'no-cache, no-store, must-revalidate',
                  'Pragma': 'no-cache',
                  'Expires': 0
                }
              }
            );
        
            const responseJson = await response.json();
            if (responseJson.success === true) {
              this.setState({isLoading:false});
            AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id)
             AsyncStorage.setItem("Tripflag", 'true');
             AsyncStorage.setItem("initiateFlag", 'flaged');
             ToastAndroid.show("Your trip has been initiated successfully.",ToastAndroid.LONG);
             this.updateLocation();
             this.props.navigation.navigate("StartTrip",{manual_ride:this.state.manual_ride})
            } else {
              this.setState({isLoading:false});
              ToastAndroid.show("Please fill all mandatory fields.",ToastAndroid.LONG);
              console.log("api res")
            }
          }
    
   } catch (error) {
    this.setState({isLoading:false});
    NetInfo.fetch().then(connection => {
      if(!connection.isConnected){
        Toast.show("Please Check Your Internet Connection !",Toast.LONG);
      } else {
      console.log(error);
      }
    });
    }
  }

  async getcompany(item,index){
    console.log("accepted company id",item)
    if(item.label==="Other"){
      this.setState({companyflag:true,selectedCompany:item,company_id:item.value,otherCompany:""})
    } else{
      this.setState({companyflag:false,selectedCompany:item,company_id:item.value})
    }
  }

  async getTrip_type(item,index){
    this.setState({selectedTriptype:item})
  }

  async getVehicalType(item,index){
    this.setState({selectedVehical:item})
  }
  

  onCompanyName(text) {
      this.setState({
        otherCompany: text.replace(/[^a-zA-Z ]/g,""),
      });
    }

  onNameChange(text){
    this.setState({
    name:text.replace(/[^a-zA-Z ]/g,"")
})
}
  
 
  render() {
    return (
     
      <View style={{flex:1}}>
        <Header
          text={"Start Trip"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <ScrollView>
        <ImageBackground  style={{width:"100%",height:100,marginBottom:20,backgroundColor:appColor.Blue,borderBottomRightRadius:30,borderBottomLeftRadius:30}}>
           <Text style={{fontFamily:Fonts.Medium,color:appColor.white,marginTop:50,textAlign
          :'center',fontSize:16}}>Initiate Trip</Text>
        </ImageBackground>
        {/* <Image style={styles.circleView}
                 source={ProfileIcon}
          /> */}
        <View style={{padding:20}}>  
        <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
        <Spinner
              visible={this.state.isLoading}
              style={{color:'#000'}}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            /> 
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Company Name<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
          <Form>
                <Item regular success style={{ borderRadius: 5,backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey,height:50 }}>
          <Picker
              mode="note"
              placeholder="Select Company"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              selectedValue={this.state.selectedCompany}
              onValueChange={(item,index) => this.getcompany(item,index)}>
              <Picker.Item label="Select" value="Select"/>
              {
                this.state.company_name.map((item, index) => {
                  return (<Picker.Item label={item.label} value={item} key={index} />)
                })
              }
              </Picker>
              </Item>
               </Form>
        </View>
      {this.state.companyflag?<View style={{marginBottom:20}}>
              <Form>
                <Item regular success style={{ borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey }}>
                  <Input 
                  keyboardType='default' 
                  placeholder="Company Name" 
                  style={{ color: appColor.white, fontSize: 16, color: appColor.black }} 
                  onChangeText={text=>this.onCompanyName(text)}
                  value={this.state.otherCompany}
                  />
                  
                </Item>
               </Form>
        </View>:null}
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Trip Type<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
          <Form>
                <Item regular success style={{ borderRadius: 5,backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey,height:50 }}>
          <Picker
              mode="note"
              placeholder="Select Trip Type"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              selectedValue={this.state.selectedTriptype}
              onValueChange={(item, index) => this.getTrip_type(item, index)}>
                <Picker.Item label="Select" value="Select" />
              {
                this.state.Trip_type.map((item, index) => {
                  return (<Picker.Item label={item.label} value={item.value} key={index} />)
                })
              }
              </Picker>
              </Item>
               </Form>
        </View>
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Vehicle Type<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
          <Form>
                <Item regular success style={{ borderRadius: 5,backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey,height:50 }}>
          <Picker
              mode="note"
              placeholder="Vehicle Type"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              selectedValue={this.state.selectedVehical}
              onValueChange={(item, index) => this.getVehicalType(item, index)}>
                <Picker.Item label="Select" value="Select" />
              {
               this.state.vehicleType.map((item, index) => {
                  return (<Picker.Item label={item.label} value={item.value} key={index} />)
                })
              }
              </Picker>
              </Item>
               </Form>
        </View>
        </View>
       </ScrollView>
       <View style={styles.buttonContainer}>
              <TouchableOpacity
               disabled={this.state.manual_ride=="0"?true:false}
                onPress={()=>this.InitiateTripSubmit()}
                style={[styles.verifyButton,{backgroundColor:this.state.manual_ride=="0"?appColor.darkGrey:appColor.Blue}]}>
                <Text style={styles.buttonText}>Initiate Trip</Text>
              </TouchableOpacity>
        </View>
       </View>
    );
  }
}

const styles = StyleSheet.create({
   
headerView: {
  justifyContent: "center",
  alignContent: "center",
  alignItems: "center",
},
headerTextView: {
  fontSize: 18,
  fontFamily:Fonts.Medium,
  textAlign: "center",
  alignItems: "center",
  color: "#fff",
  alignContent: "center",
  alignSelf: "center",
  marginLeft:Dimensions.get("window").width/4
},
buttonContainer: {
  width: "100%",
  position: "absolute",
  bottom: 0,
  left: 0,
  right: 0,
  height: 40
},
verifyButton: {
  position: "absolute",
  height: 50,
  bottom: 10,
  left: 10,
  right: 10,
  alignItems: "center",
  justifyContent: "center",
  borderRadius: 10,
  backgroundColor:appColor.Blue
},
buttonText: {
  color: "#fff",
  fontSize: Fonts.h16,
  fontFamily:Fonts.Medium
},

});
