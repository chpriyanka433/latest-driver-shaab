import React from "react";
import {   StyleSheet,
    Text,
    View,
    Image,
    Modal,
    TouchableOpacity,
    TouchableHighlight,
    Linking,
    AsyncStorage,
    FlatList,
    Platform,
    StatusBar,
    ToastAndroid,RefreshControl
  } from "react-native";

  import { AppButton,InputText, appColor,BaseUrl,Toast,RNPickerSelect,CheckBox,Spinner,ImagePicker,RazorpayCheckout,MapViewDirections,geolib,Switch,OneSignal,NavigationEvents,DrawerActions,CodeInput,RNAndroidLocationEnabler,Button,getDirections,image,Fonts,Header,RestApiKey,ClientId,ClientSecret,grantType,moment,Geolocation,NetInfo} from "././common";

  import {Container, Item} from "native-base";
import BackgroundTimer from 'react-native-background-timer';



export default class MyRideList extends React.Component {
  intervalID;
 
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            srcLatToNavigate: 0,
            srcLongToNavigate: 0,
            latitude: 0,
            longitude: 0,
            activeDrives:[],
            isLoading: false,
            poc_id: "",
            request_id: "",
            source: "",
            destination: "",
            company_name: "",
            schedule_time: "",
            company_id: "",
            poc_name: "",
            trip_type: "",
            booking_status:"",
            sourceLng:"",
            sourceLat:"",
            destinationLng:"",
            destinationLat:"",
            tour_type:"",
            vehicle_type:"",
            request_type:"",
            vehicle_number:"",
            poc_number:"",
            numbers_of_drivers:"",
            notification_type:"",
            reachLocationFlag:true,
            from_portal:"",
            driverType:"",
            refreshing:false
        };
        OneSignal.inFocusDisplaying(2);
       
      }
 
    
  componentDidMount() {
    // this.intervalID = setInterval(this.getBookingData.bind(this), 5000);
    this.getBookingData();
   
    this._fetchCurrentLocationToRedirect().then(()=>{
      return this.getBookingData();
    })
 }

 async callreachAPIStatus(){
    this.getBookingData();
    this.RideSessionDetails();
 }

 async RideSessionDetails() {
   this.setState({from_portal:""})
   try {
     const driverId = await AsyncStorage.getItem('driverId');
     var formData = new FormData();
     formData.append("action", "get_running_ride");
     formData.append("driver_id", driverId);
     const response = await fetch(BaseUrl.url2+"service.php", {
       body: formData,
       method: "POST",
       headers: {
         "Accept": "multipart/form-data",
         "Content-Type": "multipart/form-data",
         'Cache-Control': 'no-cache, no-store, must-revalidate',
         'Pragma': 'no-cache',
         'Expires': 0
       }
     });
     const responseJson = await response.json();
     if(responseJson.success==true){
      if(responseJson.data.from_portal){
        this.setState({from_portal:responseJson.data.from_portal})
       } 
     } else {
      //this.setState({from_portal:false})
     }
       
   } catch (error) {
     //alert(error);
   }
 }

 async getBookingData(){
 
   this.setState({activeDrives:[]})
    try {
      this.setState({
        isLoading: true,
        refreshing:true
      });
      const driverId = await AsyncStorage.getItem("driverId");
      console.log(driverId);
      var formData = new FormData();
      formData.append("action", "get_active_booking_list");
      formData.append("driver_id", driverId);
      const response = await fetch(
        BaseUrl.url4,
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      console.log("my ride list==",responseJson.data)
      if (responseJson.success === true) {
        this.setState({
          isLoading: false,
         
          activeDrives:responseJson.data,
          refreshing:false,
        });
        
        
      } else {
        this.setState({isLoading: false});
        console.log(JSON.stringify(responseJson))
      }
    } catch (error) {
      this.setState({isLoading:false})
      console.log(error)
    }
    
   }
  

  async _navigateDriver(destination) {
    this.updateGeoAddress(destination)
  }

  async updateGeoAddress(destination){
    const tokenType = await AsyncStorage.getItem("tokenType");
    const token = await AsyncStorage.getItem("token")
    try {
      this.setState({isLoading:true})
      const response = await fetch(
        "https://atlas.mapmyindia.com/api/places/geocode?address="+destination,
        {
          method: "GET",
          headers: {
            Authorization:JSON.parse(tokenType)+" "+JSON.parse(token),
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      if(responseJson.error==="invalid_token"){
        this.setState({isLoading:false})
          this.updateToken().then(()=>{
            return this.updateGeoAddress(destination)
          })
      } else {
        this.setState({isLoading:false})
        //console.log("final source lat",responseJson.copResults.latitude)
        const destLat = responseJson.copResults.latitude;
        const destLong = responseJson.copResults.longitude;
        this.navigateChange(destLat, destLong)
      }
    } catch(error){
      this.setState({isLoading:false})
      alert(error)
    }
  }

  async updateToken(){
    try {
      var formData = new FormData();
      formData.append("grant_type", grantType);
      formData.append("client_id", ClientId);
      formData.append("client_secret",ClientSecret);
      const response = await fetch(
        "https://outpost.mapmyindia.com/api/security/oauth/token",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      console.log("update token==",responseJson)
      await AsyncStorage.setItem("token",JSON.stringify(responseJson.access_token))
      await AsyncStorage.setItem("tokenType",JSON.stringify(responseJson.token_type))
    } catch(error){
        alert(error)
    }
  }

  navigateChange(desLat, desLng,item,surclat,surclug){
    if(item.status=='scheduled' || item.status=='initiated'){
    const asdsd = item.sourceLng
    console.log('asdsd');
    console.log(asdsd);
    const data = {
      source: {
      latitude: this.state.sourceLat,
      longitude: this.state.sourceLng
    },
    destination: {
      latitude: JSON.parse(surclat),
      longitude: JSON.parse(surclug)
    },
    params: [
      {
        key: "travelmode",
        value: "driving"        // may be "walking", "bicycling" or "transit" as well
      },
      {
        key: "dir_action",
        value: "navigate"       // this instantly initializes navigation using the given travel mode
      }
    ]
  }
  getDirections(data)

  }
  else{
      const data = {
      source: {
      latitude: this.state.sourceLat,
      longitude: this.state.sourceLng
    },
    destination: {
      latitude: JSON.parse(desLat),
      longitude: JSON.parse(desLng)
    },
    params: [
      {
        key: "travelmode",
        value: "driving"        // may be "walking", "bicycling" or "transit" as well
      },
      {
        key: "dir_action",
        value: "navigate"       // this instantly initializes navigation using the given travel mode
      }
    ]
  }
  getDirections(data)
}
    // console.log("map data ==",data)
    // console.log(data.destination.latitude)
    // console.log(data.destination.longitude)
   
 
  }
    
  componentWillUnmount() {
    // clearInterval(this.intervalID);
    // this.focusListener.remove();
    // clearTimeout(this.intervalID);
  }
      async _fetchCurrentLocationToRedirect() {
        await Geolocation.getCurrentPosition(
          position => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            });
          },
          error => ToastAndroid.show(error.message, ToastAndroid.LONG),
          // { enableHighAccuracy: false, timeout: 20000 }
        );
      }

      async onInitiateRide(company_id,company_name,trip_type,vehicle_type,vehicle_number,item){
        //alert(JSON.stringify(item))
        console.log("ITEM LIST++++++",item.id)
        let currentStatus = await AsyncStorage.getItem("checkedin_id")
        console.log("check",currentStatus)
        if(currentStatus==null || currentStatus==undefined){
          ToastAndroid.show("You haven't check-in, Please check-in first.",ToastAndroid.LONG);
          this.props.navigation.navigate("Checkin",{checktype:"notification"});
        } else {
           await AsyncStorage.setItem("ride_history_id_notification",item.id);
           if(item.status=="initiated"){
            this.updateLocation();
            this.props.navigation.navigate("ActiveBooking",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,item:item,driverType:this.state.driverType,vehicle_number:vehicle_number,bookingType:"start",item:item})
           }
           else if(item.status=="ride_started"){
            this.state.reachLocationFlag=false
            // this.updateLocation();
          
            this.props.navigation.navigate("AddFuel",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,item:item,driverType:this.state.driverType,item:item});
            // this.props.navigation.navigate("AddFuel",{manual:"no"});
            // this.props.navigation.navigate("AddFuel",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,manual:"no",driverType:this.state.driverType});
           }
           else {
            this.InitiateTripSubmit(company_id,company_name,trip_type,vehicle_type,vehicle_number,item);
           }
          //  this.props.navigation.navigate("AddFuel");
        }
      }

      async checkReachLocation(company_id,company_name,trip_type,vehicle_type,vehicle_number){
        ToastAndroid.show("You are not located at service location area.",ToastAndroid.LONG);
      }

      async InitiateTripSubmit(company_id,company_name,trip_type,vehicle_type,vehicle_number,item) {
      var ride_history_id_data="";
      if(this.state.from_portal=="1"){
        var ride_history_id_data = item.id
      } else {
        var ride_history_id_data = await AsyncStorage.getItem("ride_history_id")
      }

           try {
                 this.setState({isLoading:true});
                 let time= moment(new Date()).format("YYYY-MM-DD HH:mm")
                 console.log("Date",time)
                 var driverId = await AsyncStorage.getItem("driverId");
                 var formData = new FormData();
                 formData.append("action", "b2b_rideStart_rideEnd");
                 formData.append("type", "initiate_trip");
                 formData.append("driver_id", driverId);
                 formData.append("company_id", company_id);
                 formData.append("trip_type", trip_type);
                 formData.append("vehicle_type", vehicle_type);
                 formData.append("company_name", company_name);
                 formData.append("trip_initiate_time",time)
                 {
                  this.state.from_portal=="1"&&(
                    formData.append("ride_history_id",ride_history_id_data)
                   )
                 }
                 console.log('my ride initial submit formdata==',formData)
                 const response = await fetch(
                   BaseUrl.url2+"service.php",
                   {
                     body: formData,
                     method: "POST",
                     headers: {
                       Accept: "multipart/form-data",
                       "Content-Type": "multipart/form-data",
                       'Cache-Control': 'no-cache, no-store, must-revalidate',
                       'Pragma': 'no-cache',
                       'Expires': 0
                     }
                   }
                 );
                 const responseJson = await response.json();
                 //alert(JSON.stringify(responseJson))
                 if (responseJson.success === true) {
                   this.setState({isLoading:false});
                   let portal = this.state.from_portal;
                   if(portal=="0"){
                    AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id)
                    AsyncStorage.setItem("Tripflag", 'true');
                    AsyncStorage.setItem("initiateFlag", 'flaged');
                    console.log("=====booked from manual.")
                  } else {
                    console.log("=====booked from notification.")
                  }
                  ToastAndroid.show("Your trip has been initiated successfully.",ToastAndroid.LONG);
                  this.updateLocation();
                  this.props.navigation.navigate("ActiveBooking",{vehicle_number:vehicle_number,bookingType:"start",item:item})
                 } else {
                   this.setState({isLoading:false});
                   ToastAndroid.show(responseJson.message,ToastAndroid.LONG);
                   console.log("api res")
                 }
             } catch (error) {
               //alert(error)
              this.setState({isLoading:false});
              NetInfo.getConnectionInfo().then((connectionInfo) => {
                if(connectionInfo.type==='none' || connectionInfo.type ==='NONE'){
                  ToastAndroid.show("Please Check Your Internet Connection !",ToastAndroid.LONG);
                } else {
                console.log(error);
                }
            });
         }
       }

       async updateLocation(){
        longArray=[]; 
        timeStampArray=[];
        let rideStatus = await AsyncStorage.getItem("initiateFlag");
        if(rideStatus=="flaged"){
          BackgroundTimer.runBackgroundTimer(() => { 
           Geolocation.getCurrentPosition(
               (position) => {
                  const currentLongitude = JSON.stringify(position.coords.longitude);
                  const currentLatitude = JSON.stringify(position.coords.latitude);
                  const timeStamp = JSON.stringify(position.timestamp);
                  longArray.push(currentLongitude.concat(",").concat(currentLatitude))
                  timeStampArray.push(timeStamp)
                  var resultLatlng = {};
                    for (var i = 0; i < longArray.length; ++i){
                      resultLatlng["position" + (i+1)] = longArray[i];                                                                                                                          
                  }
                  var resultTimestamp = {};
                    for (var i = 0; i < timeStampArray.length; ++i){
                      resultTimestamp["position" + (i+1)] = timeStampArray[i];                                                
                  }
                  //console.log("resultLatlng==",resultLatlng)
                  const finalLatLong = Object.keys(resultLatlng).map(function(k){return resultLatlng[k]}).join(";");
                  const finaltimestamp = Object.keys(resultTimestamp).map(function(k){return parseInt(resultTimestamp[k]/1000)}).join(";");
                  AsyncStorage.setItem("finalLatLong",JSON.stringify(finalLatLong))
                  AsyncStorage.setItem("finaltimestamp",JSON.stringify(finaltimestamp))
                  //console.log("finaltimestamp==",finaltimestamp)
                  //console.log("finalLatLong==",finalLatLong)
                  this.sendLocation(finalLatLong,finaltimestamp)
               },
               (error) => console.log("error.message",error.message),
               //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
            );
          }, 50000);
        } else {
          BackgroundTimer.stopBackgroundTimer();
        }
      }
    
      async sendLocation(finalLatLong,finaltimestamp){
        const driverId = await AsyncStorage.getItem('driverId');
        var bookingType = await AsyncStorage.getItem("bookingType");
        var ride_history_id="";
        if(bookingType=="notification"){
          var ride_history_id = await AsyncStorage.getItem("ride_history_id_notification")
        } else {
          var ride_history_id = await AsyncStorage.getItem("ride_history_id")
        }
        try {
          var formData = new FormData();
          formData.append("action", "update_live_location");
          formData.append("driver_id",driverId);
          formData.append("driver_booking_id", "");
          formData.append("b2b_ride_history_id",ride_history_id);
          formData.append("latlong_coordinate",finalLatLong);
          formData.append("timestamp", finaltimestamp);
          //console.log("formdata request for location==",formData)
          const response = await fetch(BaseUrl.url5, {
            body: formData,
            method: "POST",
            headers: {
              "Accept": "multipart/form-data",
              "Content-Type": "multipart/form-data",
              'Cache-Control': 'no-cache, no-store, must-revalidate',
              'Pragma': 'no-cache',
              'Expires': 0
            }
          });
          const responseJson = await response.json();
          //console.log('driverdataride===',responseJson)  
          if(responseJson.success===true){
           console.log('responseJson.success',responseJson)  
           } else {
             console.log('responseJson.false',responseJson)  
          }  
        } catch (error) {
          console.log("error rsp==",error);
        }
      }

  render() {
    // const  refreshing = this.state.activeDrives
    const renderItem = ({item})=>{
      const surclat = item.sourceLat
      const surclug = item.sourceLng
       console.log('scheduled'  + item.sourceLat )
       console.log('source lng' + item.sourceLng)
       
     
      if(item.status=='ride_started'){
        this.state.reachLocationFlag=false
      }
      const desLat = item.destinationLat
      const desLng = item.destinationLng
      console.log('desLat destiiiiiiiiiiiiiiiiii');
      console.log(desLat);
      const destlatitu = this.state.destinationLat
     console.log('state destination lat',destlatitu)
            return(
            <View
            style={{ margin: 15,borderColor:appColor.lightGrey,borderWidth:2,borderRadius:5}}
            
          >
            <View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  backgroundColor: "#e5e5e5"
                }}
              >
                <View
                  style={{
                    left: 0,
                    paddingLeft: 10,
                    paddingBottom: 5,
                    paddingTop: 5
                  }}
                >
                  <Text style={{ color: "#9b9b9b",fontFamily:Fonts.Regular}}>
                    {item.trip_type=="one_way"?"One Way":item.trip_type=="round_trip"?"Round Trip":item.trip_type}
                  </Text>
                </View>

                <View
                  style={{
                    right: 0,
                    paddingRight: 10,
                    paddingBottom: 5,
                    paddingTop: 5
                  }}
                >
                  <Text style={{ color: "#9b9b9b",fontFamily:Fonts.Regular}}>
                    { item.status=="scheduled"?"Scheduled":item.status=="initiated"?"Initiated":item.status=="ride_started"?"Started":item.status}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  marginTop: 10,
                  marginBottom: 10
                }}
              >
                <View>
                  <Image
                    source={image.calender}
                    style={styles.imageStyle}
                  />
                </View>

                <View>
                  <Text style={{color:appColor.darkGrey,fontSize:14,fontFamily:Fonts.Regular,marginTop:"6%"}}>
                    Scheduled Time:
                  </Text>
                </View>

                <View style={{ width: "50%", justifyContent: "center" }}>
                  <TouchableHighlight>
                    <Text style={{ fontSize: Fonts.h14, color: "#5d5d5d",fontFamily:Fonts.Regular,marginLeft:5,marginTop:5}}>
                      {item.ride_start_time}
                    </Text>
                  </TouchableHighlight>
                </View>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  marginTop: 10,
                  marginBottom: 10
                }}
              >
                <View>
                  <Image
                    source={image.bookingImage}
                    style={styles.imageStyle}
                  />
                </View>

                <View>
                  <Text style={{color:appColor.darkGrey,fontSize:14,fontFamily:Fonts.Regular,marginTop:"6%"}}>
                    Booking Id:
                  </Text>
                </View>

                <View style={{ width: "50%", justifyContent: "center" }}>
                  <TouchableHighlight>
                    <Text style={{ fontSize: Fonts.h14, color: "#5d5d5d",fontFamily:Fonts.Regular,marginLeft:5,marginTop:5}}>
                      {item.booking_id}
                    </Text>
                  </TouchableHighlight>
                </View>
              </View>

              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <View style={styles.circle} />

                <View>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{ marginRight: 30,fontFamily:Fonts.Regular}}
                  >
                    {item.source}
                  </Text>
                </View>
              </View>

              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <View
                  style={[styles.circle, { backgroundColor: "#FF0000" }]}
                />

                <View
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{ marginRight: 30}}
                >
                  <Text style={{fontFamily:Fonts.Regular}}>{item.destination}</Text>
                </View>
              </View>

              <View
                style={[
                  styles.Button,
                  { flexDirection: "row", justifyContent: "space-evenly" }
                ]}
              >

                <Button
                  style={{ backgroundColor: appColor.Blue,fontFamily:Fonts.Regular}}
                  mode="contained"
                  onPress={() => {
                    this.onInitiateRide(item.company_id,item.company_name,item.trip_type,item.vehicle_type,item.vehicle_number,item)
                  }}
                  // onPress={() => {
                  //   this.state.reachLocationFlag?this.checkReachLocation(item.company_id,item.company_name,item.trip_type,item.vehicle_type,item.vehicle_number):this.onStartRide(item.company_id,item.company_name,item.trip_type,item.vehicle_type,item.vehicle_number,item)
                  // }}
                >
                  {item.status=="initiated"?"Reach Location":item.status=="ride_started"?"View Ride":"Initiate Ride"}
                </Button>

                <Button
                  style={{ backgroundColor: appColor.Blue,fontFamily:Fonts.Regular}}
                  mode="contained"
                  onPress={() =>
                    Linking.openURL(`tel:${item.poc_number}`)
                  }
                >
                  Call
                </Button>

                <Button
                  style={{ backgroundColor: appColor.Blue,fontFamily:Fonts.Regular}}
                  mode="contained"
                  loading={false}
                  onPress={() => this.navigateChange(desLat,desLng,item,surclat,surclug)}
                >
                  Navigate
                </Button>
              </View>
            </View>
          </View>
        )
    }
    return (
        <Container>
        <StatusBar backgroundColor="#19376c" barStyle="light-content" />
        <Header
           text={"My Rides"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <NavigationEvents onDidFocus={() => this.callreachAPIStatus()} />
        <View style={{flex:1}}>
        <Spinner
              visible={this.state.isLoading}
              style={{ color: '#000' }}
              color={appColor.Blue}
              overlayColor="rgba(0, 0, 0, 0.55)"
            />
         <FlatList
            data={this.state.activeDrives}
            renderItem={
               renderItem
            }
            onRefresh={()=>this.getBookingData()}
            refreshing={this.state.refreshing}
            // refreshControl={
            //   <RefreshControl refreshing={this.state.refreshing} onPress={()=>this.getBookingData()} />
            // }
            ListHeaderComponent={() => (!this.state.activeDrives.length ? 
              <Text style={styles.emptyMessageStyle}>No Rides Available</Text>  
              : null)}
            keyExtractor={item => item.id}
            // refreshing={this.state.activeDrives}
           
      />
          
        </View>
    </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    height: 610,
    padding: 20,
    justifyContent: "space-between",
    backgroundColor: "white"
  },
  stepOneView: {
    backgroundColor: "white",
    borderRadius: 10,
    borderColor: "#BDBDBD",
    borderWidth: 1,
    paddingLeft: 5,
    paddingTop: 5,
    margin: 15
  },
  stepOneText: {
    fontSize: Fonts.h16,
    color: "black"
  },
  stepTwoView: {
    backgroundColor: "white",

    justifyContent: "space-between",
    borderRadius: 10,
    borderColor: "#BDBDBD",
    borderWidth: 1,
    paddingLeft: 5,
    paddingTop: 5,
    margin: 15
  },
  stepTwoText: {
    fontSize: Fonts.h16,
    color: "black"
  },
  stepThreeView: {
    backgroundColor: "white",
    flex: 0.4,
    justifyContent: "space-between",
    borderRadius: 10,
    borderColor: "#BDBDBD",
    borderWidth: 1,
    paddingLeft: 5,
    paddingTop: 5,
    margin: 15
  },

  stepThreeText: {
    fontSize: Fonts.h16,
    color: "black"
  },
  Button: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 10
  },
  contentContainer: {
    justifyContent: "space-between"
  },
  containerScroll: {
    flex: 1
  },
  verifyButton: {
    height: 50,

    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  buttonText: {
    color: "#fff",
    fontFamily: "Helvetica",
    fontSize: Fonts.h22,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    fontWeight: "bold"
  },
  imageStyle: {
    padding: 10,
    marginLeft: 10,
    marginRight: 5,
    marginTop: 5,
    height: 25,
    width: 25,
    resizeMode: "stretch",
    alignItems: "center"
  },

  circle: {
    width: 6,
    height: 6,
    borderRadius: 5 / 2,
    backgroundColor: "#228B22",
    alignItems: "center",
    marginLeft: 10,
    marginTop: 7,
    marginRight: 10
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 130,
    borderRadius: 30,
    backgroundColor: appColor.DarkBlue,
    borderColor: "#000"
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 20,
    backgroundColor: "#ecf0f1"
  },
  button: {
    backgroundColor: appColor.DarkBlue,
    width: "45%",
    borderRadius: 1,
    borderColor: "#000",
    height: 40,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    borderRadius: 4
  },
  headerTextView: {
    fontSize: Fonts.h18,
    fontWeight: "bold",
    textAlign: "center",
    color: "#fff",
  },
  spinnerStyle: {
    color: "#000000"
  },
  emptyMessageStyle:{
    fontSize:16,
    fontFamily:Fonts.SemiBold,
    textAlign:"center",
    marginTop:30
  }
});

