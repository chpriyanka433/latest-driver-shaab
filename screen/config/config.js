const baseUrl = {
   // live server
   // url1:'https://drivershaab.com/crm/webservices/',
   // url2:'https://drivershaab.com/crm/DriverShaabAPIServices/B2B/',
   // url3:"https://preprod.aadhaarapi.com/",
   // url4:'https://drivershaab.com/crm/DriverShaabAPIServices/B2B/service.php',
   // url5:'https://drivershaab.com/crm/DriverShaabAPIServices/Driver/service.php',
   // url6:'http://drivershaab.com/crm/upload/',

   // Development Server
   url1:'https://drivershaab.com/development/drivershaab/webservices/',
   url2:'https://drivershaab.com/development/drivershaab/DriverShaabAPIServices/B2B/',
   url3:"https://preprod.aadhaarapi.com/",
   url4:'https://drivershaab.com/development/drivershaab/DriverShaabAPIServices/B2B/service.php',
   url5:'https://drivershaab.com/development/drivershaab/DriverShaabAPIServices/Driver/service.php',
   url6:'https://drivershaab.com/development/drivershaab/upload/',
}

export default baseUrl
      
