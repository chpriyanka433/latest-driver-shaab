import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right, Button} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  BackHandler,
  TouchableOpacity,
  AsyncStorage,
  ToastAndroid,
  RefreshControl,
  Dimensions,
  PermissionsAndroid,
  Platform
} from 'react-native';


import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,moment,image,Fonts,Geolocation,RestApiKey,ClientId,ClientSecret,grantType,RNAndroidLocationEnabler,NetInfo} from "././common";
  import VIForegroundService from '@voximplant/react-native-foreground-service';

import { ScrollView } from 'react-native-gesture-handler';



 class CheckOut extends Component {
  constructor(props){
    super(props);
    this.state ={
      path:"",
      images:"",
      imagePath:"",
      latitude:"",
      longitude:"",
      userName:"",
      isLoading:false,
      geofencingData:[],
      checkOutflag:false,
      test:""
    }
    this.checkOutflag=false
  }
  handleBackButton(){
    return true
  }

  async updateCallback(){
    var TimeIn=await AsyncStorage.getItem("check_in_time");
    var checkedInDateTime=await AsyncStorage.getItem("checkedInDateTime");
    this.setState({TimeIn})
    this.setState({checkedInDateTime})
    //this.geofencing();
    this.checkFirst()
  }

  async updateGeoAddress(lat,long){
    const tokenType = await AsyncStorage.getItem("tokenType");
    const token = await AsyncStorage.getItem("token")
    console.log("url==", "http://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long)

    try {
      const response = await fetch(
        "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      if(responseJson.error==="invalid_token"){
          this.updateToken().then(()=>{
            return this.updateGeoAddress(lat,long)
          })
      } else {
        console.log("responseJson===",responseJson.results[0].formatted_address)
        this.setState({test:responseJson.results[0].formatted_address})
      }
    } catch(error){
        console.log(error)
    }
  }

  async updateToken(){
    try {
      var formData = new FormData();
      formData.append("grant_type", grantType);
      formData.append("client_id", ClientId);
      formData.append("client_secret",ClientSecret);
      const response = await fetch(
        "https://outpost.mapmyindia.com/api/security/oauth/token",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      await AsyncStorage.setItem("token",JSON.stringify(responseJson.access_token))
      await AsyncStorage.setItem("tokenType",JSON.stringify(responseJson.token_type))
    } catch(error){
        console.log(error)
    }
  }

    
  componentDidMount(){
    this.checkFirst()
  }

  async checkFirst(){
     const {navigation} =this.props;
    this.focusListener = navigation.addListener('willFocus', () => {});
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this._fetchUserDetails().then(()=>{
      return this._fetchCurrentLocationToRedirect()
    })
    //this.geofencing();
    console.log("After geoCheckout === ",this.checkOutflag)
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }
  

  async _fetchUserDetails() {
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "get_driver_info");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url1+"index.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      console.log('driverdata===',responseJson)
      this.setState({isLoading:false})
      this.setState({
        userName: responseJson.name
      })
    } catch (error) {
      console.log(error);
    }
  }

  // async componentWillMount() {
  //   await this._fetchCurrentLocationToRedirect();

  // }
  async _fetchCurrentLocationToRedirect() {
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (!granted) {
        this.props.navigation.navigate("GpsScreen");
      } else {
        this.checkGpsStatus();
      }
      }
    }

checkGpsStatus(){
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
    .then(data => {
      if(data){
        this.checkNavigation();
      } else {
        this.props.navigation.navigate("GpsScreen");
      }
    }).catch(err => {
      this.props.navigation.navigate("GpsScreen");
    });
  }

  async checkNavigation(){
    await Geolocation.getCurrentPosition(
      (position) => {
        console.log("position",position.coords.latitude)
         this.setState({latitude: position.coords.latitude,longitude:position.coords.longitude}) 
         this.updateGeoAddress(position.coords.latitude,position.coords.longitude)
      });
  }


    async checkDistance() {
      console.log(this.state.geofencingData, 'chgeckdistance');
      let size = this.state.geofencingData.length;
                                      
  
      if (size > 0) {
        AsyncStorage.setItem("geoCheckout","");
        this.checkOutflag = false;
        for (var i = 0; i < size; i++) {
          let lat = this.state.geofencingData[i].latitude;
          let long = this.state.geofencingData[i].longitude;
           await Geolocation.getCurrentPosition(
            function (position) {
              let value = geolib.getDistance(position.coords, { latitude: lat, longitude: long })
              console.log('value===in meter', value)
              if (value < 200) {
                console.log("checkout=============")
                AsyncStorage.setItem("geoCheckout","flagCheckout")
              } 
            }
          );
        }
       
      let geoCheckOutFlag = await AsyncStorage.getItem("geoCheckout");
       console.log("Async ==== ",JSON.stringify(geoCheckOutFlag))
        if(JSON.stringify(geoCheckOutFlag).replace(/"/g,'') == "flagCheckout"){
          this.checkOutflag = true;
          this.setState({isLoading:false})
        } 
        else{
          AsyncStorage.setItem("geoCheckout","")
          this.setState({isLoading:false})
        }
        
        console.log("geoCheckout === ",this.checkOutflag)
      }
        return this.checkOutflag;
    }

    async geofencing(){
   try {
        const driverId = await AsyncStorage.getItem('driverId');
        var formData = new FormData();
        formData.append("action", "get_company_geofencing");
        
        const response = await fetch(BaseUrl.url2+"service.php", {
          body: formData,
          method: "POST",
          headers: {
            "Accept": "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        });
        const responseJson = await response.json();
    
        if (responseJson.success === true){
          this.setState({geofencingData: responseJson.data});
          return this.checkDistance()
          
        }
       
      } catch (error) {
        console.log(error);
    }
  }
  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  };
 
  async CheckOut() {
    //await this.geofencing();
    // if(this.checkOutflag === false){
    //   ToastAndroid.show("It seems that you are away from the office.", ToastAndroid.LONG);
    //   // this.props.navigation.navigate("CheckOut");
    //   } else {
        try {
          var ride_history_id = await AsyncStorage.getItem("ride_history_id");
          if(ride_history_id!=null){
            ToastAndroid.show("Trip is running, Please end your current trip.",ToastAndroid.LONG);
            this.setState({isLoading:false})
          }
          else{
            this.setState({ isLoading: true });
          let time= await moment(new Date()).format("YYYY-MM-DD HH:mm")
          console.log("Date",time)
          var checkedin_id = await AsyncStorage.getItem("checkedin_id");
          console.log("cherckin",checkedin_id)
          var driverId = await AsyncStorage.getItem("driverId");
          var formData = new FormData();
          formData.append("action", "b2b_check_in_check_out");
          formData.append("type", "checkout");
          formData.append("driver_id", driverId);
          formData.append("check_out_time",time)
          formData.append("check_out_latitude", this.state.latitude);
          formData.append("check_out_longitude", this.state.longitude);
          formData.append("checkedin_id",checkedin_id)
          formData.append("location", this.state.test);
          console.log('formdata',formData)
          console.log('url',BaseUrl.url2+"service.php")
    
          const response = await fetch(
            BaseUrl.url2+"service.php",
            {
              body: formData,
              method: "POST",
              headers: {
                Accept: "multipart/form-data",
                "Content-Type": "multipart/form-data",
                'Cache-Control': 'no-cache, no-store, must-revalidate',
                'Pragma': 'no-cache',
                'Expires': 0
              }
            }
          );
    
          const responseJson = await response.json();
          console.log("formdata==",formData)
          console.log("url==",BaseUrl.url2+"service.php")
          console.log('response===',responseJson)
          if (responseJson.success === true) {
            this.setState({isLoading:false})
            ToastAndroid.show(responseJson.message,ToastAndroid.LONG);
            VIForegroundService.stopService();
            AsyncStorage.removeItem("checkedin_id")
            AsyncStorage.removeItem("checkflag");
            this.props.navigation.navigate("Checkin")
               
          } else {
            this.setState({isLoading:false})
            NetInfo.fetch().then(connection => {
              if(!connection.isConnected){
                Toast.show("Please Check Your Internet Connection !",Toast.LONG);
              } else {
                alert(error)
              }
            });
          }
        }
       } catch (error) {
          console.log(error);
       }
        // }
  }


   _onRefresh(){
    this.setState({refreshing:true});
    this.geofencing().then(() =>{
      this.setState({refreshing:false})
    });
  }

  render() {
    return (
      <View style={{flex:1}}>
        <Header
          text={"End Your Day"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <ScrollView
        // refreshControl={
        //   <RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)} />}
          >
        <ImageBackground  style={{width:"100%",height:180,marginBottom:20,backgroundColor:appColor.Blue,borderBottomRightRadius:30,borderBottomLeftRadius:30}}>
        
         {this.state.userName===false?<Image style={styles.circleView}
          source={image.profile}
          />:null}
  {this.state.userName?<View style={styles.circleView}><Text style={styles.circleViewText}>{this.state.userName.split().map((word)=> word.charAt(0)).join(' ')}</Text></View>:null}

          <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,marginTop:10,textAlign
          :'center',fontSize:25}}>{this.state.userName}</Text>
        </ImageBackground>
        <View style={{padding:20}}>
        <NavigationEvents
                onDidFocus={() => this.updateCallback()}
              />  
        <Spinner
              visible={this.state.isLoading}
              style={{color:'#000'}}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            />
         <View style={{justifyContent:'center', alignItems:'center',marginBottom:20}}>
        <Text style={{fontSize:18, fontFamily:Fonts.Regular, fontWeight:'600'}}>Check In Time:</Text>
        <Text style={{fontSize:18,fontFamily:Fonts.Regular, fontWeight:'600'}}> {this.state.checkedInDateTime}</Text>
      </View>
      {/* <View style={{width:100,height:100,backgroundColor:appColor.darkGrey,marginHorizontal:"34%"}}></View> */}
      <View style={{justifyContent:'center',marginBottom:20,alignItems:'center', marginTop:80}}>
            <AppButton
            text='Check Out'
            // disabled={this.checkOutflag ? false:true}
            // bgColor={this.checkOutflag ? appColor.Blue:appColor.darkGrey}
            bgColor={appColor.Blue}
            width={'100%'}
            textColor={appColor.white}       
            onClickFunc={()=>this.CheckOut()}     
            />
        </View>
        <TouchableOpacity  onPress={this.navigateToScreen("AttendanceHistory")}>
        <Text style={{fontSize:15, fontFamily:'Roboto Light', fontFamily:Fonts.Bold,textAlign:"center",color:appColor.Blue,marginBottom:30}}>View Your Attendance History</Text>
        </TouchableOpacity>
        {/* <SwipeButton
            disabled={false}
            //disable the button by doing true (Optional)
            swipeSuccessThreshold={70}
            height={45}
            //height of the button (Optional)
            width={330}
            //width of the button (Optional)
            title="Swipe to Submit"
            //Text inside the button (Optional)
            //thumbIconImageSource={thumbIcon}
            //You can also set your own icon for the button (Optional)
            onSwipeSuccess={() => {
              alert('Submitted Successfully!');
            }}
            //After the completion of swipe (Optional)
            railFillBackgroundColor="#e688a1" //(Optional)
            railFillBorderColor="#e688ff" //(Optional)
            thumbIconBackgroundColor="#ed9a73" //(Optional)
            thumbIconBorderColor="#ed9aff" //(Optional)
            railBackgroundColor="#bbeaa6" //(Optional)
            railBorderColor="#bbeaff" //(Optional)
          /> */}
        </View>
       </ScrollView>
       
       </View>
    );
  }
}

const styles = StyleSheet.create({
  
  RegisterButton: {
    backgroundColor: appColor.yellow,
    marginTop:20
  },
  circleView:{
    width:60,
    height:60,
    borderRadius:30,
    backgroundColor:appColor.lightGrey,
    marginHorizontal:"40%",
    marginTop:30
    // marginRight:20
    
  },
  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  headerTextView: {
    fontSize: 18,
    fontFamily:Fonts.Medium,
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    marginLeft: Dimensions.get("window").width/5
  },
  circleViewText:{
    fontSize:25,
    fontFamily:Fonts.Medium,
    textAlign:"center",
    color:appColor.Blue,
    marginTop:12
 },

});

export default CheckOut;