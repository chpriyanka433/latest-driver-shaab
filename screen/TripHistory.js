import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right,Form,Item,Input} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground, 
  TouchableOpacity,
  PermissionsAndroid,
  AsyncStorage,
  Dimensions
} from 'react-native';

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase,moment,image,Fonts,NetInfo} from "././common";

import { ScrollView, FlatList } from 'react-native-gesture-handler';


 class TripHistory extends Component {
  constructor(props){
    super(props);
    
    this.state ={
      data:[],
      latitude:"",
      longitude:"",
      checkedin_id:"",
      userName: "",
      chosenDate: new Date(),
      date:"",
      isLoading:false
    }
   
  }
 
 componentDidMount(){
    this.TripHistory();
 }

  async TripHistory() {
    this.setState({isLoading:true})
   try {
      let time= moment(new Date()).format("YYYY-MM-DD")
      console.log("Date",time)
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "get_booking_history_list");
      formData.append("search_date",time);
      formData.append("driver_id", driverId);
     
      console.log('formdata',formData)
      console.log('url',BaseUrl.url2+"service.php")

      const response = await fetch(
        BaseUrl.url2+"service.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data"
          }
        }
      );

      const responseJson = await response.json();
      console.log('data===',responseJson)
      if (responseJson.success === true) {
        this.setState({isLoading:false,data:responseJson.data})
      } else {
        this.setState({isLoading:false})
        this.setState({data:[]})
        console.log("Check chote");
      }
    } catch (error) {
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          console.log(error);
        }
      });  
    }
  }

    async  filterDetails(val){
     this.setState({date:val})
    this.setState({ isLoading: true });
   try {
      let time = await moment(this.state.date).format("YYYY-MM-DD");
      console.log("filter time ==== ",time);
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "get_booking_history_list");
      formData.append("search_date",time);
      formData.append("driver_id", driverId);
     
      console.log('formdata',formData)
      console.log('url',BaseUrl.url2+"service.php")

      const response = await fetch(
        BaseUrl.url2+"service.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data"
          }
        }
      );

      const responseJson = await response.json();
      console.log('data===',responseJson)
      if (responseJson.success === true)
      {
        this.setState({isLoading:false})
        this.setState({data:responseJson.data})
    
      } else {
        this.setState({isLoading:false})
        this.setState({data:[]})
        console.log("Check chote");
      }
    } catch (error) {
      this.setState({isLoading:false})
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          console.log(error);
        }
      });  
    }
  }

  async updateCallback(){
    await this.TripHistory();
    this.setState({date:""})
}
  
  

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  };
 
  render() {
    return (
      <View style={{flex:1}}>
        <Header
          text={"Trip History"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <ScrollView>
          <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
          <Spinner
              visible={this.state.isLoading}
              style={{color:'#000'}}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            />
            <View style={{backgroundColor:appColor.lightGrey,height:60}}>
            <View style={{flexDirection:"row",justifyContent:"flex-start"}}>
              <Text style={{fontFamily:Fonts.Regular,fontSize:15,color:appColor.Blue,marginLeft:20,marginTop:20}}>Filter By Date:</Text>
              <DatePicker
                    style={{width:100,backgroundColor:appColor.white,marginTop:10,height:35,marginLeft:10}}
                    date={this.state.date} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    // placeholder="new Date()"
                    format="DD-MMM-YYYY"
                    minDate="1970-01-01"
                    maxDate={new Date()}
                    confirmBtnText="Confirm"
                    onDateChange={val=>this.filterDetails(val)}
                    value={this.state.date}
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                      // dateIcon: {
                      //   position: 'absolute',
                      //   left: 0,
                      //   top: 4,
                      //   marginLeft: 0
                      // },
                      dateInput: {
                        marginLeft: 10,
                        alignItems:'flex-start',
                        borderColor: appColor.transparent,
                        fontFamily:Fonts.Regular
                      }
                    }}
        />

              </View>
              </View>
              <View style={{padding:20}}>
          {this.state.data.length>0?
          <FlatList data={this.state.data}
          extraData={this.state.data}
            renderItem={({item}) => <View style={styles.flatliststyle}>
                 <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:8}}>
                 <Text style={{fontFamily:Fonts.Regular,fontSize:10,color:appColor.lightBlack}}>Date: <Text style={{fontFamily:Fonts.Regular,fontSize:10,color:appColor.black}}>{item.ride_start_time}</Text></Text>
                 {item.total_km?<Text style={{fontFamily:Fonts.Regular,fontSize:10,color:appColor.lightBlack}}>Total Km: <Text style={{fontFamily:Fonts.Regular,fontSize:10,color:appColor.black}}>{item.total_km}</Text></Text>:null}
                 </View>
                 <View style={{height:2,width:"100%",backgroundColor:appColor.darkGrey,marginBottom:10}}></View>
                 
                 <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:8}}>
                 <Text style={{fontFamily:Fonts.Regular,fontSize:10,color:appColor.lightBlack}}>Total Time: <Text style={{fontFamily:Fonts.Regular,fontSize:10,color:appColor.black}}>{item.duration}</Text></Text>
                 </View>
                 <View style={{height:2,width:"100%",backgroundColor:appColor.darkGrey,marginBottom:10}}></View>

                 <View style={{flexDirection:"row",justifyContent:"space-between"}}>
               <Text style={{fontFamily:Fonts.Regular,fontSize:14,marginBottom:5}}>Booking Id:</Text>
               <Text style={{fontFamily:Fonts.Regular,fontSize:14,marginBottom:5}}>Initiate Time:</Text>
               </View>
               <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:5}}>
               <Text style={{fontFamily:Fonts.Regular,fontSize:12,marginBottom:5,color:appColor.black}}>{item.booking_id}</Text>
               <Text style={{fontFamily:Fonts.Regular,fontSize:12,marginBottom:5,color:appColor.black}}>{item.initiate_time}</Text>
               </View>
                 <View style={{flexDirection:"row"}}>
                 <Image 
                    source={image.location}
                    resizeMode={"contain"}
                    style={{ width: 20, height:160}}
                  />
                  <View style={{flexDirection:"column",marginBottom:10, flexShrink:1}}>
                  <View style={{flexDirection:"row",marginLeft:10,marginBottom:40, flexShrink:1}}>
                     <View style={{flexDirection:'column',marginBottom:10,flexShrink:1}}>
                         <Text style={{color:appColor.HeaderBlue,fontFamily:Fonts.Medium,fontSize:12,}}>Start Trip</Text>
                         <Text style={{fontFamily:Fonts.Regular,fontSize:10}}>{item.ride_start_time}</Text>
                         {/* <Text style={{fontFamily:"Roboto Light",fontSize:10}}>11:00am</Text> */}
                     </View>
                     <View style={{width:"60%",marginLeft:20,flexShrink:1}}>
                         <Text 
            style={{fontFamily:Fonts.Regular,fontSize:12,letterSpacing:1}}>{item.ride_start_location}</Text>
                     </View>
                  </View>  
                  <View style={{flexDirection:"row",marginLeft:10, flexShrink:1}}>
                     <View style={{flexDirection:'column',flexShrink:1}}>
                         <Text style={{color:appColor.HeaderBlue,fontFamily:Fonts.Regular,fontSize:12}}>End Trip</Text>
                         <Text style={{fontFamily:Fonts.Regular,fontSize:10}}>{item.ride_end_time}</Text>
                         {/* <Text style={{fontFamily:"Roboto Light",fontSize:10}}>11:00am</Text> */}
                     </View>
                     <View style={{width:"60%",marginLeft:20,flexShrink:1}}>
                         <Text 
                          style={{fontFamily:Fonts.Regular,fontSize:12,letterSpacing:1}}>{item.ride_end_location}</Text>
                     </View>
                  </View> 
                  </View>
                 </View>
             </View>
            }
            />:<View style={{justifyContent:"center",marginTop:"30%",width:"100%",height:200}}><Image style={{height:100, width:100,marginHorizontal:"38%"}}
            resizeMode={"contain"} source={image.noHistory}/>
            <Text style={{fontSize:18, fontFamily:Fonts.Medium, color:appColor.Blue, marginTop:10,textAlign:"center"}}>No Trip History Found</Text>
            </View>}
           </View>
        </ScrollView>
       </View>
    );
  }
}

const styles = StyleSheet.create({
  
  RegisterButton: {
    backgroundColor: appColor.yellow,
    marginTop:20
  },
  flatliststyle: {
   width:"100%",
   height:360,
   borderRadius:10,
   borderWidth:2,
   borderColor:appColor.darkGrey,
   padding:10,
   marginBottom:10
  },
  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  headerTextView: {
    fontSize: 18,
    fontFamily:Fonts.Medium,
    textAlign: "center",
    alignItems: "center",
    color:appColor.white,
    alignContent: "center",
    alignSelf: "center",
    marginLeft: Dimensions.get("window").width/4
  }

});

export default TripHistory;