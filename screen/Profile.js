import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right,Form,Item,Input} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import appcolor from "../common/color"
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground
} from 'react-native';

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase,image,Fonts} from "././common";

import { ScrollView } from 'react-native-gesture-handler';

export default class Profile extends Component {
  closeDrawer = () => {
    this.drawer._root.close()
  };
  openDrawer = () => {
    this.drawer._root.open()
  };


  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  };
 
  render() {
    return (
     
      <View style={{flex:1}}>
         <Header 
          title="My Profile"
          color={appcolor.white}
          leftIcon={image.menu}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <ScrollView>
        <ImageBackground  style={{width:"100%",height:180,marginBottom:20,backgroundColor:appcolor.HeaderBlue,borderBottomRightRadius:30,borderBottomLeftRadius:30}}>
           <Text style={{fontFamily:Fonts.Regular,color:appcolor.white,marginTop:50,textAlign
          :'center',fontSize:18}}>Driver Name</Text>
        </ImageBackground>
        <Image style={styles.circleView}
                 source={image.profile}
          />
        <View style={{padding:20}}>   
        <View style={{marginTop:60,marginBottom:20}}>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Your Phone Number" 
                  style={styles.inputStyle} 
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{marginBottom:20}}>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Email" 
                  style={styles.inputStyle} 
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{marginBottom:20}}>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Address" 
                  style={styles.inputStyle} 
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{marginBottom:20}}>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="D.O.B" 
                  style={styles.inputStyle} 
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
     
      <View style={{justifyContent:'center',marginBottom:20,alignItems:'center', marginTop:80}}>
            <AppButton
            text='Edit Profile'
            bgColor={appcolor.Blue}
            width={'100%'}
            textColor={appcolor.white}           
            />
        </View>
        
        </View>
       </ScrollView>
       
       </View>
    );
  }
}

const styles = StyleSheet.create({
  
  RegisterButton: {
    backgroundColor: appcolor.yellow,
    marginTop:20
  },
  circleView:{
    width:140,
    height:140,
    borderRadius:70,
    backgroundColor:appcolor.lightGrey,
    position:"absolute",
    marginTop:120,
    marginHorizontal:110
    // marginRight:20
    
  },
  inputView: {
    borderRadius:5,
    backgroundColor:appcolor.white,
     borderColor:appcolor.white
},
inputStyle:{
  fontFamily:Fonts.Regular
}

});