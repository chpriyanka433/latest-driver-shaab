/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
  Alert,
  phone,
  BackHandler,
  PermissionsAndroid,
  StatusBar,
  ScrollView
} from "react-native";

import {appColor,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,image,Geolocation,Fonts,RNAndroidLocationEnabler} from "././common";

const googleMaps_apiKey = "AIzaSyComH3uVSYHWeEtzyEByzgi76SOseHvSG0";


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstNumber: "8448445030",
      secondNumber: "9073032030",
      thirdNumber: "9073032404",
      latitude:0,
      longitude:0
    };
  }
  call = phoneNumber2 => {
    let phoneNumber = phone;
    if (Platform.OS !== "android") {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phoneNumber2}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert("Phone number is not available");
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };

  async componentDidMount() {
    //this._checkPermissionAndRedirect()
  }

  _checkPermissionAndRedirect() {
    if (
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      )
    ) {
      this.checkLocation();
    } else {
      try {
        const granted = PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: "Cool Location App required Location permission",
            message:
              "We required Location permission in order to get device location " +
              "Please grant us."
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.checkLocation();
        } else {
          Alert.alert("You don't have access for the location");
        }
      } catch (err) {
        console.log(err);
      }
    }
  }

  checkLocation(){
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
  .then(data => {
    if(data){
      this._fetchCurrentLocationToRedirect()
    } else {
      ToastAndroid.show('Please Turn ON your Location.',ToastAndroid.LONG)
    }
  }).catch(err => {
    console.log(err.message)
    //ToastAndroid.show(err.message,ToastAndroid.LONG)
  });
  }


  _fetchCurrentLocationToRedirect() {
    this.setState({
      isSpinnerLoading: true
    });
    Geolocation.getCurrentPosition(
      position => {
        this.setState({
          isSpinnerLoading: false,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
      },
      error => console.log(error)
    );
  }

  openDirections() {
    var destination = "23A Chandra Nath Roy Road, Tilajala,Kolkata - 700039"
    var encodedSourceUrl = destination
    .toString()
    .split(" ")
    .join("+");
  fetch(
    "https://maps.googleapis.com/maps/api/geocode/json?address=" +
      encodedSourceUrl +
      "&key=" +
      googleMaps_apiKey
  )
    .then(response => response.json())
    .then(responseJson => {
      var srcLatToNavigate = responseJson.results[0].geometry.location.lat;
      var srcLongToNavigate = responseJson.results[0].geometry.location.lng
      this.navigateChange(srcLatToNavigate, srcLongToNavigate)
    });
  }

  navigateChange(desLat, desLng){
    const data = {
      source: {
      latitude: this.state.latitude,
      longitude: this.state.longitude
    },
    destination: {
      latitude: desLat,
      longitude: desLng
    },
    params: [
      {
        key: "travelmode",
        value: "driving"        // may be "walking", "bicycling" or "transit" as well
      },
      {
        key: "dir_action",
        value: "navigate"       // this instantly initializes navigation using the given travel mode
      }
    ]
  }
  getDirections(data)
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#19376c" barStyle="light-content" />
        <View style={styles.logoView}>
          <Image
            source={image.newLogo}
            style={styles.logoImage}
          />
        </View>
        <View style={{marginHorizontal:10}}>
        <ScrollView>
        <View style={styles.textView}>
          <Text style={styles.text}>
            You have been registered successfully.Please visit Drivershaab
            office with aadhar card and driving licence to get verified.
          </Text>
        </View>
        {/* <View style={styles.thirdView}>
          <View style={styles.locationView}>
            <View style={styles.officeAddressView}>
              <Image
                source={require("../src/location.png")}
                style={styles.locationImage}
              />
              <Text style={styles.officeAddress}>OFFICE ADDRESS -</Text>
            </View>
            <Text style={styles.officeAddressText}>
              23A Chandra Nath Roy Road, Tilajala,Kolkata - 700039
            </Text>
          </View>
        </View> */}
        <View
          style={{
            flexDirection: "column",
            height: 100
          }}
        >
          <View style={styles.phoneView}>
            <Image
              source={image.phoneIcon}
              style={styles.phoneIcon}
            />
            <Text style={styles.phoneNumberText}>CONTACT NUMBERS -</Text>
          </View>
          <View
            style={{
              flexDirection: "column",
              flex: 1,
              justifyContent: "space-between",
              paddingTop: 5
            }}
          >
            <TouchableOpacity onPress={() => this.call(this.state.firstNumber)}>
              <Text style={styles.phoneNumbers}>
                +91 {this.state.firstNumber}
              </Text>
            </TouchableOpacity>
            {/* <TouchableOpacity
              onPress={() => this.call(this.state.secondNumber)}
            >
              <Text style={styles.phoneNumbers}>
                +91 {this.state.secondNumber}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.call(this.state.thirdNumber)}>
              <Text style={styles.phoneNumbers}>
                +91 {this.state.thirdNumber}
              </Text>
            </TouchableOpacity> */}
          </View>
        </View>

        <View style={styles.documentView}>
          <View style={{ flexDirection: "row", paddingRight: 30 }}>
            <Image
              source={image.document}
              style={{ height: 20, width: 20, marginRight: 10 }}
            />
            <Text style={{ fontFamily:Fonts.Medium, color: appColor.black }}>
              While visiting office ,the driver must bring below documents-
            </Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: appColor.black,fontFamily:Fonts.Regular}}>
              6 copy driver passport photo .
            </Text>
          </View>
          <View style={{ paddingTop: 5 }}>
            <Text style={{ color: appColor.black,fontFamily:Fonts.Regular}}>
              2 copy xerox of AADHAR card ,voter card,driving licence ,cancel
              cheque, bank passbook, police verificationcertificate, local
              councillior certificate .
            </Text>
          </View>
        </View>
        </ScrollView>
       </View>
        <View style={styles.buttonView}>
          {/* <View>
            <Button
              icon={"navigation"}
              // style={{ width: 120 }}
              mode="contained"
              //onPress={() => this.openDirections("22.532260", "88.390010")}
              onPress={() => this.openDirections()}
            >
              Navigate
            </Button>
          </View> */}

            <View>
            <Button
              icon={"phone"}
              style={{ fontFamily:Fonts.Medium }}
              mode="contained"
              //onPress={() => this.openDirections("22.532260", "88.390010")}
              onPress={() => this.call(this.state.firstNumber)}
            >
              Call
            </Button>
          </View>

          <View>
            <Button
              style={{ fontFamily:Fonts.Medium}}
              icon={"exit-to-app"}
              mode="contained"
              onPress={() => BackHandler.exitApp()}
            >
              EXIT
            </Button>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    padding: 10,
    flex: 1,
    backgroundColor: appColor.white
  },
  logoView: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 20
  },
  logoImage: {
    width: 150,
    height: 150
  },

  text: { color: appColor.black, paddingTop: 1,fontFamily:Fonts.Medium},
  locationView: {
    paddingTop: 2,
  },
  officeAddressView: {
    flexDirection: "row"
  },
  phoneView: {
    paddingTop: 20,
    flexDirection: "row"
  },
  phoneIcon: {
    height: 20,
    width: 20,
    marginRight: 15
  },
  phoneNumberText: {
    textAlign: "left",
    fontFamily:Fonts.Medium,
    color: appColor.black
  },
  locationImage: {
    height: 20,
    width: 20,
    marginRight: 15
  },
  officeAddress: {
    textAlign: "left",
    fontWeight: "bold",
    color: appColor.black
  },
  officeAddressText: {
    color: appColor.black
  },
  phoneNumbers: {
    color: appColor.black,
    fontFamily:Fonts.Regular
  },
  documentView: {
    paddingTop: 20,
    justifyContent: "space-between"
  },

  buttonView: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    bottom: 20,
    position: "absolute"
  }
});
