import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Alert,
  Text,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
  StatusBar,
  PixelRatio,
  PermissionsAndroid,
  AsyncStorage,
  Picker,
  ToastAndroid,
  TouchableWithoutFeedback,
  Dimensions
} from "react-native";

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
RazorpayCheckout,Toast,image,Fonts,Geolocation,moment,NetInfo,Avatar} from "././common";
import DatePicker from 'react-native-datepicker';


import {Icon,Form,Item} from "native-base";

export default class DriverRegistration extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.aadharImagePickerFunc = this.aadharImagePickerFunc.bind(this);
    this.licenseImagePickerFunc = this.licenseImagePickerFunc.bind(this);
    this.driverImagePickerFunc = this.driverImagePickerFunc.bind(this);
    this.PassbookImagePickerFunc = this.PassbookImagePickerFunc.bind(this);

    this.inputRefs = {
      firstTextInput: null,
      favSport0: null,
      mBankName: null,
      lastTextInput: null
    };
    this.state = {
      name: "",
      emailId: "",
      isSpinnerLoading: false,
      isChecked: false,
      mFatherName: "",
      aadharImage: null,
      mAdharNumber: "",
      licenseImage: null,
      mLicenceNumber: "",
      driverImage: null,
      mDropDownList: [],
      mBeneficiaryName: "",
      mIfscCode: "",
      mAccountNumber: "",
      mPassbookImage: null,
      favSport0: undefined,
      mBankName: undefined,
      favSport2: undefined,
      favSport3: undefined,
      favSport4: "baseball",
      favNumber: undefined,
      mErrorName: false,
      mErrorFatherName: false,
      mErrorBeneficiary: false,
      mErrorIFSC: false,
      mErrorAccount: false,
      mErrorAdhar: false,
      mErrorLicence: false,
      cnfmLicenceNumber:"",
      cnfmErrorLicence:false,
      cnflicenceError:"",
      licenceVerified:false,
      DlRegex:/^[A-Z][A-Z]-\d{13}$/,
      DlRegexNext:/^[A-Z][A-Z]{1}[\w ]+\d{11}$/,
      VoterRegex:/^([a-zA-Z]){3}([0-9]){7}?$/,
      PanRegex: /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/,
      ElectricityRegex:/^[0-9]*$/,
      VoterErrorLicence:false,
      VoterLicenceNumber:'',
      VoterlicenceError:"",
      cnfmVoterErrorLicence:false,
      cnfVoterLicenceNumber:"",
      PanErrorLicence:false,
      PanLicenceNumber:"",
      cnfPanErrorLicence:false,
      cnfPanLicenceNumber:"",
      VoterlicenceVerified:false,
      ElectricityErrorLicence:false,
      ElectricityLicenceNumber:"",
      cnfElectricityErrorLicence:false,
      cnfElectricityLicenceNumber:"",
      PanlicenceVerified:false,
      ElectricitylicenceVerified:false,
      electricity_service:"",
      mErrorElectricityService:false,
      selectOtherBank:false,
      mErrorOtherBankName:false,
      otherBankName:"",
      flag1:false,
      flag2:false,
      flag3:false,
      flag4:false,
      mob_num:"",
      date:"",
      driverImage:""
    }
  }


  async aadharImagePickerFunc() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Alert.alert(
          "Choose image from",
          "",
          [
            {
              text: "Camera",
              onPress: () => {
                ImagePicker.openCamera({
                  width: 300,
                  height: 400,
                  compressImageQuality:0.1,
                  title: "Select Aadhar Photo"
                }).then(images => {
                  this.setState({ aadharImage: images.path });
                });
              }
            },
            {
              text: "Gallery",
              onPress: () => {
                ImagePicker.openPicker({
                  title: "Select Aadhar Photo",
                  compressImageQuality:0.1
                }).then(images => {
                  this.setState({ aadharImage: images.path });
                });
              }
            },
            { text: "Cancel", onPress: () => console.log("OK Pressed") }
          ],
          { cancelable: false }
        );
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async licenseImagePickerFunc() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Alert.alert(
          "Choose image from",
          "",
          [
            {
              text: "Camera",
              onPress: () => {
                ImagePicker.openCamera({
                  width: 300,
                  height: 400
                }).then(images => {
                  this.setState({ licenseImage: images.path });
                });
              }
            },
            {
              text: "Gallery",
              onPress: () => {
                ImagePicker.openPicker({
                  imageLoader: "PICASSO",
                  title: "Select Aadhar Photo",
                  compressImageQuality:0.1
                }).then(images => {
                  this.setState({ licenseImage: images.path });
                });
              }
            },
            { text: "Cancel", onPress: () => console.log("OK Pressed") }
          ],
          { cancelable: false }
        );
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async driverImagePickerFunc() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Alert.alert(
          "Choose image from",
          "",
          [
            {
              text: "Camera",
              onPress: () => {
                ImagePicker.openCamera({
                  width: 300,
                  height: 400,
                  compressImageQuality:0.1
                }).then(images => {
                  this.setState({ driverImage: images.path });
                });
              }
            },
            {
              text: "Gallery",
              onPress: () => {
                ImagePicker.openPicker({
                  imageLoader: "PICASSO",
                  title: "Select Driver's Photo",
                  compressImageQuality:0.1
                }).then(images => {
                  this.setState({ driverImage: images.path });
                });
              }
            },
            { text: "Cancel", onPress: () => console.log("OK Pressed") }
          ],
          { cancelable: false }
        );
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async PassbookImagePickerFunc() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Alert.alert(
          "Choose image from",
          "",
          [
            {
              text: "Camera",
              onPress: () => {
                ImagePicker.openCamera({
                  width: 300,
                  height: 400,
                  compressImageQuality:0.1,
                  title: "Select Passbook Photo"
                }).then(images => {
                  this.setState({ mPassbookImage: images.path });
                });
              }
            },
            {
              text: "Gallery",
              onPress: () => {
                ImagePicker.openPicker({
                  title: "Select Passbook Photo",
                  compressImageQuality:0.1
                }).then(images => {
                  this.setState({ mPassbookImage: images.path });
                  console.log(this.state.mPassbookImage);
                });
              }
            },
            { text: "Cancel", onPress: () => console.log("OK Pressed") }
          ],
          { cancelable: false }
        );
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async componentDidMount() {
    this._isMounted = true;
    console.disableYellowBox = true;
    const driverNumber = await AsyncStorage.getItem('mob_num');
    this.setState({mob_num:driverNumber})

    try {
      var formData = new FormData();
      formData.append("action", "get_bank_list");
      const response = await fetch(
        BaseUrl.url1+"index.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data"
          }
        }
      );

      const responseJson = await response.json();
      console.log("responseJson.data.bank_list",responseJson.data.bank_list)

      if (this._isMounted) {
        this.setState({ mDropDownList: responseJson.data.bank_list });
      }
    } catch (error) {
      console.error(error);
    }
    // this._spinnerLoad();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // async _spinnerLoad() {
  //   // } finally {
  //   //   this.setState({ isSpinnerLoading: false });
  //   // }
  // }

  async _registerUser() {
    if(this.state.licenceVerified===true&&this.state.VoterlicenceVerified===true){
      this.registerAfterPayment()
    } else {
      this.onLicencePay();
    }

  //   if(this.state.licenceVerified===true&&this.state.VoterlicenceVerified===true){
  //     this.registerAfterPayment()
  //   } else {
  //     var options = {
  //       description: "Licence Validation fees",
  //       image: "http://www.drivershaab.com/images/logo.png",
  //       currency: "INR",
  //       key: "rzp_live_7yGYTgHBoZWTLJ",
  //       //amount: this.state.finalPriceAmount * 100, //Amount shoud be integer and in Paise.
  //       amount: 10000,
  //       name: "DriverShaab",
  //       theme: { color: "#744BAC" }
  //     };
  
  //     console.log(options);
  //     RazorpayCheckout.open(options)
  //       .then(data => {
  //         // handle success
  //         console.log(data);
  //         this.onLicencePay();
  //         //Toast.show(`Payment Success: ${data.razorpay_payment_id}`, Toast.SHORT);
  //         Toast.show(`Payment done successfully`, Toast.SHORT);
  //         //this.props.navigation.navigate('RatingScreen')
  //       })
  //       .catch(error => {
  //         // handle failure
  //         NetInfo.getConnectionInfo().then((connectionInfo) => {
  //           if(connectionInfo.type==='none' || connectionInfo.type ==='NONE'){
  //             Toast.show("Please Check Your Internet Connection !",Toast.LONG);
  //           } else {
  //             console.log(error);
  //             this.onLicenceCancel(error.description);
  //             Toast.show(`${error.description}`, Toast.SHORT);
  //           }
  //         });
  //       });
  //     RazorpayCheckout.onExternalWalletSelection(data => {
  //       alert(`External Wallet Selected: ${data.external_wallet} `);
  //     });
  //   }
  }

  async updatePaymentMoney(){
    this.setState({isSpinnerLoading:true})
    try {
      const driverId = await AsyncStorage.getItem("id");
      var formData = new FormData();
      formData.append("action", "update_licence_payment");
      formData.append("driver_id", driverId);
      formData.append("payment_money", 0);
      const response = await fetch(BaseUrl.url4, {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data"
        }
      });
      const responseJson = await response.json();
      if(responseJson.success===true){
        //Toast.show(responseJson.message,Toast.LONG)
        console.log('Payment done',responseJson)
        this.setState({isSpinnerLoading:false})
      } else {
        console.log("Payment response",responseJson)
        this.setState({isSpinnerLoading:false})
        //Toast.show(responseJson.message,Toast.LONG);
      }
    } catch (error) {
      this.setState({isSpinnerLoading:false})
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          console.log(error)
        }
      });
    }
  }

  async onLicenceCancel(obj){
    if(obj=="Payment Cancelled"){
      console.log(obj)
    }
  }

  async checkOtherLicence(){
    this.checkVoterIdVerification().then(()=>{
      return this.finalRegistration()
      //return this.checkPanLicence()
    })
  }

  async checkPanLicence(){
    this.checkPanVerification().then(()=>{
      return this.registerBeforeLastCheck()
    })
  }

  async registerBeforeLastCheck(){
    this.checkElectricityVerification().then(()=>{
      return this.finalRegistration()
    })
  }

  async finalRegistration(){
    if(this.state.licenceVerified===true&&this.state.VoterlicenceVerified===true){
      this.registerAfterPayment()
    } else {
      console.log("Check again")
      //Toast.show("Something went wrong,Please try again!",Toast.LONG)
    }
  }

  async updateLicenceVerification(){
    this.checkDlVerification().then(()=>{
      return this.checkOtherLicence()
    })
  }

  async onLicencePay(){
    console.log("payment success")
    this.updatePaymentMoney().then(()=>{
      return this.updateLicenceVerification()
    })
  }

  async checkDlVerification(){
    this.setState({isSpinnerLoading:true})
    try {
      var formData = new FormData();
      formData.append("action", "check_aadhar_api");
      formData.append("driver_id", await AsyncStorage.getItem("id"));
      formData.append("dl_no", this.state.mLicenceNumber);
      formData.append("dob", this.state.date);
      const response = await fetch(BaseUrl.url4, {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data"
        }
      });
      const responseJson = await response.json();
      if(responseJson.status===true){
        console.log("dl licence ==",responseJson)
        this.setState({isSpinnerLoading:false,licenceVerified:true})
      } else {
        console.log("dl licence",responseJson)
        this.setState({isSpinnerLoading:false,licenceVerified:false})
        Toast.show("Please enter valid driving licence number",Toast.LONG);
      }
    } catch (error) {
      this.setState({isSpinnerLoading:false})
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          alert(error)
        }
      });
    }
  }

  async checkVoterIdVerification(){
    this.setState({isSpinnerLoading:true})
    try {
      var formData = new FormData();
      formData.append("action", "check_aadhar_api");
      formData.append("driver_id", await AsyncStorage.getItem("id"));
      formData.append("epic_no", this.state.VoterLicenceNumber);
      const response = await fetch(BaseUrl.url4, {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data"
        }
      });
      const responseJson = await response.json();
      if(responseJson.status===true){
        console.log("voter licence ==",responseJson)
        this.setState({isSpinnerLoading:false,VoterlicenceVerified:true})
      } else {
        console.log("voter licence",responseJson)
        this.setState({isSpinnerLoading:false,VoterlicenceVerified:false})
        Toast.show("Please enter valid voter id number",Toast.LONG);
      }
    } catch (error) {
      this.setState({isSpinnerLoading:false})
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          alert(error)
        }
      });
    }
  }

  async checkPanVerification(){
    this.setState({isSpinnerLoading:true})
      try {
        var obj = {
          "pan": this.state.PanLicenceNumber,
          "consent": "Y",
          "consent_text": "I am going to check driver licence verification and some another verification also"
        }
        const response = await fetch(
          BaseUrl.url3+"pan-lite",
          {
            
            body: JSON.stringify(obj),
            method: "POST",
            headers: {
              Accept: "application/json",
              qt_api_key:"71100411-d1dc-4d8f-8030-56b3731a79b7",
              qt_agency_id:"07876b7e-40ae-4aed-93ae-4f6092d8f6b2",
              "Content-Type": "application/json",
            }
          }
        );
        const responseJson = await response.json();
        console.log("licence pan",responseJson);
  
        if(responseJson.response_msg==="Success"){
          this.setState({isSpinnerLoading:false,PanlicenceVerified:true})
        } else {
          console.log("licence ==",responseJson.response_msg)
          this.setState({isSpinnerLoading:false,PanlicenceVerified:false})
          //Toast.show("Please enter valid Pan number",Toast.LONG);
        }
      } catch (error) {
        this.setState({isSpinnerLoading:false})
        Toast.show(error,Toast.SHORT);
      }
  }

  async checkElectricityVerification(){
    this.setState({isSpinnerLoading:true})
      try {
        var obj = {
          "consumer_id": this.state.ElectricityLicenceNumber,
          "service_provider":this.state.electricity_service,
          "consent": "Y",
          "consent_text": "I am going to check driver licence verification and some another verification also"
        }
        const response = await fetch(
          BaseUrl.url3+"verify-electricitybill",
          {
            body: JSON.stringify(obj),
            method: "POST",
            headers: {
              Accept: "application/json",
              qt_api_key:"71100411-d1dc-4d8f-8030-56b3731a79b7",
              qt_agency_id:"07876b7e-40ae-4aed-93ae-4f6092d8f6b2",
              "Content-Type": "application/json",
            }
          }
        );
        const responseJson = await response.json();
        console.log("licence electricity",responseJson);
  
        if(responseJson.response_msg==="Success"){
          this.setState({isSpinnerLoading:false,ElectricitylicenceVerified:true})
        } else {
          console.log("licence ==",responseJson.response_msg)
          this.setState({isSpinnerLoading:false,ElectricitylicenceVerified:false})
          //Toast.show("Please enter valid Electricity number",Toast.LONG);
        }
      } catch (error) {
        this.setState({isSpinnerLoading:false})
        Toast.show(error,Toast.SHORT);
      }
  }

  async registerAfterPayment(){
    this.setState({ isSpinnerLoading: true });
    try {
      var formData = new FormData();
      formData.append("action", "registration");
      formData.append("driver_id", await AsyncStorage.getItem("id"));
      formData.append("full_name", this.state.name);
      // formData.append("email_address", this.state.emailId);
      formData.append("aadhar_card_number", this.state.mAdharNumber);
      formData.append("driving_licence_number", this.state.mLicenceNumber);
      formData.append("father_name", this.state.mFatherName);
      formData.append("beneficiary_name", this.state.mBeneficiaryName);
      formData.append("bank_name", this.state.mBankName);
      formData.append("other_bank_name", this.state.otherBankName);
      formData.append("ifsc_code", this.state.mIfscCode);
      formData.append("account_number", this.state.mAccountNumber);

      formData.append("aadhar_card_picture", {
        uri: this.state.aadharImage,
        type: "image/jpeg",
        name: this.state.aadharImage.substring(
          this.state.aadharImage.lastIndexOf("/") + 1,
          this.state.aadharImage.length
        )
      });
      formData.append("driving_licence_picture", {
        uri: this.state.licenseImage,
        type: "image/jpeg",
        name: this.state.licenseImage.substring(
          this.state.licenseImage.lastIndexOf("/") + 1,
          this.state.licenseImage.length
        )
      });
      formData.append("driver_image", {
        uri: this.state.driverImage,
        type: "image/jpeg",
        name: this.state.driverImage.substring(
          this.state.driverImage.lastIndexOf("/") + 1,
          this.state.driverImage.length
        )
      });

      formData.append("passbook_image", {
        uri: this.state.mPassbookImage,
        type: "image/jpeg",
        name: this.state.mPassbookImage.substring(
          this.state.mPassbookImage.lastIndexOf("/") + 1,
          this.state.mPassbookImage.length
        )
      });
      console.log("formdata driver reg==",formData)
      const response = await fetch(
        BaseUrl.url1+"index.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data"
          }
        }
      );

      const responseJson = await response.json();
      //console.log('responseJson register',responseJson)
      // if (
      //   responseJson.data.driver_id === AsyncStorage.getItem("id")
      // ) {
        Toast.show("Driver Registered successfully", Toast.LONG);
        this.props.navigation.replace("drivernotverified_screen");
        this.setState({ isSpinnerLoading: false });
      // }
    } catch (error) {
      this.setState({isSpinnerLoading:false})
      alert(error);
    } finally {
      this.setState({ isSpinnerLoading: false });
    }
  }

  _fetchCurrentLocationToRedirect() {
    this.setState({
      isSpinnerLoading: true
    });
    Geolocation.getCurrentPosition(
      position => {
        this.setState({
          isSpinnerLoading: false
        });

        this.props.navigation.replace("schedule_booking", {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
      },
      error => Toast.show(error.message, Toast.LONG),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }

  async _createLoginSession() {
    try {
      await AsyncStorage.setItem("isLoggedIn", "true");
      await AsyncStorage.setItem(
        "userId",AsyncStorage.getItem("id"));
    } catch (error) {
      console.log(error);
    }
  }

  async selectBank(value){
    if(value=="other"){
      this.setState({mBankName: value, selectOtherBank:true,otherBankName
      :""});
    } else {
      this.setState({mBankName: value,selectOtherBank:false,otherBankName:""});
    }
    console.log(value);
  }

  async enterBankName(text){
    this.setState({ otherBankName: text.replace(/[^a-zA-Z ]/g,"")})
  }

  async ondobChange(val){
    const dateFormat = moment(moment(val, 'DD/MM/YYYY')).format('DD-MM-YYYY');
    this.setState({date:dateFormat})
  }

  async selectDriverImage(){
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Alert.alert(
          "Choose image from",
          "",
          [
            {
              text: "Camera",
              onPress: () => {
                ImagePicker.openCamera({
                  width: 300,
                  height: 400,
                  compressImageQuality:0.1,
                  title: "Select Your Photo"
                }).then(images => {
                  this.setState({ driverImage: images.path });
                });
              }
            },
            {
              text: "Gallery",
              onPress: () => {
                ImagePicker.openPicker({
                  title: "Select Your Photo",
                  compressImageQuality:0.1
                }).then(images => {
                  this.setState({ driverImage: images.path });
                  console.log(this.state.driverImage);
                });
              }
            },
            { text: "Cancel", onPress: () => console.log("OK Pressed") }
          ],
          { cancelable: false }
        );
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  render() {
    const placeholder = {
      label: "Select Bank Name...",
      value: null,
      color: "#000"
    };

    return (
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        <View style={{ flex: 1, backgroundColor: "#fff" }}>
        <StatusBar backgroundColor="#19376c" barStyle="light-content" />
          <View style={styles.header}>
            <Text style={styles.description}>DRIVER REGISTRATION</Text>
          </View>

          <View style={{ height: 1, backgroundColor: "#636363" }} />
          <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
            <View style={styles.container}>
              <Spinner
                visible={this.state.isSpinnerLoading}
                style={styles.spinnerStyle}
                color={appColor.spinnerColor}
                overlayColor="rgba(0, 0, 0, 0.55)"
              />
              <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ alignItems: "center", flex: 1 }}>
                  {/* <TouchableOpacity style={{justifyContent:"center",marginVertical:20}} onPress={()=>this.selectDriverImage()}>
                  <Avatar.Image size={100} source={image.userImage} style={{backgroundColor:"transparent"}}/>
                  </TouchableOpacity> */}
                  <View style={{ alignItems: "flex-start", marginRight: 180 }}>
                    <Text style={{ color: "#000",fontFamily:Fonts.Regular}}>Basic Information</Text>
                  </View>

                 

                  <View style={styles.sectionStyle}>
                    <InputText
                          placeholder="Driver Name"
                          mode="outlined"
                          error={this.state.mErrorName}
                          underlineColorAndroid="transparent"
                          textContentType={"name"}
                          onBlur={() =>
                            this.state.name === ""
                              ? this.setState(() => ({
                                  nameError: "Driver name required",
                                  mErrorName: true
                                }))
                              : this.setState(() => ({
                                  nameError: null,
                                  mErrorName: false
                                }))
                          }
                          onChangeText={text => this.setState({ name: text })}
                      />
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.nameError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left"
                        }}
                      >
                        {this.state.nameError}
                      </Text>
                    )}
                  </View>

                  {/* <View style={styles.sectionStyle}>
                    <TextInput
                      style={{ flex: 1 }}
                      placeholder="Your Email Id"
                      underlineColorAndroid="transparent"
                      mode="outlined"
                      onChangeText={text => this.setState({ emailId: text })}
                      textContentType={"emailAddress"}
                      keyboardType={"email-address"}
                    />
                  </View> */}

                  <View style={styles.sectionStyle}>
                  <InputText
                         mode="outlined"
                         placeholder="Father's Name"
                         error={this.state.mErrorFatherName}
                         underlineColorAndroid="transparent"
                         onChangeText={text =>
                           this.setState({ mFatherName: text })
                         }
                         onBlur={() =>
                           this.state.mFatherName === ""
                             ? this.setState(() => ({
                                 fatherError: "father name required",
                                 mErrorFatherName: true
                               }))
                             : this.setState(() => ({
                                 fatherError: null,
                                 mErrorFatherName: false
                               }))
                         }
                    />
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.fatherError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left"
                        }}
                      >
                        {this.state.fatherError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                         editable={false}
                         disabled={true}
                         maxLength={10}
                         mode="outlined"
                         placeholder={
                          this.state.mob_num + ""
                         }
                         underlineColorAndroid="transparent"
                    />
                  </View>

                  <View style={[styles.sectionStyle,{marginTop:38}]}>
                    <InputText
                          mode="outlined"
                          placeholder="Beneficiary Name"
                          error={this.state.mErrorBeneficiary}
                          underlineColorAndroid="transparent"
                          onChangeText={text =>
                            this.setState({ mBeneficiaryName: text })
                          }
                          onBlur={() =>
                            this.state.mBeneficiaryName === ""
                              ? this.setState(() => ({
                                  beneficiaryError: "Account Holder name required",
                                  mErrorBeneficiary: true
                                }))
                              : this.setState(() => ({
                                  beneficiaryError: null,
                                  mErrorBeneficiary: false
                                }))
                          }
                      />
                  </View>
                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.beneficiaryError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left"
                        }}
                      >
                        {this.state.beneficiaryError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.sectionStyle}>
                    <RNPickerSelect
                      placeholder={placeholder}
                      items={this.state.mDropDownList}
                      onValueChange={value => {
                        this.selectBank(value)
                      }}
                      style={pickerSelectStyles}
                      value={this.state.mBankName}
                      useNativeAndroidPickerStyle={false}
                      ref={el => {
                        this.inputRefs.mBankName = el;
                      }}
                    />
                  </View>

                 {this.state.selectOtherBank&&(
                 <View style={styles.sectionStyle}>
                  <InputText
                     placeholder="Other Bank Name"
                     mode="outlined"
                     error={this.state.mErrorName}
                     underlineColorAndroid="transparent"
                     textContentType={"name"}
                     value={this.state.otherBankName}
                     onBlur={() =>
                       this.state.otherBankName === ""
                         ? this.setState(() => ({
                             otherBanknameError: "Other bank name required",
                             mErrorOtherBankName: true
                           }))
                         : this.setState(() => ({
                            otherBanknameError: null,
                            mErrorOtherBankName: false
                           }))
                     }
                     onChangeText={text => this.enterBankName(text)}
                   />
                </View>
                 )}

                 {this.state.selectOtherBank&&(
                    <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.otherBanknameError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left"
                        }}
                      >
                        {this.state.otherBanknameError}
                      </Text>
                    )}
                  </View>
                 )}

                  <View style={styles.sectionStyle}>
                  <InputText
                         mode="outlined"
                         placeholder="IFSC Code"
                         error={this.state.mErrorIFSC}
                         underlineColorAndroid="transparent"
                         onChangeText={text => this.setState({ mIfscCode: text })}
                         onBlur={() =>
                           this.state.mIfscCode === ""
                             ? this.setState(() => ({
                                 ifscError: "IFSC Code required",
                                 mErrorIFSC: true
                               }))
                             : this.setState(() => ({
                                 ifscError: null,
                                 mErrorIFSC: false
                               }))
                         }
                    />
                  </View>
                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.ifscError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left"
                        }}
                      >
                        {this.state.ifscError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                           mode="outlined"
                           placeholder="Account Number"
                           keyboardType={"number-pad"}
                           error={this.state.mErrorAccount}
                           underlineColorAndroid="transparent"
                           onChangeText={text =>
                             this.setState({ mAccountNumber: text })
                           }
                           onBlur={() =>
                             this.state.mAccountNumber === ""
                               ? this.setState(() => ({
                                   accountError: "Account Number required",
                                   mErrorAccount: true
                                 }))
                               : this.setState(() => ({
                                   accountError: null,
                                   mErrorAccount: false
                                 }))
                           }
                    />
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.accountError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left"
                        }}
                      >
                        {this.state.accountError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.imageUploadView}>
                    <TouchableOpacity
                      style={styles.submitButtonStyle}
                      onPress={this.PassbookImagePickerFunc.bind(this)}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between"
                        }}
                      >
                        <Text style={styles.submitTextStyle}>
                          Passbook Image
                        </Text>

                        <Image
                          source={image.addImage}
                          style={styles.chosenImage}
                        />
                      </View>
                    </TouchableOpacity>

                    <View style={{ justifyContent: "center", margin: 5 }}>
                      <Image
                        source={{ uri: this.state.mPassbookImage }}
                        style={{ width: 50, height: 40 }}
                      />
                    </View>
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                        placeholder="Adhar Number"
                        error={this.state.mErrorAdhar}
                        underlineColorAndroid="transparent"
                        mode="outlined"
                        onChangeText={text =>
                          this.setState({ mAdharNumber: text })
                        }
                        maxLength={12}
                        textContentType={"telephoneNumber"}
                        keyboardType={"number-pad"}
                        onBlur={() =>
                          this.state.mAdharNumber === "" ||
                          this.state.mAdharNumber.length < 12
                            ? this.setState(() => ({
                                adhaarError:
                                  "Adhaar Number required & must be of 12 digits",
                                mErrorAdhar: true
                              }))
                            : this.setState(() => ({
                                adhaarError: null,
                                mErrorAdhar: false
                              }))
                        }
                    />
                  </View>
                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.adhaarError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left"
                        }}
                      >
                        {this.state.adhaarError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.imageUploadView}>
                    <TouchableOpacity
                      style={styles.submitButtonStyle}
                      onPress={this.aadharImagePickerFunc.bind(this)}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between"
                        }}
                      >
                        <Text style={styles.submitTextStyle}>
                          Upload Adhar Image
                        </Text>
                        
                        <Image
                          source={image.addImage}
                          style={styles.chosenImage}
                        />
                      </View>
                    </TouchableOpacity>

                    <View style={{ justifyContent: "center", margin: 5 }}>
                      <Image
                        source={{ uri: this.state.aadharImage }}
                        style={{ width: 50, height: 40 }}
                      />
                    </View>
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                      placeholder="DL Number"
                      underlineColorAndroid="transparent"
                      error={this.state.mErrorLicence}
                      mode="outlined"
                      maxLength={17}
                      editable={this.state.licenceVerified==true?false:true}
                      onChangeText={text =>
                        this.setState({ mLicenceNumber: text})
                      }
                      textContentType={"telephoneNumber"}
                      keyboardType="default"
                      onBlur={() =>
                        this.state.mLicenceNumber === ""||this.state.mLicenceNumber.length>17||this.state.mLicenceNumber.length<15
                          ? this.setState(() => ({
                              licenceError: this.state.mLicenceNumber === ""?"Driving Licence required":this.state.mLicenceNumber.length>17||this.state.mLicenceNumber.length<15?"Enter valid driving licence.(e.g. \nMH-0123456789012/MH0 12345678912)":"",
                              mErrorLicence: true
                            }))
                          : this.setState(() => ({
                              licenceError: null,
                              mErrorLicence: false
                            }))
                      }
                    />
                    {this.state.licenceVerified&&(
                      <Icon type="FontAwesome5" name="check-circle" style={{color:"green",justifyContent:"center", marginLeft:5}}/>
                    )}
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.licenceError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left",
                          marginTop:5
                        }}
                      >
                        {this.state.licenceError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                        placeholder="Confirm DL Number"
                        underlineColorAndroid="transparent"
                        error={this.state.cnfmErrorLicence}
                        mode="outlined"
                        maxLength={17}
                        onChangeText={text =>
                          this.setState({ cnfmLicenceNumber: text })
                        }
                        textContentType={"telephoneNumber"}
                        keyboardType="default"
                        onBlur={() =>
                          this.state.cnfmLicenceNumber != this.state.mLicenceNumber ||this.state.cnfmLicenceNumber==""
                            ? this.setState(() => ({
                                cnflicenceError: "Confirm DL should be same as DL Number",
                                cnfmErrorLicence: this.state.cnfmLicenceNumber === this.state.mLicenceNumber?false:true,
                                flag1:true
                              }))
                            : this.setState(() => ({
                                cnflicenceError: null,
                                cnfmErrorLicence: false
                              }))
                        }
                    />
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {
                      this.state.cnfmLicenceNumber != this.state.mLicenceNumber&&this.state.flag1==true?
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left",
                          marginTop:5
                        }}
                      >
                        {this.state.cnfmLicenceNumber != this.state.mLicenceNumber?"Confirm DL should be same as DL Number":this.state.cnflicenceError?this.state.cnflicenceError:""}
                      </Text>:null
                    }
                  </View>

              <Form >
                <Item regular success style={[styles.FormItemstyle,{justifyContent:'space-between', height:55}]}>
                <DatePicker
                    style={{width:Dimensions.get("window").width/1.3}}
                    date={this.state.date} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    placeholder="Date of Birth"
                    format="DD/MM/YYYY"
                    minDate="01/01/1948"
                    maxDate={new Date()}
                    confirmBtnText="Confirm"
                    onDateChange={val=>this.ondobChange(val)}
                    value={this.state.date}
                    cancelBtnText="Cancel"
                    showIcon={true}
                    customStyles={{
                      // dateIcon: {
                      //   position: 'absolute',
                      //   left: 0,
                      //   top: 4,
                      //   marginLeft: 0
                      // },
                      dateInput: {
                        marginLeft: 10,
                        alignItems:'flex-start',
                        fontFamily:Fonts.Regular,
                        borderColor: appColor.transparent
                      },
                      placeholderText:{
                        color: appColor.darkGrey,
                        fontFamily:Fonts.Regular
                      },
                      dateText:{
                        fontFamily:Fonts.Regular
                      }
                    }}
                />
              
             {/* <Image
                source={image.location}
                resizeMode='contain'
                style={{ width: 20, height: 20,marginBottom:5,right:10 }}
                /> */}
              </Item>
             </Form>
                  
                  {/* Voter Id */}
                  <View style={styles.sectionStyle}>
                  <InputText
                      placeholder="Voter Id Number"
                      underlineColorAndroid="transparent"
                      error={this.state.VoterErrorLicence}
                      mode="outlined"
                      maxLength={10}
                      editable={this.state.VoterlicenceVerified==true?false:true}
                      onChangeText={text =>
                        this.setState({ VoterLicenceNumber: text })
                      }
                      textContentType={"telephoneNumber"}
                      keyboardType="default"
                      onBlur={() =>
                        this.state.VoterLicenceNumber === ""||this.state.VoterRegex.test(this.state.VoterLicenceNumber)===false
                          ? this.setState(() => ({
                              VoterlicenceError: this.state.VoterLicenceNumber === ""?"Voter Id required":this.state.VoterRegex.test(this.state.VoterLicenceNumber)===false?"Enter valid Voter Id.(e.g. ABC1234567)":"",
                              VoterErrorLicence: true
                            }))
                          : this.setState(() => ({
                              VoterlicenceError: null,
                              VoterErrorLicence: false
                            }))
                      }
                    />
                    {this.state.VoterlicenceVerified&&(
                      <Icon type="FontAwesome5" name="check-circle" style={{color:"green",justifyContent:"center", marginLeft:5}}/>
                    )}
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.VoterlicenceError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left",
                          marginTop:5
                        }}
                      >
                        {this.state.VoterlicenceError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                        placeholder="Confirm Voter Id Number"
                        underlineColorAndroid="transparent"
                        error={this.state.cnfmVoterErrorLicence}
                        mode="outlined"
                        maxLength={10}
                        onChangeText={text =>
                          this.setState({ cnfVoterLicenceNumber: text })
                        }
                        textContentType={"telephoneNumber"}
                        keyboardType="default"
                        onBlur={() =>
                          this.state.cnfVoterLicenceNumber != this.state.VoterLicenceNumber ||this.state.cnfVoterLicenceNumber==""
                            ? this.setState(() => ({
                                cnfVoterlicenceError: "Confirm Voter Id should be same as Voter Id",
                                cnfmVoterErrorLicence: this.state.cnfVoterLicenceNumber === this.state.VoterLicenceNumber?false:true,
                                flag2:true
                              }))
                            : this.setState(() => ({
                                cnfVoterlicenceError: null,
                                cnfmVoterErrorLicence: false
                              }))
                        }
                    />
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {
                      this.state.cnfVoterLicenceNumber != this.state.VoterLicenceNumber&&this.state.flag2==true? <Text
                      style={{
                        color: "#8B0000",
                        justifyContent: "flex-start",
                        alignContent: "flex-start",
                        alignItems: "flex-start",
                        alignSelf: "flex-start",
                        textAlign: "left",
                        marginTop:5
                      }}
                    >
                      {this.state.cnfVoterLicenceNumber != this.state.VoterLicenceNumber?"Confirm Voter Id should be same as Voter Id":this.state.cnfVoterlicenceError?this.state.cnfVoterlicenceError:""}
                    </Text>:null
                    }
                  </View>

                {/* Pan Number */}
                {/* <View style={styles.sectionStyle}>
                <InputText
                    placeholder="Pan Number"
                    underlineColorAndroid="transparent"
                    error={this.state.PanErrorLicence}
                    mode="outlined"
                    maxLength={10}
                    onChangeText={text =>
                      this.setState({ PanLicenceNumber: text })
                    }
                    textContentType={"telephoneNumber"}
                    keyboardType="default"
                    onBlur={() =>
                      this.state.PanLicenceNumber === ""||this.state.PanRegex.test(this.state.PanLicenceNumber)===false
                        ? this.setState(() => ({
                            PanlicenceError: this.state.PanLicenceNumber === ""?"Pan Number required":this.state.PanRegex.test(this.state.PanLicenceNumber)===false?"Enter valid Pan no.(e.g. ABCDE1234D)":"",
                            PanErrorLicence: true,
                            
                          }))
                        : this.setState(() => ({
                            PanlicenceError: null,
                            PanErrorLicence: false
                          }))
                    }
                  />
                    {this.state.PanlicenceVerified&&(
                      <Icon type="FontAwesome5" name="check-circle" style={{color:"green",justifyContent:"center", marginLeft:5}}/>
                    )}
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.PanlicenceError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left",
                          marginTop:5
                        }}
                      >
                        {this.state.PanlicenceError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                        placeholder="Confirm Pan Number"
                        underlineColorAndroid="transparent"
                        error={this.state.cnfPanErrorLicence}
                        mode="outlined"
                        maxLength={10}
                        onChangeText={text =>
                          this.setState({ cnfPanLicenceNumber: text })
                        }
                        textContentType={"telephoneNumber"}
                        keyboardType="default"
                        onBlur={() =>
                          this.state.cnfPanLicenceNumber != this.state.PanLicenceNumber ||this.state.cnfPanLicenceNumber==""
                            ? this.setState(() => ({
                                cnfPanlicenceError: "Confirm Pan no should be same as Pan no.",
                                cnfPanErrorLicence: this.state.cnfPanLicenceNumber === this.state.PanLicenceNumber?false:true,
                                flag3:true
                              }))
                            : this.setState(() => ({
                                cnfPanlicenceError: null,
                                cnfPanErrorLicence: false
                              }))
                        }
                    />
                  </View>
               <View
                  style={{
                    marginTop:10,
                    alignContent: "flex-start",
                    justifyContent: "flex-start",
                    alignItems: "flex-start",
                    alignSelf: "flex-start"
                  }}
                >
              {
                  this.state.cnfPanLicenceNumber != this.state.PanLicenceNumber&&this.state.flag3?
                  <Text
                  style={{
                    color: "#8B0000",
                    justifyContent: "flex-start",
                    alignContent: "flex-start",
                    alignItems: "flex-start",
                    alignSelf: "flex-start",
                    textAlign: "left",
                    marginTop:5
                  }}
                >
                  {this.state.cnfPanLicenceNumber != this.state.PanLicenceNumber?"Confirm Pan no should be same as Pan no.":this.state.cnfPanlicenceError?this.state.cnfPanlicenceError:""}
                </Text>:null
              }
                </View>
                 */}
                    
                 

                  {/* Electricity Bill */}
{/* 
                  <View style={styles.sectionStyle}>
                  <InputText
                        placeholder="Electricity Bill Number."
                        underlineColorAndroid="transparent"
                        error={this.state.ElectricityErrorLicence}
                        mode="outlined"
                        maxLength={20}
                        onChangeText={text =>
                          this.setState({ ElectricityLicenceNumber: text })
                        }
                        textContentType={"telephoneNumber"}
                        keyboardType="phone-pad"
                        onBlur={() =>
                          this.state.ElectricityLicenceNumber === ""||this.state.ElectricityRegex.test(this.state.ElectricityLicenceNumber)===false
                            ? this.setState(() => ({
                                ElectricitylicenceError: this.state.ElectricityLicenceNumber === ""?"Electricity bill required":this.state.ElectricityRegex.test(this.state.ElectricityLicenceNumber)===false?"Enter valid Electricity bill no.(e.g. 12345...)":"",
                                ElectricityErrorLicence: true
                              }))
                            : this.setState(() => ({
                                ElectricitylicenceError: null,
                                ElectricityErrorLicence: false
                              }))
                        }
                    />
                    {this.state.ElectricitylicenceVerified&&(
                      <Icon type="FontAwesome5" name="check-circle" style={{color:"green",justifyContent:"center", marginLeft:5}}/>
                    )}
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.ElectricitylicenceError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left",
                          marginTop:5
                        }}
                      >
                        {this.state.ElectricitylicenceError}
                      </Text>
                    )}
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                        placeholder="Confirm Electricity Bill Number"
                        underlineColorAndroid="transparent"
                        error={this.state.cnfElectricityErrorLicence}
                        mode="outlined"
                        maxLength={20}
                        onChangeText={text =>
                          this.setState({ cnfElectricityLicenceNumber: text })
                        }
                        textContentType={"telephoneNumber"}
                        keyboardType="phone-pad"
                        onBlur={() =>
                          this.state.cnfElectricityLicenceNumber != this.state.ElectricityLicenceNumber ||this.state.cnfPanLicenceNumber==""
                            ? this.setState(() => ({
                                cnfElectricitylicenceError: "Confirm Electricity bill number should be \nsame as Electricity number.",
                                cnfElectricityErrorLicence: this.state.cnfElectricityLicenceNumber === this.state.ElectricityLicenceNumber?false:true,
                                flag4:true
                              }))
                            : this.setState(() => ({
                                cnfElectricitylicenceError: null,
                                cnfElectricityErrorLicence: false
                              }))
                        }
                    />
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {
                      this.state.cnfElectricityLicenceNumber != this.state.ElectricityLicenceNumber && this.state.flag4? <Text
                      style={{
                        color: "#8B0000",
                        justifyContent: "flex-start",
                        alignContent: "flex-start",
                        alignItems: "flex-start",
                        alignSelf: "flex-start",
                        textAlign: "left",
                        marginTop:5
                      }}
                    >
                      {this.state.cnfElectricityLicenceNumber != this.state.ElectricityLicenceNumber?"Confirm Electricity bill number should be \nsame as Electricity number.":this.state.cnfElectricitylicenceError?this.state.cnfElectricitylicenceError:""}
                    </Text>:null
                    }
                  </View>

                  <View style={styles.sectionStyle}>
                  <InputText
                        placeholder="Electricity Service Provider"
                        mode="outlined"
                        error={this.state.mErrorElectricityService}
                        underlineColorAndroid="transparent"
                        textContentType={"name"}
                        onBlur={() =>
                          this.state.electricity_service === ""
                            ? this.setState(() => ({
                                ElectricityServiceError: "Electricity Service Provider required",
                                mErrorElectricityService: true
                              }))
                            : this.setState(() => ({
                               ElectricityServiceError: null,
                               mErrorElectricityService: false
                              }))
                        }
                        onChangeText={text => this.setState({ electricity_service: text })}
                    />
                  </View>

                  <View
                    style={{
                      marginTop: 10,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    {!!this.state.ElectricityServiceError && (
                      <Text
                        style={{
                          color: "#8B0000",
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          alignItems: "flex-start",
                          alignSelf: "flex-start",
                          textAlign: "left"
                        }}
                      >
                        {this.state.ElectricityServiceError}
                      </Text>
                    )}
                  </View> */}


                  <View style={styles.imageUploadView}>
                    <TouchableOpacity
                      style={styles.submitButtonStyle}
                      onPress={this.licenseImagePickerFunc.bind(this)}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between"
                        }}
                      >
                        <Text style={styles.submitTextStyle}>
                          Upload License Image
                        </Text>

                        <Image
                          source={image.addImage}
                          style={styles.chosenImage}
                        />
                      </View>
                    </TouchableOpacity>

                    <View style={{ justifyContent: "center", margin: 5 }}>
                      <Image
                        source={{ uri: this.state.licenseImage }}
                        style={{ width: 50, height: 40 }}
                      />
                    </View>
                  </View>

                  <View style={styles.imageUploadView}>
                    <TouchableOpacity
                      style={styles.submitButtonStyle}
                      onPress={this.driverImagePickerFunc.bind(this)}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between"
                        }}
                      >
                        <Text style={styles.submitTextStyle}>
                          Upload Driver Image
                        </Text>

                        <Image
                          source={image.addImage}
                          style={styles.chosenImage}
                        />
                      </View>
                    </TouchableOpacity>

                    <View style={{ justifyContent: "center", margin: 5 }}>
                      <Image
                        source={{ uri: this.state.driverImage }}
                        style={{ width: 50, height: 40 }}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      marginBottom: 120,
                      flexDirection: "row",
                      marginTop: 20,
                      alignContent: "flex-start",
                      justifyContent: "flex-start",
                      alignItems: "flex-start",
                      alignSelf: "flex-start"
                    }}
                  >
                    <View>
                      <CheckBox
                        onClick={() => {
                          this.setState({ isChecked: !this.state.isChecked });
                        }}
                        isChecked={this.state.isChecked}
                        checkBoxColor={appColor.Blue}
                      />
                    </View>

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("TermsService")}>
                      <Text style={{ textDecorationLine: "underline",fontFamily:Fonts.Regular,textAlign:"justify"}}>
                        Please Agree with terms & conditions
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </ScrollView>

              <View />

              <TouchableOpacity
                onPress={this._registerUser.bind(this)}
                disabled={
                  this.state.name.length === 0 ||
                  this.state.otherBankName.length === 0 && this.state.selectOtherBank===true ||
                  this.state.mAdharNumber.length < 12 ||
                  this.state.mLicenceNumber.length < 1 ||
                  this.state.VoterLicenceNumber.length < 1 ||
                  //this.state.PanLicenceNumber.length < 1 ||
                  //this.state.ElectricityLicenceNumber.length < 1 ||
                  this.state.licenseImage === null ||
                  this.state.aadharImage === null ||
                  this.state.driverImage === null ||
                  this.state.mPassbookImage === null ||
                  this.state.cnfmLicenceNumber != this.state.mLicenceNumber ||
                  this.state.cnfVoterLicenceNumber != this.state.VoterLicenceNumber ||
                  //this.state.cnfPanLicenceNumber != this.state.PanLicenceNumber ||
                  //this.state.cnfElectricityLicenceNumber != this.state.ElectricityLicenceNumber ||
                  this.state.mLicenceNumber.length>17||
                  this.state.mLicenceNumber.length<15||
                  this.state.VoterRegex.test(this.state.VoterLicenceNumber)===false||
                  //this.state.PanRegex.test(this.state.PanLicenceNumber)===false||
                  //this.state.ElectricityRegex.test(this.state.ElectricityLicenceNumber)===false||
                  this.state.mFatherName.length === 0 ||
                  this.state.date==""||
                  this.state.date==undefined||
                  //this.state.electricity_service.length === 0||
                  this.state.isChecked === false
                    ? true
                    : false
                }
                style={[
                  styles.verifyButton,
                  {
                    backgroundColor:
                  this.state.name.length === 0 ||
                  this.state.otherBankName.length === 0 && this.state.selectOtherBank===true ||
                  this.state.mAdharNumber.length < 12 ||
                  this.state.mLicenceNumber.length < 1 ||
                  this.state.VoterLicenceNumber.length < 1 ||
                  //this.state.PanLicenceNumber.length < 1 ||
                  //this.state.ElectricityLicenceNumber.length < 1 ||
                  this.state.licenseImage === null ||
                  this.state.aadharImage === null ||
                  this.state.driverImage === null ||
                  this.state.mPassbookImage === null ||
                  this.state.cnfmLicenceNumber != this.state.mLicenceNumber ||
                  this.state.cnfVoterLicenceNumber != this.state.VoterLicenceNumber ||
                  //this.state.cnfPanLicenceNumber != this.state.PanLicenceNumber ||
                  //this.state.cnfElectricityLicenceNumber != this.state.ElectricityLicenceNumber ||
                  this.state.mLicenceNumber.length>17||
                  this.state.mLicenceNumber.length<15||
                  this.state.VoterRegex.test(this.state.VoterLicenceNumber)===false||
                  //this.state.PanRegex.test(this.state.PanLicenceNumber)===false||
                  //this.state.ElectricityRegex.test(this.state.ElectricityLicenceNumber)===false||
                  this.state.mFatherName.length === 0 ||
                  this.state.date==""||
                  this.state.date==undefined||
                  //this.state.electricity_service.length === 0||
                  this.state.isChecked === false
                        ? "#dedede"
                        : appColor.Blue
                  }
                ]}
              >
                <Text style={styles.buttonText}>{"Register"}</Text>
                {/* <Text style={styles.buttonText}>{this.state.licenceVerified==true&&this.state.VoterlicenceVerified==true?"Register":"Pay & Continue"}</Text> */}
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </View>
        </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    margin: 10
  },
  imageUploadView: {
    flexDirection: "row",
    marginTop: 20,
    left: 0,
    alignContent: "flex-start",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    alignSelf: "flex-start"
  },
  chosenImage: {
    alignItems: "center",
    height: 20,
    width: 20,
    marginLeft: 5
  },
  sectionStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    height: 40,
    marginTop: 30
  },
  sectionStyle1: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    height: 40,
    marginTop: 30
  },
  imageStyle: {
    padding: 10,
    marginLeft: 10,
    marginRight: 5,
    marginTop: 5,
    height: 25,
    width: 25,
    resizeMode: "stretch",
    alignItems: "center"
  },
  verifyButton: {
    position: "absolute",
    marginTop: 20,
    height: 50,
    bottom: 10,
    left: 10,
    right: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  buttonText: {
    color: "#fff",
    fontFamily:Fonts.Medium,
    fontSize: 16,
  },
  textInputUnderlineStyle: {
    height: StyleSheet.hairlineWidth,
    width: "90%",
    marginTop: 3,
    marginBottom: 5,
    backgroundColor: "#000"
  },
  header: {
    paddingTop: 20,
    padding: 15,
    backgroundColor: appColor.HeaderBlue
  },
  description: {
    fontSize: 16,
    color: "#fff",
    textAlign: "center",
    fontFamily:Fonts.Medium
  },
  tabsContainerStyle: {
    padding: 5,
    borderRadius: 5
  },
  tabStyle: {
    borderColor: "#E0E0E0"
  },
  tabTextStyle: {
    color: "#616161"
  },
  activeTabStyle: {
    backgroundColor: "#F5F5F5"
  },
  activeTabTextStyle: {
    color: appColor.Blue,
    fontWeight: "bold"
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150
  },
  submitButtonStyle: {
    paddingTop: 15,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 15,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: appColor.Blue
  },
  submitTextStyle: {
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
    color: appColor.Blue
  },
  FormItemstyle:{
    width:"100%",
    marginTop:25,
    borderRadius:5,
    borderColor:appColor.darkGrey
  }
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "#000",
    borderRadius: 1,
    height: 40,
    color: "#000",
    paddingRight: 30,
    width: 300
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: "#000",
    borderRadius: 3,
    color: "#000",
    paddingRight: 30,
    width: 300,
    height: 55
  },
});
