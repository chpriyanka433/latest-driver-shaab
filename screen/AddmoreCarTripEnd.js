import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right,Form,Item,Input,ActionSheet} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  PermissionsAndroid,
  ToastAndroid,
  BackHandler,
  Dimensions,
  Platform
} from 'react-native';

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,moment,image,Geolocation,Fonts,RestApiKey,ClientId,ClientSecret,grantType,NetInfo} from "././common";

import { ScrollView } from 'react-native-gesture-handler';
import BackgroundTimer from 'react-native-background-timer';


var BUTTONS = ["Camera", "Cancel"];
var CANCEL_INDEX = 1;

const googleMaps_apiKey = "AIzaSyComH3uVSYHWeEtzyEByzgi76SOseHvSG0";
export default class AddmoreCarTripEnd extends Component {

  constructor(props){
    super(props);
    this.image_1 = this.image_1.bind(this);
    this.image_2 = this.image_2.bind(this);
    this.image_3 = this.image_3.bind(this);
    this.image_4 = this.image_4.bind(this);
    this.image_5 = this.image_5.bind(this);

    this.state ={
      fileSource:"",
      imageName:'',
      path:"",
      imageType:"",
      imageArray:[],
      image1:null,
      image2:null,
      image3:null,
      image4:null,
      image5:null,
      lat:"",
      long:"",
      isLoading:false,
      BookingIDD:"",
      vechileNo:"",
      endKM:""
    }
  }

    async componentDidMount(){
      this.setState({image1:null,image2:null,image3:null,image4:null,image5:null})
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (!granted) {
          return;
        } else {
          await Geolocation.getCurrentPosition(
            (position) => {
              console.log("position",position.coords.latitude)
              this.updateGeoAddress(position.coords.latitude,position.coords.longitude)
            });
        }
      } else {
        console.log("ios platform")
      }
      this.RideSessionDetails()
    }

    async RideSessionDetails() {
      try {
        const driverId = await AsyncStorage.getItem('driverId');
        var formData = new FormData();
        formData.append("action", "get_running_ride");
        formData.append("driver_id", driverId);
        const response = await fetch(BaseUrl.url2+"service.php", {
          body: formData,
          method: "POST",
          headers: {
            "Accept": "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        });
        const responseJson = await response.json();
        if(responseJson.success==true){
          AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id);
          this.setState({BookingIDD:responseJson.data.booking_id,vechileNo:responseJson.data.vehicle_number})
          if(responseJson.data.status == 'initiated'){
             AsyncStorage.setItem("initiateFlag",'flaged')
          }
          else if(responseJson.data.status == 'ride_started'){
            this.setState({BookingIDD:responseJson.data.booking_id,vechileNo:responseJson.data.vehicle_number})
            console.log("responseJson.data.vehicle_number",responseJson.data.vehicle_number)
             AsyncStorage.setItem("initiateFlag",'flaged')
             AsyncStorage.setItem("startTipFlag",'flaged')
          }
        }
        else{
          AsyncStorage.setItem("ride_history_id","")
          AsyncStorage.setItem("startTipFlag","")
          AsyncStorage.setItem("initiateFlag","")
        }
             
      } catch (error) {
        console.log(error);
      }
    }

    async updateGeoAddress(lat,long){
      const tokenType = await AsyncStorage.getItem("tokenType");
      const token = await AsyncStorage.getItem("token")
      console.log("url==", "http://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long)
  
      try {
        const response = await fetch(
          "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              'Cache-Control': 'no-cache, no-store, must-revalidate',
              'Pragma': 'no-cache',
              'Expires': 0
            }
          }
        );
        const responseJson = await response.json();
        if(responseJson.error==="invalid_token"){
            this.updateToken().then(()=>{
              return this.updateGeoAddress(lat,long)
            })
        } else {
          console.log("fg address==",responseJson)
          console.log("responseJson===",responseJson.results[0].formatted_address)
          this.setState({test:responseJson.results[0].formatted_address})
        }
      } catch(error){
          console.log(error)
      }
    }

    async updateToken(){
      try {
        var formData = new FormData();
        formData.append("grant_type", grantType);
        formData.append("client_id", ClientId);
        formData.append("client_secret",ClientSecret);
        const response = await fetch(
          "https://outpost.mapmyindia.com/api/security/oauth/token",
          {
            body: formData,
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "multipart/form-data",
              'Cache-Control': 'no-cache, no-store, must-revalidate',
              'Pragma': 'no-cache',
              'Expires': 0
            }
          }
        );
        const responseJson = await response.json();
        await AsyncStorage.setItem("token",JSON.stringify(responseJson.access_token))
        await AsyncStorage.setItem("tokenType",JSON.stringify(responseJson.token_type))
      } catch(error){
          console.log(error)
      }
    }



    _geoCodeToAddress(lat,lng) {
      fetch(
        "https://maps.googleapis.com/maps/api/geocode/json?address=" +
        lat +
          "," +
          lng +
          "&key=" +
          googleMaps_apiKey
      )
        .then(response => response.json())
        .then(responseJson => {
          console.log('destination responseJson is ====',responseJson)
          this.setState({
            srcAddress: responseJson.results[0].formatted_address
          });
          let test = this.state.srcAddress;
          this.setState({test:this.state.srcAddress})
          console.log('Current Address  ====',test)
         
        }
        )}  


  async EndTrip() {
    if(this.state.endKM==""||this.state.endKM==undefined){
      ToastAndroid.show("Please enter end trip KM.",ToastAndroid.LONG)
    } else {
      this.setState({ isLoading: true });
   try {
      let time= moment(new Date()).format("YYYY-MM-DD HH:mm")
      console.log("Date",time)
      //var ride_history_id = await AsyncStorage.getItem("ride_history_id");
      let bookingType = await AsyncStorage.getItem("bookingType")
      var ride_history_id="";
      if(bookingType=="notification"){
        var ride_history_id = await AsyncStorage.getItem("ride_history_id_notification")
      } else {
        var ride_history_id = await AsyncStorage.getItem("ride_history_id")
      }
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "b2b_rideStart_rideEnd");
      formData.append("type", "ride_end");
      formData.append("add_more_car","yes")
      formData.append("driver_id", driverId);
      formData.append("ride_end_time",time)
      formData.append("ride_end_location",this.state.test);
      formData.append("image_type","Before_End_Ride");
      formData.append("ride_history_id",ride_history_id)
      formData.append("endkm",this.state.endKM)

      formData.append("image_1", {
        uri: this.state.image1,
        type: "image/jpeg",
        name: "Vehicle_"+"Rightside_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeEndTrip",
        // name: this.state.image1.substring(
        //   this.state.image1.lastIndexOf("/") + 1,
        //   this.state.image1.length
        // )
      })
      formData.append("image_2", {
        uri: this.state.image2,
        type: "image/jpeg",
        name: "Vehicle_"+"Leftside_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeEndTrip",
        // name: this.state.image2.substring(
        //   this.state.image2.lastIndexOf("/") + 1,
        //   this.state.image2.length
        // )
      })
      formData.append("image_3", {
        uri: this.state.image3,
        type: "image/jpeg",
        name: "Vehicle_"+"Frontside_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeEndTrip",
        // name: this.state.image3.substring(
        //   this.state.image3.lastIndexOf("/") + 1,
        //   this.state.image3.length
        // )
      })
      formData.append("image_4", {
        uri: this.state.image4,
        type: "image/jpeg",
        name: "Vehicle_"+"Backside_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeEndTrip",
        // name: this.state.image4.substring(
        //   this.state.image4.lastIndexOf("/") + 1,
        //   this.state.image4.length
        // )
      })
      formData.append("image_5", {
        uri: this.state.image5,
        type: "image/jpeg",
        name: "Vehicle_"+"Dashboard_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeEndTrip",
        // name: this.state.image5.substring(
        //   this.state.image5.lastIndexOf("/") + 1,
        //   this.state.image5.length
        // )
      })
      console.log('endtrip formdata===',formData)
      const response = await fetch(
        BaseUrl.url2+"service.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );

      const responseJson = await response.json();
      console.log('data===',responseJson)
      if (responseJson.success === true) {
        this.setState({isLoading:false})
        await AsyncStorage.removeItem("ride_history_id")
        AsyncStorage.removeItem("Tripflag");
        AsyncStorage.removeItem("startTipFlag");
        AsyncStorage.removeItem("initiateFlag");
        BackgroundTimer.stopBackgroundTimer(); 
        ToastAndroid.show("Your trip has been ended successfully.",ToastAndroid.LONG);
        // this.props.navigation.navigate("MobileAuthenticationCheckOut")
        this.props.navigation.navigate("StartTrip")
        // Alert.alert(
        //   'Trip Ended!',
        //   'Do you want to start another trip.',
        //   [
        //     {text: 'NO', onPress: () => this.props.navigation.navigate("CheckOut")},
        //     {text: 'YES', onPress: () => this.props.navigation.navigate("MyRideList")},
        //   ],
        //   { cancelable: false }
        // );
    
      } else {
        this.setState({isLoading:false})
        ToastAndroid.show("Please upload all the pictures.",ToastAndroid.LONG);
        console.log("Check chote");
      }
    } catch (error) {
      this.setState({isLoading:false})
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          ToastAndroid.show("Please upload all the pictures.",ToastAndroid.LONG);
          console.log(error);
        }
      });
     }
    }
  }

  async getStoredLatLong(){
    let latlng =  await AsyncStorage.getItem("finalLatLong")
    let finaltimestamp =  await AsyncStorage.getItem("finaltimestamp")
    console.log("updated latlng",latlng)
    console.log("updated timestamp",finaltimestamp)
    console.log("updated latlng1",JSON.parse(latlng))
    console.log("updated timestamp1",JSON.parse(finaltimestamp))
    this.getSnaptoRoad(JSON.parse(latlng),JSON.parse(finaltimestamp))
  }
  async getSnaptoRoad(latlng,timestamp){
    console.log("url==", "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/snapToRoad?timestamps="+timestamp+"&pts="+latlng)

    try {
      const response = await fetch(
        "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/snapToRoad?timestamps="+timestamp+"&pts="+latlng,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      if(responseJson.responseCode==200){
        console.log("success", responseJson)
        Alert.alert(
          'Trip Ended!',
          'Do you want to start another trip.',
          [
            {text: 'NO', onPress: () => this.props.navigation.navigate("CheckOut")},
            {text: 'YES', onPress: () => this.props.navigation.navigate("Initiate")},
          ],
          { cancelable: false }
        );
      } else {
        console.log("success ! check", responseJson)
      }
    } catch(error){
        console.log(error)
    }
  }

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  };

  async image_1() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image1: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_2() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image2: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_3() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image3: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_4() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image4: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_5() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image5: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  handleBackButton(){
    return true
  }

  async updateCallback(){
    this.setState({image1:null,image2:null,image3:null,image4:null,image5:null,endKM:""})
}

onEndKM(text){
  this.setState({endKM:text.replace(/[^0-9]/g,"")})
}
 
  render() {
    return (
      <View style={{flex:1}}>
          <Header
          text={"End Trip For Add more Car"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <ScrollView>
        <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
        <ImageBackground  style={{width:"100%",height:100,marginBottom:20,backgroundColor:appColor.Blue,borderBottomRightRadius:30,borderBottomLeftRadius:30}}>
           <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:50,textAlign
          :'center',fontSize:15}}>Upload vehicle images to end your trip</Text>
        </ImageBackground>
        {/* <Image style={styles.circleView}
                 source={ProfileIcon}
          /> */}
        <View style={{padding:20}}> 
        <Spinner
              visible={this.state.isLoading}
              style={{color:'#000'}}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            /> 
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Enter Trip End KM</Text>
        <Form>
                <Item regular success style={{ borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey }}>
                  <Input
                  maxLength={13} 
                  minLength={3} 
                  keyboardType="number-pad"
                  placeholder="End KM" 
                  style={{ color: appColor.white, fontSize: 16, color: appColor.black }} 
                  onChangeText={text => this.onEndKM(text)}
                  value={this.state.endKM}
                  />
                   </Item>
               </Form>
        </View>

        <View style={{justifyContent:"center"}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5,fontWeight:"700"}}>Vehicle Images</Text>
        </View>
       <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:5}}>
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_1.bind(this)}
                     >
                        <Image
                        source={{ uri: this.state.image1}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
         
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_2.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image2}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around"}}>
       <Text style={{fontFamily:Fonts.Medium,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5,marginRight:10}}>Right Side<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       <Text style={{fontFamily:Fonts.Medium,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Left Side<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:5}}>
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_3.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image3}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_4.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image4}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around"}}>
       <Text style={{fontFamily:Fonts.Medium,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Front Side<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       <Text style={{fontFamily:Fonts.Medium,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Back Side<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       </View>
       
       <View style={{width:"45%",height:100,borderColor:appColor.lightGrey,marginHorizontal:"25%"}}>
       <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_5.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image5}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
       </View>
       <Text style={{color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5,fontFamily:Fonts.Medium}}>Dashboard<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text> </Text>
        
     
      <View style={{justifyContent:'center',marginBottom:20,alignItems:'center', marginTop:60}}>
            <AppButton
            text='End Trip'
            bgColor={appColor.Blue}
            width={'100%'}
            textColor={appColor.white}   
            onClickFunc={()=>this.EndTrip()}         
            />
        </View>
        
        </View>
       </ScrollView>
       
       </View>
    );
  }
}

const styles = StyleSheet.create({
  
  RegisterButton: {
    backgroundColor: appColor.yellow,
    marginTop:20
  },
  circleView:{
    width:140,
    height:140,
    borderRadius:70,
    backgroundColor:appColor.lightGrey,
    position:"absolute",
    marginTop:120,
    marginHorizontal:110
    // marginRight:20
    
  },
  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  headerTextView: {
    fontSize: 18,
    fontFamily:Fonts.Medium,
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    marginLeft: Dimensions.get("window").width/4
  },
  inputView: {
    borderRadius:5,
    backgroundColor:appColor.white,
     borderColor:appColor.darkGrey
},
UploadTextStyle: {
    
    backgroundColor: appColor.lightGrey,
    width:"100%",
    flexDirection:"row",
    height: 100,
    
    
  },
  UploadButtonStyle:{
    borderBottomLeftRadius:0,
    borderTopLeftRadius:0,
    borderBottomRightRadius:10,
    borderTopRightRadius:10,
    width:"40%",
    flexDirection:"row",
    height: 50,
    borderRadius: 50/2,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:appColor.Blue,
    
  },
  ButtonTextStyle: {
    fontSize: 15,
    color: appColor.black,
    fontFamily:'Roboto Medium',
    marginLeft:8,
    
},
UploadButtonText: {
  fontSize: 15,
  color: appColor.white,
  fontFamily:'Roboto Medium'
}

});

