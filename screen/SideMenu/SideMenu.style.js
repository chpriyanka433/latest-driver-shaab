import appColor from "../common/color";
import {Fonts} from "../common";

export default {
  container: {
    flex: 1,
    backgroundColor: appColor.Blue
  },
  navItemStyle: {
    marginLeft:15,
    color:appColor.white,
    fontSize: 15,
    fontFamily:Fonts.Regular,
    letterSpacing:1,
    top:3
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  IconSize: {
    width: 30,
    height: 30
  },
  circle: {
    width: 70,
    height: 70,
    borderRadius: 70 / 2,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    
  },
  Outercircle:{
    width: 80,
    height: 80,
    borderRadius: 80 / 2,
    backgroundColor: appColor.transparent,
    borderColor:appColor.white,
    alignItems: "center",
    justifyContent: "center",
    borderWidth:0.5
  },
  HeadingText:{
    color: "#fff",
    fontSize:20,
    fontFamily:Fonts.Medium,
    marginLeft:20,
    letterSpacing:1
  }
};
