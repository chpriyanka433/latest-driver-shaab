import PropTypes from "prop-types";
import React, { Component } from "react";
import styles from "./SideMenu.style";

import { ScrollView, Text, View, Image, TouchableOpacity, AsyncStorage, StatusBar,ToastAndroid,Linking} from "react-native";

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,image,Fonts} from "../common";
import BackgroundTimer from 'react-native-background-timer';
import VIForegroundService from '@voximplant/react-native-foreground-service';


import {Icon} from "native-base";


class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      from_portal:false
    }
  }
 
  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.closeDrawer();
    
  };

  async _logoutUser() {
    try {
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "update_driver_live_status");
      formData.append("driver_id", driverId);
      formData.append("driver_live_status", false);
      console.log(formData);
      const response = await fetch(
        BaseUrl.url1+"index.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );

      const responseJson = await response.json();
      console.log(responseJson);
      if (responseJson.success === true) {
        BackgroundTimer.stopBackgroundTimer(); 
        VIForegroundService.stopService();
        ToastAndroid.show("You have logged out successfully.",ToastAndroid.LONG);
        this.props.navigation.replace('mobile_auth')
        AsyncStorage.clear();
      } else {
       this.props.navigation.replace('mobile_auth')
       AsyncStorage.clear();
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount(){
    this._fetchUserDetails();
   
   }

   async RideSessionDetails() {
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "get_running_ride");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url2+"service.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      if(responseJson.success==true){
        if(responseJson.data.from_portal=="1"){
          this.setState({from_portal:true})
         } 
       } else {
        this.setState({from_portal:false})
       }       
    } catch (error) {
      console.log(error);
    }
  }
   
  async updateCallback(){
    console.log("hh")
    this._fetchUserDetails();
    this.RideSessionDetails();

  }
   async CheckIn(){
    let currentStatus = await AsyncStorage.getItem("checkedin_id")
    console.log("currentStatus==",currentStatus)
    if(currentStatus!=null){
      this.props.navigation.navigate("CheckOut")
      this.props.navigation.closeDrawer();  
          
    }
    else{
      this.props.navigation.navigate("Checkin")
      this.props.navigation.closeDrawer();
    }
  }


  async StartRide(){
    let fromPortal = this.state.from_portal;
    //alert(fromPortal)
    if(fromPortal==true){
      this.props.navigation.navigate("Initiate");
      this.props.navigation.closeDrawer();
    } else {
      let currentStatus = await AsyncStorage.getItem("ride_history_id")
      let startTipFlag = await AsyncStorage.getItem("startTipFlag")
      let initiateFlag = await AsyncStorage.getItem("initiateFlag")
      let checkStatus = await AsyncStorage.getItem("checkedin_id")
      if(checkStatus==null||checkStatus==undefined){
        ToastAndroid.show("You haven't check-in, Please check-in first.",ToastAndroid.LONG);
        this.props.navigation.navigate("Checkin")
        this.props.navigation.closeDrawer();
      } else if(currentStatus==null && initiateFlag == null){
        this.props.navigation.navigate("Initiate")
        this.props.navigation.closeDrawer();
      } else if(currentStatus!=null && initiateFlag == 'flaged' && startTipFlag == null){
        this.props.navigation.navigate("StartTrip")
        this.props.navigation.closeDrawer();
      } else if(currentStatus!=null && startTipFlag == 'flaged' && initiateFlag == 'flaged'){
        this.props.navigation.navigate("AddFuel")
        this.props.navigation.closeDrawer();
      } else {
        this.props.navigation.navigate("EndTrip")
        this.props.navigation.closeDrawer();
        }
      }
  }

 async myRides(){
  this.props.navigation.navigate("MyRideList");
  this.props.navigation.closeDrawer();
 }

  async createToken(){
    let currentStatus = await AsyncStorage.getItem("ride_history_id");
    if(currentStatus !=null){
      this.props.navigation.navigate("CreateToken");
      this.props.navigation.closeDrawer();
    } else {
      ToastAndroid.show("Please start the trip to create token.",ToastAndroid.LONG)
      this.props.navigation.closeDrawer();
    }
  }

    
   async TripHistory(){
     this.props.navigation.navigate("TripHistory")
     this.props.navigation.closeDrawer();
   }  
 
  
  async AttendanceHistory(){
    this.props.navigation.navigate("AttendanceHistory")
    this.props.navigation.closeDrawer();
  }

  async filterDetails(val){
    this.setState({date:val})
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      if(connectionInfo.type==='none' || connectionInfo.type ==='NONE'){
        ToastAndroid.show("Please Check Your Internet Connection !",ToastAndroid.LONG);
      }
    });
    this.setState({ isLoading: true });
   try {
      let time= this.state.date;
      console.log("Date",time)
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "get_booking_history_list");
      formData.append("search_date",time);
      formData.append("driver_id", driverId);
     
      console.log('formdata',formData)
      console.log('url',BaseUrl.url2+"service.php")

      const response = await fetch(
        BaseUrl.url2+"service.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );

      const responseJson = await response.json();
      console.log('data===',responseJson)
      if (responseJson.success === true)
      {
        this.setState({isLoading:false})
        this.setState({data:responseJson.data})
    
      } else {
        this.setState({isLoading:false})
        this.setState({data:[]})
        console.log("Check chote");
      }
    } catch (error) {
      console.log(error);
    }
  }
  
  async _fetchUserDetails() {
    let currentStatus = await AsyncStorage.getItem("checkedin_id")
    console.log("check",currentStatus)
    if(currentStatus!==null){
      this.props.navigation.navigate("CheckOut")
    }
    else{
      this.props.navigation.navigate("CheckIn")
    }
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "get_driver_info");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url1+"index.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      console.log('driverdata===',responseJson)
      if(responseJson.name!=null){
        this.setState({
          userName: responseJson.name
        })
      } else {
        this._logoutUser()
      }
      
    } catch (error) {
      console.log(error);
    }
  }

  appTour(){
    const URL = "https://youtu.be/kiyoDa9bHGQ"
    Linking.openURL(URL).catch((err) => console.error('An error occurred', err));
    this.props.navigation.closeDrawer();
  }

  render() {
    return (
      <View style={styles.container}>
         <ScrollView>
       
        <StatusBar backgroundColor="#19376c" barStyle="light-content" />
          <View
            style={{
              backgroundColor: appColor.HeaderBlue,marginBottom:30
            }}
          >
            <TouchableOpacity onPress={this.navigateToScreen("ViewProfileScreen")}>
              <View
                style={{
                  // alignItems: "center",
                  // justifyContent: "center",
                  marginTop:30,
                  marginLeft:20,
                  flexDirection: "row",
                  height: 100
                }}
              >
                <View style={styles.Outercircle}>
                <View style={styles.circle}>
                  
                  <Text style={{ fontFamily:Fonts.Medium, fontSize: 25,color:appColor.Blue }}>{this.state.userName.split().map((word)=> word.charAt(0)).join(' ')}</Text>
                </View>
                </View>
                <View style={{marginTop:30,marginLeft:15}}>
                  <Text style={{ color: "#fff",fontSize:18,fontFamily:Fonts.Medium }}>{this.state.userName}</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
         
          <View style={{
              backgroundColor: appColor.Blue}}>
                <View>
                <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
                  <Text style={styles.HeadingText}>Dashboard</Text>
                </View>
            <View style={{ flexDirection: "column",marginBottom:20 }}>
              <TouchableOpacity
                style={{
                 marginLeft:40,
                  marginTop:15,
                  flexDirection: "row",
                  marginBottom:10
                }}
                onPress={this.navigateToScreen("ViewProfileScreen")}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                   style={styles.IconSize}
                    source={image.profile}
                  />
                </View>

                <View>
                  <Text
                    style={styles.navItemStyle}
                    
                  >
                    My Profile
                  </Text>
                </View>
              </TouchableOpacity>
               </View>
               <View>
                  <Text style={styles.HeadingText}>Trips</Text>
                </View>
            <View style={{ flexDirection: "column",marginBottom:30 }}>
              <TouchableOpacity
                style={{
                  marginLeft:40,
                  marginTop:15,
                  flexDirection: "row"
                }}
                onPress={()=>this.StartRide()}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.IconSize}
                    source={image.startEndTrip}
                  />
                </View>
                <View>
                  <Text
                    style={styles.navItemStyle}
                    
                  >
                    Start / End Trip
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginLeft:40,
                  marginTop:10,
                  flexDirection: "row",
                  marginBottom:0
                }}
                onPress={()=>this.myRides()}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.IconSize}
                    source={image.tripHistory}
                  />
                </View>

                <View>
                  <Text
                    style={styles.navItemStyle}
                    
                  >
                    My Rides
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginLeft:40,
                  marginTop:15,
                  flexDirection: "row"
                }}
                onPress={()=>this.createToken()}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.IconSize}
                    source={image.token}
                  />
                </View>
                <View>
                  <Text
                    style={styles.navItemStyle}
                    
                  >
                    Create Issue Ticket
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                 marginLeft:40,
                  marginTop:10,
                  flexDirection: "row",
                  marginBottom:10
                }}
                onPress={()=>this.TripHistory()}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.IconSize}
                    source={image.tripHistory}
                  />
                </View>

                <View>
                  <Text
                    style={styles.navItemStyle}
                    
                  >
                    View Trip History
                  </Text>
                </View>
              </TouchableOpacity>
               </View>
               
               <View>
                  <Text style={styles.HeadingText}>Attendance</Text>
                </View>
            <View style={{ flexDirection: "column" }}>
             <TouchableOpacity
                style={{
                  marginLeft:40,
                  marginTop:15,
                  flexDirection: "row"
                }}
                onPress={()=>this.CheckIn()}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.IconSize}
                    source={image.tripHistory}
                  />
                </View>
                <View>
                  <Text
                    style={styles.navItemStyle}
                    
                  
                  >
                   Check In / Out
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                 marginLeft:40,
                  marginTop:10,
                  flexDirection: "row",
                  marginBottom:10
                }}
                onPress={() =>this.AttendanceHistory()}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    style={styles.IconSize}
                    source={image.attendance}
                  />
                </View>

                <View>
                  <Text
                    style={styles.navItemStyle}
                    
                  >
                  Attendance History

                  </Text>
                </View>
              </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.appTour()}>
                  <Text style={[styles.HeadingText,{marginTop:10}]}>App Guide</Text>
                </TouchableOpacity>
              {/* <TouchableOpacity
                style={{
                  paddingBottom: 5,
                  paddingLeft: 20,
                  paddingTop: 15,
                  flexDirection: "row"
                }}
                onPress={()=>this.appTour()}>
                <View style={{ justifyContent: "center" }}>
                  <Icon type="Foundation" name="play-video" style={{ width: 30, height: 30, color:appColor.lightGrey}}/>
                </View>
                <View>
                  <Text
                    style={styles.navItemStyle}
                  >
                    App Guide
                  </Text>
                </View>
              </TouchableOpacity> */}
               </View>
               
             
          </View>
       
        <View style={{marginBottom:20,marginLeft:20,marginTop:20}}>
            <AppButton
            text='LOGOUT'
            bgColor="#061432"
            textColor="#fff"
            width={'80%'}
            onClickFunc={()=>this._logoutUser()}           
            />
        </View>
        {/* <View style={styles.footerContainer}>
          <Text>This is my fixed footer</Text>
        </View> */}
         </ScrollView>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};


export default SideMenu;
