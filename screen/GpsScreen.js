import React, {Component} from 'react';
//Import React
import { SafeAreaView, View, Text, ToastAndroid,PermissionsAndroid,Image,Dimensions,AsyncStorage,Platform,ScrollView} from 'react-native';
import BackgroundTimer from 'react-native-background-timer';
import VIForegroundService from '@voximplant/react-native-foreground-service';


import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,Geolocation,Fonts,RNAndroidLocationEnabler,image,NetInfo} from "././common";

const googleMaps_apiKey = "AIzaSyCcSBfkyEnp5QMLiMY__OKOxjzeIAiTVNk";
export default class App extends Component {

  componentDidMount(){
    const channelConfig = {
      id: "channelId",
      name: 'DriverShaab B2B',
      description: 'Channel description',
      enableVibration: false
  };
  VIForegroundService.createNotificationChannel(channelConfig);
  }

  async checkpermission(){
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Why we need your location even when the application is in the background?',
          message: 'When you are starting a trip, then we need to track your location so that we can inform the rider where you are exactly as well as the trip status.',
          buttonPositive:"Ok",
          buttonNegative:"Cancel"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.checkLocation();
      } else {
        console.log('Location Permission Denied');
      }
    } else {
      console.log('Next Permission');
    }
  }
    
      checkLocation(){
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
      .then(data => {
        if(data){
          this._fetchCurrentLocationToRedirect().then(()=>{
            return this.updateLocation();
          })
        } else {
          ToastAndroid.show('Please Turn ON your Location.',ToastAndroid.LONG)
        }
      }).catch(err => {
        console.log(err)
      });
      }

      async updateLocation(){
        longArray=[]; 
        timeStampArray=[];
        let rideStatus = await AsyncStorage.getItem("initiateFlag");
        if(rideStatus=="flaged"){
          BackgroundTimer.runBackgroundTimer(() => { 
           Geolocation.getCurrentPosition(
               (position) => {
                  const currentLongitude = JSON.stringify(position.coords.longitude);
                  const currentLatitude = JSON.stringify(position.coords.latitude);
                  const timeStamp = JSON.stringify(position.timestamp);
                  longArray.push(currentLongitude.concat(",").concat(currentLatitude))
                  timeStampArray.push(timeStamp)
                  var resultLatlng = {};
                    for (var i = 0; i < longArray.length; ++i){
                      resultLatlng["position" + (i+1)] = longArray[i];                                                                                                                          
                  }
                  var resultTimestamp = {};
                    for (var i = 0; i < timeStampArray.length; ++i){
                      resultTimestamp["position" + (i+1)] = timeStampArray[i];                                                
                  }
                  const finalLatLong = Object.keys(resultLatlng).map(function(k){return resultLatlng[k]}).join(";");
                  const finaltimestamp = Object.keys(resultTimestamp).map(function(k){return parseInt(resultTimestamp[k]/1000)}).join(";");
                  AsyncStorage.setItem("finalLatLong",JSON.stringify(finalLatLong))
                  AsyncStorage.setItem("finaltimestamp",JSON.stringify(finaltimestamp))
                  console.log("finaltimestamp==",finaltimestamp)
                  console.log("finalLatLong==",finalLatLong)
                  this.sendLocation(finalLatLong,finaltimestamp)
               },
               (error) => console.log("error.message",error.message),
               //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
            );
          }, 5000);
        } else {
          BackgroundTimer.stopBackgroundTimer();
        }
      }
    
      async sendLocation(finalLatLong,finaltimestamp){
        const driverId = await AsyncStorage.getItem('driverId');
        const ride_history_id = await AsyncStorage.getItem("ride_history_id");
        try {
          var formData = new FormData();
          formData.append("action", "update_live_location");
          formData.append("driver_id",driverId);
          formData.append("driver_booking_id", "");
          formData.append("b2b_ride_history_id",ride_history_id);
          formData.append("latlong_coordinate",finalLatLong);
          formData.append("timestamp", finaltimestamp);
          //console.log("formdata==",formData)
          const response = await fetch(BaseUrl.url5, {
            body: formData,
            method: "POST",
            headers: {
              "Accept": "multipart/form-data",
              "Content-Type": "multipart/form-data",
              'Cache-Control': 'no-cache, no-store, must-revalidate',
              'Pragma': 'no-cache',
              'Expires': 0
            }
          });
          const responseJson = await response.json();
          //console.log('driverdataride===',responseJson)  
          if(responseJson.success===true){
           console.log('responseJson.success',responseJson)  
           } else {
             console.log('responseJson.false',responseJson)  
          }  
        } catch (error) {
          console.log("error rsp==",error);
        }
      }

      async _fetchCurrentLocationToRedirect() {
        const notificationConfig = {
          channelId: 'channelId',
          id: 3456,
          title: 'DriverShaab B2B',
          text: 'App is running in the background',
          icon: image.driver
      };
      try {
          await VIForegroundService.startService(notificationConfig);
      } catch (e) {
          console.error("errrr==",e);
      }
        await Geolocation.getCurrentPosition(
          position => {
            this._geoCodeToAddress(
              position.coords.latitude,
              position.coords.longitude
            );
          },
          error => {
            // alert(error)
            //ToastAndroid.show(error.message, ToastAndroid.SHORT);
          },
          // { enableHighAccuracy: false,
          //   timeout: 5000,
          //   maximumAge: 10000 }
        );
      }
    
      /**
       * Function to convert Geo-code(Latitude, Longitude) to Human readable address
       */
      _geoCodeToAddress(latitude, longitude) {
        try {
          AsyncStorage.getItem("isLoggedIn").then(result => {
              if (result === "true") {
                console.log('result===',true)
                this.props.navigation.replace("Checkin", {
                  latitude: latitude,
                  longitude: longitude
                });
              } else {
                this.props.navigation.replace("mobile_auth");
              }
          });
        } catch (error) {
          NetInfo.fetch().then(connection => {
            if(!connection.isConnected){
              Toast.show("Please Check Your Internet Connection !",Toast.LONG);
            } else {
            console.log(error);
            }
          });
        }
      }

      
  render() {
    return (
      <ScrollView style={{flex:1}}>
      <SafeAreaView>
        <View style={{ padding: 20 }}>
        <Image
            source={image.gps}
            resizeMode={"contain"}
            style={{ width:"100%", height:200,marginTop:50}}
          />
         <Text style={{color:appColor.Blue,textAlign
          :"center",fontSize:20,marginBottom:10,fontFamily:Fonts.Medium,marginRight:10,marginTop:30}}>Location Permission required.</Text>
           <Text style={{fontFamily:Fonts.Medium,color:appColor.lightBlack,textAlign
          :"left",fontSize:16,marginBottom:5,marginRight:10,marginTop:30}}>Why we need your location even when the application is in the background?</Text>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.lightBlack,textAlign
          :"left",fontSize:16,marginBottom:5,marginRight:10}}>When you are starting a trip, then we need to track your location so that we can inform the rider where you are exactly as well as the trip status.
          </Text>
          <View style={{justifyContent:'center',marginBottom:20,alignItems:'center', marginTop:"40%"}}>
            <AppButton
            text='ALLOW'
            bgColor={appColor.Blue}
            width={'100%'}
            textColor={appColor.white} 
            onClickFunc={()=>this.checkpermission()}     
            />
        </View>
        </View>
        
      </SafeAreaView>
      </ScrollView>
    );
  }
}