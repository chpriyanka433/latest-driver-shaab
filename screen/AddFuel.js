import React, { Component } from 'react';
import { Form, Item, Input,Picker} from 'native-base';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  PermissionsAndroid,
  AsyncStorage, ToastAndroid,ScrollView,
  Platform
} from 'react-native';

import {appColor,Header,InputText,AppButton,Spinner,ImagePicker,DrawerActions,NavigationEvents,DatePicker,moment, image,Fonts,BaseUrl,Geolocation,RestApiKey,ClientId,ClientSecret,grantType,getDirections} from "././common";

import {RadioButton} from 'react-native-paper';
import { latitude, longitude } from 'geolib';

export default class AddFuel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount:"",
      storedImage:null,
      isLoading:false,
      submit_type:"one",
      selected: "0",
      payment_mode:"Cash",
      sourceLng:"",
      sourceLat:"",
      desLat:"",
      desLng:"",
      notificationRequestType:"",
      manual:"",
      driverType:""
    }


  }
  intervalID;
  async componentDidMount() {
    this.getdesvalue();
    this._reloadDrives();
    this.intervalID = setInterval(this._reloadDrives.bind(this), 5000);
    
   
    this._fetchCurrentLocationToRedirect()
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (!granted) {
        return;
      } else {
        await Geolocation.getCurrentPosition(
          (position) => {
            console.log("position",position.coords.latitude)
            this.updateGeoAddress(position.coords.latitude,position.coords.longitude)
          });
      }
    } else {
      console.log("ios platform")
    }
    let notificationRequest = await AsyncStorage.getItem("request_type");
    this.setState({notificationRequestType:notificationRequest})
  }
  componentWillUnmount() {
    
    clearInterval(this.intervalID);
  }
  addMoreCar(){
    this.props.navigation.navigate("EndTrip",{add_more_car:"yes"});
    // alert('hello')
    //AddmoreCarTripEnd this.props.navigation.navigate("StartTrip",{driverType:this.state.driverType,sendType:"multiple"})
    // this.props.navigation.navigate("AddmoreCarTripEnd",{add_more_car:"yes",Vehicle_No:this.state.Vehicle_No})
  }
  async getdesvalue(){
  
  
    // const datadestination = await this.props.navigation.getParam('desLat');
    // const ddesLngdestination = await this.props.navigation.getParam('desLng');
    // console.log(' Add fuel  data.destinationLng')
    // console.log(datadestination)
    // this.setState({
    //   destinationLat:datadestination,
    //   destinationLng:ddesLngdestination,
      // destinationLat:data.destinationLat,
      // driverType:data.request_type
      
  // })
    // console.log('desLat value Add fuel page')
    // console.log(this.state.destinationLng)
    // console.log(this.state.driverType)
  }
async _reloadDrives(){
  
  // let data = await AsyncStorage.getItem("item");
 let data= this.props.navigation.getParam("item") 
  //alert(JSON.stringify(data))
  this.setState({
      //poc_id: data.poc_id,
     // request_id: data.poc_id.request_id,
     // source: data.source,
     // destination: data.destination,
     // company_name: data.company_name,
     // schedule_time: data.rideTime,
    //  company_id: data.company_id,
    //  poc_name: data.poc_name,
    //  trip_type: data.trip_type,
    //  booking_status:data.status,
     // sourceLng:data.sourcelng,
    //  sourceLat:data.sourcelat,
     destinationLng:data.destinationLng,
     destinationLat:data.destinationLat,
    //  tour_type:data.tour_type,
     // vehicle_type:data.vehicle_type,
    //  request_type:data.request_type,
    //  vehicle_number:data.vehicle_number,
//poc_number:data.poc_number,
    //  numbers_of_drivers:data.numbers_of_drivers,
    //  notification_type:data.notification_type,
    driverType:data.request_type
  })
  console.log('reload drive value',this.state.destinationLng)
}
  async updateCallback(){
    let notificationRequest = await AsyncStorage.getItem("request_type");
    this.setState({notificationRequestType:notificationRequest,storedImage:null,amount:""})
    let manualBooking = await this.props.navigation.getParam("manual");
    let desLat = await this.props.navigation.getParam("desLat");
    let desLng = await this.props.navigation.getParam("desLng");
    let driverType = await this.props.navigation.getParam("driverType");
    this.setState({manual:manualBooking,desLat:desLat,desLng:desLng,driverType:driverType})
    this._fetchCurrentLocationToRedirect();
    if(manualBooking=="no"){
      this._fetchCurrentLocationToRedirect();
    } else {
      console.log("manual book")
    }
  }

  async _fetchCurrentLocationToRedirect() {
    await Geolocation.getCurrentPosition(
       position => {
         this.setState({
           sourceLat: position.coords.latitude,
           sourceLng: position.coords.longitude
         });
       },
       error => ToastAndroid.show(error.message, ToastAndroid.LONG),
       { enableHighAccuracy: false, timeout: 20000 }
     );
   }

  async imageupload(){
    try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "Cool Photo App Camera Permission",
            message:
              "Cool Photo App needs access to your camera " +
              "so you can take awesome pictures."
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          ImagePicker.openCamera({
            compressImageQuality: 0.1,
            title: "Select Photo"
          }).then(images => {
            this.setState({ storedImage: images.path });
          });
        } else {
          console.log("Camera permission denied");
        }
      } catch (err) {
        console.warn(err);
      }
  }

  async onEnterAmount(value){
    this.setState({
        amount: value.replace(/[^0-9]/g, "")
      });
  }

  async onSubmit(){
    if((this.state.storedImage==null||this.state.storedImage=="") && (this.state.submit_type=="one"||this.state.submit_type=="two"||this.state.submit_type=="three"||this.state.submit_type=="five")){
      ToastAndroid.show("Please upload receipt image",ToastAndroid.LONG)
    } else if((this.state.amount==null||this.state.amount=="")&&this.state.payment_mode!=="Net_Banking"){
        ToastAndroid.show("Please enter amount",ToastAndroid.LONG)
    } else {
       this.setState({isLoading:true})
       if(this.state.submit_type=="one"){
        var receiptType = "fuel";
    } else if(this.state.submit_type=="two"){
        var receiptType = "toll";
    } else if(this.state.submit_type=="three"){
        var receiptType = "one_way_cost";
    }  else if(this.state.submit_type=="four"){
      var receiptType = "Cash_Collection";
    } else if(this.state.submit_type=="five"){
      var receiptType = "Delivery_Proof";
    }  else if(this.state.submit_type=="six"){
       var receiptType = "Police_Payment";
    }  else if(this.state.submit_type=="seven"){
       var receiptType = "RTO_Payment";
    }  else {
      var receiptType = "Breakdown_Payment";
    }
        try {
        const driverId = await AsyncStorage.getItem('driverId');
        const ride_history_id = await AsyncStorage.getItem("ride_history_id");
        var formData = new FormData();
        formData.append("action", "b2b_ride_extra_expenses");
        formData.append("driver_id", driverId);
        formData.append("type", receiptType);
        formData.append("amount", this.state.amount);
        formData.append("booking_id", ride_history_id);
        formData.append("location", this.state.test);
        this.state.submit_type=="five"?formData.append("payment_mode", this.state.payment_mode):null
        this.state.submit_type=="one"||this.state.submit_type=="two"||this.state.submit_type=="three"||this.state.submit_type=="five"?formData.append("receipt_image", {
          uri: this.state.storedImage,
          type: "image/jpeg",
          name: this.state.storedImage.substring(
            this.state.storedImage.lastIndexOf("/") + 1,
            this.state.storedImage.length
          )
        }):formData.append("receipt_image","");
        
        console.log("Add fuel formdata==",formData)
        const response = await fetch(BaseUrl.url4, {
            body: formData,
            method: "POST",
            headers: {
            "Accept": "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
            }
        });
        const responseJson = await response.json();
        if(responseJson.success===true){
            ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
            console.log("rsp ===",JSON.stringify(responseJson))
            this.setState({ isLoading: false, storedImage:null,amount:""}); 
        } else {
            ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
            this.setState({ isLoading: false});
        }        
        } catch (error) {
        this.setState({ isLoading: false });
        console.log(error);
        }
    }
  }

  async updateGeoAddress(lat,long){
    const tokenType = await AsyncStorage.getItem("tokenType");
    const token = await AsyncStorage.getItem("token")
    console.log("url==", "http://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long)

    try {
      const response = await fetch(
        "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      if(responseJson.error==="invalid_token"){
          this.updateToken().then(()=>{
            return this.updateGeoAddress(lat,long)
          })
      } else {
        console.log("responseJson===",responseJson.results[0].formatted_address)
        this.setState({test:responseJson.results[0].formatted_address})
      }
    } catch(error){
        console.log(error)
    }
  }

  async updateToken(){
    try {
      var formData = new FormData();
      formData.append("grant_type", grantType);
      formData.append("client_id", ClientId);
      formData.append("client_secret",ClientSecret);
      const response = await fetch(
        "https://outpost.mapmyindia.com/api/security/oauth/token",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      await AsyncStorage.setItem("token",JSON.stringify(responseJson.access_token))
      await AsyncStorage.setItem("tokenType",JSON.stringify(responseJson.token_type))
    } catch(error){
        console.log(error)
    }
  }

  async onsubmitContinue(){
    if((this.state.storedImage==null||this.state.storedImage=="") && (this.state.submit_type=="one"||this.state.submit_type=="two"||this.state.submit_type=="three")){
      ToastAndroid.show("Please upload receipt image",ToastAndroid.LONG)
  } else if(this.state.amount==null||this.state.amount==""){
      ToastAndroid.show("Please enter amount",ToastAndroid.LONG)
    } else {
       this.setState({isLoading:true})
        if(this.state.submit_type=="one"){
            var receiptType = "fuel";
        } else if(this.state.submit_type=="two"){
            var receiptType = "toll";
        } else if(this.state.submit_type=="three"){
            var receiptType = "one_way_cost";
        } else {
          var receiptType = "Cash_Collection";
        }
        try {
        const driverId = await AsyncStorage.getItem('driverId');
        const ride_history_id = await AsyncStorage.getItem("ride_history_id");
        var formData = new FormData();
        formData.append("action", "b2b_ride_extra_expenses");
        formData.append("driver_id", driverId);
        formData.append("type", receiptType);
        formData.append("amount", this.state.amount);
        formData.append("booking_id", ride_history_id);
        this.state.submit_type=="one"||this.state.submit_type=="two"||this.state.submit_type=="three"?formData.append("receipt_image", {
          uri: this.state.storedImage,
          type: "image/jpeg",
          name: this.state.storedImage.substring(
            this.state.storedImage.lastIndexOf("/") + 1,
            this.state.storedImage.length
          )
        }):formData.append("receipt_image","");

        console.log("form data ===",JSON.stringify(formData))
        const response = await fetch(BaseUrl.url4, {
            body: formData,
            method: "POST",
            headers: {
            "Accept": "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
            }
        });
        const responseJson = await response.json();
        console.log("api response==",responseJson)
        if(responseJson.success===true){
            ToastAndroid.show(responseJson.message,ToastAndroid.LONG);
            this.setState({ isLoading: false});
            if(this.state.submit_type=="one"){
                this.setState({submit_type:"two",storedImage:null,amount:""})
            } else if(this.state.submit_type=="two"){
                this.setState({submit_type:"three",storedImage:null,amount:""})
            } else if(this.state.submit_type=="three"){
              this.setState({submit_type:"four",storedImage:null,amount:""})
           } else {
               AsyncStorage.setItem("isFuel", 'done');
               this.props.navigation.navigate("EndTrip");
            }
        } else {
            ToastAndroid.show(responseJson.message,ToastAndroid.LONG);
            this.setState({ isLoading: false});
        }        
        } catch (error) {
        this.setState({ isLoading: false });
        console.log(error);
        }
    }
  }

  async onsubmittoEndTrip(){

    // this.props.navigation.navigate("EndTrip");
      
      
         const driverId = await AsyncStorage.getItem('driverId');
         const rideHistoryId = await AsyncStorage.getItem('ride_history_id')
         var formData = new FormData();
         formData.append("action", "driver_reach_location");
         formData.append("driver_id", driverId);
         formData.append("ride_history_id",rideHistoryId)
         formData.append("{add_more_car","no")
         console.log("form data")
         console.log(formData)
         const response = await fetch(BaseUrl.url5, {
           body: formData,
           method: "POST",
           headers: {
             "Accept": "multipart/form-data",
             "Content-Type": "multipart/form-data",
             'Cache-Control': 'no-cache, no-store, must-revalidate',
             'Pragma': 'no-cache',
             'Expires': 0
           }
         });
         const responseJson = await response.json();
         console.log(responseJson)
         console.log('driverdataridedddddddd===',responseJson)
          console.log(responseJson.data)
         
         if (responseJson.success === true) {
          
         if(responseJson.data.status=== true){
          this.props.navigation.navigate("EndTrip",{add_more_car:"no"});
         }
          else{
            this.props.navigation.navigate("OTPAuthenticationCheckOut",{add_more_car:"no"});
          }
        } else {
        console.log('else condition')
        // this.props.navigation.navigate("OTPAuthenticationCheckOut");
             
        }
        // if (responseJson.success === true) {
        //   if(responseJson.data.status==false){
        //     console.log("end otp",responseJson)
        //     this.props.navigation.navigate("OTPAuthenticationCheckOut");
        //   } else {
        //     this.props.navigation.navigate("EndTrip");
           
        //   }
        // } else {
        //   this.props.navigation.navigate("EndTrip");
        // }
         
       } 
     
  async skipContinue(){
    if(this.state.submit_type=="one"){
        this.setState({submit_type:"two",storedImage:null,amount:""})
    } else if(this.state.submit_type=="two"){
        this.setState({submit_type:"three",storedImage:null,amount:""})
    } else if(this.state.submit_type=="three"){
      this.setState({submit_type:"four",storedImage:null,amount:""})
   } else {
        AsyncStorage.setItem("isFuel", 'done');
        this.props.navigation.navigate("EndTrip");
    }
  }

  async getaddType(value: string){
    this.setState({selected: value});
    if(value==0){
      this.setState({submit_type:"one",storedImage:null,amount:""})
    } else if(value==1){
      this.setState({submit_type:"two",storedImage:null,amount:""})
    } else if(value==2){
      this.setState({submit_type:"three",storedImage:null,amount:""})
    } else if(value==3){
      this.setState({submit_type:"four",storedImage:null,amount:""})
    } else if(value==4){
      this.setState({submit_type:"five",storedImage:null,amount:""})
    } else if(value==5){
      this.setState({submit_type:"six",storedImage:null,amount:""})
    } else if(value==6){
      this.setState({submit_type:"seven",storedImage:null,amount:""})
    } else if(value==7){
      this.setState({submit_type:"eight",storedImage:null,amount:""})
    } else if(value==8){
      this.setState({submit_type:"nine",storedImage:null,amount:""})
    }else {
      this.setState({submit_type:"one",storedImage:null,amount:""})
    }
  }

  async paymentModeSelect(value){
    if(value==="Cash"){
      this.setState({payment_mode:value})
    }else{
     this.setState({payment_mode:value,amount:""})
    }
  }
  
  navigateChange(deslat,desLng){
   

//       if(deslat==='undefined'|| (desLng ==='undefined')){
// alert('undefined')
// this.props.navigation.navigate("AddFuel",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,driverType:this.state.driverType,item:item});
//       }
    const data = {
      source: {
       
      latitude: this.state.sourceLat,
      longitude: this.state.sourceLng,
     
    },
    destination: {
      latitude: JSON.parse(deslat),
      longitude: JSON.parse(desLng)
    },
    params: [
      {
        key: "travelmode",
        value: "driving"        // may be "walking", "bicycling" or "transit" as well
      },
      {
        key: "dir_action",
        value: "navigate"       // this instantly initializes navigation using the given travel mode
      }
    ]
  }
  console.log("map data ==",data)
  getDirections(data)
  // if(data.destination.latitude=="undefined"|| (data.destination.longitude="undefined")){
  //   alert('data undefined')
  //   this.props.navigation.navigate("AddFuel",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,driverType:this.state.driverType,item:item});
  // }
  // else{
  //   getDirections(data)
  // }
   
  }
  

  render() {
    const deslat = this.state.destinationLat
    const desLng = this.state.destinationLng
    const drivertype = this.state.driverType
    // const desLat =  this.props.navigation.getParam("desLat");
    console.log('deslat value fuel pageeeeeeeee')
    console.log(deslat,desLng,drivertype)
//     const isInvalid = this.state.driverType==="single_permanent"||this.state.driverType==="permanent"
    

// const display = isInvalid ? "none" : "flex";

    return (
      <ScrollView>
      <View style={{ flex: 1 }}>
        <Header
          text={"Add Requirements"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
          <Spinner
            visible={this.state.isLoading}
            style={{ color: '#000' }}
            color={appColor.spinnerColor}
            overlayColor="rgba(0, 0, 0, 0.55)"
          /> 
        <View style={styles.ContainerView}>
        <View style={{marginBottom:10}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Select</Text>
          <Form>
                <Item regular success style={{ borderRadius: 5,backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey,height:50 }}>
          <Picker
              mode="note"
              placeholder="Select"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              selectedValue={this.state.selected}
              onValueChange={this.getaddType.bind(this)}>
              <Picker.Item label="Add Fuel" value="0" />
              <Picker.Item label="Add Toll" value="1" />
              <Picker.Item label="Add One Way Cost" value="2" />
              <Picker.Item label="Cash Collection" value="3" />
              <Picker.Item label="Delivery Proof" value="4" />
              <Picker.Item label="Police Payment" value="5" />
              <Picker.Item label="RTO Payment" value="6" />
              <Picker.Item label="Breakdown / Maintenance" value="7" />
              <Picker.Item label="HAMALI" value="8" />
              </Picker>
              </Item>
               </Form>
        </View>
          {/* <View style={styles.textView}>
            <Text style={[styles.textStyle,{textAlign:"center", marginTop:12}]}>
                {
                    this.state.submit_type=="one"?"Add Fuel":this.state.submit_type=="two"?"Add Toll":this.state.submit_type=="three"?"Add One Way Cost":this.state.submit_type=="four"?"Cash Collection":null
                }
            
            </Text>
            <AppButton
            text='Skip & Continue'
            bgColor={appColor.darkGrey}
            width={'50%'}
            height={30}
            fontSize={12}
            borderRadius={5}
            textColor={appColor.white}   
            onClickFunc={()=>this.skipContinue()}         
            />
         </View> */}
         {
           this.state.submit_type=="one"||this.state.submit_type=="two"||this.state.submit_type=="three"||this.state.submit_type=="five"||this.state.submit_type=="seven"||this.state.submit_type=="eight"?
            <View style={[styles.uploadview,{marginBottom:20}]}>
            <AppButton
              text='Upload Receipt'
              bgColor={appColor.Blue}
              width={'50%'}
              borderRadius={5}
              textColor={appColor.white}   
              onClickFunc={()=>this.imageupload()}         
              />
              <View style={{borderColor:appColor.white,justifyContent:"center"}}>
                <Image
                  source={{ uri: this.state.storedImage }}
                  //resizeMode="contain"
                  style={styles.ImageStyle}
                />
              </View>  
           </View>
          :null
         }

          { 
            this.state.submit_type=="five"&&(
            <View style={{padding:0, marginBottom:10}}>
            <Text style={styles.staticlighttext}>{"Payment Mode"}</Text>    
                <RadioButton.Group onValueChange={value => this.paymentModeSelect(value)} value={this.state.payment_mode}>
                   <View style={{flexDirection:"row",}}>
                     <View style={{flexDirection:"row", justifyContent:"flex-start"}}>
                    <RadioButton value="Cash" />
                  <Text style={{color:appColor.darkGrey,marginTop:7, fontFamily:Fonts.Regular}}>{"Cash"}</Text>
                    </View>
                  <View style={{flexDirection:"row", justifyContent:"flex-start",marginLeft:30}}>
                <RadioButton value="Net_Banking" />
                <Text style={{color:appColor.darkGrey,marginTop:7,fontFamily:Fonts.Regular}}>{"Net Banking"}</Text>
                </View>
                </View>
               </RadioButton.Group>
            </View>
            )
          }
         
         {
           this.state.payment_mode!=="Net_Banking"&&(
            <View style={[styles.InputView,{marginBottom:20}]}>
            <Text style={styles.rupeeStyle}>{"Enter Amount in"} {'\u20B9 '}</Text>
            <Form style={{width:"100%"}}>
                   <Item regular success style={styles.textInputView}>
                   <Input 
                     keyboardType="number-pad" 
                     placeholder="Amount" 
                     style={styles.textInputStyle} 
                     onChangeText={text=>this.onEnterAmount(text)}
                     value={this.state.amount}
                     />
                   </Item>
           </Form>
           </View>
           )
         }
        {/* <View>
          <Text style={[styles.TextHeadingStyle,{marginBottom:10, color:appColor.black,fontSize:18}]}>
            {"Features of Button:"}
          </Text>
          <Text style={[styles.textInputStyle,{color:appColor.darkGrey}]}>
          <Text style={styles.TextHeadingStyle}>1. Submit & Add </Text>- Want to add same type of expanses for atleast 1 time or increase the same expanse for additional bills.{"\n"}
          Example - if you have at least 1 fuel /toll/one-way bill or you want add more than 1 bills with same type. {"\n"}
          <Text style={styles.TextHeadingStyle}>2. End Trip</Text>- Proceed to end the running trip.{"\n"}
          Example - if you want to end trip then you can click on proceed to end button to end your running trip.{"\n"}
          </Text>
        </View> */}
         <View style={[styles.uploadview,{marginBottom:20,flexShrink:1, marginTop:"10%"}]}>
          <AppButton
            text='Submit & Add'
            bgColor={appColor.Blue}
            width={'45%'}
            borderRadius={5}
            textColor={appColor.white}   
            onClickFunc={()=>this.onSubmit()}         
            />
             <AppButton
            text='End Trip'
            bgColor={appColor.Blue}
            width={'45%'}
            borderRadius={5}
            textColor={appColor.white}   
            onClickFunc={()=>this.onsubmittoEndTrip()}         
            />
         </View>
         {/* <View style={[styles.uploadview,{marginBottom:20,flexShrink:1, marginTop:"10%"}]}>
          <AppButton
            text='Navigate to Location'
            bgColor={appColor.Blue}
            width={'85%'}
            borderRadius={5}
            textColor={appColor.white}   
            onClickFunc={()=>this.navigateChange(this.state.desLat,this.state.desLng)}         
            />
           
         </View> */}
         {/* {
           this.state.manual=="no" &&( */}
            <View style={{flexDirection:"row",justifyContent:"center"}}>
            <AppButton
              text='Navigate to Location'
              bgColor={appColor.Blue}
              width={'100%'}
              borderRadius={5}
              textColor={appColor.white}   
              onClickFunc={()=>this.navigateChange(deslat,desLng)}         
              />
           </View>
           {/* {/* )
         } */}               
         
         {
           (this.state.driverType==="single_permanent"||this.state.driverType==="permanent"|| this.state.driverType==="bulk")&&(
            <View style={{flexDirection:"row",justifyContent:"center", marginTop:10}}>
            <AppButton
              text='Add More Car'
              bgColor={appColor.Blue}
              width={'100%'}
              borderRadius={5}
              textColor={appColor.white}   
              onClickFunc={()=>this.addMoreCar()}         
              />
         </View>
           )
         }
         {/* <View style={{flexDirection:"row",justifyContent:"center", marginTop:10}}>
            <AppButton
              text='Add More Car'
              bgColor={appColor.Blue}
              width={'100%'}
              borderRadius={5}
              textColor={appColor.white}   
              onClickFunc={()=>this.addMoreCar()}         
              />
         </View> */}
        </View> 
      </View>
      </ScrollView>
    );
  }
}



const styles = StyleSheet.create({

    ContainerView:{
        padding:20
    },
    textStyle:{
        fontFamily:Fonts.Bold,
        fontSize:16,
        color:appColor.darkGrey,
    },
    textView:{
        marginBottom:20,
        flexDirection:"row",
        justifyContent:"space-between"
    },
    uploadview:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    textInputView:{
        borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey 
    },
    textInputStyle:{
        color: appColor.white, fontSize: 16, color: appColor.black
    },
    InputView:{
        marginBottom:20
    },
    ImageStyle:{
        width: 50, height: 50, borderRadius:5, marginTop:8
    },
    rupeeStyle:{
        marginBottom:10,color:appColor.darkGrey,fontFamily:Fonts.Medium,fontSize:14
    },
    TextHeadingStyle:{
      fontFamily:Fonts.Bold,
      fontSize:Fonts.h20,
      textAlign:"left"
    },
    staticlighttext: {
      color: appColor.darkGrey,
      marginBottom: 10,
      fontSize: 14,
      fontFamily: Fonts.Medium,
    },
  
});