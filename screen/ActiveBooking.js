import React from "react";
import {   StyleSheet,
    Text,
    View,
    Image,
    Modal,
    TouchableOpacity,
    TouchableHighlight,
    Linking,
    AsyncStorage,
    FlatList,
    Platform,
    StatusBar,
    ToastAndroid
  } from "react-native";

  import { AppButton,InputText, appColor,BaseUrl,Toast,RNPickerSelect,CheckBox,Spinner,ImagePicker,RazorpayCheckout,MapViewDirections,geolib,Switch,OneSignal,NavigationEvents,DrawerActions,CodeInput,RNAndroidLocationEnabler,Button,getDirections,image,Fonts,Header,RestApiKey,ClientId,ClientSecret,grantType,Geolocation,moment,NetInfo} from "././common";

  import {Container, Item} from "native-base";
import BackgroundTimer from 'react-native-background-timer';



export default class ActiveBooking extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            srcLatToNavigate: 0,
            srcLongToNavigate: 0,
            latitude: 0,
            longitude: 0,
            activeDrives: "",
            isLoading: false,
            poc_id: "",
            request_id: "",
            source: "",
            destination: "",
            company_name: "",
            schedule_time: "",
            company_id: "",
            poc_name: "",
            trip_type: "",
            booking_status:"",
            sourceLng:"",
            sourceLat:"",
            destinationLng:"",
            destinationLat:"",
            tour_type:"",
            vehicle_type:"",
            request_type:"",
            vehicle_number:"",
            poc_number:"",
            numbers_of_drivers:"",
            notification_type:"",
            reachLocationFlag:true,
            tripStart:false,
            otpTxt:"",
            driverType:""
        };
        OneSignal.inFocusDisplaying(2);
      }


 async callreachAPI(intervalId1){
  try {
    const driverId = await AsyncStorage.getItem('driverId');
    const ride_history_id = await AsyncStorage.getItem("ride_history_id_notification");
    var formData = new FormData();
    formData.append("action", "driver_reach_location");
    formData.append("driver_id", driverId);
    formData.append("ride_history_id", ride_history_id);
    const response = await fetch(
      BaseUrl.url5,
      {
        body: formData,
        method: "POST",
        headers: {
          Accept: "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      }
    );
    const responseJson = await response.json();
    if (responseJson.success === true) {
      if(responseJson.data.status==false){
        console.log("driver not reached to hub. hjhjhkjhkjhkgg",responseJson)
        this.setState({reachLocationFlag:true})
      } else {
        this.setState({reachLocationFlag:false})
        console.log("driver has reached to hub.",responseJson)
        //BackgroundTimer.clearInterval(intervalId1);
      }
    } else {
      this.setState({reachLocationFlag:true})
    }
  } catch (error) {
    console.log(error)
    BackgroundTimer.clearInterval(intervalId1);
  }
 }

  async _navigateDriver(destination) {
    this.updateGeoAddress(destination)
  }

  async updateGeoAddress(destination){
    const tokenType = await AsyncStorage.getItem("tokenType");
    const token = await AsyncStorage.getItem("token")
    try {
      this.setState({isLoading:true})
      const response = await fetch(
        "https://atlas.mapmyindia.com/api/places/geocode?address="+destination,
        {
          method: "GET",
          headers: {
            Authorization:JSON.parse(tokenType)+" "+JSON.parse(token),
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      if(responseJson.error==="invalid_token"){
        this.setState({isLoading:false})
          this.updateToken().then(()=>{
            return this.updateGeoAddress(destination)
          })
      } else {
        this.setState({isLoading:false})
        //console.log("final source lat",responseJson.copResults.latitude)
        const destLat = responseJson.copResults.latitude;
        const destLong = responseJson.copResults.longitude;
        this.navigateChange(destLat, destLong)
      }
    } catch(error){
      this.setState({isLoading:false})
        alert(error)
    }
  }

  async updateToken(){
    try {
      var formData = new FormData();
      formData.append("grant_type", grantType);
      formData.append("client_id", ClientId);
      formData.append("client_secret",ClientSecret);
      const response = await fetch(
        "https://outpost.mapmyindia.com/api/security/oauth/token",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );
      const responseJson = await response.json();
      console.log("update token==",responseJson)
      await AsyncStorage.setItem("token",JSON.stringify(responseJson.access_token))
      await AsyncStorage.setItem("tokenType",JSON.stringify(responseJson.token_type))
    } catch(error){
        alert(error)
    }
  }

  navigateChange(desLat, desLng){
    const data = {
      source: {
      latitude: this.state.sourceLat,
      longitude: this.state.sourceLng
    },
    destination: {
      latitude: JSON.parse(desLat),
      longitude: JSON.parse(desLng)
    },
    params: [
      {
        key: "travelmode",
        value: "driving"        // may be "walking", "bicycling" or "transit" as well
      },
      {
        key: "dir_action",
        value: "navigate"       // this instantly initializes navigation using the given travel mode
      }
    ]
  }
  console.log("map data ==",data)
    getDirections(data)
  }
    
      async _reloadDrives(){
        if(this.state.reachLocationFlag==true){
          this.refs.codeInputRef1.clear();
        }
        let data = await this.props.navigation.getParam("item");
       console.log('data',data)
        this.setState({
            poc_id: data.poc_id,
            request_id: data.poc_id.request_id,
            source: data.source,
            destination: data.destination,
            company_name: data.company_name,
            schedule_time: data.rideTime,
            company_id: data.company_id,
            poc_name: data.poc_name,
            trip_type: data.trip_type,
            booking_status:data.status,
            sourceLng:data.sourcelng,
            sourceLat:data.sourcelat,
            destinationLng:data.destinationLng,
            destinationLat:data.destinationLat,
            tour_type:data.tour_type,
            vehicle_type:data.vehicle_type,
            request_type:data.request_type,
            vehicle_number:data.vehicle_number,
            poc_number:data.poc_number,
            numbers_of_drivers:data.numbers_of_drivers,
            notification_type:data.notification_type,
            driverType:data.request_type
        })
    this._fetchCurrentLocationToRedirect();
    let startTrip = await AsyncStorage.getItem("startTipFlag");
    if(startTrip=="flaged"){
      this.setState({reachLocationFlag:false,tripStart:true})
    } else {
      this.setState({tripStart:false})
      try{
        const intervalId1 = BackgroundTimer.setInterval(() => {
          this.callreachAPI(intervalId1);
        }, 6000);
          } catch(error){
            console.log(error)
          }
        }
      }

      async componentWillUnmount(intervalId1){
        await BackgroundTimer.clearInterval(intervalId1);
      }

      async _fetchCurrentLocationToRedirect() {
       await Geolocation.getCurrentPosition(
          position => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            });
          },
          error => ToastAndroid.show(error.message, ToastAndroid.LONG),
          { enableHighAccuracy: false, timeout: 20000 }
        );
      }

      async onStartRide(){
        let currentStatus = await AsyncStorage.getItem("checkedin_id");
          //  let data = await this.props.navigation.getParam("item");
        console.log("check",currentStatus)
        if(currentStatus==null || currentStatus==undefined){
          ToastAndroid.show("You haven't check-in, Please check-in first.",ToastAndroid.LONG);
          this.props.navigation.navigate("Checkin");
        } else {
          this.updateLocation();
          
          let startTrip = await AsyncStorage.getItem("startTipFlag");
          if(startTrip==="flaged"){
            this.props.navigation.navigate("AddFuel",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,manual:"no",driverType:this.state.driverType});
          } else {
            this.props.navigation.navigate("StartTrip",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,vehicle_number:this.state.vehicle_number,bookingType:"start",reachLocationFlag:false,driverType:this.state.driverType,manual:"no"})
          }
        }
      }

      async checkReachLocation(){
        ToastAndroid.show("You are not located at service location area.",ToastAndroid.LONG);
      }

    async updateLocation(){
     longArray=[]; 
     timeStampArray=[];
     let rideStatus = await AsyncStorage.getItem("initiateFlag");
     if(rideStatus=="flaged"){
       BackgroundTimer.runBackgroundTimer(() => { 
        Geolocation.getCurrentPosition(
            (position) => {
               const currentLongitude = JSON.stringify(position.coords.longitude);
               const currentLatitude = JSON.stringify(position.coords.latitude);
               const timeStamp = JSON.stringify(position.timestamp);
               longArray.push(currentLongitude.concat(",").concat(currentLatitude))
               timeStampArray.push(timeStamp)
               var resultLatlng = {};
                 for (var i = 0; i < longArray.length; ++i){
                   resultLatlng["position" + (i+1)] = longArray[i];                                                                                                                          
               }
               var resultTimestamp = {};
                 for (var i = 0; i < timeStampArray.length; ++i){
                   resultTimestamp["position" + (i+1)] = timeStampArray[i];                                                
               }
               //console.log("resultLatlng==",resultLatlng)
               const finalLatLong = Object.keys(resultLatlng).map(function(k){return resultLatlng[k]}).join(";");
               const finaltimestamp = Object.keys(resultTimestamp).map(function(k){return parseInt(resultTimestamp[k]/1000)}).join(";");
               AsyncStorage.setItem("finalLatLong",JSON.stringify(finalLatLong))
               AsyncStorage.setItem("finaltimestamp",JSON.stringify(finaltimestamp))
               //console.log("finaltimestamp==",finaltimestamp)
               //console.log("finalLatLong==",finalLatLong)
               this.sendLocation(finalLatLong,finaltimestamp)
            },
            (error) => console.log("error.message",error.message),
            //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
         );
       }, 50000);
     } else {
       BackgroundTimer.stopBackgroundTimer();
     }
   }
 
   async sendLocation(finalLatLong,finaltimestamp){
     const driverId = await AsyncStorage.getItem('driverId');
     const ride_history_id = await AsyncStorage.getItem("ride_history_id_notification");
     try {
       var formData = new FormData();
       formData.append("action", "update_live_location");
       formData.append("driver_id",driverId);
       formData.append("driver_booking_id", "");
       formData.append("b2b_ride_history_id",ride_history_id);
       formData.append("latlong_coordinate",finalLatLong);
       formData.append("timestamp", finaltimestamp);
       console.log("formdata request for location==++++++",formData)
       const response = await fetch(BaseUrl.url5, {
         body: formData,
         method: "POST",
         headers: {
           "Accept": "multipart/form-data",
           "Content-Type": "multipart/form-data",
           'Cache-Control': 'no-cache, no-store, must-revalidate',
           'Pragma': 'no-cache',
           'Expires': 0
         }
       });
       const responseJson = await response.json();
       console.log('driverdataride===final ====++++++++',responseJson)  
       if(responseJson.success===true){
        //console.log('responseJson.success',responseJson)  
        } else {
          //console.log('responseJson.false',responseJson)  
       }  
     } catch (error) {
       console.log("error rsp==",error);
     }
   }

   async submitOTP(){
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      const ride_history_id = await AsyncStorage.getItem("ride_history_id_notification");
      var formData = new FormData();
      formData.append("action", "b2b_start_ride_with_otp");
      formData.append("driver_id", driverId);
      formData.append("ride_history_id", ride_history_id);
      formData.append("startotp",this.state.otpTxt);
      const response = await fetch(
        BaseUrl.url5,
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );

      const responseJson = await response.json();
      console.log('submit otp data')
      console.log(responseJson)
      console.log(formData)
      if (responseJson.success === true) {
        if(responseJson.data.status==true){
          this.setState({reachLocationFlag:false});
          this.onStartRide()
        } else {
          console.log("check status")
        }
      } else {
        ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
        //this.refs.codeInputRef1.clear();
      }
    } catch (error) {
      console.log(error)
    }
   }

  render() {
    // if (item.status=="ride_started"){
    //   reachLocationFlag:false
    // }
    return (
        <Container>
        <StatusBar backgroundColor="#19376c" barStyle="light-content" />
        <Header
          text={"Active Rides"}
          leftImage={image.backArrow}
          leftPress={()=>this.props.navigation.goBack()}
        />
        <NavigationEvents onDidFocus={() => this._reloadDrives(null)} />
        <View style={{flex:1}}>
        <Spinner
              visible={this.state.isLoading}
              style={styles.spinnerStyle}
              color="#841584"
              overlayColor="rgba(0, 0, 0, 0.55)"
            />
          <View
                style={{ margin: 15,borderColor:appColor.lightGrey,borderWidth:2,borderRadius:5}}
                
              >
                <View>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                      backgroundColor: "#e5e5e5"
                    }}
                  >
                    <View
                      style={{
                        left: 0,
                        paddingLeft: 10,
                        paddingBottom: 5,
                        paddingTop: 5
                      }}
                    >
                      <Text style={{ color: "#9b9b9b",fontFamily:Fonts.Regular}}>
                        {this.state.trip_type=="one_way"?"One Way":this.state.trip_type=="round_trip"?"Round Trip":this.state.trip_type}
                      </Text>
                    </View>

                    <View
                      style={{
                        right: 0,
                        paddingRight: 10,
                        paddingBottom: 5,
                        paddingTop: 5
                      }}
                    >
                      <Text style={{ color: "#9b9b9b",fontFamily:Fonts.Regular}}>
                        { this.state.booking_status=="scheduled"?"Scheduled":this.state.booking_status=="initiated"?"Initiated":this.state.booking_status=="ride_started"?"Started":this.state.booking_status}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 10,
                      marginBottom: 10
                    }}
                  >
                    <View>
                      <Image
                        source={image.calender}
                        style={styles.imageStyle}
                      />
                    </View>

                    <View style={{ width: "50%", justifyContent: "center" }}>
                      <TouchableHighlight>
                        <Text style={{ fontSize: Fonts.h14, color: "#5d5d5d",fontFamily:Fonts.Regular,marginLeft:5,marginTop:5}}>
                          {this.state.schedule_time}
                        </Text>
                      </TouchableHighlight>
                    </View>
                  </View>

                  <View style={{ flexDirection: "row", marginBottom: 10 }}>
                    <View style={styles.circle} />

                    <View>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={{ marginRight: 30,fontFamily:Fonts.Regular}}
                      >
                        {this.state.source}
                      </Text>
                    </View>
                  </View>

                  <View style={{ flexDirection: "row", marginBottom: 10 }}>
                    <View
                      style={[styles.circle, { backgroundColor: "#FF0000" }]}
                    />

                    <View
                      numberOfLines={1}
                      ellipsizeMode="tail"
                      style={{ marginRight: 30}}
                    >
                      <Text style={{fontFamily:Fonts.Regular}}>{this.state.destination}</Text>
                    </View>
                  </View>

                  <View
                    style={[
                      styles.Button,
                      { flexDirection: "row", justifyContent: "space-evenly" }
                    ]}
                  >
                    <Button
                      style={{ backgroundColor: appColor.Blue,fontFamily:Fonts.Regular}}
                      mode="contained"
                      onPress={() => {
                        this.state.reachLocationFlag?this.checkReachLocation():this.onStartRide()
                      }}
                    >
                      {this.state.reachLocationFlag?"Reach Location":this.state.tripStart==true?"End Ride":"Start Ride"}
                    </Button>

                    <Button
                      style={{ backgroundColor: appColor.Blue,fontFamily:Fonts.Regular}}
                      mode="contained"
                      onPress={() =>
                        Linking.openURL(`tel:${this.state.poc_number}`)
                      }
                    >
                      Call
                    </Button>

                    <Button
                      style={{ backgroundColor: appColor.Blue,fontFamily:Fonts.Regular}}
                      mode="contained"
                      loading={false}
                      onPress={() => this.navigateChange(this.state.destinationLat,this.state.destinationLng)}
                    >
                      Navigate
                    </Button>

                  </View>
                </View>
              </View>
              {
                this.state.reachLocationFlag==true&&(
                  <View style={{marginTop:20}}>
                <View>
                  <Text style={{textAlign:"center",fontFamily:Fonts.Medium,fontSize:16,color:appColor.darkGrey}}>
                    Start Ride By Entering OTP
                  </Text>
                </View>
              <View style={styles.codeInputStyles}>
              <CodeInput
                ref="codeInputRef1"
                className={"border-b"}
                activeColor="#1e4281"
                inactiveColor="#dedede"
                space={20}
                autoFocus={true}
                keyboardType="numeric"
                fontFamily={Fonts.Regular}
                size={50}
                inputPosition="left"
                codeLength={4}
                onFulfill={code => this.setState({ otpTxt: code + "" })}
              />
             </View>
             <View style={{flexDirection:"row",justifyContent:"center", marginTop:0}}>
            <AppButton
              text='Submit'
              disabled={this.state.otpTxt.length>3? false : true}
              bgColor={this.state.otpTxt.length>3?appColor.Blue:appColor.darkGrey}
              width={'80%'}
              borderRadius={5}
              textColor={appColor.white}   
              onClickFunc={()=>this.submitOTP()}         
              />
         </View>
              </View>
                )
              }
        </View>
    </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    height: 610,
    padding: 20,
    justifyContent: "space-between",
    backgroundColor: "white"
  },
  stepOneView: {
    backgroundColor: "white",
    borderRadius: 10,
    borderColor: "#BDBDBD",
    borderWidth: 1,
    paddingLeft: 5,
    paddingTop: 5,
    margin: 15
  },
  stepOneText: {
    fontSize: Fonts.h16,
    color: "black"
  },
  stepTwoView: {
    backgroundColor: "white",

    justifyContent: "space-between",
    borderRadius: 10,
    borderColor: "#BDBDBD",
    borderWidth: 1,
    paddingLeft: 5,
    paddingTop: 5,
    margin: 15
  },
  stepTwoText: {
    fontSize: Fonts.h16,
    color: "black"
  },
  stepThreeView: {
    backgroundColor: "white",
    flex: 0.4,
    justifyContent: "space-between",
    borderRadius: 10,
    borderColor: "#BDBDBD",
    borderWidth: 1,
    paddingLeft: 5,
    paddingTop: 5,
    margin: 15
  },

  stepThreeText: {
    fontSize: Fonts.h16,
    color: "black"
  },
  Button: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 10
  },
  contentContainer: {
    justifyContent: "space-between"
  },
  containerScroll: {
    flex: 1
  },
  verifyButton: {
    height: 50,

    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  buttonText: {
    color: "#fff",
    fontFamily: "Helvetica",
    fontSize: Fonts.h22,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    fontWeight: "bold"
  },
  imageStyle: {
    padding: 10,
    marginLeft: 10,
    marginRight: 5,
    marginTop: 5,
    height: 25,
    width: 25,
    resizeMode: "stretch",
    alignItems: "center"
  },

  circle: {
    width: 6,
    height: 6,
    borderRadius: 5 / 2,
    backgroundColor: "#228B22",
    alignItems: "center",
    marginLeft: 10,
    marginTop: 7,
    marginRight: 10
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 130,
    borderRadius: 30,
    backgroundColor: appColor.DarkBlue,
    borderColor: "#000"
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 20,
    backgroundColor: "#ecf0f1"
  },
  button: {
    backgroundColor: appColor.DarkBlue,
    width: "45%",
    borderRadius: 1,
    borderColor: "#000",
    height: 40,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    borderRadius: 4
  },
  headerTextView: {
    fontSize: Fonts.h18,
    fontWeight: "bold",
    textAlign: "center",
    color: "#fff",
  },
  spinnerStyle: {
    color: "#000000"
  },
  codeInputStyles: {
    height: 70,
    alignItems: "center",
    marginLeft: 10,
    marginBottom: 50
  },
});

