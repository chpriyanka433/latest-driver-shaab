import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right, Button, ActionSheet, Item,Radio } from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  PermissionsAndroid,
  NetInfo,
  AsyncStorage,
  ToastAndroid,
  BackHandler,
  RefreshControl,
  Dimensions
} from 'react-native';
// import SearchHeader from "../common/SearchHeader";

import {appColor,AppButton,Spinner,image, Fonts,Header} from "../common";

import { ScrollView } from 'react-native-gesture-handler';


class ResultFail extends Component {


  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      percentage:"",
      blankAnswer:"",
      wrongAnswer:"",
      rightAnswer:"",
      passingPercent:""
    }
  }

  async componentDidMount(){
    var percentage= await this.props.navigation.getParam("percentage");
    var wrongAnswer= await this.props.navigation.getParam("wrongAnswer");
    var blankAnswer= await this.props.navigation.getParam("blankAnswer");
    var rightAnswer= await this.props.navigation.getParam("rightAnswer");
    var passingPercent = await this.props.navigation.getParam("passingPercent")
    this.setState({
      percentage:percentage,
      wrongAnswer:wrongAnswer,
      blankAnswer:blankAnswer,
      rightAnswer:rightAnswer,
      passingPercent:passingPercent
    })  
  }



  render() {
    return (

      <Container style={{Height:Dimensions.get('window').height,Width:Dimensions.get('window').width}}>
        <Content>
        <Header
          text={"Your Test Result"}
          leftImage={image.backArrow}
          leftPress={()=>this.props.navigation.goBack()}
          />
          <ScrollView>
            <ImageBackground style={{ width: "100%", height: 200, backgroundColor: appColor.Blue, borderBottomRightRadius: 30, borderBottomLeftRadius: 30 }}>
            
                  <Image
                   style={styles.IconSize}
                   resizeMode="contain"
                    source={image.failIcon}
                  />
             
             <Text style={styles.BlueBackgroundText}>Sorry,You have Failed.</Text>
             <Text style={{fontSize:Fonts.h14,fontFamily:Fonts.Regular,color:appColor.white,textAlign:"center"}}>Better luck next time</Text>
            </ImageBackground>
            
              <Spinner
                visible={this.state.isLoading}
                style={{ color: '#000' }}
                color={appColor.Blue}
                overlayColor="rgba(0, 0, 0, 0.55)"
              />
              {/* <NavigationEvents
                onDidFocus={() => this.updateCallback()}
              />  */}
              <Text style={[styles.testResultStyle,{textAlign:"center",fontFamily:Fonts.Medium,fontSize:Fonts.h22, marginTop:20}]}>Your score is</Text>
              <View style={styles.scoreView}>
              <View style={styles.resultPerView}>
                <Text style={styles.scoreTextStyle}>{parseInt(this.state.percentage)}%</Text>
              </View>
              </View>
              <View style={styles.testResultView}>
                <Text style={styles.testResultStyle}>
                      {"Correct Answer: "}
                </Text>
                <Text style={[styles.testResultStyle,{marginLeft:5}]}>
                    {this.state.rightAnswer}
                </Text>
              </View>
              <View style={styles.testResultView}>
                <Text style={styles.testResultStyle}>
                      {"Wrong Answer: "}
                </Text>
                <Text style={[styles.testResultStyle,{marginLeft:5}]}>
                    {this.state.wrongAnswer}
                </Text>
              </View>
              <View style={styles.testResultView}>
                <Text style={styles.testResultStyle}>
                      {"Blank Answer: "}
                </Text>
                <Text style={[styles.testResultStyle,{marginLeft:5}]}>
                    {this.state.blankAnswer}
                </Text>
              </View>

              <View style={styles.testResultView}>
                <Text style={styles.testResultStyle}>
                      {"Eligibility Criteria: "}
                </Text>
                <Text style={[styles.testResultStyle,{marginLeft:5}]}>
                    {this.state.passingPercent}{"%"}
                </Text>
              </View>
              
              <View style={styles.button_view}>
                <AppButton
                  text='Exit & Try Again'
                  bgColor={appColor.Blue }
                  width={'100%'}
                  textColor={appColor.white}
                  onClickFunc={() => BackHandler.exitApp()}
                />
              </View>
            
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  headerTextView: {
    fontSize: Fonts.h18,
    fontFamily:Fonts.Regular,
    fontWeight: "bold",
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    marginLeft: Dimensions.get("window").width/5
  },
  BlueBackgroundText:{
      fontSize:Fonts.h18,
      fontFamily:Fonts.Medium,
      color:appColor.white,
      textAlign:"center",
      marginTop:30,
      letterSpacing:1
  },
  IconSize: {
    width:70,
    height: 70,
    marginTop:20,
    marginHorizontal:"40%"
  },
  resultPerView:{
      width:100,
      height:100,
      borderRadius:100/2,
      backgroundColor:"#ff0303",
      flexDirection:"row",
      justifyContent:"center",
      alignItems:"center",
  },
  scoreView:{
      flex:1,
      flexDirection:"row",
      justifyContent:"center",
      alignItems:"center",
      marginTop:10

  },
  scoreTextStyle:{
    fontSize:Fonts.h26,fontFamily:Fonts.Bold,color:appColor.white,textAlign:"center"
  },
  button_view:{
    justifyContent: 'center',
     marginBottom: 20,
      alignItems: 'center'
      ,padding:20,
      marginTop:"30%"
  },
  scoreText:{
    fontSize:Fonts.h20,fontFamily:Fonts.Medium,color:appColor.darkGrey,textAlign:"center",marginTop:20
  },
  testResultStyle:{
    color:appColor.darkGrey, fontFamily:Fonts.Regular,fontSize:Fonts.h14
  },
  testResultView:{
    flexDirection:"row", justifyContent:"center",marginHorizontal:40,
    marginTop:10
  }

});


export default ResultFail;
