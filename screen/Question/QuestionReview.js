import React, { Component, useCallback } from 'react';
import { Container, Content, ListItem, Body, Left, Right, Button, ActionSheet, Item,Radio } from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  PermissionsAndroid,
  NetInfo,
  AsyncStorage,
  ToastAndroid,
  BackHandler,
  RefreshControl,
  Dimensions
} from 'react-native';
// import SearchHeader from "../common/SearchHeader";

import {appColor,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,moment,image, Fonts,Header} from "../common";

import { ScrollView } from 'react-native-gesture-handler';
import SideMenu from "../SideMenu/SideMenu";
import TripHistory from "../TripHistory";
import CheckOut from '../CheckOut';
import ViewProfileScreen from "../ViewProfileScreen";
import StartTrip from "../StartTrip";
import EndTrip from "../EndTrip";
import Initiate from "../Initiate";
import AttendanceHistory from "../AttendanceHistory"


var BUTTONS = ["Camera", "Cancel"];
var CANCEL_INDEX = 1;


class QuestionReview extends Component {


  constructor(props) {
    super(props);
    this.state = {
      path: "",
      images: "",
      imagePath: "",
      latitude: "",
      longitude: "",
      checkedin_id: "",
      userName: "",
      geofencingData: [],
      loading: true,
      dataSource: [],
      mTotalKilometers: "",
      checkStatus: "",
      isLoading: false,
      checkInflag: false ,
      answerarray:'',
      data:[],
      answer:[]
    }

    this.checkInflag=false
  }


  //   _onRefresh(){
  
  //   this.setState({refreshing:true});
  //   this.geofencing().then(() =>{
  //     this.setState({refreshing:false})
  //   });
  // }
   
  componentDidMount(){
    let obj= this.props.navigation.getParam("checkedarray");
    this.setState({answerarray:JSON.stringify(obj),answer:obj})  
  }

   async final_submit(){
    this.setState({isLoading:true})
    try {
     const driverId = await AsyncStorage.getItem('id');
      var formData = new FormData();
      formData.append("action", "calculate_examination_result");
      formData.append("driver_id", driverId);
      formData.append("vehicalType",await this.props.navigation.getParam("vehicalType"))
      //formData.append("driver_id", "b9c74e91-c4e3-5870-ee04-5eb545b9dc19");
      formData.append("questionAnsList",this.state.answerarray);
      const response = await fetch(BaseUrl.url4, {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data"
        }
      });
      const responseJson = await response.json();
      console.log("formdata is===",formData)
      if(responseJson.success === true){
        this.setState({ isLoading: false,data:responseJson.data.result });
        // console.log("responseJson is===",responseJson)
        // console.log("typeof responseJson.data.result.percentage",typeof parseInt(responseJson.data.result.percentage))
        // console.log("typeof responseJson.data.result.passing_percent",typeof parseInt(responseJson.data.result.passing_percent))
        // console.log("check status",parseInt(responseJson.data.result.percentage) >=parseInt(responseJson.data.result.passing_percent))
        //alert(JSON.stringify(responseJson.data))

        if(parseInt(responseJson.data.result.percentage) >=parseInt(responseJson.data.result.passing_percent)){
        this.props.navigation.navigate("ResultPass",{
          percentage:responseJson.data.result.percentage,
          wrongAnswer:responseJson.data.result.wrongAnswered,
          blankAnswer:responseJson.data.result.blankAnswered,
          rightAnswer:responseJson.data.result.rightAnswered,
          passingPercent:responseJson.data.result.passing_percent
        })
        }
        else {
          this.setState({ isLoading: false });
          this.props.navigation.navigate("ResultFail",{
            percentage:responseJson.data.result.percentage,
            wrongAnswer:responseJson.data.result.wrongAnswered,
            blankAnswer:responseJson.data.result.blankAnswered,
            rightAnswer:responseJson.data.result.rightAnswered,
            passingPercent:responseJson.data.result.passing_percent
          })
        }
      }
      else{
       this.setState({ isLoading: false });
      }            
    } catch (error) {
       this.setState({ isLoading: false });
      console.log(error);
    }
    
  }

       async  recheckOption(item,index){
        // alert(item.question)
        
        // if(item.question_id)
        // AsyncStorage.removeItem('option_id')
        // AsyncStorage.setItem('option_id',JSON.stringify(index))
        // alert(AsyncStorage.getItem('option_id'))
        this.props.navigation.navigate("QuestionScreen", AsyncStorage.setItem('option_id',JSON.stringify(index)))
        }

  render() {
    return (

      <Container style={{Height:Dimensions.get('window').height,Width:Dimensions.get('window').width}}>
        <Content>
        <Header
          text={"Questionaire"}
          leftImage={image.backArrow}
          leftPress={()=>this.props.navigation.goBack()}
          />
          <ScrollView
    //       refreshControl={
    // <RefreshControl    refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)} />}
    >
            
              <Spinner
                visible={this.state.isLoading}
                style={{ color: '#000' }}
                color={appColor.Blue}
                overlayColor="rgba(0, 0, 0, 0.55)"
              />
              {/* <NavigationEvents
                onDidFocus={() => this.updateCallback()}
              />  */}
             <FlatList data={this.state.answer}
                       extraData={this.state}
                       renderItem={({item,index}) =>
                      <View style={{padding:20,backgroundColor: index % 2 === 0 ? '#DFE5EB' : '#fff'}}>
                 <View style={{flexDirection:"row"}}>
                 <View style={{width:30,height:30,borderRadius:30/2,backgroundColor:appColor.Blue}}>
            <Text style={{color:appColor.white,textAlign:"center",marginTop:5}}>{index + 1}</Text>
                 </View>
                 <View style={{width:"90%",flexShrink:1}}>
                <Text style={styles.question_text}>{item.question}</Text>
                 </View>
                 <TouchableOpacity onPress={()=>this.recheckOption(item,index)}>
                  <Image
                   style={styles.IconSize}
                   resizeMode="contain"
                    source={image.editIcon}
                  />
                  </TouchableOpacity>
  
                 </View>
            <Text style={styles.option_text}>{item.option_name}</Text>
                 </View>
                       }/>
              <View style={styles.Button_view}>
                <AppButton
                  text='Submit Your Test'
                //   disabled={this.checkInflag ? false : true}
                  bgColor={appColor.Blue }
                  width={'100%'}
                  textColor={appColor.white}
                  onClickFunc={() => this.final_submit()}
                />
              </View>
            
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  headerTextView: {
    fontSize: Fonts.h18,
    fontWeight: "bold",
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    fontFamily:Fonts.Regular,
     marginLeft: Dimensions.get("window").width/5
  },
  
  IconSize: {
    width:15,
    height:15,
    marginTop:5
  },
  option_text:{
    fontSize:Fonts.h14,
    color:appColor.Blue,
    marginLeft:35,
    fontWeight:"500",
    fontFamily:Fonts.Regular
  },
  question_text:{
    fontSize:Fonts.h14,color:appColor.black,
    marginLeft:5,
    fontFamily:Fonts.Regular

  },
  Button_view:{
    justifyContent: 'center',
     marginBottom: 20,
      alignItems: 'center',padding:20 
  }


});

export default QuestionReview;
