import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right, Button, ActionSheet, Item,Radio } from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  PermissionsAndroid,
  NetInfo,
  AsyncStorage,
  ToastAndroid,
  BackHandler,
  RefreshControl,
  Dimensions
} from 'react-native';
// import SearchHeader from "../common/SearchHeader";

import {appColor,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,moment,image,Fonts,Header} from "../common";
  import RadioButton from 'react-native-radio-button'
import { ScrollView } from 'react-native-gesture-handler';
import SideMenu from "../SideMenu/SideMenu";
import TripHistory from "../TripHistory";
import CheckOut from '../CheckOut';
import ViewProfileScreen from "../ViewProfileScreen";
import StartTrip from "../StartTrip";
import EndTrip from "../EndTrip";
import Initiate from "../Initiate";
import AttendanceHistory from "../AttendanceHistory"


var BUTTONS = ["Camera", "Cancel"];
var CANCEL_INDEX = 1;

class Questiontype extends Component {


  constructor(props) {
    super(props);
    this.state = {
      path: "",
      images: "",
      imagePath: "",
      latitude: "",
      longitude: "",
      checkedin_id: "",
      userName: "",
      geofencingData: [],
      loading: true,
      dataSource: [],
      mTotalKilometers: "",
      checkStatus: "",
      isLoading: false,
      checkInflag: false ,
      radio1:true,
      radio2:false,
      radio3:false,
      light:'Light Vehicle',
      heavy:'',
      light_heavy:'',
      radio4:false,
      electric_vehicle:""
    }
    this.checkInflag=false
  }

//   componentWillMount() {

//     this._fetchCurrentLocationToRedirect();

//   }




//   componentDidMount() {
//     const {navigation} =this.props;
//     this.focusListener = navigation.addListener('willFocus', () => {});
//     this._fetchUserDetails();
//     this.geofencing();
//     this.RideSessionDetails();
//     this.CheckInSessionDetails();
    

// async componentDidMount(){
//   const driverId = await AsyncStorage.getItem('id');
//   alert(JSON.stringify(driverId))
// }


toggleRadio1() {
    this.setState({
      radio1: true,
      radio2: false,
      radio3:false,
      radio4:false,
      light:"Light Vehicle"
      });
  }
  toggleRadio2() {
    this.setState({
      radio1: false,
      radio2: true,
      radio3:false,
      radio4:false,
      heavy:"Heavy Vehicle"
      });
  }

  toggleRadio3() {
    this.setState({
      radio1: false,
      radio2: false,
      radio3:true,
      radio4:false,
      light_heavy:"Both"
      });
  }

  toggleRadio4(){
    this.setState({
      radio1: false,
      radio2: false,
      radio3:false,
      radio4:true,
      electric_vehicle:"Electric Vehicle"
      });
  }

  questiontype(){
     if(this.state.radio1 == true){
    this.props.navigation.navigate("QuestionScreen",{question_type:this.state.light})
     }else if (this.state.radio2 == true){
    this.props.navigation.navigate("QuestionScreen",{question_type:this.state.heavy})
   }else if(this.state.radio3 == true){
    this.props.navigation.navigate("QuestionScreen",{question_type:this.state.light_heavy})
    }else if(this.state.radio4 == true){
          this.props.navigation.navigate("QuestionScreen",{question_type:this.state.electric_vehicle})
        }
  }



  _onRefresh(){
    this.setState({refreshing:true});
    this.geofencing().then(() =>{
      this.setState({refreshing:false})
    });
  }

  render() {
    return (

      <Container style={{Height:Dimensions.get('window').height,Width:Dimensions.get('window').width}}>
        <Content>
        <Header
          text={"Vehical Type"}
          //leftImage={image.backArrow}
          //leftPress={()=>this.props.navigation.goBack()}
          />
          <ScrollView
    //       refreshControl={
    // <RefreshControl    refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)} />}
    >
            <View style={{backgroundColor:appColor.lightGrey}}>
            <View style={{flex:1, width: "100%", height: 120, backgroundColor: appColor.Blue, borderBottomRightRadius: 30, borderBottomLeftRadius: 30 }}>

             <Text style={styles.BlueBackgroundText}>Select your vehicle type</Text>
            </View>
            </View>
            
              {/* <Spinner
                visible={this.state.isLoading}
                style={{ color: '#000' }}
                color={appColor.Blue}
                overlayColor="rgba(0, 0, 0, 0.55)"
              />
              <NavigationEvents
                onDidFocus={() => this.updateCallback()}
              /> */}
              <View style={styles.greybackground_view}>
            <View style={{ padding: 20 }}>
              <View style={styles.Vehicle_view}>
                  <View>
                  <Image
                   style={styles.IconSize}
                   resizeMode="contain"
                    source={image.Lightvehicle}
                  />
                <Text style={styles.Vehicle_text}>Light Vehicle</Text>
                </View>
                <RadioButton
  animation={'bounceIn'}
  isSelected={false}
  isSelected={this.state.radio1}
  onPress={() =>  this.toggleRadio1()}
/>
                {/* <Radio selected={false} selectedColor={appColor.Blue}
                    selected={this.state.radio1}
                    onPress={() => this.toggleRadio1()}
                    color={appColor.darkGrey}
                    style={{top:25}}
                    /> */}
              </View>
              </View>
              </View>
              <View style={{ padding: 20 }}>
              <View style={styles.Vehicle_view}>
                  <View>
                  <Image
                   style={styles.IconSize}
                   resizeMode="contain"
                    source={image.Heavyvehicle}
                  />
                <Text style={styles.Vehicle_text}>Heavy Vehicle</Text>
                </View>
                <RadioButton
  animation={'bounceIn'}
  isSelected={false}
  isSelected={this.state.radio2}
                    onPress={() => this.toggleRadio2()}
                   
                    />
              </View>
              </View>
              
              <View style={styles.greybackground_view}>
              <View style={{ padding: 20 }}>
              <View style={styles.Vehicle_view}>
                  <View>
                  <Image
                   style={styles.IconSize}
                   resizeMode="contain"
                    source={image.Heavy_Lightvehcile}
                  />
                <Text style={styles.Vehicle_text}>Both Vehicle</Text>
                </View>
                <RadioButton
  animation={'bounceIn'}
  isSelected={false}
  isSelected={this.state.radio3}
                    onPress={() => this.toggleRadio3()}
                   
                    />
              </View>
              </View>
              </View>
              <View style={{ padding: 20 }}>
              <View style={styles.Vehicle_view}>
                  <View>
                  <Image
                   style={[styles.IconSize]}
                   resizeMode="contain"
                    source={image.electric}
                  />
                <Text style={styles.Vehicle_text}>Electric Vehicle</Text>
                </View>
                <RadioButton
  animation={'bounceIn'}
  isSelected={false}
  isSelected={this.state.radio4}
                    onPress={() => this.toggleRadio4()}
                  
                    />
              </View>
              </View>
              {/* <View style={{ justifyContent: 'center', marginBottom: 20, alignItems: 'center',padding:20 }}>
                <AppButton
                  text='Submit'
                //   disabled={this.checkInflag ? false : true}
                  bgColor={appColor.Blue }
                  width={'100%'}
                  textColor={appColor.white}
                  onClickFunc={() => this.questiontype()}
                />
              </View> */}
            
          </ScrollView>
        </Content>
        <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={()=>this.questiontype()}
                style={[styles.verifyButton]}>
                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
            </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  RegisterButton: {
    backgroundColor: appColor.yellow,
    marginTop: 20
  },
  circleView: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: appColor.lightGrey,
    marginHorizontal: "42%",
    marginTop: 30
    // marginRight:20

  },
  circleViewText: {
    fontSize: Fonts.h24,
    fontFamily: "Roboto-Medium",
    textAlign: "center",
    color:appColor.Blue,
    marginTop:12
  },
  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  headerTextView: {
    fontSize:Fonts.h18,
    fontFamily:Fonts.regular,
    fontWeight: "bold",
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    marginLeft: Dimensions.get("window").width/5
  },
  BlueBackgroundText:{
      fontSize:Fonts.h18,
      color:appColor.white,
      textAlign:"center",
      marginTop:50,
      fontFamily:Fonts.regular
  },
  IconSize: {
    width:40,
    height: 40
  },
  Vehicle_text:{
    fontSize:Fonts.h16,  fontFamily:Fonts.regular,color:appColor.black
  },
  Vehicle_view:{
    justifyContent:'space-between',
     marginBottom: 20,
     flexDirection:"row"
  },
greybackground_view:{width:"100%",
height:120,
backgroundColor:appColor.lightGrey},
  buttonContainer: {
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: 40
  },
  verifyButton: {
    position: "absolute",
    height: 50,
    bottom: 10,
    left: 10,
    right: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor:appColor.Blue
  },
  buttonText: {
    color: "#fff",
    fontSize: Fonts.h16,
    fontFamily:Fonts.Medium
  },

});


export default Questiontype;
