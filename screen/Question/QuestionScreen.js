import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right, Button, ActionSheet, Item,Radio,Picker,Form,Title} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  PermissionsAndroid,
  NetInfo,
  AsyncStorage,
  ToastAndroid,
  BackHandler,
  RefreshControl,
  Dimensions
} from 'react-native';
// import SearchHeader from "../common/SearchHeader";

import {appColor,AppButton,BaseUrl,Spinner,NavigationEvents,image,Fonts,Header} from "../common";
import { PowerTranslator, ProviderTypes, TranslatorConfiguration, TranslatorFactory } from 'react-native-power-translator';
import RadioButton from 'react-native-radio-button'


var BUTTONS = ["Camera", "Cancel"];
var CANCEL_INDEX = 1;

const selectLanguage = [
  { shortform: 'en', longform: 'English' },
  { shortform: 'hi', longform: 'Hindi' },
  { shortform: 'bn', longform: 'Bengali' },
  { shortform: 'mr', longform: 'Marathi' },
  { shortform: 'pa', longform: 'Punjabi' },
  { shortform: 'kn', longform: 'Kannada' },
  { shortform: 'gu', longform: 'Gujarati'}
];


class QuestionScreen extends Component {


  constructor(props) {
    super(props);
    this.state = {
      path: "",
      images: "",
      imagePath: "",
      latitude: "",
      longitude: "",
      checkedin_id: "",
      userName: "",
      geofencingData: [],
      loading: true,
      dataSource: [],
      mTotalKilometers: "",
      checkStatus: "",
      isLoading: false,
      checkInflag: false ,
      checked:[],
      data:[],
      value: null,
      question_len:[],
      answer_color:false,
      questionnumber:1,
      editquestion:'',
      selected: "Select",
      title:Title,
      languageCode: 'en'
    }
    this.qno = 0
    this.checkInflag=false  

  }

  onValueChange(value: string) {
    this.setState({
      selected: value
    });
    let val = selectLanguage[value]
    if(value=="Select"){
      return false
    } else {
    this.changeLanguage(val)
    }
  }

  changeLanguage(languageCode) {
    this.setState({ languageCode: languageCode.shortform });
  }

  async componentDidMount(){
    this.getQuestions();
   
    // this.setState({editquestion:obj})
  }

  async handleTranslate() {
    // this.setState({submit: true})
    // const translator =await TranslatorFactory.createTranslator();
    // translator.translate(this.state.question,this.state.languageCode).then(translate=>{console.log(translate)})
   this.setState({questionnew:this.state.question})
  }

  async getQuestions() {
    this.setState({isLoading:true})
     try {
       const driverId = await AsyncStorage.getItem('id');
       let questiontype= this.props.navigation.getParam("question_type");
       var formData = new FormData();
       formData.append("action", "get_questions_list");
       formData.append("driver_id", driverId);
       //formData.append("driver_id", "b9c74e91-c4e3-5870-ee04-5eb545b9dc19");
       formData.append("vehicalType", questiontype);
       const response = await fetch(BaseUrl.url4, {
         body: formData,
         method: "POST",
         headers: {
           "Accept": "multipart/form-data",
           "Content-Type": "multipart/form-data"
         }
       });
       const responseJson = await response.json();
       if(responseJson.success === true){
         this.setState({ isLoading: false,data:responseJson.data.questionDetails });
         this.questions();
       }
       else{
         this.setState({ isLoading: false });
         alert(JSON.stringify(responseJson))
       }            
     } catch (error) {
      this.setState({ isLoading: false });
       alert(JSON.stringify(error))
       console.log(error);
     }
   }

   async questions(){
    let jdata=this.state.data
     arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
    this.setState ( {
      question : arrnew[this.qno].question,
      question_id:arrnew[this.qno].question_id,
      options : arrnew[this.qno].options,
      question_len :arrnew 
     })
    this.question_all();
   }

    async question_all(){
       let jdata=this.state.data
       var test = this.state.checked
       for(i=0;i<jdata.length;i++){
       test.push({question:jdata[i].question,question_id:jdata[i].question_id,})
       }
       this.setState({checked:test})
      //  alert(JSON.stringify(test))
      
    }
  

   prev(){
    if(this.qno > 0){
      this.qno--;
      this.setState({ question: arrnew[this.qno].question, options: arrnew[this.qno].options,question_id:arrnew[this.qno].question_id,questionnumber:this.state.questionnumber  - 1,},this.handleTranslate)
    }
  }
  next(){
    if(this.qno < arrnew.length-1){
      this.qno++;
      this.setState({ question: arrnew[this.qno].question, options: arrnew[this.qno].options,question_id:arrnew[this.qno].question_id,questionnumber:this.state.questionnumber  + 1},this.handleTranslate)
      }
    else if(this.qno == arrnew.length-1){
     this.props.navigation.navigate("QuestionReview",{checkedarray:this.state.checked,vehicalType:this.props.navigation.getParam("question_type")})
    }
  }

 handleChange = (item,index) => {
      let obj = this.state.options
    for (a of obj) {
     if(a.option_id == item.option_id){
         a.isSelected = ! a.isSelected         
     }else{
      a.isSelected = false
      }
    }
     this.setState({
      options: obj,
      option_id:item.option_id,
      option_name:item.option_name
      })
     this.Questionreview(item);
   }

       Questionreview(item){
      var test =this.state.checked
      if(test && test.length>0){
        // var checkInsert='';
        for(let i=0;i<test.length;i++){
        if(test[i].question_id == this.state.question_id){
          if(item.isSelected ==true){
            test[i].option_name=item.option_name
            test[i].option_id=item.option_id
            // this.setState({answer_color:test[i]})

          }else{
             test[i].option_name=''
              test[i].option_id=''
              // this.setState({answer_color:''})
           }
        }
        }
        }
        this.setState({checked:test})
        //  alert(JSON.stringify(test))
      
     }


    async updateCallback(){
     
      this.setState({editvalue:await AsyncStorage.getItem('option_id')})
      if(this.state.editvalue){
        this.editoption()
      }
     }

      async editoption(){
        var test =this.state.checked 
        // var number=
        this.setState({ question: arrnew[this.state.editvalue].question, options: arrnew[this.state.editvalue].options,question_id:arrnew[this.state.editvalue].question_id,questionnumber: (+this.state.editvalue) + (+1)})
        AsyncStorage.removeItem('option_id')
        
      }
     
  _onRefresh(){
  
    this.setState({refreshing:true});
    this.geofencing().then(() =>{
      this.setState({refreshing:false})
    });
  }

  render() {
    TranslatorConfiguration.setConfig(ProviderTypes.Google, 'AIzaSyComH3uVSYHWeEtzyEByzgi76SOseHvSG0', this.state.languageCode);
    let { options } = this.state;
    return (

      <Container style={{Height:Dimensions.get('window').height,Width:Dimensions.get('window').width}}>
        <Content>
        <Header
          text={"Questionaire"}
          leftImage={image.backArrow}
          leftPress={()=>this.props.navigation.goBack()}
          />
        
            <ImageBackground style={{ width: "100%", height:160, backgroundColor: appColor.Blue, borderBottomRightRadius: 30, borderBottomLeftRadius: 30 }}>
            {/* Change language  */}
            {/* <View style={{flex:1, flexDirection:"row", justifyContent:"space-between",marginHorizontal:15, marginTop:15}}>
              <Text style={styles.changeStyle}>Change Language</Text>
              <View>
              <Form style={{borderRadius:5,borderColor:appColor.white, borderWidth:1.5, width:120,height:35,justifyContent:"center"}}>
            <Picker
                  note
                  mode="note"
                  style={{ width: 120, color:appColor.white,fontFamily:Fonts.Regular}}
                  iosIcon={<Icon name="arrow-down" style={{color:appColor.white}}/>}
                  selectedValue={this.state.selected}
                  onValueChange={(selected)=>this.onValueChange(selected)}
                >
                  {
                    selectLanguage.map((item, index) => {
                      return (<Picker.Item label={item.longform} value={index} key={index} style={{color:appColor.white}}/>) 
                  })
                  }
                </Picker>
            </Form>
        </View>

            </View> */}
           
            {/* <PowerTranslator style={styles.BlueBackgroundText} text={"Driver's must follow the questions and answer correctly."}/> */}
            <Text style={[styles.BlueBackgroundText,{marginTop:45,textAlign:"center"}]}>{"Driver's must follow the questions and answer correctly."}</Text>
             
            </ImageBackground>
            
              <Spinner
                visible={this.state.isLoading}
                style={{ color: '#000' }}
                color={appColor.Blue}
                overlayColor="rgba(0, 0, 0, 0.55)"
              />
              <NavigationEvents
                onDidFocus={() => this.updateCallback()}
              /> 
             <View style={{padding:20}}>
                 <View style={{flexDirection:"row"}}>
                     <View style={{width:"95%"}}>
                     <Text style={[styles.question_text,{color:appColor.black}]}>{this.state.question}</Text>
                     {/* <PowerTranslator style={[styles.question_text,{color:appColor.black}]} text={this.state.question} style={{lineHeight: 35 * 0.75,
    paddingTop: 35 - (35 * 0.75)}}/> */}
                  
                     <View style={{width:"20%",backgroundColor:appColor.Blue,height:4,marginTop:10}}></View>
                       <FlatList data={options}
                       extraData={this.state}
                       renderItem={({item,index}) =>
                      <TouchableOpacity style={{flexDirection:"row",marginTop:40,marginBottom:10}} onPress={()=>this.handleChange(item,index)}>
                     <RadioButton
  animation={'bounceIn'}
  isSelected={false}
  isSelected={options[index].isSelected}
                    value={options[index].isSelected === item}
                    onPress={() =>this.handleChange(item,index)}
                   // color={appColor.darkGrey}
                    // style={{top:25}}
                    />
                    <Text style={styles.option_text}>
                    {item.option_name}
                    </Text>
                   {/* <PowerTranslator style={styles.option_text} text={item.option_name}/> */}
                  </TouchableOpacity>
                        }/>

                        
                        
                 </View>
                 <View style={{flexDirection:"column",width:"14%",height:40,alignItems:"center"}}>
                      
                      <Text style={{color:appColor.black,marginRight:10}}> {this.state.questionnumber}/{this.state.question_len.length}</Text>
                      
                     <View style={{backgroundColor:appColor.Blue,width:"70%",height:2,marginBottom:10,marginTop:5}}></View>
                     {/* <Image
                   style={styles.IconSize}
                   resizeMode="contain"
                    source={image.upper_arrow}
                  /> */}
                    
                       {/* <FlatList data={this.state.question_len}
                       extraData={this.state}
                       renderItem={({item,index}) =>
                  <View style={{width:20,height:20,borderRadius:15,backgroundColor:  this.state.questionnumber === index + 1 ?  '#74e706' :'#213e7a',marginBottom:10}}>
                      <Text style={{color:appColor.white,fontSize:10,textAlign:"center",marginTop:3}}>{index + 1}</Text>
                  </View>
                    }/>
                  */}
                  {/* <Image
                   style={styles.IconSize}
                   resizeMode="contain"
                    source={image.lower_arrow}
                  /> */}
              </View>
                 </View>
              <View style={styles.buttonview}>
                <AppButton
                  text='Previous'
                  disabled={this.state.questionnumber < 1 ? true:false  }
                  bgColor={this.state.questionnumber < 1 ?  appColor.darkGrey : this.state.questionnumber > 1 ? appColor.Blue :appColor.darkGrey  }
                  width={'45%'}
                  textColor={appColor.white}
                  onClickFunc={() => this.prev()}
                />
                <AppButton
                  text='Next'
                //   disabled={this.checkInflag ? false : true}
                  bgColor={appColor.Blue }
                  width={'45%'}
                  textColor={appColor.white}
                  onClickFunc={() => this.next()}
                />
              </View>
              </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  headerView: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  headerTextView: {
    fontSize: Fonts.h18,
    fontFamily:Fonts.Regular,
    fontWeight: "bold",
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    marginLeft: Dimensions.get("window").width/5
  },
  BlueBackgroundText:{
      fontSize:Fonts.h14,
      color:appColor.white,
      marginLeft:15,
      marginTop:0,
      letterSpacing:1,
      marginBottom:30,
      fontFamily:Fonts.Regular
  },
  IconSize: {
    width:20,
    height:20,
    marginBottom:5
  },
  buttonview:{
    flexDirection:"row",
    justifyContent:"space-between",
    marginTop:Dimensions.get("window").
    height/26,marginBottom:20
  },
 
    option_text:{color:appColor.black,
        fontSize:Fonts.h14,
        marginLeft:10,
        marginRight:10,
        fontFamily:Fonts.Regular},


    question_text:{
        fontSize:Fonts.h16,
        color:appColor.black,
        fontFamily:Fonts.Medium
    },
    changeStyle:{
      color:appColor.white,
      fontSize:Fonts.h18,
      textAlign:"center",
      fontFamily:Fonts.Regular
    }
  
});


export default QuestionScreen;
