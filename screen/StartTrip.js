import React, { Component } from 'react';
import { Container, Content, ListItem, Body, Left, Right,Form,Item,Input,ActionSheet} from 'native-base';
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  PermissionsAndroid,
  ToastAndroid,
  Dimensions,
  Platform
} from 'react-native';

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase, moment,image,Geolocation,Fonts,RestApiKey,ClientId,ClientSecret,grantType,NetInfo} from "././common";

import { ScrollView, } from 'react-native-gesture-handler';
const googleMaps_apiKey = "AIzaSyComH3uVSYHWeEtzyEByzgi76SOseHvSG0";
var BUTTONS = ["Camera", "Cancel"];
var CANCEL_INDEX = 1;
export default class StartTrip extends Component {
  constructor(props){
    super(props);
    this.image_1 = this.image_1.bind(this);
    this.image_2 = this.image_2.bind(this);
    this.image_3 = this.image_3.bind(this);
    this.image_4 = this.image_4.bind(this);
    this.image_5 = this.image_5.bind(this);
    this.image_6 = this.image_6.bind(this);
    this.image_7 = this.image_7.bind(this);
   
    this.state ={
     isLoading:false,
      fileSource:"",
      imageName:'',
      path:"",
      imageType:"",
      imageArray:[],
      ride_history_id:"",
      image1:null,
      image2:null,
      image3:null,
      image4:null,
      image5:null,
      image6:null,
      image7:null,
      // name:"",
      // MobileNo:"",
      vechileNo:"",
      lat:"",
      long:"",
      Editflag:false,
      BookingIDD:"",
      notificationRequestType:"",
      startKM:"",
      manual_ride:"",
      driverType:""
    }
  }

    async componentDidMount(){
      this._reloadDrives();
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (!granted) {
          return;
        } else {
          await Geolocation.getCurrentPosition(
            (position) => {
              console.log("position",position.coords.latitude)
              this.updateGeoAddress(position.coords.latitude,position.coords.longitude)
            });
        }
      } else {
        console.log("ios platform")
      }
      let bookingType = await this.props.navigation.getParam("bookingType");
      let vehicle_number = await this.props.navigation.getParam("vehicle_number");
      if(bookingType == "start"){
        console.log("bookingType","started")
        this.setState({vechileNo:vehicle_number, Editflag:true})
      } else {
        console.log("bookingType==","not started")
      }
      if(this.props.navigation.getParam("notificationRequestType")=="permanent"||this.props.navigation.getParam("notificationRequestType")=="single_permanent"){
        this.setState({notificationRequestType:this.props.navigation.getParam("notificationRequestType"),vechileNo:"",Editflag:false})
      }
      if(vehicle_number==""||vehicle_number==undefined){
        this.setState({Editflag:false})
      }
      this.RideSessionDetails();
    }
    async _reloadDrives(){
  
      let data = await AsyncStorage.getItem("item");
      //alert(JSON.stringify(data))
      this.setState({
          //poc_id: data.poc_id,
         // request_id: data.poc_id.request_id,
         // source: data.source,
         // destination: data.destination,
         // company_name: data.company_name,
         // schedule_time: data.rideTime,
        //  company_id: data.company_id,
        //  poc_name: data.poc_name,
        //  trip_type: data.trip_type,
        //  booking_status:data.status,
         // sourceLng:data.sourcelng,
        //  sourceLat:data.sourcelat,
         destinationLng:data.destinationLng,
         destinationLat:data.destinationLat,
        //  tour_type:data.tour_type,
         // vehicle_type:data.vehicle_type,
        //  request_type:data.request_type,
        //  vehicle_number:data.vehicle_number,
    //poc_number:data.poc_number,
        //  numbers_of_drivers:data.numbers_of_drivers,
        //  notification_type:data.notification_type,
        driverType:data.request_type
      })
      console.log('reload drive value',this.state.destinationLng)
    }
    async RideSessionDetails() {
       try {
         const driverId = await AsyncStorage.getItem('driverId');
         var formData = new FormData();
         formData.append("action", "get_running_ride");
         formData.append("driver_id", driverId);
         const response = await fetch(BaseUrl.url2+"service.php", {
           body: formData,
           method: "POST",
           headers: {
             "Accept": "multipart/form-data",
             "Content-Type": "multipart/form-data"
           }
         });
         const responseJson = await response.json();
         if(responseJson.data.ride_history_id){
           AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id);
           this.setState({BookingIDD:responseJson.data.booking_id})
           if(responseJson.data.status == 'initiated'){
              AsyncStorage.setItem("initiateFlag",'flaged')
           }
           else if(responseJson.data.status == 'ride_started'){
              AsyncStorage.setItem("initiateFlag",'flaged')
              AsyncStorage.setItem("startTipFlag",'flaged')
           }
         }
         else{
           AsyncStorage.setItem("ride_history_id","")
           AsyncStorage.setItem("startTipFlag","")
           AsyncStorage.setItem("initiateFlag","")
         }
              
       } catch (error) {
         console.log(error);
       }
     }

    async updateGeoAddress(lat,long){
      const tokenType = await AsyncStorage.getItem("tokenType");
      const token = await AsyncStorage.getItem("token")
      console.log("url==", "http://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long)
  
      try {
        const response = await fetch(
          "https://apis.mapmyindia.com/advancedmaps/v1/"+RestApiKey+"/rev_geocode?lat="+lat+"&lng="+long,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            }
          }
        );
        const responseJson = await response.json();
        if(responseJson.error==="invalid_token"){
            this.updateToken().then(()=>{
              return this.updateGeoAddress(lat,long)
            })
        } else {
          console.log("responseJson===",responseJson.results[0].formatted_address)
          this.setState({test:responseJson.results[0].formatted_address})
        }
      } catch(error){
          console.log(error)
      }
    }

    async updateToken(){
      try {
        var formData = new FormData();
        formData.append("grant_type", grantType);
        formData.append("client_id", ClientId);
        formData.append("client_secret",ClientSecret);
        const response = await fetch(
          "https://outpost.mapmyindia.com/api/security/oauth/token",
          {
            body: formData,
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "multipart/form-data",
            }
          }
        );
        const responseJson = await response.json();
        await AsyncStorage.setItem("token",JSON.stringify(responseJson.access_token))
        await AsyncStorage.setItem("tokenType",JSON.stringify(responseJson.token_type))
      } catch(error){
          console.log(error)
      }
    }
   
   
  _geoCodeToAddress(lat,lng) {
    fetch(
      "https://maps.googleapis.com/maps/api/geocode/json?address=" +
      lat +
        "," +
        lng +
        "&key=" +
        googleMaps_apiKey
    )
      .then(response => response.json())
      .then(responseJson => {
        console.log('destination responseJson is ====',responseJson)
        this.setState({
          srcAddress: responseJson.results[0].formatted_address
        });
        let test = this.state.srcAddress;
        this.setState({test:this.state.srcAddress})
        console.log('current address is ====',test)
       
      });
  }


  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  };




  async StartTrip() {
    let currentStatus = await AsyncStorage.getItem("checkedin_id");
    if(currentStatus==null || currentStatus==undefined){
      ToastAndroid.show("You haven't check-in, Please check-in first.",ToastAndroid.LONG);
    } else if(this.state.vechileNo==""||this.state.vechileNo==undefined){
      ToastAndroid.show("Please enter vehicle number.",ToastAndroid.LONG);
    } else if(this.state.vechileNo.length<3 || this.state.vechileNo.length>14){
     ToastAndroid.show("Please enter valid vehicle number",ToastAndroid.LONG);
    } else if(this.state.startKM==""||this.state.startKM==undefined){
      ToastAndroid.show("Please enter trip start KM",ToastAndroid.LONG);
     } else {
      try {
           this.setState({isLoading:true});
           let bookingType = await AsyncStorage.getItem("bookingType");
              var ride_history_id=await AsyncStorage.getItem("ride_history_id_notification");
            // if(bookingType=="notification"){
            //   var ride_history_id = await AsyncStorage.getItem("ride_history_id_notification")
            // } else {
            //   var ride_history_id = await AsyncStorage.getItem("ride_history_id")
            // }
           let time= moment(new Date()).format("YYYY-MM-DD HH:mm")
           console.log("Date",time)
           var driverId = await AsyncStorage.getItem("driverId");
           var formData = new FormData();
           formData.append("action", "b2b_rideStart_rideEnd");
           formData.append("type", this.state.notificationRequestType=="single_permanent"||this.state.notificationRequestType=="permanent"?"ride_new_vehicle":"ride_start");
           formData.append("ride_history_id",ride_history_id);
           formData.append("driver_id", driverId);
           formData.append("ride_start_time",time)
           // formData.append("name",this.state.name);
           // formData.append("customer_number",this.state.MobileNo);
           formData.append("vechicale_number",this.state.vechileNo);
           formData.append("startkm",this.state.startKM);
           formData.append("ride_start_location",this.state.test);
           formData.append("image_type","Before_Start_Ride");
     
           formData.append("image_1", {
             uri: this.state.image1,
             type: "image/jpeg",
               name: "Vehicle_"+"Rightside_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeStartTrip",
            //  name: this.state.image1.substring(
            //    this.state.image1.lastIndexOf("/") + 1,
            //    this.state.image1.length
            //  )
           })
           formData.append("image_2", {
             uri: this.state.image2,
             type: "image/jpeg",
             name: "Vehicle_"+"Leftside_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeStartTrip",
            //  name: this.state.image2.substring(
            //    this.state.image2.lastIndexOf("/") + 1,
            //    this.state.image2.length
            //  )
           })
           formData.append("image_3", {
             uri: this.state.image3,
             type: "image/jpeg",
             name: "Vehicle_"+"Frontside_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeStartTrip",
            //  name: this.state.image3.substring(
            //    this.state.image3.lastIndexOf("/") + 1,
            //    this.state.image3.length
            //  )
           })
           formData.append("image_4", {
             uri: this.state.image4,
             type: "image/jpeg",
             name: "Vehicle_"+"Backside_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeStartTrip",
            //  name: this.state.image4.substring(
            //    this.state.image4.lastIndexOf("/") + 1,
            //    this.state.image4.length
            //  )
           })
           formData.append("image_5", {
             uri: this.state.image5,
             type: "image/jpeg",
             name: "Vehicle_"+"Dashboard_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeStartTrip",
           })

           formData.append("image_6", {
            uri: this.state.image6,
            type: "image/jpeg",
            name: "Vehicle_"+"Dashboard_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeStartTrip",
          })
          formData.append("image_7", {
            uri: this.state.image7,
            type: "image/jpeg",
            name: "Vehicle_"+"Dashboard_"+this.state.vechileNo+"_"+this.state.BookingIDD+"_"+"BeforeStartTrip",
          })

           console.log('formdata start ride',formData)
           console.log('url',BaseUrl.url2+"service.php")
     
           const response = await fetch(
             BaseUrl.url2+"service.php",
             {
               body: formData,
               method: "POST",
               headers: {
                 Accept: "multipart/form-data",
                 "Content-Type": "multipart/form-data"
               }
             }
           );
     
           const responseJson = await response.json();
           console.log('data===',responseJson)
           if (responseJson.success === true) {
            this.setState({isLoading:false});
            
           AsyncStorage.setItem("ride_history_id",responseJson.data.ride_history_id)
           console.log("check",responseJson.data.ride_history_id)
           AsyncStorage.setItem("Tripflag", 'true');
           AsyncStorage.setItem("startTipFlag", 'flaged');
           ToastAndroid.show("Your trip has been started successfully.",ToastAndroid.LONG);
           this.props.navigation.navigate("AddFuel",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,driverType:this.state.driverType,item:item})
          //  let fuelCheck = await AsyncStorage.getItem("isFuel")
          //  console.log('fuelCheck'+ fuelCheck)
          
           // if(fuelCheck=="done"){
           //   this.props.navigation.navigate("EndTrip")
           // } else {
           //  this.props.navigation.navigate("AddFuel",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,driverType:this.state.driverType,item:item})
           // }
          
           
          } else {
            this.setState({isLoading:false});
            ToastAndroid.show("Please fill all mandatory fields.",ToastAndroid.LONG);
            console.log("Check chote");
          }
     } catch (error) {
      this.setState({isLoading:false});
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          ToastAndroid.show("Please fill all mandatory fields.",ToastAndroid.LONG);
          console.log(error);
        }
      });
      }
    }
  }


  async image_1() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image1: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_2() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image2: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_3() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image3: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_4() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image4: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_5() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image5: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }
  async image_6() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image6: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async image_7() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          compressImageQuality:0.1,
          title: "Select Aadhar Photo"
        }).then(images => {
          this.setState({ image7: images.path });
        });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  async updateCallback(){
    let Tripflag = JSON.stringify(AsyncStorage.getItem("Tripflag"))
    let driverType = await this.props.navigation.getParam("driverType");
    let sendType = await this.props.navigation.getParam("sendType");
    if(sendType=="multiple"){
      this.setState({vechileNo:""})
    }
    this.setState({driverType:driverType})
    if(Tripflag==='true')
    {
      console.log("aa")
    }
    else {
      console.log("ss")
      this.setState({image1:null,image2:null,image3:null,image4:null,image5:null,image6:null,image7:null,startKM:""})
    }
      let bookingType = await this.props.navigation.getParam("bookingType");
      let vehicle_number = await this.props.navigation.getParam("vehicle_number");
      if(bookingType == "start"){
        console.log("bookingType","started")
        this.setState({vechileNo:vehicle_number, Editflag:true})
      } else {
        console.log("bookingType==","not started")
      }
      if(this.props.navigation.getParam("notificationRequestType")=="permanent"||this.props.navigation.getParam("notificationRequestType")=="single_permanent"){
        this.setState({notificationRequestType:this.props.navigation.getParam("notificationRequestType"),Editflag:false})
      } 
      if(vehicle_number==""||vehicle_number==undefined){
        this.setState({Editflag:false})
      }
      this.RideSessionDetails();
}

async RideSessionDetails() {
  try {
    const driverId = await AsyncStorage.getItem('driverId');
    var formData = new FormData();
    formData.append("action", "get_running_ride");
    formData.append("driver_id", driverId);
    const response = await fetch(BaseUrl.url2+"service.php", {
      body: formData,
      method: "POST",
      headers: {
        "Accept": "multipart/form-data",
        "Content-Type": "multipart/form-data"
      }
    });
    const responseJson = await response.json();
    if(responseJson.success==true){
      if(responseJson.data.from_portal=="0"){
        this.setState({
          vechileNo:"",
          Editflag:false,
          from_portal:responseJson.data.from_portal
        })
       } 
     } else {
      this.setState({from_portal:false})
     }  
  } catch (error) {
    console.log(error);
  }
}

  // onMobNumTxtInputChanged(text) {
  //   this.setState({
  //     MobileNo: text.replace(/[^0-9]/g, "")
  //   });
  // }
  onMobNumTxtInputChangedVechile(text) {
    this.setState({
      vechileNo: text
    });
  }

//   onNameChange(text){
//     this.setState({
//  name:text.replace(/[^a-zA-Z ]/g,"")
// })
// }

onStartKM(text){
  this.setState({
        startKM: text.replace(/[^0-9]/g, "")
    });
}
  
 
  render() {
    return (
     
      <View style={{flex:1}}>
        <Header
          text={"Start Trip"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <ScrollView>
        <ImageBackground  style={{width:"100%",height:100,marginBottom:20,backgroundColor:appColor.Blue,borderBottomRightRadius:30,borderBottomLeftRadius:30}}>
           <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:50,textAlign
          :'center',fontSize:15}}>Upload vehicle images to start your trip</Text>
        </ImageBackground>
        {/* <Image style={styles.circleView}
                 source={ProfileIcon}
          /> */}
        <View style={{padding:20}}>  
        <NavigationEvents
                    onDidFocus={() => this.updateCallback()}
                />
        <Spinner
              visible={this.state.isLoading}
              style={{color:'#000'}}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            /> 
        {/* <View style={{marginBottom:20}}>
          <Text style={{fontFamily:"Roboto Light",color:appColor.black,fontSize:15,marginBottom:5}}>Customer Name<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
        <Form>
                <Item regular success style={{ borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey }}>
                  <Input 
                  maxLength={50}
                  keyboardType='email-address' 
                  placeholder="Name" 
                  style={{ color: appColor.white, fontSize: 16, color: appColor.black }} 
                  onChangeText={text=>this.onNameChange(text)}
                  value={this.state.name}
                  />
                  
                </Item>
               </Form>
        </View>
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:"Roboto Light",color:appColor.black,fontSize:15,marginBottom:5}}>Customer Phone Number<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
        <Form>
                <Item regular success style={{ borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey }}>
                  <Input 
                  maxLength={10}
                  keyboardType='phone-pad' 
                  placeholder="Phone Number" 
                  style={{ color: appColor.white, fontSize: 16, color: appColor.black }} 
                  onChangeText={text=>this.onMobNumTxtInputChanged(text)}
                  value={this.state.MobileNo}
                  />
                  
                </Item>
               </Form>
        </View> */}
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Enter Vehicle Number</Text>
        <Form>
                <Item regular success style={{ borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey }}>
                  <Input
                  maxLength={13} 
                  minLength={3} 
                  keyboardType='email-address'
                  disabled={this.state.Editflag?true:false} 
                  placeholder="Vehicle Number" 
                  style={{ color: appColor.white, fontSize: 16, color: appColor.black }} 
                  onChangeText={text => this.onMobNumTxtInputChangedVechile(text)}
                  value={this.state.vechileNo.toUpperCase()}
                  />
                   </Item>
               </Form>
        </View>
        <View style={{marginBottom:20}}>
          <Text style={{fontFamily:Fonts.Regular,color:appColor.black,fontSize:15,marginBottom:5}}>Enter Trip Start KM</Text>
        <Form>
                <Item regular success style={{ borderRadius: 5, backgroundColor: appColor.lightGrey, borderColor: appColor.lightGrey }}>
                  <Input
                  maxLength={13} 
                  minLength={3} 
                  keyboardType="number-pad"
                  placeholder="Start KM" 
                  style={{ color: appColor.white, fontSize: 16, color: appColor.black }} 
                  onChangeText={text => this.onStartKM(text)}
                  value={this.state.startKM}
                  />
                   </Item>
               </Form>
        </View>
        <View style={{justifyContent:"center"}}>
          <Text style={{color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5,fontFamily:Fonts.Regular}}>Vehicle Images</Text>
        </View>
       <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:5}}>
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_1.bind(this)}
                     >
                        <Image
                        source={{ uri: this.state.image1}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
         
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_2.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image2}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around"}}>
       <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5,marginRight:10}}>Right Side<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Left Side<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:5}}>
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_3.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image3}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_4.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image4}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around"}}>
       <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Front Side<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Back Side<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       </View>

       <View style={{flexDirection:"row",justifyContent:"space-between",marginBottom:5}}>
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_6.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image6}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
         <View style={{width:"45%",height:100,borderColor:appColor.lightGrey}}>
         <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_7.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image7}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
         </View>
       </View>
       <View style={{flexDirection:"row",justifyContent:"space-around"}}>
       <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Vehicle RC<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Vehicle Insurance<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       </View>
       
       <View style={{width:"45%",height:100,borderColor:appColor.lightGrey,marginHorizontal:"25%"}}>
       <TouchableOpacity
                      style={styles.UploadTextStyle}
                       onPress={this.image_5.bind(this)}
                     >
                       <Image
                        source={{ uri: this.state.image5}}
                         style={{ width: "100%", height: 100 }}
                       />
                       </TouchableOpacity>
       </View>
       <Text style={{fontFamily:Fonts.Regular,color:appColor.Blue,textAlign
          :"center",fontSize:15,marginBottom:5}}>Dashboard<Text style={{color:appColor.red, fontSize:15, marginLeft:10,marginBottom:5,marginTop:5}}>*</Text></Text>
       
 
      <View style={{justifyContent:'center',marginBottom:20,alignItems:'center', marginTop:60}}>
            <AppButton
            text='Start Trip'
            bgColor={appColor.Blue}
            width={'100%'}
            textColor={appColor.white} 
           
            onClickFunc={()=>this.StartTrip()}       
            />
        </View>
        </View>
       </ScrollView>
       
       </View>
    );
  }
}

const styles = StyleSheet.create({
  
UploadTextStyle: {
  borderWidth:2,
  backgroundColor: appColor.lightGrey,
  width:"100%",
  flexDirection:"row",
  height: 100,
  borderColor:appColor.transparent
 
},
headerView: {
  justifyContent: "center",
  alignContent: "center",
  alignItems: "center",
},
headerTextView: {
  fontSize: 18,
  fontFamily:Fonts.Medium,
  textAlign: "center",
  alignItems: "center",
  color: "#fff",
  alignContent: "center",
  alignSelf: "center",
  marginLeft: Dimensions.get("window").width/4
},
});

