import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, ActivityIndicator, StatusBar, ToastAndroid, Dimensions} from "react-native";

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase,image,Fonts,NetInfo} from "././common";

import {Form,Item,Input} from 'native-base';

import { ScrollView } from "react-native-gesture-handler";
import baseUrl from "./config/config";

export default class ViewProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: null,
      userEmail: null,
      Name: "",
      Phonenum:"",
      FatherName:"",
      AccountNo:"",
      IFSCCode:"",
      AdharNum:"",
      DLNum:"",
      BeneficiaryName:"",
      BankName:"",
      mobileNumber: null,
      isLoading:false,
      idNumber:"",
      driverImage:"",
      driver_image_exist:""
    }
  }
 
  componentDidMount() {
    this._fetchUserDetails();
  }

  // componentDidUpdate() {
  //   this._fetchUserDetails();
  // }

 

  async _fetchUserDetails() {
    this.setState({isLoading:true})
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      const driverImage = baseUrl.url6+driverId+"_driver_image_c";
      //alert(driverImage)
      var formData = new FormData();
      formData.append("action", "get_driver_info");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url2+"service.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data"
        }
      });
      const responseJson = await response.json();
      console.log("detail view==",responseJson)
      //alert(JSON.stringify(responseJson.data.driver_image))
      if (responseJson.success === true)
      this.setState({isLoading:false})
        this.setState({
          Name:responseJson.data.name,
          AccountNo:responseJson.data.account_number,
          BankName:responseJson.data.bank_name,
          BeneficiaryName:responseJson.data.beneficiary_name,
          IFSCCode:responseJson.data.ifsc_code,
          FatherName:responseJson.data.father_name,
          AdharNum:responseJson.data.adhar_number,
          Phonenum:responseJson.data.phone_number,
          DLNum:responseJson.data.dl_number,
          idNumber:responseJson.data.IdNumber,
          driver_image_exist:responseJson.data.driver_image,
          userId: driverId,
          driverImage:driverImage,
        })
    } catch (error) {
      NetInfo.fetch().then(connection => {
        if(!connection.isConnected){
          Toast.show("Please Check Your Internet Connection !",Toast.LONG);
        } else {
          alert(error);
        }
      });  
    }
  }

  async _logoutUser() {
    try {
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "update_driver_live_status");
      formData.append("driver_id", driverId);
      formData.append("driver_live_status", false);
      console.log(formData);
      const response = await fetch(
        BaseUrl.url1+"index.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data"
          }
        }
      );

      const responseJson = await response.json();
      console.log(responseJson);
      if (responseJson.success === true) {
        this.props.navigation.replace('mobile_auth')
        AsyncStorage.clear();
      } else {
       this.props.navigation.replace('mobile_auth')
        AsyncStorage.clear();
      }
    } catch (error) {
      alert(error);
    }
  }

  render() {
    return (
      <View style={{ flex: 1}}>
        <StatusBar backgroundColor="#19376c" barStyle="light-content" />
        <Header
          text={"My Profile"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
        <ScrollView>
        <View style={styles.header} >
        {
        this.state.Name?<Text style={{ fontFamily:Fonts.Medium, fontSize: 18,textAlign:'center', color: "white",marginTop:50}}>
            {this.state.Name}
        </Text>:<Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:50,textAlign:'center',fontSize:18}}>Driver Name</Text>} 
      
  </View>
  
  {/* {this.state.Name?<View style={styles.circle}><Text style={styles.circleViewText}>{this.state.Name.split(' ').map((word)=> word.charAt(0)).join(' ')}</Text></View>:null} */}
        
        {
          this.state.driver_image_exist=="1"?<Image style={[styles.circle,{backgroundColor:"transparent"}]}
          source={{uri:this.state.driverImage}}
            />:<Image style={styles.circle}
            source={image.profile}
          />
        }
        
          {
            this.state.idNumber?<View>
            <Text style={{ fontFamily:Fonts.Medium, fontSize: 18,textAlign:'center', color:appColor.darkGrey,marginTop:50}}>
              {this.state.idNumber}
          </Text>
          </View>:null
          }
          
         <Spinner
              visible={this.state.isLoading}
              style={{color:'#000'}}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            />
        {/* <View style={styles.body}>
          <View style={styles.bodyContent}> */}
           {/* <Text style={styles.name}>Maninder</Text>
            <Text style={styles.email}>maninder@rediansoftware.com</Text>
            <Text style={styles.mobileNumber}>8505231254</Text> */}
            {/* <Text style={styles.name}>{this.state.userName}</Text>
            <Text style={styles.email}>{this.state.userEmail}</Text>
            <Text style={styles.mobileNumber}>{this.state.mobileNumber}</Text> */}

            {/* <TouchableOpacity style={{ marginTop: 40 }} onPress={() => this.props.navigation.navigate("edit_profile")}>
              <Text style={{ color: "#1e4281", fontWeight: "bold" }}>Edit Account</Text>
            </TouchableOpacity> */}
          {/* </View>
        </View> */}
        <View style={{padding:20}}>   
        <View style={{marginTop:20}}>
        <Text style={{fontFamily:Fonts.Regular,color:appColor.lightBlack,fontSize:15,marginLeft:8}}>Name</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Name" 
                  style={{fontSize:15,fontFamily:Fonts.Regular,fontWeight:"700"}}  
                  // onChangeText={value=>this.setState({Name:value})}
                  editable={false}
                  value={this.state.Name}
                  secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{height:2,width:"100%",backgroundColor:appColor.lightGrey,marginBottom:5}}></View>
        <View style={{marginTop:10}}>
        <Text style={{fontFamily:Fonts.Regular,color:appColor.lightBlack,fontSize:15,marginLeft:8}}>Phone Number</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Your Phone Number" 
                  style={{fontSize:15,fontFamily:Fonts.Medium}}  
                  // onChangeText={value=>this.setState({Phonenum:value})}
                  value={this.state.Phonenum}
                  editable={false}
                  secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{height:2,width:"100%",backgroundColor:appColor.lightGrey,marginBottom:5}}></View>
        <View style={{marginTop:10}}>
        <Text style={{fontFamily:Fonts.Regular,color:appColor.lightBlack,fontSize:15,marginLeft:8}}>Father Name</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Father Name" 
                  style={{fontSize:15,fontFamily:Fonts.Regular}}
                  // onChangeText={value=>this.setState({FatherName:value})}
                  value={this.state.FatherName}
                  editable={false}
                  secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{height:2,width:"100%",backgroundColor:appColor.lightGrey,marginBottom:5}}></View>
        <View style={{marginTop:10}}>
        <Text style={{fontFamily:"Roboto Light",color:appColor.lightBlack,fontSize:15,marginLeft:8}}>Adhar Number</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Adhar Number" 
                  style={{fontSize:15,fontFamily:Fonts.Regular,fontWeight:"700"}}
                  // onChangeText={value=>this.setState({AdharNum:value})}
                  value={this.state.AdharNum}
                  editable={false}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{height:2,width:"100%",backgroundColor:appColor.lightGrey,marginBottom:5}}></View>
        <View style={{marginTop:10}}>
        <Text style={{fontFamily:"Roboto Light",color:appColor.lightBlack,fontSize:15,marginLeft:8}}>DL Number</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="DL Number" 
                  style={{fontSize:15,fontFamily:Fonts.Regular,fontWeight:"700"}} 
                  value={this.state.DLNum}
                  editable={false}
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{height:2,width:"100%",backgroundColor:appColor.lightGrey,marginBottom:5}}></View>
        <View style={{marginTop:10}}>
        <Text style={{fontFamily:"Roboto Light",color:appColor.lightBlack,fontSize:15,marginLeft:8}}>Bank Name</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Bank Name" 
                  style={{fontSize:15,fontFamily:Fonts.Regular,fontWeight:"700"}}
                  value={this.state.BankName}
                  editable={false}
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{height:2,width:"100%",backgroundColor:appColor.lightGrey,marginBottom:5}}></View>
        <View style={{marginTop:10}}>
        <Text style={{fontFamily:"Roboto Light",color:appColor.lightBlack,fontSize:15,marginLeft:8}}>IFSC Code</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="IFSC Code" 
                  style={{fontSize:15,fontFamily:Fonts.Regular,fontWeight:"700"}} 
                  value={this.state.IFSCCode}
                  editable={false}
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{height:2,width:"100%",backgroundColor:appColor.lightGrey,marginBottom:5}}></View>
        <View style={{marginTop:10}}>
        <Text style={{fontFamily:"Roboto Light",color:appColor.lightBlack,fontSize:15,marginLeft:8}}>Account Number</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Account Name" 
                 style={{fontSize:15,fontFamily:Fonts.Regular,fontWeight:"700"}} 
                  value={this.state.AccountNo}
                  editable={false}
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        <View style={{height:2,width:"100%",backgroundColor:appColor.lightGrey,marginBottom:5}}></View>
        <View style={{marginTop:10}}>
        <Text style={{fontFamily:"Roboto Light",color:appColor.lightBlack,fontSize:15,marginLeft:8}}>Beneficiary Name</Text>
              <Form >
                <Item regular success style={styles.inputView}>
                  <Input placeholder="Beneficiary Name" 
                  style={{fontSize:15,fontFamily:Fonts.Regular,fontWeight:"700"}} 
                  value={this.state.BeneficiaryName}
                  editable={false}
                //   onChangeText={value=>this.setState({password:value})}
                //   secureTextEntry={this.state.isVisible}
                  />
               
                </Item>
              </Form>
        </View>
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width:"100%",
    height:180,
    marginBottom:20,
    backgroundColor:appColor.Blue,
    borderBottomRightRadius:30,
    borderBottomLeftRadius:30
  },
  logoutView: {
    justifyContent: 'center',
  },
  logoutText: {
    fontSize: 17,
    fontWeight: "bold",
    color: "#fff",
    margin:20
  },
  profileView: {
    
      justifyContent: "center",
      alignContent: "center",
      alignItems: "center"
    
  },
  profileText: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
    marginLeft: Dimensions.get("window").width/4
  },
  circleViewText:{
    fontSize:18,
    fontFamily:"Roboto-Medium",
    textAlign:"center",
    marginTop:60
    
 },
   circle: {
    width:140,
    height:140,
    borderRadius:70,
    backgroundColor:appColor.lightGrey,
    position:"absolute",
    marginTop:100,
    marginLeft:Dimensions.get("window").width/3
  },
  inputView: {
    borderRadius:5,
    backgroundColor:appColor.white,
    borderColor:appColor.white
}
});
