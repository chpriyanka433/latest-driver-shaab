import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  KeyboardAvoidingView,
  ToastAndroid,
  PermissionsAndroid,
  AsyncStorage,
  Alert,
  Platform
} from "react-native";

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase,moment,Geolocation,Fonts,NetInfo} from "././common";

  var SQLite = require("react-native-sqlite-storage");

var db = SQLite.openDatabase({
  name: "locationStore.db",
  createFromLocation: "~locationdatabase.db"
});


const brandColor = appColor.Blue;

export default class OTPAuthenticationCheckOut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otpTxt: "",
      clipboardContent: null,
      verifyButtonStatus: true,
      timer: 30,
      newOTPButtonStatus: true,
      isSpinnerLoading: false
    };
  }

  async componentDidMount() {
    //StatusBar.setHidden(true);
    this.timerInterval = setInterval(
      () => this.setState(prevState => ({ timer: prevState.timer - 1 })),
      1000
    );
  }

  componentDidUpdate() {
    if (this.state.timer === 0) {
      clearInterval(this.timerInterval);
      if (this.state.newOTPButtonStatus) {
        this.setState({ newOTPButtonStatus: false });
      }
    }
  }

  static navigationOptions = {
    header: null
  };

  async _otpAuth() {
    NetInfo.fetch().then(connection => {
      if(!connection.isConnected){
        Toast.show("Please Check Your Internet Connection !",Toast.LONG);
      }
    });
    try {
      const response = await fetch(
        BaseUrl.url1+"index.php?action=check_otp&phone_number=" +
          this.props.navigation.getParam("mob_num") +
          "&otp=" +
          this.state.otpTxt +
          "&driver_id=" +
          this.props.navigation.getParam("id")
      );
      const responseJson = await response.json();
      console.log(responseJson);
      if(responseJson.success===false){
          switch (responseJson.otp) {
            case "expired":
              ToastAndroid.show("Otp expired", ToastAndroid.SHORT);
              break;
            case "Not Match":
              ToastAndroid.show("Please Enter Valid OTP Number", ToastAndroid.SHORT);
              break;
          }
      } else {
        this._fetchUserDetails();
        if (responseJson.data.driver_status === false) {
          this.props.navigation.replace("Questiontype", {
            id: this.props.navigation.getParam("id"),
            mob_num: this.props.navigation.getParam("mob_num")
          });
        } else {
          if (responseJson.data.driver_verification_status === "0") {
            this.props.navigation.replace("drivernotverified_screen");
          } else {
            this._createLoginSession(responseJson.data.driver_id);
          
          }
        }
      }
    } catch (error) {
      alert(error);
    }
    }

  async _fetchUserDetails(){
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "get_driver_info");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url1+"index.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data"
        }
      });
      const responseJson = await response.json();
      console.log('driverdata===',responseJson)
      AsyncStorage.setItem("user_name", responseJson.name);
      AsyncStorage.setItem("user_email", responseJson.email_address_c);
      AsyncStorage.setItem("mobile_number", responseJson.phone_number_c);
      //this.updateStatus()
    } catch (error) {
      console.log(error);
    }
  }

  _createLoginSession(driverId) {
    try {
      console.log(driverId);
      AsyncStorage.setItem("isLoggedIn", "true");
      AsyncStorage.setItem("driverId", driverId + "");
      this.props.navigation.navigate("GpsScreen");
      //  AsyncStorage.setItem("userId", this.props.navigation.getParam("id"));
      // AsyncStorage.setItem("user_name", userName);
      // AsyncStorage.setItem("user_email", emailId);
      // AsyncStorage.setItem("mobile_number", mobileNumber);
    } catch (error) {
      console.log(error);
    }
  }

  checkLocation(driverId){
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
  .then(data => {
    if(data){
      this._fetchCurrentLocationToRedirect(driverId)
    } else {
      ToastAndroid.show('Please Turn ON your Location.',ToastAndroid.LONG)
    }
  }).catch(err => {
    if(err){
      console.log(err.message)
      //ToastAndroid.show(err.message,ToastAndroid.LONG)
    }
  });
  }

  async _fetchCurrentLocationToRedirect(driverId) {
    this.setState({isSpinnerLoading: true});
    await Geolocation.getCurrentPosition(
      position => {this.setState({isSpinnerLoading: false});
        db.transaction(tx => {
          tx.executeSql(
            "update tblLocation set latitude=?, longitude=?, driverId=? where id = 1",
            [position.coords.latitude, position.coords.longitude, driverId],
            (tx, results) => {console.log(results);
            }
          );
        });
        this.props.navigation.navigate("CheckOut")
        //  Alert.alert(
        //   'Trip Ended!',
        //   'Do you want to start another trip.',
        //   [
        //     {text: 'NO', onPress: () => this.props.navigation.navigate("CheckOut")},
        //     {text: 'YES', onPress: () => this.props.navigation.navigate("MyRideList")},
        //   ],
        //   { cancelable: false }
        // );
        // this.props.navigation.replace("Checkin", {
        //   latitude: position.coords.latitude,
        //   longitude: position.coords.longitude
        // });
      },
      error => console.log('location not get: ',error.message),
      //{ enableHighAccuracy: true, timeout: 20000 }
    );
  }

  async updateStatus(){
    try {
      let driverId = await AsyncStorage.getItem('driverId');
      console.log('driver id =====',driverId);
      var formData = new FormData();
      formData.append("action", "get_in_progress_ride");
      formData.append("driver_id", driverId);
      const response = await fetch(
        BaseUrl.url1+"index.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data"
          }
        }
      );
      const responseJson = await response.json();
      if (responseJson.success === true) {
        console.log('status',responseJson.success)
        AsyncStorage.setItem("status",responseJson.data.booking_id)
      }
  } catch (error) {}
  }

  async _getNewOtp() {
    this.setState({
      isSpinnerLoading: true
    });
    try {
      const response = await fetch(
        BaseUrl.url1+"index.php?action=login&phone_number=" +
          this.props.navigation.getParam("mob_num")
      );
      const responseJson = await response.json();
      if (responseJson.data.otp) {
        this.interval = setInterval(
          () => this.intervalFunction(responseJson.data.otp + ""),
          1000
        );
      }
    } catch (error) {
      console.error(error);
    }
  }

  intervalFunction(otp) {
    this.setState({
      isSpinnerLoading: false,
      timer: 30,
      newOTPButtonStatus: true
    });

    this.timerInterval = setInterval(
      () => this.setState(prevState => ({ timer: prevState.timer - 1 })),
      1000
    );
    ToastAndroid.show(
      // "OTP has been sent to your number" + otp,
      "OTP has been sent to your number",
      ToastAndroid.LONG
    );
    clearInterval(this.interval);
    this.refs.codeInputRef1.clear()
  }
  async onStartRide(){
    let currentStatus = await AsyncStorage.getItem("checkedin_id");
    console.log("check",currentStatus)
    if(currentStatus==null || currentStatus==undefined){
      ToastAndroid.show("You haven't check-in, Please check-in first.",ToastAndroid.LONG);
      this.props.navigation.navigate("Checkin");
    } else {
      this.updateLocation();
      let startTrip = await AsyncStorage.getItem("startTipFlag");
      if(startTrip=="flaged"){
        this.props.navigation.navigate("AddFuel",{desLat:this.state.destinationLat,desLng:this.state.destinationLng,manual:"no",driverType:this.state.driverType});
      } else {
        this.props.navigation.navigate("StartTrip",{vehicle_number:this.state.vehicle_number,bookingType:"start",driverType:this.state.driverType,manual:"no"})
      }
    }
  }
  async submitEndOTP(){
    
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      const ride_history_id = await AsyncStorage.getItem("ride_history_id_notification");
      var formData = new FormData();
      formData.append("action", "b2b_start_ride_with_otp");
      formData.append("driver_id", driverId);
      formData.append("ride_history_id", ride_history_id);
      formData.append("endotp",this.state.otpTxt);
      const response = await fetch(
        BaseUrl.url5,
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data"
          }
        }
      );

      const responseJson = await response.json();
      console.log('form data for end otp')
      console.log(responseJson.data)
      if (responseJson.success === true) {
              // this.props.navigation.navigate("EndTrip");
              if(responseJson.data.status==true){
                this.props.navigation.navigate("EndTrip",{add_more_car:"no"});
                
              } else {
                console.log("check status")
              }
      } else {
        ToastAndroid.show(responseJson.message,ToastAndroid.LONG)
        //this.refs.codeInputRef1.clear();
      }
    } catch (error) {
      console.log(error)
    }
   }
  
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#fff" }}>
        <StatusBar backgroundColor="#19376c" barStyle="light-content" />
        <Header
           text={"Verify End OTP Number "}
          />

        <View style={{ height: 1, backgroundColor: "#636363" }} />

        <KeyboardAvoidingView behavior="padding" style={styles.form}>
          <View style={styles.container}>
            <Spinner
              visible={this.state.isSpinnerLoading}
              style={styles.spinnerStyle}
              color={appColor.spinnerColor}
              overlayColor="rgba(0, 0, 0, 0.55)"
            />

            <Text style={styles.verificationText}>
              Please Enter the End OTP{this.props.navigation.getParam("mob_num")}
            </Text>

           

            <View style={styles.codeInputStyles}>
              <CodeInput
                ref="codeInputRef1"
                className={"border-b"}
                activeColor="#1e4281"
                inactiveColor="#dedede"
                space={20}
                autoFocus={true}
                keyboardType="numeric"
                fontFamily={Fonts.Regular}
                size={50}
                inputPosition="left"
                codeLength={4}
                onFulfill={code => this.setState({ otpTxt: code + "" })}
              />
            </View>
            <AppButton 
            text='Submit'
            bgColor={appColor.Blue}
            width={'50%'}
            textColor={appColor.white}   
            onClickFunc={()=>this.submitEndOTP()}         
            />

            {/* <Text style={{ fontStyle: "italic", textAlign: "center" ,fontFamily:Fonts.Regular}}>
              Didn't get the SMS?
            </Text> */}

            {/* <TouchableOpacity
              disabled={this.state.newOTPButtonStatus}
              onPress={this._getNewOtp.bind(this)}
              style={[
                styles.submitButtonStyle,
                {
                  borderColor: this.state.newOTPButtonStatus
                    ? "#dedede"
                    : appColor.Blue
                }
              ]}
            >
              {/* <Text style={styles.submitTextStyle}>
                GET A NEW OTP IN 00:{this.state.timer<=9?"0":null}{this.state.timer}
              </Text> */}
            {/* </TouchableOpacity> */} 

           
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center"
  },
  spinnerStyle: {
    color: "#000000"
  },
  verificationText: {
    textAlign: "center",
    fontSize: 19,
    color: "#696969",
    marginTop: 50,
    marginLeft: 18,
    marginRight: 18,
    fontFamily:Fonts.Regular
  },
  codeInputStyles: {
    height: 70,
    alignItems: "center",
    marginLeft: 10,
    marginBottom: 50
  },
  buttonContainer: {
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: 40
  },
  mobileNumberStyle: {
    textAlign: "center",
    fontSize: 19,
    color: "#000",
    marginTop: 15,
    marginLeft: 18,
    marginRight: 18
  },
  submitButtonStyle: {
    width: "60%",
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 30,
    marginRight: 30,
    borderRadius: 30,
    borderWidth: 1
  },
  submitTextStyle: {
    textAlign: "center",
    fontSize: 16,
    fontFamily:Fonts.Medium
  },
  header: {
    paddingTop: 20,
    padding: 15,
    backgroundColor: brandColor
  },
  description: {
    fontSize: 16,
    color: "#fff",
    textAlign: "center",
    fontFamily:Fonts.Medium
  },
  verifyButton: {
    position: "absolute",
    height: 50,
    bottom: 10,
    left: 10,
    right: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5
  },
  buttonText: {
    color: "#fff",
    fontSize: 16,
    fontFamily:Fonts.Medium
  },
  form: {
    flex: 1
  }
});
