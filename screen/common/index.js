import appColor from "./color";
import image from "././image";
import Header from "./Header";
import Fonts from "./fonts";
import InputText from "./InputText";
import Picker from "./Picker";
import AppButton from "./Button";
import BaseUrl from "../config/config";
import RNPickerSelect from "react-native-picker-select";
import CheckBox from "react-native-check-box";
import Spinner from "react-native-loading-spinner-overlay";
import ImagePicker from "react-native-image-crop-picker";
import RazorpayCheckout from "react-native-razorpay";
import Toast from 'react-native-simple-toast';
import { NavigationActions,NavigationEvents,createAppContainer} from "react-navigation"; 
import { createDrawerNavigator,DrawerActions} from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import geolib from 'geolib';
import { Button,TextInput,Avatar} from "react-native-paper";
import CodeInput from "react-native-confirmation-code-input";
import getDirections from 'react-native-google-maps-directions';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import TextInputMask from "react-native-text-input-mask";
import OneSignal from "react-native-onesignal";
import Geolocation from 'react-native-geolocation-service';
var Sound = require('react-native-sound');
const RestApiKey = '6w1y6odiaas51zh2jrr49gad552lq7z9';
const ClientId = 'aBZ_ZIWtiVBRh2KcfS4XCZsvyNAEQWCtd4GvTVKVbClxKrrHBU8TvEdAnEDW859KnObBkpsMIus=';
const ClientSecret = '9K_q_9Q2GHP619e2x3Fae-VIYC4hWiDrUb4e5E3u7TQ-4lTkmBDYUqdw5FQ-qa7rQFqIYdlc0Wi5T89mBTJvMg==';
const grantType = "client_credentials";
const MapsSdkkey = "fd7k1j2m5ur66k71ba4vqek7ixrk44sj";
import NetInfo from "@react-native-community/netinfo";


export {
    appColor,
    image,
    Header,
    Fonts,
    InputText,
    Picker,
    AppButton,
    BaseUrl,
    RNPickerSelect,
    CheckBox,
    Spinner,
    ImagePicker,
    RazorpayCheckout,
    Toast,
    NavigationActions,
    DrawerActions,
    NavigationEvents,
    DatePicker,
    moment,
    geolib,
    createAppContainer,
    createDrawerNavigator,
    Button,
    CodeInput,
    getDirections,
    RNAndroidLocationEnabler,
    MapView,
    PROVIDER_GOOGLE,
    TextInputMask,
    TextInput,
    OneSignal,
    createStackNavigator,
    Geolocation,
    RestApiKey,
    ClientId,
    ClientSecret,
    grantType,
    MapsSdkkey,
    NetInfo,
    Sound,
    Avatar
}