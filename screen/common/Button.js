import React, { Component} from "react";
import { View, StyleSheet, TouchableOpacity, Image} from "react-native";
import {
    Text
  } from "native-base";
import appcolor from "../common/color";
import {Fonts} from "../common";



class Button extends Component {
    render() {
        let { onClickFunc, text,width ,fontSize,bgColor,image,borderWidth,borderColor,borderRadius,textColor,disabled, height} = this.props;
        return (
            <TouchableOpacity disabled={disabled} style={[styles.TaskButtonStyle,{width:(width)?width:"82%",backgroundColor:(bgColor)?bgColor:appcolor.White,borderColor:(borderColor),borderWidth:(borderWidth),borderRadius:(borderRadius)?borderRadius:50/2, height:(height)?height:50}]}
                onPress={onClickFunc}
                 >
                <Text style={[styles.ButtonTextStyle,{fontSize:(fontSize)?fontSize:16,color:(textColor)?(textColor):appcolor.Black,fontFamily:Fonts.Medium}]}>{text}</Text>
                {this.props.image?<Image source={image} resizeMode='contain' style={[styles.imageStyle]}/>:null}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    TaskButtonStyle: {
        marginTop: 10,
        width:"82%",
        flexDirection:"row",
        height: 50,
        borderRadius: 50/2,
        justifyContent: "center",
        alignItems: "center"
    },
    ButtonTextStyle: {
        fontSize: 18,
        color: appcolor.Black,
    },
    imageStyle:{
        height:20,
        width:20,
        marginLeft:10
    }
})

export default Button;