const fonts = {
    Regular: "Montserrat-Regular",
    Medium: "Montserrat-Medium",
    Light:"Montserrat-Light",
    Bold:"Montserrat-Bold",
    SemiBold:"Montserrat-SemiBold",
    ExtraBold:"Montserrat-ExtraBold",
    Italic:"Montserrat-Italic"
}

export default fonts;