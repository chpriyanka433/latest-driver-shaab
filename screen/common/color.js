const appColor = {
    yellow: "#FFC401",
    lightGrey: "#DFE5EB",
    lightBlack:"#373246",
    black:"#000",
    darkGrey:"#4A4A4A",
    green:"#20C179",
    red:"#D74258",
    spinnerColor:"#213e7a",
    white:"#fff",
    transparent:"transparent",
    Blue:"#213e7a",
    HeaderBlue:"#28498d"
}

export default appColor;