import React from "react";
import { StyleSheet, Dimensions, View, Text, TouchableOpacity, StatusBar, Image} from "react-native";
import { image,Fonts,appColor} from "../common";

export default class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
      let {leftPress, text, rightPress,leftImage} = this.props;
    return (
        <View style={styles.HeaderStyle}>
          <TouchableOpacity
            style={styles.leftView}
            onPress={leftPress}
          >
            <Image
              source={leftImage}
              style={[styles.imageStyle,{marginLeft:15}]}
            />
          </TouchableOpacity>
        <View style={styles.HeaderView}>
          <Text style={[styles.headerText,{fontFamily:Fonts.Bold}]}>{text}</Text>
        </View>
          <TouchableOpacity
            style={styles.leftView}
            onPress={rightPress}
          >
            <Image
              source={rightPress}
              style={[styles.imageStyle,{marginRight:15,opacity:0}]}
            />
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  HeaderView: {
    textAlign: "center"
  },
  headerText: {
    fontSize: 18,
    textAlign: "center",
    alignItems: "center",
    color: "#fff",
    alignContent: "center",
    alignSelf: "center",
  },
  HeaderStyle:{
    flexDirection: 'row',
    backgroundColor: appColor.HeaderBlue,
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
    width:'100%',
  },
  imageStyle:{
    width: 20, height: 20
  },
  leftView:{
    height:50,
    width:50,
    justifyContent:"center",
  }
});
