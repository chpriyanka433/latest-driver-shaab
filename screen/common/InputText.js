import React, { Component} from "react";
import { View, StyleSheet, TouchableOpacity, Image} from "react-native";
import {Text} from "native-base";
import { TextInput } from "react-native-paper";
import { Fonts } from ".";

class InputText extends Component {
    render() {
        let {placeholder,error,textContentType,onBlur,onChangeText,editable,disabled,value,maxLength,keyboardType} = this.props;
        return (
            <TextInput
            style={styles.container}
            placeholder={placeholder}
            mode="outlined"
            error={error}
            maxLength={maxLength}
            keyboardType={keyboardType}
            value={value}
            editable={editable}
            disabled={disabled}
            underlineColorAndroid="transparent"
            textContentType={textContentType}
            onBlur={onBlur}
            onChangeText={onChangeText}
          />
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        fontFamily:Fonts.Regular
    }
})

export default InputText;