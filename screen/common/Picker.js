import React, { Component} from "react";
import { View, StyleSheet, TouchableOpacity, Image} from "react-native";
import {Text,Icon,Form,Item,Picker} from "native-base";
import appColor from "../common/color";
const launchscreenLogo = require("../../assets/logo.png");
const transImage = require("../../assets/logo.png");



class PickerList extends Component {
  constructor(props){
    super(props)
    this.state={
      startText:false
    }
  }
  render() {
      let {width,mode,placeholder,selectedValue,onValueChange,ArrayList,label,value,backgroundColor} = this.props;
    return (
        <Form width={width}>
                <Item regular success style={[styles.formItemstyle,{backgroundColor:backgroundColor}]}>
                    <Picker
                      mode={mode}
                      placeholder={placeholder}
                      selectedValue={selectedValue}
                      onValueChange={onValueChange}
                      >
                        <Picker.Item label={label} value={value} />
                        {ArrayList}
                  </Picker>
                </Item>
        </Form>
    );
  }
}

const styles = StyleSheet.create({
    formItemstyle: {
        borderRadius:5,
        backgroundColor:appColor.lightGrey, 
        borderColor:appColor.lightGrey
    }
})

export default PickerList;
