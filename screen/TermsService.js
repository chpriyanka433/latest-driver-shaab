import React from "react";
import { StyleSheet, Dimensions, View, WebView, Linking, Platform, Text, TouchableOpacity, StatusBar} from "react-native";
import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase} from "././common";

export default class TermsService extends React.Component {
  constructor(props) {
    super(props);
  }                                                                                 

  render() {
    const uri = 'https://drivershaab.com/vendor-terms.php';
    return (
      <View style={{flex:1}}>
        <StatusBar backgroundColor="#19376c" barStyle="light-content" />
        <Header
          text={"Terms of Service"}
          leftImage={image.burgerWhite}
          leftPress={()=>this.props.navigation.openDrawer()}
          />
      <WebView
        ref={(ref) => { this.webview = ref; }}
        source={{ uri }}
        style={{marginTop: Platform.OS==='ios'?20:0}}
        onNavigationStateChange={(event) => {
          if (event.url !== uri) {
            this.webview.stopLoading();
            Linking.openURL(event.url);
          }
        }}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 25
  },
  pdfView: {
    flex: 1,
    width: Dimensions.get("window").width
  }
});
