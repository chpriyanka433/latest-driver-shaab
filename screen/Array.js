import React, { Component } from 'react';
 
import { StyleSheet, Text, View, Alert } from 'react-native';
 
export default class App extends Component {
 
  SampleFunction=(item)=>{
    Alert.alert(item);
  }
 
 render() {
 
  var SampleNameArray = [ "Pankaj", "Rita", "Mohan", "Amit", "Babulal", "Sakshi" ];
 
   return (
     <View style={styles.MainContainer}>
 
         { SampleNameArray.map((item, key)=>(
         <Text key={key} style={styles.TextStyle} onPress={ this.SampleFunction.bind(this, item) }> { item } </Text>)
         )}
 
     </View>
   );
 }
}
 
const styles = StyleSheet.create({
 
 MainContainer: {
   flex: 1,
   margin: 10
   
 },
 
 TextStyle:{
   fontSize : 25,
    textAlign: 'center'
 }
 
});