import React, { Component } from "react";
import { Image,StyleSheet, AsyncStorage} from "react-native";
import {
  Content,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,
  View,
  Toast,
  Text
} from "native-base";

import {appColor,Header,InputText,AppButton,BaseUrl,RNPickerSelect,CheckBox,Spinner,ImagePicker,
  RazorpayCheckout,Toast,NavigationActions,DrawerActions,NavigationEvents,DatePicker,geolib,createDrawerNavigator,createAppContainer,Button,CodeInput,getDirections,MapView,PROVIDER_GOOGLE,RNAndroidLocationEnabler,TextInputMask,firebase,image,Fonts} from "././common";

import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";



class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      isLoading:false,
      name:"",
      email:"",
      phone:"",
      userImage:"",
      flag:false,
      flag1:false,
      flag2:false,
      flag3:false,
      flag4:false,
      flag5:false,
      
    };
  }

  async _logoutUser() {
    this.setState({flag6:true,flag:false,flag1:false,flag2:false,flag3:false,flag4:false,flag5:false})
    try {
      var driverId = await AsyncStorage.getItem("driverId");
      var formData = new FormData();
      formData.append("action", "update_driver_live_status");
      formData.append("driver_id", driverId);
      formData.append("driver_live_status", false);
      console.log(formData);
      const response = await fetch(
        BaseUrl.url1+"index.php",
        {
          body: formData,
          method: "POST",
          headers: {
            Accept: "multipart/form-data",
            "Content-Type": "multipart/form-data",
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': 0
          }
        }
      );

      const responseJson = await response.json();
      console.log(responseJson);
      if (responseJson.success === true) {
        this.props.navigation.replace('Login')
        AsyncStorage.clear();
      } else {
       this.props.navigation.replace('Login')
       AsyncStorage.clear();
      }
    } catch (error) {
      console.error(error);
    }
  }

  componentDidMount(){
    this._fetchUserDetails();
  }
  async _fetchUserDetails() {
    try {
      const driverId = await AsyncStorage.getItem('driverId');
      var formData = new FormData();
      formData.append("action", "get_driver_info");
      formData.append("driver_id", driverId);
      const response = await fetch(BaseUrl.url1+"index.php", {
        body: formData,
        method: "POST",
        headers: {
          "Accept": "multipart/form-data",
          "Content-Type": "multipart/form-data",
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          'Pragma': 'no-cache',
          'Expires': 0
        }
      });
      const responseJson = await response.json();
      console.log('driverdata===',responseJson)
      this.setState({
        userName: responseJson.name
      })
    } catch (error) {
      console.log(error);
    }
  }

  navigateFun =()=>{
    this.props.navigation.navigate("Profile");
    this.props.navigation.closeDrawer();
    this.setState({flag6:true,flag:false,flag1:false,flag2:false,flag3:false,flag4:false,flag5:false})
  }
  navigateFun1 =()=>{
    this.props.navigation.navigate("TripHistory");
    this.props.navigation.closeDrawer();
    this.setState({flag6:true,flag:false,flag1:false,flag2:false,flag3:false,flag4:false,flag5:false})
  }
  navigateFun2 =()=>{
    this.props.navigation.navigate("Checkin");
    this.props.navigation.closeDrawer();
    this.setState({flag6:true,flag:false,flag1:false,flag2:false,flag3:false,flag4:false,flag5:false})
  }
  navigateFun3 =()=>{
    this.props.navigation.navigate("CheckOut");
    this.props.navigation.closeDrawer();
    this.setState({flag6:true,flag:false,flag1:false,flag2:false,flag3:false,flag4:false,flag5:false})
  }
  

  render() {
    return (
        <View style={{flex:1,backgroundColor:appColor.Blue}}>
        
          <ScrollView>
              <View style={{flexDirection:"row",marginTop:80,marginLeft:40,marginBottom:50}}>
          <Image style={styles.circleView}
                 source={image.profile}
          />
          <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:20,marginLeft:15,fontSize:20}}>Driver Name</Text>
          </View>
          <View style={{flexDirection:"column"}}>
          <TouchableOpacity onPress={()=>this.navigateFun()}>
              <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:20,marginLeft:40,fontSize:18}}>My Profile</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>this.navigateFun1()}>
              <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:30,marginLeft:40,fontSize:18}}>My Trip History</Text>
              </TouchableOpacity>
              <TouchableOpacity>
              <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:30,marginLeft:40,fontSize:18}}>Start My Today Trip</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>this.navigateFun2()}>
              <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:30,marginLeft:40,fontSize:18}}>Check In</Text>
               </TouchableOpacity>
              <TouchableOpacity onPress={()=>this.navigateFun3()}>
              <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:30,marginLeft:40,fontSize:18}}>Check Out</Text>
             </TouchableOpacity>
             <TouchableOpacity onPress={()=>_logoutUser()}>
              <Text style={{fontFamily:Fonts.Regular,color:appColor.white,marginTop:30,marginLeft:40,fontSize:18}}>Logout</Text>
              </TouchableOpacity>
          </View>
          </ScrollView>
          
     </View>
    
 
    );
  }
}

export default SideBar;

const styles = StyleSheet.create({
  circleView:{
    width:80,
    height:80,
    borderRadius:50,
    backgroundColor:appColor.lightGrey,
    // marginRight:20
    
  },
  EditText:{
    fontFamily:'Raleway-Medium',
    color:appColor.darkGrey,
    fontSize:13,
    marginTop:5
  },
  circleText:{
    textAlign:'center',
    marginTop:15,
    color:appColor.white

  },
  ImageIcon:{
    width: 15, 
    height: 15,
    marginTop:12,
    marginLeft:12

  },
  ImagedarkIcon:{
    width: 15, 
    height: 15,
    marginTop:12,
    marginLeft:12

  },
  ImageView:{
    flexDirection:'row',
    marginBottom:25,
    marginLeft:20

  },
  ImageTextView:{
    fontFamily:'Raleway-Medium',
    color:appColor.darkGrey,
    letterSpacing:2,
    marginLeft:30, 
    marginTop:10,
    marginLeft:12,
    fontSize:15,
    },

    ImageTextViewdark:{
      fontFamily:'Raleway-Medium',
      color:appColor.darkGrey,
      fontWeight:"bold",
      letterSpacing:2,
      marginLeft:30,
      marginTop:10,
      marginLeft:12,
      fontSize:15
      }
})